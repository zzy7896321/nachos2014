	.file	1 "strcat.c"
	.text
	.align	2
	.globl	strcat
	.ent	strcat
strcat:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, extra= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	lb	$2,0($4)
	#nop
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L6
	move	$6,$4
	.set	macro
	.set	reorder

$L5:
	addu	$4,$4,1
	lb	$2,0($4)
	#nop
	bne	$2,$0,$L5
$L6:
	lbu	$2,0($5)
	#nop
	sb	$2,0($4)
	lbu	$3,0($5)
	addu	$4,$4,1
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L6
	addu	$5,$5,1
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$31
	move	$2,$6
	.set	macro
	.set	reorder

	.end	strcat
