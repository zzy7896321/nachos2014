	.file	1 "atoi.c"
	.text
	.align	2
	.globl	atoi
	.ent	atoi
atoi:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, extra= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	lb	$3,0($4)
	li	$2,-1			# 0xffffffffffffffff
	move	$6,$4
	move	$7,$0
	lbu	$4,0($4)
	.set	noreorder
	.set	nomacro
	beq	$3,$2,$L9
	li	$8,1			# 0x1
	.set	macro
	.set	reorder

$L2:
	move	$5,$4
	addu	$2,$5,-48
	andi	$2,$2,0x00ff
	sltu	$2,$2,10
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L10
	mult	$7,$8
	.set	macro
	.set	reorder

$L6:
	addu	$6,$6,1
	sll	$2,$5,24
	sll	$3,$7,2
	lbu	$5,0($6)
	addu	$3,$3,$7
	sll	$3,$3,1
	sra	$2,$2,24
	addu	$4,$5,-48
	addu	$3,$3,$2
	sltu	$4,$4,10
	.set	noreorder
	.set	nomacro
	bne	$4,$0,$L6
	addu	$7,$3,-48
	.set	macro
	.set	reorder

	mult	$7,$8
$L10:
	mflo	$2
	#nop
	j	$31
$L9:
	addu	$6,$6,1
	lbu	$4,0($6)
	.set	noreorder
	.set	nomacro
	j	$L2
	li	$8,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

	.end	atoi
