	.file	1 "rm.c"
	.rdata
	.align	2
$LC0:
	.ascii	"Usage: rm <file>\n\000"
	.align	2
$LC1:
	.ascii	"Unable to remove %s\n\000"
	.text
	.align	2
	.globl	main
	.ent	main
main:
	.frame	$sp,32,$31		# vars= 0, regs= 3/0, args= 16, extra= 0
	.mask	0x80030000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,32
	sw	$17,20($sp)
	sw	$16,16($sp)
	sw	$31,24($sp)
	move	$16,$4
	.set	noreorder
	.set	nomacro
	jal	__main
	move	$17,$5
	.set	macro
	.set	reorder

	li	$2,2			# 0x2
	la	$4,$LC0
	beq	$16,$2,$L2
	jal	printf
	li	$2,1			# 0x1
$L1:
	lw	$31,24($sp)
	lw	$17,20($sp)
	lw	$16,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,32
	.set	macro
	.set	reorder

$L2:
	lw	$4,4($17)
	jal	unlink
	la	$4,$LC1
	bne	$2,$0,$L5
	.set	noreorder
	.set	nomacro
	j	$L1
	move	$2,$0
	.set	macro
	.set	reorder

$L5:
	lw	$5,4($17)
	jal	printf
	.set	noreorder
	.set	nomacro
	j	$L1
	li	$2,1			# 0x1
	.set	macro
	.set	reorder

	.end	main
