	.file	1 "sort.c"
	.text
	.align	2
	.globl	swap
	.ent	swap
swap:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, extra= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	lw	$3,0($4)
	lw	$2,0($5)
	#nop
	sw	$2,0($4)
	.set	noreorder
	.set	nomacro
	j	$31
	sw	$3,0($5)
	.set	macro
	.set	reorder

	.end	swap
	.align	2
	.globl	main
	.ent	main
main:
	.frame	$sp,40,$31		# vars= 0, regs= 6/0, args= 16, extra= 0
	.mask	0x801f0000,-4
	.fmask	0x00000000,0
	subu	$sp,$sp,40
	sw	$19,28($sp)
	sw	$31,36($sp)
	sw	$20,32($sp)
	sw	$18,24($sp)
	sw	$17,20($sp)
	sw	$16,16($sp)
	.set	noreorder
	.set	nomacro
	jal	__main
	move	$19,$0
	.set	macro
	.set	reorder

	li	$5,255			# 0xff
	la	$4,array
$L7:
	subu	$2,$5,$19
	addu	$19,$19,1
	slt	$3,$19,256
	sw	$2,0($4)
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L7
	addu	$4,$4,4
	.set	macro
	.set	reorder

	move	$19,$0
	la	$20,array
$L18:
	slt	$2,$19,256
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L30
	move	$17,$19
	.set	macro
	.set	reorder

	sll	$2,$19,2
	la $16,array($2)
	move	$18,$20
$L17:
	lw	$2,0($16)
	lw	$3,0($18)
	move	$5,$16
	slt	$2,$2,$3
	addu	$17,$17,1
	addu	$16,$16,4
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L33
	move	$4,$18
	.set	macro
	.set	reorder

	slt	$2,$17,256
$L34:
	bne	$2,$0,$L17
$L30:
	addu	$19,$19,1
	slt	$2,$19,255
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L18
	addu	$20,$20,4
	.set	macro
	.set	reorder

	move	$19,$0
	la	$3,array
$L24:
	lw	$2,0($3)
	#nop
	.set	noreorder
	.set	nomacro
	bne	$2,$19,$L2
	li	$4,1			# 0x1
	.set	macro
	.set	reorder

	addu	$19,$19,1
	slt	$2,$19,256
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L24
	addu	$3,$3,4
	.set	macro
	.set	reorder

	move	$4,$0
$L2:
	lw	$31,36($sp)
	lw	$20,32($sp)
	lw	$19,28($sp)
	lw	$18,24($sp)
	lw	$17,20($sp)
	lw	$16,16($sp)
	move	$2,$4
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,40
	.set	macro
	.set	reorder

$L33:
	jal	swap
	.set	noreorder
	.set	nomacro
	j	$L34
	slt	$2,$17,256
	.set	macro
	.set	reorder

	.end	main

	.comm	array,1024
