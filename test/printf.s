	.file	1 "printf.c"
	.rdata
	.align	2
$LC0:
	.ascii	"printf.c\000"
	.text
	.align	2
	.ent	digittoascii
digittoascii:
	.frame	$sp,32,$31		# vars= 0, regs= 3/0, args= 16, extra= 0
	.mask	0x80030000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,32
	sw	$16,16($sp)
	move	$16,$4
	sltu	$2,$16,36
	sw	$17,20($sp)
	sw	$31,24($sp)
	move	$17,$5
	la	$4,$LC0
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L8
	li	$5,5			# 0x5
	.set	macro
	.set	reorder

	addu	$2,$16,48
$L9:
	sll	$2,$2,24
	sltu	$3,$16,10
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L1
	sra	$2,$2,24
	.set	macro
	.set	reorder

	addu	$2,$16,55
	sll	$2,$2,24
	addu	$3,$16,87
	sra	$2,$2,24
	.set	noreorder
	.set	nomacro
	bne	$17,$0,$L1
	sll	$3,$3,24
	.set	macro
	.set	reorder

	sra	$2,$3,24
$L1:
	lw	$31,24($sp)
	lw	$17,20($sp)
	lw	$16,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,32
	.set	macro
	.set	reorder

$L8:
	jal	__assert
	.set	noreorder
	.set	nomacro
	j	$L9
	addu	$2,$16,48
	.set	macro
	.set	reorder

	.end	digittoascii
	.align	2
	.ent	charprint
charprint:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, extra= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	lw	$2,0($4)
	#nop
	sb	$5,0($2)
	addu	$2,$2,1
	sw	$2,0($4)
	.set	noreorder
	.set	nomacro
	j	$31
	li	$2,1			# 0x1
	.set	macro
	.set	reorder

	.end	charprint
	.align	2
	.ent	mcharprint
mcharprint:
	.frame	$sp,32,$31		# vars= 0, regs= 3/0, args= 16, extra= 0
	.mask	0x80030000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,32
	sw	$31,24($sp)
	sw	$17,20($sp)
	sw	$16,16($sp)
	move	$16,$4
	lw	$4,0($4)
	.set	noreorder
	.set	nomacro
	jal	memcpy
	move	$17,$6
	.set	macro
	.set	reorder

	lw	$3,0($16)
	move	$2,$17
	addu	$3,$3,$17
	sw	$3,0($16)
	lw	$31,24($sp)
	lw	$17,20($sp)
	lw	$16,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,32
	.set	macro
	.set	reorder

	.end	mcharprint
	.align	2
	.ent	integerprint
integerprint:
	.frame	$sp,88,$31		# vars= 32, regs= 10/0, args= 16, extra= 0
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	addu	$2,$6,-2
	subu	$sp,$sp,88
	sltu	$2,$2,34
	sw	$fp,80($sp)
	sw	$23,76($sp)
	sw	$21,68($sp)
	sw	$20,64($sp)
	sw	$19,60($sp)
	sw	$18,56($sp)
	sw	$16,48($sp)
	sw	$31,84($sp)
	sw	$22,72($sp)
	sw	$17,52($sp)
	move	$19,$6
	move	$23,$4
	move	$16,$5
	move	$20,$7
	lw	$fp,108($sp)
	li	$18,32			# 0x20
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L44
	move	$21,$0
	.set	macro
	.set	reorder

	slt	$2,$20,33
$L47:
	bne	$2,$0,$L15
	li	$20,32			# 0x20
$L15:
	bne	$16,$0,$L16
	li	$18,1			# 0x1
	slt	$2,$18,$20
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L37
	addu	$18,$20,-1
	.set	macro
	.set	reorder

$L23:
	lw	$2,104($sp)
	#nop
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L22
	li	$5,48			# 0x30
	.set	macro
	.set	reorder

	li	$5,32			# 0x20
$L22:
	.set	noreorder
	.set	nomacro
	jal	charprint
	move	$4,$23
	.set	macro
	.set	reorder

	addu	$18,$18,-1
	.set	noreorder
	.set	nomacro
	bne	$18,$0,$L23
	addu	$21,$21,$2
	.set	macro
	.set	reorder

$L37:
	move	$4,$23
	.set	noreorder
	.set	nomacro
	jal	charprint
	li	$5,48			# 0x30
	.set	macro
	.set	reorder

	addu	$2,$21,$2
$L46:
	lw	$31,84($sp)
	lw	$fp,80($sp)
	lw	$23,76($sp)
	lw	$22,72($sp)
	lw	$21,68($sp)
	lw	$20,64($sp)
	lw	$19,60($sp)
	lw	$18,56($sp)
	lw	$17,52($sp)
	lw	$16,48($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,88
	.set	macro
	.set	reorder

$L16:
	.set	noreorder
	.set	nomacro
	bltz	$16,$L45
	move	$4,$23
	.set	macro
	.set	reorder

$L24:
	.set	noreorder
	.set	nomacro
	beq	$16,$0,$L39
	addu	$22,$sp,16
	.set	macro
	.set	reorder

$L29:
	divu	$0,$16,$19
	addu	$18,$18,-1
	move	$5,$fp
	addu	$16,$22,$18
	.set	noreorder
	bne	$19,$0,1f
	nop
	break	7
1:
	.set	reorder
	mfhi	$4
	#nop
	#nop
	bgez	$4,1f
	subu	$4,$0,$4
1:
	mflo	$17
	#nop
	jal	digittoascii
	sb	$2,0($16)
	.set	noreorder
	.set	nomacro
	bne	$17,$0,$L29
	move	$16,$17
	.set	macro
	.set	reorder

$L39:
	li	$2,32			# 0x20
	subu	$2,$2,$20
	slt	$3,$2,$18
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L41
	move	$3,$2
	.set	macro
	.set	reorder

$L35:
	lw	$2,104($sp)
	addu	$18,$18,-1
	addu	$4,$22,$18
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L34
	li	$5,48			# 0x30
	.set	macro
	.set	reorder

	li	$5,32			# 0x20
$L34:
	slt	$2,$3,$18
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L35
	sb	$5,0($4)
	.set	macro
	.set	reorder

$L41:
	li	$6,32			# 0x20
	subu	$6,$6,$18
	addu	$5,$22,$18
	.set	noreorder
	.set	nomacro
	jal	mcharprint
	move	$4,$23
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L46
	addu	$2,$21,$2
	.set	macro
	.set	reorder

$L45:
	.set	noreorder
	.set	nomacro
	jal	charprint
	li	$5,45			# 0x2d
	.set	macro
	.set	reorder

	move	$21,$2
	.set	noreorder
	.set	nomacro
	j	$L24
	subu	$16,$0,$16
	.set	macro
	.set	reorder

$L44:
	la	$4,$LC0
	.set	noreorder
	.set	nomacro
	jal	__assert
	li	$5,32			# 0x20
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L47
	slt	$2,$20,33
	.set	macro
	.set	reorder

	.end	integerprint
	.align	2
	.ent	stringprint
stringprint:
	.frame	$sp,32,$31		# vars= 0, regs= 3/0, args= 16, extra= 0
	.mask	0x80030000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,32
	sw	$17,20($sp)
	move	$17,$4
	move	$4,$5
	sw	$31,24($sp)
	sw	$16,16($sp)
	.set	noreorder
	.set	nomacro
	jal	strlen
	move	$16,$5
	.set	macro
	.set	reorder

	move	$6,$2
	move	$4,$17
	.set	noreorder
	.set	nomacro
	jal	mcharprint
	move	$5,$16
	.set	macro
	.set	reorder

	lw	$31,24($sp)
	lw	$17,20($sp)
	lw	$16,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,32
	.set	macro
	.set	reorder

	.end	stringprint
	.align	2
	.ent	_vsprintf
_vsprintf:
	.frame	$sp,56,$31		# vars= 0, regs= 7/0, args= 24, extra= 0
	.mask	0x803f0000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,56
	sw	$20,40($sp)
	sw	$19,36($sp)
	sw	$18,32($sp)
	sw	$16,24($sp)
	sw	$31,48($sp)
	sw	$21,44($sp)
	sw	$17,28($sp)
	sw	$4,56($sp)
	lbu	$3,0($5)
	move	$16,$5
	sll	$3,$3,24
	move	$18,$6
	move	$19,$0
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L74
	move	$20,$0
	.set	macro
	.set	reorder

	sra	$3,$3,24
$L88:
	li	$2,37			# 0x25
	beq	$3,$2,$L82
	addu	$20,$20,1
	addu	$16,$16,1
$L50:
	lbu	$3,0($16)
	#nop
	sll	$3,$3,24
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L88
	sra	$3,$3,24
	.set	macro
	.set	reorder

$L74:
	.set	noreorder
	.set	nomacro
	blez	$20,$L72
	subu	$5,$16,$20
	.set	macro
	.set	reorder

	move	$6,$20
	.set	noreorder
	.set	nomacro
	jal	mcharprint
	addu	$4,$sp,56
	.set	macro
	.set	reorder

	addu	$19,$19,$2
$L72:
	move	$2,$19
	lw	$3,56($sp)
	lw	$31,48($sp)
	lw	$21,44($sp)
	lw	$20,40($sp)
	lw	$19,36($sp)
	lw	$18,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	sb	$0,0($3)
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,56
	.set	macro
	.set	reorder

$L82:
	.set	noreorder
	.set	nomacro
	blez	$20,$L54
	subu	$5,$16,$20
	.set	macro
	.set	reorder

	move	$6,$20
	.set	noreorder
	.set	nomacro
	jal	mcharprint
	addu	$4,$sp,56
	.set	macro
	.set	reorder

	addu	$19,$19,$2
	move	$20,$0
$L54:
	addu	$16,$16,1
	lb	$3,0($16)
	li	$2,45			# 0x2d
	lbu	$4,0($16)
	beq	$3,$2,$L83
	sll	$2,$4,24
$L87:
	sra	$2,$2,24
	li	$3,48			# 0x30
	.set	noreorder
	.set	nomacro
	beq	$2,$3,$L84
	move	$21,$0
	.set	macro
	.set	reorder

$L57:
	.set	noreorder
	.set	nomacro
	jal	atoi
	move	$4,$16
	.set	macro
	.set	reorder

	move	$7,$2
	lbu	$2,0($16)
	#nop
	addu	$2,$2,-48
	sltu	$2,$2,10
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L77
	move	$17,$16
	.set	macro
	.set	reorder

$L61:
	addu	$17,$17,1
	lbu	$3,0($17)
	#nop
	addu	$2,$3,-48
	sltu	$2,$2,10
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L61
	sll	$2,$3,24
	.set	macro
	.set	reorder

$L86:
	sra	$2,$2,24
	addu	$2,$2,-88
	sltu	$3,$2,33
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L68
	addu	$17,$17,1
	.set	macro
	.set	reorder

	sll	$2,$2,2
	lw	$3,$L69($2)
	#nop
	j	$3
	.rdata
	.align	2
$L69:
	.word	$L66
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L63
	.word	$L64
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L67
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L68
	.word	$L65
	.text
$L63:
	addu	$2,$18,3
	li	$3,-4			# 0xfffffffffffffffc
	and	$2,$2,$3
	addu	$18,$2,4
	lb	$5,-4($18)
	.set	noreorder
	.set	nomacro
	jal	charprint
	addu	$4,$sp,56
	.set	macro
	.set	reorder

	addu	$19,$19,$2
$L62:
$L85:
	.set	noreorder
	.set	nomacro
	j	$L50
	move	$16,$17
	.set	macro
	.set	reorder

$L66:
	addu	$3,$18,3
	li	$2,-4			# 0xfffffffffffffffc
	and	$3,$3,$2
	li	$2,1			# 0x1
	sw	$21,16($sp)
	sw	$2,20($sp)
	addu	$18,$3,4
$L81:
	lw	$5,-4($18)
	addu	$4,$sp,56
	li	$6,16			# 0x10
$L80:
	jal	integerprint
	.set	noreorder
	.set	nomacro
	j	$L85
	addu	$19,$19,$2
	.set	macro
	.set	reorder

$L64:
	addu	$2,$18,3
	li	$3,-4			# 0xfffffffffffffffc
	and	$2,$2,$3
	sw	$21,16($sp)
	addu	$18,$2,4
	sw	$0,20($sp)
	lw	$5,-4($18)
	addu	$4,$sp,56
	.set	noreorder
	.set	nomacro
	j	$L80
	li	$6,10			# 0xa
	.set	macro
	.set	reorder

$L67:
	addu	$2,$18,3
	li	$3,-4			# 0xfffffffffffffffc
	and	$2,$2,$3
	addu	$18,$2,4
	lw	$5,-4($18)
	.set	noreorder
	.set	nomacro
	jal	stringprint
	addu	$4,$sp,56
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L85
	addu	$19,$19,$2
	.set	macro
	.set	reorder

$L68:
	addu	$4,$sp,56
	.set	noreorder
	.set	nomacro
	jal	charprint
	li	$5,37			# 0x25
	.set	macro
	.set	reorder

	addu	$19,$19,$2
	.set	noreorder
	.set	nomacro
	j	$L62
	move	$17,$16
	.set	macro
	.set	reorder

$L65:
	addu	$2,$18,3
	li	$3,-4			# 0xfffffffffffffffc
	and	$2,$2,$3
	addu	$18,$2,4
	sw	$21,16($sp)
	.set	noreorder
	.set	nomacro
	j	$L81
	sw	$0,20($sp)
	.set	macro
	.set	reorder

$L77:
	lbu	$3,0($16)
	.set	noreorder
	.set	nomacro
	j	$L86
	sll	$2,$3,24
	.set	macro
	.set	reorder

$L84:
	.set	noreorder
	.set	nomacro
	j	$L57
	li	$21,1			# 0x1
	.set	macro
	.set	reorder

$L83:
	la	$4,$LC0
	.set	noreorder
	.set	nomacro
	jal	__assert
	li	$5,85			# 0x55
	.set	macro
	.set	reorder

	lbu	$4,0($16)
	.set	noreorder
	.set	nomacro
	j	$L87
	sll	$2,$4,24
	.set	macro
	.set	reorder

	.end	_vsprintf
	.align	2
	.globl	vsprintf
	.ent	vsprintf
vsprintf:
	.frame	$sp,24,$31		# vars= 0, regs= 1/0, args= 16, extra= 0
	.mask	0x80000000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,24
	sw	$31,16($sp)
	jal	_vsprintf
	lw	$31,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,24
	.set	macro
	.set	reorder

	.end	vsprintf
	.align	2
	.globl	vfprintf
	.ent	vfprintf
vfprintf:
	.frame	$sp,32,$31		# vars= 0, regs= 4/0, args= 16, extra= 0
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	subu	$sp,$sp,32
	sw	$17,20($sp)
	la	$17,vfprintfbuf
	sw	$18,24($sp)
	move	$18,$4
	sw	$16,16($sp)
	sw	$31,28($sp)
	.set	noreorder
	.set	nomacro
	jal	_vsprintf
	move	$4,$17
	.set	macro
	.set	reorder

	move	$16,$2
	sltu	$2,$2,256
	la	$4,$LC0
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L93
	li	$5,151			# 0x97
	.set	macro
	.set	reorder

	move	$4,$18
$L94:
	move	$5,$17
	.set	noreorder
	.set	nomacro
	jal	write
	move	$6,$16
	.set	macro
	.set	reorder

	lw	$31,28($sp)
	lw	$18,24($sp)
	lw	$17,20($sp)
	lw	$16,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,32
	.set	macro
	.set	reorder

$L93:
	jal	__assert
	.set	noreorder
	.set	nomacro
	j	$L94
	move	$4,$18
	.set	macro
	.set	reorder

	.end	vfprintf
	.align	2
	.globl	vprintf
	.ent	vprintf
vprintf:
	.frame	$sp,24,$31		# vars= 0, regs= 1/0, args= 16, extra= 0
	.mask	0x80000000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,24
	move	$6,$5
	move	$5,$4
	sw	$31,16($sp)
	.set	noreorder
	.set	nomacro
	jal	vfprintf
	li	$4,1			# 0x1
	.set	macro
	.set	reorder

	lw	$31,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,24
	.set	macro
	.set	reorder

	.end	vprintf
	.align	2
	.globl	sprintf
	.ent	sprintf
sprintf:
	.frame	$sp,24,$31		# vars= 0, regs= 1/0, args= 16, extra= 0
	.mask	0x80000000,-8
	.fmask	0x00000000,0
	sw	$5,4($sp)
	sw	$6,8($sp)
	sw	$7,12($sp)
	subu	$sp,$sp,24
	sw	$31,16($sp)
	.set	noreorder
	.set	nomacro
	jal	vsprintf
	addu	$6,$sp,32
	.set	macro
	.set	reorder

	lw	$31,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,24
	.set	macro
	.set	reorder

	.end	sprintf
	.align	2
	.globl	fprintf
	.ent	fprintf
fprintf:
	.frame	$sp,24,$31		# vars= 0, regs= 1/0, args= 16, extra= 0
	.mask	0x80000000,-8
	.fmask	0x00000000,0
	sw	$5,4($sp)
	sw	$6,8($sp)
	sw	$7,12($sp)
	subu	$sp,$sp,24
	sw	$31,16($sp)
	.set	noreorder
	.set	nomacro
	jal	vfprintf
	addu	$6,$sp,32
	.set	macro
	.set	reorder

	lw	$31,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,24
	.set	macro
	.set	reorder

	.end	fprintf
	.align	2
	.globl	printf
	.ent	printf
printf:
	.frame	$sp,24,$31		# vars= 0, regs= 1/0, args= 16, extra= 0
	.mask	0x80000000,-8
	.fmask	0x00000000,0
	sw	$4,0($sp)
	sw	$5,4($sp)
	sw	$6,8($sp)
	sw	$7,12($sp)
	subu	$sp,$sp,24
	sw	$31,16($sp)
	.set	noreorder
	.set	nomacro
	jal	vprintf
	addu	$5,$sp,28
	.set	macro
	.set	reorder

	lw	$31,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,24
	.set	macro
	.set	reorder

	.end	printf

	.lcomm	vfprintfbuf,256
