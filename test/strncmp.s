	.file	1 "strncmp.c"
	.rdata
	.align	2
$LC0:
	.ascii	"strncmp.c\000"
	.text
	.align	2
	.globl	strncmp
	.ent	strncmp
strncmp:
	.frame	$sp,32,$31		# vars= 0, regs= 4/0, args= 16, extra= 0
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	subu	$sp,$sp,32
	sw	$18,24($sp)
	sw	$17,20($sp)
	sw	$16,16($sp)
	sw	$31,28($sp)
	move	$18,$6
	move	$17,$4
	.set	noreorder
	.set	nomacro
	blez	$6,$L10
	move	$16,$5
	.set	macro
	.set	reorder

$L4:
	lb	$4,0($17)
	lb	$3,0($16)
	#nop
	slt	$2,$4,$3
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L1
	li	$5,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

	slt	$2,$3,$4
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L1
	li	$5,1			# 0x1
	.set	macro
	.set	reorder

	addu	$18,$18,-1
	addu	$17,$17,1
	.set	noreorder
	.set	nomacro
	bgtz	$18,$L4
	addu	$16,$16,1
	.set	macro
	.set	reorder

	move	$5,$0
$L1:
	lw	$31,28($sp)
	lw	$18,24($sp)
	lw	$17,20($sp)
	lw	$16,16($sp)
	move	$2,$5
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,32
	.set	macro
	.set	reorder

$L10:
	la	$4,$LC0
	.set	noreorder
	.set	nomacro
	jal	__assert
	li	$5,6			# 0x6
	.set	macro
	.set	reorder

	j	$L4
	.end	strncmp
