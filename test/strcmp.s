	.file	1 "strcmp.c"
	.text
	.align	2
	.globl	strcmp
	.ent	strcmp
strcmp:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, extra= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
$L2:
	lb	$3,0($4)
	lb	$6,0($5)
	#nop
	slt	$2,$3,$6
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L1
	li	$7,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

	slt	$2,$6,$3
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L1
	li	$7,1			# 0x1
	.set	macro
	.set	reorder

	addu	$5,$5,1
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L3
	addu	$4,$4,1
	.set	macro
	.set	reorder

	bne	$6,$0,$L2
$L3:
	move	$7,$0
$L1:
	.set	noreorder
	.set	nomacro
	j	$31
	move	$2,$7
	.set	macro
	.set	reorder

	.end	strcmp
