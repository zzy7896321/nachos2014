	.file	1 "assert.c"
	.rdata
	.align	2
$LC0:
	.ascii	"\n"
	.ascii	"Assertion failed: line %d file %s\n\000"
	.text
	.align	2
	.globl	__assert
	.ent	__assert
__assert:
	.frame	$sp,24,$31		# vars= 0, regs= 1/0, args= 16, extra= 0
	.mask	0x80000000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,24
	move	$6,$4
	la	$4,$LC0
	sw	$31,16($sp)
	jal	printf
	.set	noreorder
	.set	nomacro
	jal	exit
	li	$4,1			# 0x1
	.set	macro
	.set	reorder

	lw	$31,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,24
	.set	macro
	.set	reorder

	.end	__assert
