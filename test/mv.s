	.file	1 "mv.c"
	.rdata
	.align	2
$LC0:
	.ascii	"Usage: cp <src> <dst>\n\000"
	.align	2
$LC2:
	.ascii	"Unable to create %s\n\000"
	.align	2
$LC1:
	.ascii	"Open to open %s\n\000"
	.text
	.align	2
	.globl	main
	.ent	main
main:
	.frame	$sp,40,$31		# vars= 0, regs= 5/0, args= 16, extra= 0
	.mask	0x800f0000,-8
	.fmask	0x00000000,0
	subu	$sp,$sp,40
	sw	$18,24($sp)
	sw	$16,16($sp)
	sw	$31,32($sp)
	sw	$19,28($sp)
	sw	$17,20($sp)
	move	$16,$4
	.set	noreorder
	.set	nomacro
	jal	__main
	move	$18,$5
	.set	macro
	.set	reorder

	li	$2,3			# 0x3
	beq	$16,$2,$L2
	la	$4,$LC0
	jal	printf
	li	$2,1			# 0x1
$L1:
	lw	$31,32($sp)
	lw	$19,28($sp)
	lw	$18,24($sp)
	lw	$17,20($sp)
	lw	$16,16($sp)
	#nop
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,40
	.set	macro
	.set	reorder

$L2:
	lw	$4,4($18)
	.set	noreorder
	.set	nomacro
	jal	open
	li	$19,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$2,$19,$L11
	move	$17,$2
	.set	macro
	.set	reorder

	lw	$4,8($18)
	jal	creat
	lw	$4,8($18)
	jal	open
	.set	noreorder
	.set	nomacro
	beq	$2,$19,$L12
	move	$16,$2
	.set	macro
	.set	reorder

	move	$4,$17
$L14:
	la	$5,buf
	.set	noreorder
	.set	nomacro
	jal	read
	li	$6,1024			# 0x400
	.set	macro
	.set	reorder

	move	$6,$2
	la	$5,buf
	.set	noreorder
	.set	nomacro
	blez	$2,$L13
	move	$4,$16
	.set	macro
	.set	reorder

	jal	write
	.set	noreorder
	.set	nomacro
	j	$L14
	move	$4,$17
	.set	macro
	.set	reorder

$L13:
	.set	noreorder
	.set	nomacro
	jal	close
	move	$4,$17
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	jal	close
	move	$4,$16
	.set	macro
	.set	reorder

	lw	$4,4($18)
	jal	unlink
	.set	noreorder
	.set	nomacro
	j	$L1
	move	$2,$0
	.set	macro
	.set	reorder

$L12:
	lw	$5,8($18)
	la	$4,$LC2
$L9:
	jal	printf
	.set	noreorder
	.set	nomacro
	j	$L1
	li	$2,1			# 0x1
	.set	macro
	.set	reorder

$L11:
	lw	$5,4($18)
	la	$4,$LC1
	j	$L9
	.end	main

	.comm	buf,1024
