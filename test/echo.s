	.file	1 "echo.c"
	.rdata
	.align	2
$LC0:
	.ascii	"%d arguments\n\000"
	.align	2
$LC1:
	.ascii	"arg %d: %s\n\000"
	.text
	.align	2
	.globl	main
	.ent	main
main:
	.frame	$sp,32,$31		# vars= 0, regs= 4/0, args= 16, extra= 0
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	subu	$sp,$sp,32
	sw	$18,24($sp)
	move	$18,$4
	sw	$31,28($sp)
	sw	$17,20($sp)
	sw	$16,16($sp)
	.set	noreorder
	.set	nomacro
	jal	__main
	move	$16,$5
	.set	macro
	.set	reorder

	la	$4,$LC0
	.set	noreorder
	.set	nomacro
	jal	printf
	move	$5,$18
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	blez	$18,$L8
	move	$17,$0
	.set	macro
	.set	reorder

$L6:
	lw	$6,0($16)
	move	$5,$17
	la	$4,$LC1
	.set	noreorder
	.set	nomacro
	jal	printf
	addu	$17,$17,1
	.set	macro
	.set	reorder

	slt	$2,$17,$18
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L6
	addu	$16,$16,4
	.set	macro
	.set	reorder

$L8:
	lw	$31,28($sp)
	lw	$18,24($sp)
	lw	$17,20($sp)
	lw	$16,16($sp)
	move	$2,$0
	.set	noreorder
	.set	nomacro
	j	$31
	addu	$sp,$sp,32
	.set	macro
	.set	reorder

	.end	main
