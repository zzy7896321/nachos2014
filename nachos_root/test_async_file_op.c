#include "stdio.h"
#include "stdlib.h"
#include "coffgrader.h"

#define EXECUTE(pid, argv)	\
	int pid = exec(argv[0], sizeof(argv) / sizeof(char*), argv)

#define EXECUTE_AND_CHECK_PID(pid, argv)	\
	EXECUTE(pid, argv);	\
	if (pid < 0) {	\
		if (argv[0] != INVALID_ADDR) printf("failed to exec %s\n", argv[0]);	\
		else printf("argv[0] == INVALID_ADDR\n");	\
		break;	\
	}

#define ASSERT_TRUE( expr, ...)	\
	do {	\
		if (!(expr)) {	\
			printf("assertion failure: %s(%d): ", __FILE__, __LINE__);	\
			printf(__VA_ARGS__);	\
			exit(1);	\
		}	\
	} while(0)


#define JOIN_AND_CHECK(pid)	\
	do {	\
		int exitcode;	\
		int ret = join(pid, &exitcode);	\
		switch (ret) {	\
		case 1:	\
			printf("%d exited normally with %d\n", pid, exitcode);	\
			break;	\
		case 0:	\
			printf("%d exited because of unhandled exception %d\n", pid, exitcode);	\
			break;	\
		case -1:	\
			printf("%d is not my child?\n", pid);	\
			break;	\
		default:	\
			printf("join(%d, &exitcode) returns unknown value\n", pid, ret);	\
		}	\
	} while(0)
	
#define STATIC_ASSERT(cond)	\
	int __static_assert(int static_assertion_failure[][(cond) ? 1 : -1])


#define NUM_SUB_PROCESSES 3

#define NUM_OP_PER_CHECKPOINT 100

#define NUM_CHECKPOINTS 10

#define NUM_OP (NUM_OP_PER_CHECKPOINT * NUM_CHECKPOINTS)

int pids[NUM_SUB_PROCESSES];

void spawn(int i) {
	char buffer[10];
	char* argv[2] = {"test_async_file_op.coff", buffer};
	sprintf(buffer, "%d", i);
	printf("spawning %s\n", buffer);

	if ((pids[i] = exec(argv[0], 2, argv)) < 0) {
		printf("error when exec sub process, halting...\n");
		fail();
	}
}

enum op_t {
	OPEN = 0,
	CREATE,
	CLOSE,
	UNLINK,
};

char* op_name[] = {
	"OPEN",
	"CREATE",
	"CLOSE",
	"UNLINK",
};


#define MAX_NUM_FILES 4
#define NUM_FILE_NAMES 8

struct action_log_t {
	int global_seq;
	int op;
	int index;
	int ret;
	int fds[MAX_NUM_FILES];
	int file_no[MAX_NUM_FILES];
	
} action_log[NUM_OP];


int fds[MAX_NUM_FILES] = {-1, -1, -1, -1};
int file_no[MAX_NUM_FILES];

char* file_names[] = {
	"tmp1",
	"tmp2",
	"tmp3",
	"tmp4",
	"tmp5",
	"tmp6",
	"tmp7",
	"tmp8"
};

#define GLOBAL_SEQ 0
#define GLOBAL_DATA_LOCK (NUM_SUB_PROCESSES * 2)

void do_file_op(int num) {
	P(GLOBAL_DATA_LOCK);
	int global_seq = readValue(GLOBAL_SEQ);
	storeValue(GLOBAL_SEQ, global_seq + 1);
	V(GLOBAL_DATA_LOCK);

	action_log[num].global_seq = global_seq;
	memcpy(action_log[num].fds, fds, sizeof(int) * MAX_NUM_FILES);
	memcpy(action_log[num].file_no, file_no, sizeof(int) * MAX_NUM_FILES);

	int rand_value = random(20);
	int min_avail_fd_slot = 0;
	while (min_avail_fd_slot < MAX_NUM_FILES && fds[min_avail_fd_slot] != -1) ++ min_avail_fd_slot;

	if (min_avail_fd_slot == MAX_NUM_FILES) {
		if (rand_value < 15) {
			/* close a file */
			int index = random(MAX_NUM_FILES);
			action_log[num].ret = close(fds[index]);
			fds[index] = -1;
			
			action_log[num].op = CLOSE;
			action_log[num].index = index;
		}
		else {
			/* unlink a file */
			int index = random(NUM_FILE_NAMES);
			action_log[num].ret = unlink(file_names[index]);

			action_log[num].op = UNLINK;
			action_log[num].index = index;
		}
	}

	else {
		if (rand_value < 4) {
			/* open */
			int index = random(NUM_FILE_NAMES);
			action_log[num].ret = open(file_names[index]);
			action_log[index].op = OPEN;
			action_log[num].index = index;

			fds[min_avail_fd_slot] = action_log[num].ret;
		}

		else if (rand_value < 10) {
			/* create */	
			int index = random(NUM_FILE_NAMES);
			action_log[num].ret = creat(file_names[index]);
			action_log[num].op = CREATE;
			action_log[num].index = index;

			fds[min_avail_fd_slot] = action_log[num].ret;
		}

		else if (rand_value < 16) {
			/* unlink */
			int index = random(NUM_FILE_NAMES);
			action_log[num].ret = unlink(file_names[index]);
			action_log[num].op = UNLINK;
			action_log[num].index = index;
		}

		else {
			/* close */
			int index = random(MAX_NUM_FILES);
			action_log[num].ret = close(fds[index]);
			fds[index] = -1;
			
			action_log[num].op = CLOSE;
			action_log[num].index = index;
		}
	}

	yield();
}

void subprocess(int id) {
	int notification_semaphore_id = id + NUM_SUB_PROCESSES;
	P(id);

	int i, j;
	for (i = 0; i < NUM_CHECKPOINTS; ++i) {
		for (j = 0; j < NUM_OP_PER_CHECKPOINT; ++j) {
			do_file_op( i * NUM_OP_PER_CHECKPOINT + j);	
		}
		V(id + NUM_SUB_PROCESSES);
		
	}
	
	P(id);

	printf("Process %d log:\n", id);
	printf("global_seq\top\tret\tindex\n");
	for (i = 0; i < (NUM_CHECKPOINTS * NUM_OP_PER_CHECKPOINT); ++i) {
		printf("%d\t%s\t%d\t%d\n", action_log[i].global_seq, op_name[action_log[i].op],
				action_log[i].ret, action_log[i].index);
	}

}

int main(int argc, char* argv[]) {

	if (argc <= 1) {
		V(GLOBAL_DATA_LOCK);

		printf("I'll spawn %d processes, each randomly performs %d file operations\n", NUM_SUB_PROCESSES, NUM_OP);

		int i, j;
		for (i = 0; i < NUM_SUB_PROCESSES; ++i) 
			spawn(i);

		for (i = 0; i < NUM_SUB_PROCESSES; ++i)
			V(i);	
		
		for (i = 0; i < NUM_CHECKPOINTS; ++i) {
			for (j = 0; j < NUM_SUB_PROCESSES; ++j) {
				P(NUM_SUB_PROCESSES + j);
				printf("sub-process %d has finished %d%\n", j, 100 * (i+1) / NUM_CHECKPOINTS);
			}
		}

		for (i = 0; i < NUM_SUB_PROCESSES; ++i) {
			V(i);	
			join(pids[i], (int*) 0xFFFFFFFF);
		}

		done();
	}

	else {
		int id = atoi(argv[1]);
		subprocess(id);
	}

	return 0;
}

