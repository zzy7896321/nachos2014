#include "syscall.h"

#define BUF_SIZE 4096
#define N 4

const int n = N;
char a[N * BUF_SIZE];
char* argv[N + 1];


void read_file(char* buffer, char* file) {
	int fd = open(file);
	if (fd < 0) {
		printf("long_arg: failed to open file\n");
		exit(1);
	}

	
	int num = read(fd, buffer, 4096);
	if (num < 0) {
		printf("long_arg: error when reading\n");
		exit(1);
	}

	buffer[num ++] = '\0';
	close(fd);
}

int main() {	
	int i;
	argv[0] = "echo.coff";
	for (i = 0; i < n; ++i) {
		argv[i + 1] = a + BUF_SIZE * i;
	}

	read_file(a, "printf.c");	
	read_file(a + BUF_SIZE, "test_num_files.c");
	a[2 * BUF_SIZE] = '\0'; /* empty string */
	read_file(a + 3 * BUF_SIZE, "vm_recursion.c");
	
	int pid = exec(argv[0], n + 1, argv);

	if (pid > 0) {
		join(pid, (void*) 0xFFFFFFFF);
	}
	else {
		printf("many_arg.c: failed to exec echo.coff");
	}

	return 0;
}
