#!/bin/sh

seeds="12341 1231432 1232131 325423411 1231235321 123135 235324523 32 3425326"

for seed in $seeds
do
	nachos -[] ../nachos-sjtu/conf/proj2-ll.conf -- nachos.ag.CoffGrader -x test_async_file_op.coff -# quiet=1 -s $seed
	echo ""
done

