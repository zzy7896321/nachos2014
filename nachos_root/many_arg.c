#include "syscall.h"
#include "stdio.h"

#define BUF_SIZE 100
#define N 4096

const int n = N;
char a[N * BUF_SIZE];
char* argv[N + 1];


int main() {
	int i;
	argv[0] = "echo.coff";

	for (i = 0; i < n; ++i) {
		argv[i + 1] = a + i * BUF_SIZE;	
	}
	

	for (i = 0; i < n; ++i) {
		sprintf(a + i * BUF_SIZE, "%d", i);
	};

	int pid = exec(argv[0], n + 1, argv);

	if (pid > 0) {
		join(pid, (void*) 0xFFFFFFFF);
	}
	else {
		printf("many_arg.c: failed to exec echo.coff");
	}
	
	return 0;
}
