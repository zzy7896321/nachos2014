#include "stdio.h"

int x = 1;

void f() {
	x += 1;	
}

int main() {
	f();

	printf("%d\n", x);

	return 0;
}
