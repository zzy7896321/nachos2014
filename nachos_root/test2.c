#include "stdio.h"
#include "stdlib.h"

int global_value = 566;
#define BUF_SIZE 128
char buffer[BUF_SIZE];

int main() {
	int local_value = 777;
	write(fdStandardOutput, "msg\n", 4);
	printf("Hello, world! BTW, global_value = %d, local_value = %d\n", global_value, local_value);
	
	printf("I'm going to print my self, and copy myself to test2.c\n");
	int in = open("test.c");
	int out = creat("test2.c");
	int num_read;
	while ((num_read = read(in, buffer, BUF_SIZE)) > 0) {
		write(fdStandardOutput, buffer, num_read);
		write(out, buffer, num_read);
	}

	close(in);
	close(out);
	
	return 0;
}
