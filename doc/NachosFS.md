NachosFS
====================

### Nachos Phase 5 File System

The Nachos virtual machine provides a 4096-sector, 512-byte-block Disk, which have 2MB space in total. The file system for phase 5 needs to support file names with up to 255 characters, hard links, symbolic links.

To provide efficient swap area, the disk is divided up into two partitions, first the swap partition, and the NachosFS partition. The first block of the disk contains information about the sizes and offsets of both partitions.

We use 16-bit unsigned number as the block number, so up to 65536 blocks are supported. File size is a 32-bit unsigned number. The maximum file size is 2^25 bytes;

### Disk layout

####Block 0: Partition Table && NachosFS header


|  content                  |  offset(bytes)    |  length(bytes)    | Description               |
|:--------------------------|:-----------------:|:-----------------:|:--------------------------|
| magic number              |   0               |   4               | always 0x3A8EFF89         |
| sector size               |   4               |   2               | always 512                |
| number of sectors         |   6               |   2               |                           |
| swap parition offset      |   8               |   2               |                           |
| swap partition length     |   10              |   2               | if set to 0, then there's no swap partition |
| NachosFS partition offset |   12              |   2               |                           |
| NachosFS partition length |   14              |   2               |                           |
| fs state                  |   16              |   1               | 1 if os's running, 0 o.w. |    
| reserved 0                |   17              |   15              | always 0                  |
| inode count               |   32              |   2               | n_inodes in total         |
| block count               |   34              |   2               | n_blocks in total         |
| number of free inodes     |   36              |   2               |                           |
| number of free blocks     |   38              |   2               |                           |
| block group offset        |   40              |   2               | relative to NachosFS's start |
| block group size			|   42              |   2               | including the inode block	|
| number of block groups	|	44				|	2				|							|
| bitmap offset       		|   46              |   2               | relative to NachosFS's start  |
| number of bitmap blocks	|	48				|	2				|							|
| root inode number         |   50              |   2               | always 0                  |
| reserved 1                |   52              |   460             | always 0                  | 

####Block [swap_offset .. swap_offset + swap_length): swap partition

raw disk

#### Block [NachoFS_offset .. NachosFS_offset + NachosFS_length):

as described in NachosFS header

#### Block groups

Inode blocks and data blocks are divided into block groups. Each block group contains 1 inode block and
block group size - 1 data blocks. 

The Nth block group (starting from 0) is at NachosFS_partition_offset + block_group_offset + N * block_group_size.

The last block group may be incomplete, and may or may not have an inode block depending on the inode count.
If inode_count / num_inodes_per_block == n_block_groups, there is an inode block in the last block
group. Otherwise, there is not.

The procedure of deciding where N-th (starting from 0) data block is as follows:

1. Calculate the block group number M, and group index Ig

	M = N / (block_group_size - 1)

	Ig = N % (block_group_size - 1)

2. If M < n_block_groups or last block group contains an inode block, then the index of the block I and sector no. S are

	I = M * (num_inodes_per_block + block_group_size - 1) + num_inodes_per_block +  Ig

	S = NachosFS_partition_offset + block_group_offset + M * block_group_size + 1 + Ig.

3. Otherwise, index of the block I and sector no. S are

	I = M * (num_inodes_per_block + block_group_size - 1) + Ig

	S = NachosFS_partition_offset + block_group_offset + M * block_group_size + Ig


The procedure of deciding the index of N-th (starting from 1) inode is as follows:

1. Calculate the block group number M , and group index Ig
	
	M = (N - 1) / num_inodes_per_block

	Ig = (N - 1) % num_inodes_per_block

2. Index 

	I = M * (num_inodes_per_block + block_group_size - 1) + Ig;

3. Sector no. of this block is 

	S = NachosFS_partition_offset + block_group_offset + M * block_group_size

4. The inode starts in the inode block from position 

	P = inode_size * Ig

#### Bitmaps:

Inodes and Bitmaps share a bitmap. They are positioned according to the block groups. The index can be calculated according 
to the above rules.


#### Inode structure:

An inode takes up 32 bytes. A 512-byte block contain exactly 16 inodes. Number starts from 1.

|  content                  |  offset(bytes)    |  length(bytes)    | Description               |
|:--------------------------|:-----------------:|:-----------------:|:--------------------------|
| allocation status         |   0:0             |   1 bit           | this bit is set if the inode is allocated |
| file type                 |   0:1             |   2 bits          | 0: normal, 1: dir, 2: symlink |
| inlined                   |   0:3             |   1 bit           | 0: not inlined, 1: inlined|
| reserved 0                |   0:4             |   12 bits         |                           |
| # of allocated sectors 	|   2               |   2               |                           |
| size (in bytes)           |   4               |   4               |                           |
| ref count                 |   8               |   2               | number of hard links      |
| reserved 1                |   10              |   2               |                           |
| 8 direct block pointers   |   12              |   2 * 8 = 16      |                           |
| 1 single indirection ptr  |   28              |   2               |                           |
| 1 double indirection ptr  |   30              |   2               |                           |


#### Directory structures:

##### Directory header:
The directory header takes up 16 bytes.

|  content                  |  offset(bytes)    |  length(bytes)    | Description               |
|:--------------------------|:-----------------:|:-----------------:|:--------------------------|
| number of entries         |   0               |   2               | not including ., .. and ext. filename   |
| magic number              |   2:0             |   4 bits          | 0x5                      	|
| reserved 0                |   2:4             |   12 bits         |                           |
| self inode no. 			|   4               |   2               | (.)						|
| parent inode no.			|	6				|	2				| (..)						|
| ext. filename inode number|   8               |   2               | an unamed file    		|  
| reserved 2                |   10              |   6               |                           |

##### Directory entry structure: 
A directory entry takes up 16 bytes. Number starts from 0 (2 bytes).

|  content                  |  offset(bytes)    |  length(bytes)    | Description               |
|:--------------------------|:-----------------:|:-----------------:|:--------------------------|
| inode number              |   0               |   2               |                           |
| magic number              |   2:0             |   4 bits          |  0x7                     	|
| ext. file name entry no. 0|   2:4             |   2 bits          |  see below                |
| reserved					|	2:6				|	2 bits			|  							|
| file name length          |   3               |   1               |  at most 255              |
| primary file name         |   4               |   10              |  the first 10 characters  |
| ext. file name block no.  |   14              |   2               |  see below                |

If the length of the file name <= 12, the extended file name block no. field stores the 11th ~ 12th characters of the file name.

Otherwise, the ext. file name block no. field stores the block no. in the unnamed ext. filename file (starting from 0). Each extended file name entry is of length 128 bytes. Each directory entry needs at most 2 entries. Only the first entry is stored in the directory entry.

Entry i is at offset 16 + i * 16.

##### Extended file name block:
Each block is divided into 4 entries, 128 bytes each. Number starts from 0 (2 bytes).
Each directory entry need at most 2 extended file name blocks. Only the 11th ~ 255th characters are stored in the
extended file name entries.

The file name entry:

|  content                  |  offset(bytes)    |  length(bytes)    | Description               |
|:--------------------------|:-----------------:|:-----------------:|:--------------------------|
| parent entry number       |   0               |   2               |                           |
| magic number              |   2:0             |   4 bits          | 0xb                    	|
| ext. file name entry no.	|	2:4				|	2 bits			|							|
| type						|   2:6             |   2 bits          | 0x0 for head, 0x1 for tail|
| content ( + next block no.)|   3               |   125             |                           |

parent entry number is the no. of the directory entry by which this file name entry is pointed or the previous
ext. entry it is pointed by.

type is set to 0 if this is the first extended file name entry, 0x1 if this is the second.

If this is the head, the last two bytes of the content is set to the block number of the next entry, the index into the block is stored in 2:4 ~ 2:5. If this is the head, the last two bytes of contents are part of the file name (though not used), and the index into the parent entry is stored in 2:4 ~ 2:5.

#### Directory layout

The first block's first entry is always the directory header. Directory entries immediately follows the header.

The extended file name blocks are in a separate unamed file, the inode of which is recorded in the directory header.
The index to the extended file name entry consists of two parts: the in-file block number (2 bytes) and it's in-block index (2 bits).

The magic numbers are useful when checking for file system inconsistency.

We do not use directory inline.

#### Symbolic links:

Sym links can be inlined into Inodes when the length of the linked name does not exceed 20 characters.


#### Ordinary files:

May be inlined.

currently inline is not properly implemented.
