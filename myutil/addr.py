#!/usr/bin/python

pageShift = 10
pageSize = 1 << pageShift

while True:
	line = raw_input()
	print line
	items = line.split()
	if (len(items) == 1):
		try:
			addr = eval(items[0])
		except:
			print "error"
			continue
		
		pn = addr >> pageShift
		off = addr & (pageSize - 1)
		print "pn = %d [0x%x], offset = %d [0x%x]" % (pn, pn, off, off)
	elif (len(items) == 2):
		try:
			pn = eval(items[0])
			off = eval(items[1])
		except:
			print "error"	
			continue
		addr = (pn << pageShift) + off
		print "addr = %d [0x%x]" % (addr, addr)
	else:
		if line == '':
			break
		print "error"
		continue
