package nachos.userprog;

import nachos.machine.OpenFile;
import nachos.machine.FileSystem;

/**
 * Base class of OpenFile wrappers.
 * 
 * @author ZZy
 *
 */
public abstract class OpenFileWrapper extends OpenFile {
		protected OpenFileWrapper(OpenFile openFile, FileInfo info, FileSystemWrapper fs) {
			this.openFile = openFile;
			this.info = info;
			this.fs = fs;
		}
		
		@Override
		public FileSystem getFileSystem() {
			return fs;
		}

		@Override
		public String getName() {
			return openFile.getName();
		}

		@Override
		public int read(int pos, byte[] buf, int offset, int length) {
			return openFile.read(pos, buf, offset, length);
		}
		
		@Override
		public int write(int pos, byte[] buf, int offset, int length) {
			return openFile.write(pos, buf, offset, length);
		}

		@Override
		public int length() {
			return openFile.length();
		}

		@Override
		public void close() {
			if (openFile == null) return ;
			
			info.sem.P();
			
			close_impl();
			
			info.sem.V();
			
			fs.releaseFileInfo(info);
			openFile = null;
			info = null;
		}

		@Override
		public void seek(int pos) {
			openFile.seek(pos);
		}

		@Override
		public int tell() {
			return openFile.tell();
		}

		@Override
		public int read(byte[] buf, int offset, int length) {
			return openFile.read(buf, offset, length);
		}

		@Override
		public int write(byte[] buf, int offset, int length) {
			return openFile.write(buf, offset, length);
		}
	
		/**
		 * The unsynchronised implementation of close.
		 */
		protected void close_impl() {
			openFile.close();
		}
		
		public OpenFile openFile;
		public FileInfo info;
		public FileSystemWrapper fs;
}
