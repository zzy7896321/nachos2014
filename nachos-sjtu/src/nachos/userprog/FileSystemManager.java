package nachos.userprog;

import nachos.machine.*;

/**
 * This is the factory class to the FileSystem to provide a consistent file management interface.
 *
 */
public class FileSystemManager {
	
	/**
	 * Returns a FileSystem or a wrapper that
	 * <ol>
	 * <li> remove fully conforms to FileSystem interface document. When
	 * remove is called when a file is already open, the access to the file
	 * is not affected and the file will be removed as soon as the last access
	 * to the file is closed.
	 * </ol>
	 * 
	 * @param fs		the underlying FileSystem
	 * @return			a FileSystem conforming to the specification
	 */
	public static FileSystem createFileSystemConformingToSpecification(FileSystem fs) {
		if (fs instanceof nachos.machine.StubFileSystem) {
			/* Already know that the stub file system does not support delayed remove. 
			 * And it even doesn't support UNIX-style path. */
			return new StubFileSystemWrapper((nachos.machine.StubFileSystem) fs);
		}
		
		// final int magicNumber = 0x20141017;
		/* Otherwise, have to do some test. */
		String tmpFileName = getTmpFile(fs);
		if (tmpFileName == null) {
			/* cannot do the test. have to be conservative. */
			return new PendingRemoveFileSystemWrapper(fs);
		}
		OpenFile openFile;
		
		/* test the semantics of remove */
		boolean delayedRemove = false;
		openFile = fs.open(tmpFileName, true);
		fs.remove(tmpFileName);	// don't rely on the return value
		OpenFile openFile2 = fs.open(tmpFileName, false);
		openFile.close();
		if (openFile2 == null) {
			/* need further test */
			openFile2 = fs.open(tmpFileName, false);
			if (openFile2 == null) {
				/* yes, delayedRemove is properly supported */
				delayedRemove = true;
			}
			else {
				/* no */
				openFile2.close();
				fs.remove(tmpFileName);
			}
		}
		else {
			/* no, at least the file still can be opened */
			openFile2.close();
		}
		
		if (!delayedRemove) {
			/* need wrapper to support delayed remove */
			return new PendingRemoveFileSystemWrapper(fs);
		}
		
		/* the raw file system fully conforms to the specification */
		return fs;
	}
	
	/**
	 * Find a usable file name.
	 * 
	 * @param fs
	 * @return
	 */
	public static String getTmpFile(FileSystem fs) {
		final int maxAttempts = 2;
		final String[] tmpFileName = {"nachos.userprog.FileSystemManager.getTmpFile", "nachos.userprog.FileSystemManager.getTmpFile_backup_name"};
		
		int attempt = 0;
		while (attempt < maxAttempts) {
			OpenFile file = fs.open(tmpFileName[attempt], false);
			if (file == null) {
				return tmpFileName[attempt];
			}
			else {
				file.close();
				++attempt;
			}
		}
		
		/* Are you kidding me? I'm going to unlink the file nevertheless. */
		String tmpFile = tmpFileName[0];
		OpenFile src = fs.open(tmpFile, false);
		OpenFile dst = fs.open(tmpFile + ".bak", true);
		byte[] buffer = new byte[4096];
		int num_read = 0;
		while ((num_read = src.read(buffer, 0, 4096)) > 0) {
			dst.write(buffer, 0, num_read);
		}
		dst.close();
		src.close();
		
		if (fs.remove(tmpFile)) return tmpFile;
		
		return null;
	}
	
	/**
	 * Returns a canonical path from the current working directory.
	 * Now it's from the root (/).
	 * 
	 * @param path
	 * @return
	 */
	public static String getCanonicalPath(String path) {
		if (path == null || path.isEmpty()) {
			return null;
		}
		
		int len = path.length();
		int cur = 0;
		if (path.charAt(0) == '/') {
			++cur;
		}
		
		java.util.LinkedList<String> pathNames = new java.util.LinkedList<String>();
		while (cur < len) {
			int next_slash = path.indexOf('/', cur);
			if (next_slash == -1) {
				next_slash = len;
			}
			
			if (next_slash == cur) {
				++cur;
			}
			else {
				String name = path.substring(cur, next_slash);
				if (name.equals("..")) {
					if (pathNames.isEmpty()) {
						return null;
					}
					pathNames.removeLast();
				}
				else if (name.equals(".")) {
					/* do nothing */
				}
				else {
					pathNames.addLast(name);
				}
				cur = next_slash + 1;
			}
		}
		
		if (pathNames.isEmpty()) return "/";
		StringBuilder builder = new StringBuilder();
		for (String name : pathNames) {
			builder.append("/").append(name);
		}
		return builder.toString();
	}
}
