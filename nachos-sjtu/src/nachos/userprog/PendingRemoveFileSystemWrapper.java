package nachos.userprog;

import nachos.machine.FileSystem;
import nachos.machine.OpenFile;

/**
 * This class is a wrapper class for a FileSystem that does not support removing an
 * opened file after it's closed. The wrapper class
 * <ul>
 * <li> rejects the open operation unless there is no pending unlinking operation to the file
 * <li> delays the unlinking operation until the last OpenFile to the file is closed when unlink is called on an open file
 * </ul>
 *
 *<p>
 * To implement this feature, a mutex lock is used, which will lead to serious performance loss.
 */
public class PendingRemoveFileSystemWrapper extends StubFileSystemWrapper {

	public PendingRemoveFileSystemWrapper(FileSystem fs) {
		super(fs);
	}
	
	@Override
	public OpenFile open(String name, boolean truncate) {
		String cname = FileSystemManager.getCanonicalPath(name);
		if (cname == null || cname.equals("/")) return null;
		
		return super.open(cname, truncate);
	}

	@Override
	public boolean remove(String name) {
		String cname = FileSystemManager.getCanonicalPath(name);
		if (cname == null) return false;
		
		return super.remove(cname);
	}
	
}