package nachos.userprog;

import nachos.machine.TranslationEntry;

/**
 * A simple implementation of memory map using an array of pages.
 * 
 */
public class SimpleMemMap implements MemMap {
	
	/**
	 * Constructs a SimpleMemMap with numPhyPages pages.
	 * 
	 * @param numPhyPages
	 */
	public SimpleMemMap(int numPhyPages) {
		lock = new nachos.threads.Lock();
		
		pageInfo = new PageInfo[numPhyPages];
		
		pageInfo[numPhyPages - 1] = new PageInfo(numPhyPages - 1, null);
		for (int i = numPhyPages - 2; i >= 0; --i) {
			pageInfo[i] = new PageInfo(i, pageInfo[i + 1]);
		}
		freePage = pageInfo[0];
		
		numFreePages = numPhyPages;
	}

	@Override
	public int allocateMem(TranslationEntry[] pageTable, int vpn, int count) {
		boolean acquiredWithinCall = false;
		if (!lock.isHeldByCurrentThread()) {
			acquiredWithinCall = true;
			lock.acquire();
		}
		
		if (numFreePages >= count) {
			for (int i = 0; i < count; ++i) {
				allocateAndFillEntry(pageTable[vpn + i], vpn + i);
			}
		}
		
		else {
			count = -1;
		}
		
		if (acquiredWithinCall) {
			lock.release();
		}
		
		return count;
	}

	@Override
	public int allocateMem(TranslationEntry entry, int vpn) {
		boolean acquiredWithinCall = false;
		if (!lock.isHeldByCurrentThread()) {
			acquiredWithinCall = true;
			lock.acquire();
		}
		
		int count;
		if (numFreePages >= 1) {
			count = 1;
			allocateAndFillEntry(entry, vpn);
		}
		else {
			count = -1;
		}
		
		if (acquiredWithinCall) {
			lock.release();
		}
		
		return count;
	}
	
	private void allocateAndFillEntry(TranslationEntry entry, int vpn) {
		PageInfo page = freePage;
		freePage = freePage.nextFreePage;
		page.nextFreePage = null;
		--numFreePages;
		
		entry.vpn = vpn;
		entry.ppn = page.ppn;
		entry.valid = true;
		entry.dirty = false;
		entry.used = false;
	}

	@Override
	public void releaseMem(TranslationEntry[] pageTable, int vpn, int count) {
		boolean acquiredWithinCall = false;
		if (!lock.isHeldByCurrentThread()) {
			acquiredWithinCall = true;
			lock.acquire();
		}
		
		for (int i = 0; i < count; ++i) {
			releasePage(pageTable[vpn + i]);
		}
		
		if (acquiredWithinCall) {
			lock.release();
		}
	}

	@Override
	public void releaseMem(TranslationEntry entry) {
		boolean acquiredWithinCall = false;
		if (!lock.isHeldByCurrentThread()) {
			acquiredWithinCall = true;
			lock.acquire();
		}
		
		releasePage(entry);
		
		if (acquiredWithinCall) {
			lock.release();
		}

	}
	
	private void releasePage(TranslationEntry entry) {
		pageInfo[entry.ppn].nextFreePage = freePage;
		freePage = pageInfo[entry.ppn];
		entry.valid = false;
		++numFreePages;
	}

	@Override
	public int reuseMem(TranslationEntry[] pageTable,
			TranslationEntry[] oldPageTable, int vpn, int old_vpn, int count) {
		/* not supported */
		return -1;
	}

	@Override
	public int reuseMem(TranslationEntry entry, TranslationEntry oldEntry) {
		/* not supported */
		return -1;
	}

	@Override
	public boolean requireUninterruptible() {
		lock.acquire();
		return true;
	}

	@Override
	public void releaseUninterruptible() {
		lock.release();
	}

	@Override
	public boolean supportUninterruptible() {
		return true;
	}
	
	@Override
	public int pagesAvailable() {
		return numFreePages;
	}
	
	private static class PageInfo {
		private PageInfo(int ppn, PageInfo nextFreePage) {
			this.ppn = ppn;
			this.nextFreePage = nextFreePage;
		}
		private int ppn;
		private PageInfo nextFreePage;
	}
	
	private nachos.threads.Lock lock;
	
	private PageInfo[] pageInfo;
	private PageInfo freePage;
	
	private int numFreePages;
}
