package nachos.userprog;

import nachos.machine.*;

import java.io.EOFException;
import java.util.Arrays;

/**
 * Encapsulates the state of a user process that is not contained in its user
 * thread (or threads). This includes its address translation state, a file
 * table, and information about the program being executed.
 * 
 * <p>
 * This class is extended by other classes to support additional functionality
 * (such as additional syscalls).
 * 
 * @see nachos.vm.VMProcess
 * @see nachos.network.NetProcess
 */
public class UserProcess {
	/**
	 * Allocate a new process.
	 */
	public UserProcess() {
		pageTable = null;
		openFiles = null;
	}

	/**
	 * Allocate and return a new process of the correct class. The class name is
	 * specified by the <tt>nachos.conf</tt> key
	 * <tt>Kernel.processClassName</tt>.
	 * 
	 * @return a new process of the correct class.
	 */
	public static UserProcess newUserProcess() {
		return (UserProcess) Lib.constructObject(Machine.getProcessClassName());
	}

	/**
	 * Execute the specified program with the specified arguments. Attempts to
	 * load the program, and then forks a thread to run it.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return the user thread if the program was successfully executed, or <tt>null</tt> on failure.
	 */
	public UThread execute(String name, String[] args) {
		/* Initializes PCB. */
		/* don't allocate pid until the program is loaded */
		
		/* Tries initializing the memory. */
		if (!load(name, args)) {
			clearProcess();
			return null;
		}
		
		/* File Descriptors. */
		initializeFD();
		
		/* Initializes STDIN */
		openFiles[STDIN_FILENO] = UserKernel.console.openForReading();
		if (openFiles[STDIN_FILENO] == null) {
			clearProcess();
			return null;
		}
		
		/* Initializes STDOUT */
		openFiles[STDOUT_FILENO] = UserKernel.console.openForWriting();
		if (openFiles[STDOUT_FILENO] == null) {
			clearProcess();
			return null;
		}
		
		/* Initializes child process set. */
		childProcess = new java.util.HashMap<Integer, UThread>();
		
		/* Initializes states. */
		inSysCall = false;
		exceptionHandled = true;
		
		UThread thread;
		/* Creates the main thread. PID is allocated in the constrcutor of UThread. */
		try {
			thread = new UThread(this, name);
		}
		catch (PIDOverflow e) {
			Lib.debug(dbgProcess, "PID overflow");
			return null;
		}
		thread.fork();

		return thread;
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
		
		UserKernel.processor.setPageTable(pageTable);
	}

	/**
	 * Read a null-terminated string from this process's virtual memory. Read at
	 * most <tt>maxLength + 1</tt> bytes from the specified address, search for
	 * the null terminator, and convert it to a <tt>java.lang.String</tt>,
	 * without including the null terminator. If no null terminator is found,
	 * returns <tt>null</tt>.
	 * 
	 * @param vaddr
	 *            the starting virtual address of the null-terminated string.
	 * @param maxLength
	 *            the maximum number of characters in the string, not including
	 *            the null terminator.
	 * @return the string read, or <tt>null</tt> if no null terminator was
	 *         found or unhandled exception occurred when accessing the vm
	 */
	public String readVirtualMemoryString(int vaddr, int maxLength) {
		/* don't break the interface */
		Lib.assertTrue(maxLength >= 0);
		
		++maxLength;	// to include the NULL-terminator
		byte[] bytes = new byte[pageSize];
		StringBuilder builder = new StringBuilder();
		
		/* one page a time is safe */
		int bytesRead = 0;
		while (bytesRead < maxLength) {
			int num_to_read = Math.min(maxLength - bytesRead, 
					Processor.makeAddress(Processor.pageFromAddress(vaddr) + 1, 0) - vaddr);
			
			if (readVirtualMemory(vaddr, 1, bytes, 0, num_to_read) != num_to_read) {
				return null;
			}
			vaddr += num_to_read;
			bytesRead += num_to_read;
			
			for (int i = 0; i < num_to_read; ++i) {
				if (bytes[i] == 0)
					return builder.toString();
				else
					builder.append((char) bytes[i]);
			}
		}

		return null;
	}
	
	/**
	 * Read a null-terminated string from this process's virtual memory. 
	 * 
	 * @param vaddr
	 *            the starting virtual address of the null-terminated string.
	 * @return the string read, or null on failure
	 */
	public String readVirtualMemoryString(int vaddr) {
		
		return readVirtualMemoryString(vaddr, Integer.MAX_VALUE - 1);
	}
	
	/**
	 * Transfer data from this process's virtual memory to all of the specified
	 * array. Same as <tt>readVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data) {
		return readVirtualMemory(vaddr, 1, data, 0, data.length);
	}
	
	
	/**
	 * Transfer data from this process's virtual memory to the specified array.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param data
	 *            the array where the data will be stored.
	 * @param offset
	 *            the first byte to write in the array.
	 * @param length
	 *            the number of bytes to transfer from virtual memory to the
	 *            array.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		/* don't break the interface */
		
		// just make the minimal alignment requirement
		return readVirtualMemory(vaddr, 1, data, offset, length);
	}
	
	/**
	 * Read the virtual memory with alignment requirement.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param alignment
	 * 			the alignment requirement (must be 1, 2, or 4)
	 * @param data
	 *            the array where the data will be stored.
	 * @param offset
	 *            the first byte to write in the array.
	 * @param length
	 *            the number of bytes to transfer from virtual memory to the
	 *            array.
	 * @return the number of bytes successfully transferred. (have to be length or 0)
	 */
	public int readVirtualMemory(int vaddr, int alignment, byte[] data, int offset, int length) {
		if (length == 0) return 0;
		Lib.assertTrue(data != null && offset >= 0 && offset < data.length
				&& length >= 0 && offset + length <= data.length);
		
		int amount = 0;
		
		byte[] memory = UserKernel.processor.getMemory();
		while (amount < length) {
			int paddr;
			try {
				paddr = translateVAddr(vaddr, alignment, false);
			} catch (MipsException e) {
				e.handle();	// assume that handle either has successfully solved the problem,
							// or terminated the process
				if (!exceptionHandled) {
					return 0;
				}
				continue;
			}
			
			int ppn = Processor.pageFromAddress(paddr);
			
			/* read at most one page a time */
			int num_to_read = Math.min(length - amount, Processor.makeAddress(ppn + 1, 0) - paddr);
			System.arraycopy(memory, paddr, data, offset + amount, num_to_read);
			amount += num_to_read;
			vaddr += num_to_read;
		}
		
		return amount;
	}

	/**
	 * Transfer all data from the specified array to this process's virtual
	 * memory. Same as <tt>writeVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data) {
		return writeVirtualMemory(vaddr, 1, data, 0, data.length);
	}

	/**
	 * Transfer data from the specified array to this process's virtual memory.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to write.
	 * @param data
	 *            the array containing the data to transfer.
	 * @param offset
	 *            the first byte to transfer from the array.
	 * @param length
	 *            the number of bytes to transfer from the array to virtual
	 *            memory.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
		/* Don't break the interface */
		
		// just make the minimal alignment requirement
		return writeVirtualMemory(vaddr, 1, data, offset, length);	
	}
	
	/**
	 * Write the virtual memory with alignment requirement.
	 * 
	 * @param vaddr
	 *            the first byte of virtual memory to read.
	 * @param alignment
	 * 			the alignment requirement (must be 1, 2, or 4)
	 * @param data
	 *            the array where the data will be stored.
	 * @param offset
	 *            the first byte to write in the array.
	 * @param length
	 *            the number of bytes to transfer from virtual memory to the
	 *            array.
	 * @return the number of bytes successfully transferred. (have to be length)
	 */
	public int writeVirtualMemory(int vaddr, int alignment, byte[] data, int offset, int length) {
		if (length == 0) return 0;
		Lib.assertTrue(data != null && offset >= 0 && offset < data.length
				&& length >= 0 && offset + length <= data.length);
		
		int amount = 0;
		
		byte[] memory = UserKernel.processor.getMemory();
		while (amount < length) {
			int paddr;
			try {
				paddr = translateVAddr(vaddr, alignment, true);
			} catch (MipsException e) {
				e.handle();	// assume that handle have either solved the problem
							// or terminated the process
				if (!exceptionHandled) {
					return 0;
				}
				continue;
			}
			
			int ppn = Processor.pageFromAddress(paddr);
			
			/* write at most one page a time */
			int num_to_write = Math.min(length - amount, Processor.makeAddress(ppn + 1, 0) - paddr);
			System.arraycopy(data, offset + amount, memory, paddr, num_to_write);
			amount += num_to_write;
			vaddr += num_to_write;
		}
		
		return amount;
	}

/* aligned load/store */
	public byte lb(int vaddr) throws LoadStoreFailure {
		byte[] buf = new byte[1];
		if (readVirtualMemory(vaddr, 1, buf, 0, 1) != 1) {
			throw new LoadStoreFailure();
		}
		return buf[0];
	}

	public short lh(int vaddr) throws LoadStoreFailure {
		byte[] buf = new byte[2];
		if (readVirtualMemory(vaddr, 2, buf, 0, 2) != 2) {
			throw new LoadStoreFailure();
		}
		return Lib.bytesToShort(buf, 0);
	}

	public int lw(int vaddr) throws LoadStoreFailure {
		byte[] buf = new byte[4];
		if (readVirtualMemory(vaddr, 4, buf, 0, 4) != 4) {
			throw new LoadStoreFailure();
		}
		return Lib.bytesToInt(buf, 0);
	}

	public void sb(int vaddr, byte value) throws LoadStoreFailure {
		byte[] buf = {value};
		if (writeVirtualMemory(vaddr, 1, buf, 0, 1) != 1) {
			throw new LoadStoreFailure();
		}
	}

	public void sh(int vaddr, short value) throws LoadStoreFailure {
		byte[] buf = new byte[2];
		Lib.bytesFromShort(buf, 0, value);
		if (writeVirtualMemory(vaddr, 2, buf, 0, 2) != 2) {
			throw new LoadStoreFailure();
		}
	}

	public void sw(int vaddr, int value) throws LoadStoreFailure {
		byte[] buf = new byte[4];
		Lib.bytesFromInt(buf, 0, value);
		if (writeVirtualMemory(vaddr, 4, buf, 0, 4) != 4) {
			throw new LoadStoreFailure();
		}
	}
	
	@SuppressWarnings("serial")
	public
	static class LoadStoreFailure extends Exception {}
	
	
	/**
	 * Translates the virtual address according to the page table. Checks are performed to
	 * ensure the access is valid.
	 * 
	 * @param vaddr			the virtual address to translate
	 * @param alignment		the alignment requirement, must be 1, 2, or 4
	 * @param isWrite		<tt>true</tt> if the memory access is a write
	 * @return				the physical address
	 * @exception nachos.userprog.MipsException 
	 * 						when an error occurred. (AddrError, PageFault, TLBMiss, ReadOnly, BusError)
	 */
	public int translateVAddr(int vaddr, int alignment, boolean isWrite) throws MipsException {
		/* Cannot do stats since we don't have the privilege */
		
		Lib.debug(dbgProcessor, "\ttranslate vaddr=0x" + Lib.toHexString(vaddr, 8)
				+ (isWrite ? ", write": ", read..."));
		
		/* checks alignment */
		if ((vaddr & (alignment - 1)) != 0) {
			Lib.debug(dbgProcess, "\t\talignment error");
			throw new MipsException(Processor.exceptionAddressError, vaddr);
		}
		
		/* calculate virtual page number and offset from vaddr */
		int vpn = Processor.pageFromAddress(vaddr);
		int offset = Processor.offsetFromAddress(vaddr);
		
		TranslationEntry entry = null;
		int tlbNumber = -1; // to ensure tlbNumber is properly. If not, an assertion failure will be triggered.
		
		/* if not using a TLB, the vpn is an index into the translation table */
		if (!UserKernel.usingTLB) {
			/* use this.pageTable instead of machine's, for we may need to write argc, argv to
			 * the vm before starting the execution of the process */
			TranslationEntry[] translations = this.pageTable;	
			if (translations == null || vpn >= translations.length
					|| translations[vpn] == null || !translations[vpn].valid) {
				UserKernel.stats.numPageFaults++;
				Lib.debug(dbgProcessor, "\t\tpage fault");
				throw new MipsException(Processor.exceptionPageFault, vaddr);
			}
			entry = translations[vpn];
		}
		
		/* otherwise, looks through the fully-associative TLB for matching vpn */
		else {
			for (int i = 0; i < UserKernel.tlbSize; ++i) {
				TranslationEntry tlbEntry = UserKernel.processor.readTLBEntry(i);
				if (tlbEntry.valid && tlbEntry.vpn == vpn) {
					entry = tlbEntry;
					tlbNumber = i;
					break;
				}
			}
			if (entry == null) {
				UserKernel.stats.numTLBMisses++;
				Lib.debug(dbgProcessor, "\t\tTLB miss");
				throw new MipsException(Processor.exceptionTLBMiss, vaddr);
			}
		}
		
		/* checks if trying to write a read-only page */
		if (entry.readOnly && isWrite) {
			Lib.debug(dbgProcessor, "\t\tread-only exception");
			throw new MipsException(Processor.exceptionReadOnly, vaddr);
		}
		
		/* checks if ppn is out of range */
		int ppn = entry.ppn;
		if (ppn < 0 || ppn >= UserKernel.numPhysPages) {
			Lib.debug(dbgProcessor, "\t\tbad ppn");
			throw new MipsException(Processor.exceptionBusError, vaddr);
		}
		
		/* sets used and dirty bits as appropriate */
		entry.used = true;
		if (isWrite)
			entry.dirty = true;
		if (UserKernel.usingTLB) {
			/* since we are not directly manipulating TLB entries in the processor, have to write back the TLB entry */
			UserKernel.processor.writeTLBEntry(tlbNumber, entry);
		}
		
		int paddr = (ppn * pageSize) + offset; // using the same way that Processor make the address, pageSize is a copy from Processor
		
		Lib.debug(dbgProcessor, "\t\tpaddr=0x" + Lib.toHexString(paddr, 8));
		
		return paddr;
	}

	/**
	 * Load the executable with the specified name into this process, and
	 * prepare to pass it the specified arguments. Opens the executable, reads
	 * its header information, and copies sections and arguments into this
	 * process's virtual memory.
	 * 
	 * @param name
	 *            the name of the file containing the executable.
	 * @param args
	 *            the arguments to pass to the executable.
	 * @return <tt>true</tt> if the executable was successfully loaded.
	 */
	protected boolean load(String name, String[] args) {
		Lib.debug(dbgProcess, "UserProcess.load(\"" + name + "\")");
		
		OpenFile executable = UserKernel.fileSystem.open(name, false);
		if (executable == null) {
			Lib.debug(dbgProcess, "\topen failed");
			return false;
		}

		try {
			coff = new Coff(executable);
		} catch (EOFException e) {
			executable.close();
			Lib.debug(dbgProcess, "\tcoff load failed");
			return false;
		}

		// make sure the sections are contiguous and start at page 0
		numPages = 0;
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);
			if (section.getFirstVPN() != numPages) {
				coff.close();
				Lib.debug(dbgProcess, "\tfragmented executable");
				return false;
			}
			numPages += section.getLength();
		}

		// make sure the argv array will fit in one page
		byte[][] argv = new byte[args.length][];
		int argsSize = 8;	// in case that gcc stores argc and argv at initial $sp
		for (int i = 0; i < args.length; i++) {
			argv[i] = args[i].getBytes();
			// 4 bytes for argv[] pointer; then string plus one for null byte
			argsSize += 4 + argv[i].length + 1;
		}
		if (argsSize > pageSize) {
			coff.close();
			Lib.debug(dbgProcess, "\targuments too long");
			return false;
		}

		// program counter initially points at the program entry point
		initialPC = coff.getEntryPoint();

		// next comes the stack; stack pointer initially points to top of it
		numPages += stackPages;
		initialSP = numPages * pageSize;

		// and finally reserve 1 page for arguments
		numPages++;
		
		/* allocate the page table */
		pageTable = new TranslationEntry[numPages];
		for (int i = 0; i < numPages; ++i) {
			pageTable[i] = new TranslationEntry();
		}
		if (numPages != UserKernel.memMap.allocateMem(pageTable, 0, numPages)) {
			/* out of memory */
			Lib.debug(dbgProcess, "\tout of memory");
			coff.close();
			return false;
		}
		
		if (!loadSections()) {
			coff.close();
			return false;
		}

		// store arguments in last page
		
		/* Bug fixed: gcc may write argc and argv back to the stack
		 * even if -O2 is enabled.
		 * 
		 * test_async_file_op.s:
		 * in main:
		 * 
		 * sw $a0, 32($sp)
		 * sw $a1, 36($sp)
		 *  */
		int entryOffset = 8 + (numPages - 1) * pageSize;
		int stringOffset = entryOffset + args.length * 4;

		this.argc = args.length;
		this.argv = entryOffset;
		
		boolean success = true;
		for (int i = 0; i < argv.length; i++) {
			byte[] stringOffsetBytes = Lib.bytesFromInt(stringOffset);
			if (writeVirtualMemory(entryOffset, stringOffsetBytes) != 4) {
				success = false;
				break;
			}
			entryOffset += 4;
			if (writeVirtualMemory(stringOffset, argv[i]) != argv[i].length) {
				success = false;
				break;
			}
			stringOffset += argv[i].length;
			if (writeVirtualMemory(stringOffset, new byte[] { 0 }) != 1) {
				success = false;
				break;
			}
			stringOffset += 1;
		}
		
		coff.close();
		return success;
	}

	/**
	 * Loads the COFF sections into
	 * memory. If this returns successfully, the process will definitely be run
	 * (this is the last step in process initialisation that can fail).
	 * 
	 * @return <tt>true</tt> if the sections were successfully loaded.
	 */
	protected boolean loadSections() {
		// memory allocation is now done in load()
		
		// load sections
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);

			Lib.debug(dbgProcess, "\tinitializing " + section.getName()
					+ " section (" + section.getLength() + " pages)");
			
			boolean readOnly = section.isReadOnly();
			for (int i = 0; i < section.getLength(); i++) {
				int vpn = section.getFirstVPN() + i;

				// for now, just assume virtual addresses=physical addresses
				section.loadPage(i, pageTable[vpn].ppn);
				pageTable[vpn].readOnly = readOnly;
			}
		}

		return true;
	}

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
	}

	/**
	 * Initialize the processor's registers in preparation for running the
	 * program loaded into this process. Set the PC register to point at the
	 * start function, set the stack pointer register to point at the top of the
	 * stack, set the A0 and A1 registers to argc and argv, respectively, and
	 * initialize all other registers to 0.
	 */
	public void initRegisters() {
		Processor processor = UserKernel.processor;

		// by default, everything's 0
		for (int i = 0; i < Processor.numUserRegisters; i++)
			processor.writeRegister(i, 0);

		// initialize PC and SP according
		processor.writeRegister(Processor.regPC, initialPC);
		processor.writeRegister(Processor.regSP, initialSP);

		// initialize the first two argument registers to argc and argv
		processor.writeRegister(Processor.regA0, argc);
		processor.writeRegister(Processor.regA1, argv);
	}

	/**
	 * Handle the halt() system call.
	 * <p>
	 * <tt>void halt();</tt>
	 * <p>
	 * Halt the Nachos VM. Only the root process is allowed to call this syscall. Other process
	 * calling the halt syscall will return immediately.
	 */
	private int handleHalt() {
		if (UThread.currentThread() == UserKernel.root) {
			signaledHalt = true;
			UThread.exit(0, false);
		}
		
		return -1;
	}

	
	/**
	 * Handle the exit syscall.
	 * <p>
	 * <tt>void exit(int status);</tt>
	 * <p>
	 * Terminate the current process immediately with the specified exit status.
	 * exit() never returns;
	 * 
	 * @param status	the specified exit status
	 */
	private int handleExit(int status) {
		UThread.exit(status, true);
		Lib.assertNotReached();
		return -1;
	}
	
	/**
	 * Handle the exec syscall.
	 * <p>
	 * <tt>int exec(char* file, int argc, char* argv[]);</tt>
	 * <p>
	 *  
	 * @param pstrName
	 * @param argc
	 * @param ppstrArgv
	 * @return
	 */
	private int handleExec(int pstrName, int argc, int ppstrArgv) {
		String fileName = readVirtualMemoryString(pstrName, maxPathLength);
		if (fileName == null) {
			return -1;
		}
		
		if (argc < 0) {
			return -1;
		}
		String[] args = new String[argc];
		for (int i = 0; i < argc; ++i) {
			int pstrArg;
			try {
				pstrArg = lw(ppstrArgv + 4 * i);
			} catch (LoadStoreFailure e) {
				/* error */
				return -1;
			}
			args[i] = readVirtualMemoryString(pstrArg);
			if (args[i] == null) {
				return -1;
			}
		}
		
		UserProcess process = newUserProcess();
		UThread thread = process.execute(fileName, args);
		if (thread == null) {
			return -1;
		}
		
		childProcess.put(thread.pid, thread);
		
		return thread.pid;
	}
	
	/**
	 * <tt>int join(int pid, int* status);</tt>
	 * 
	 * @param pid
	 * @param piStatus
	 * @return
	 */
	private int handleJoin(int pid, int piStatus) {
		UThread thread = childProcess.get(pid);
		if (pid < 0 || thread == null) {
			return -1;
		}
		
		thread.join();
		childProcess.remove(pid);
		
		try {
			sw(piStatus, thread.exitCode);
		} catch (LoadStoreFailure e) {
			/* unspecified what to do. I'm just going to ignore it. */
		}
		
		if (!thread.normalExit) {
			return 0;
		}
		
		return 1;
	}

	private int handleCreate(int pstrName) {
		String fileName = readVirtualMemoryString(pstrName, maxPathLength);
		if (fileName == null) return -1;
		
		int fd = allocateFD();
		if (fd == -1) return -1;
	
		OpenFile openFile = UserKernel.fileSystem.open(fileName, true);
		if (openFile == null) {
			deallocateFD(fd);	// in case of future change
			return -1;
		}
		
		openFiles[fd] = openFile;
		return fd;
	}

	private int handleOpen(int pstrName) {
		String fileName = readVirtualMemoryString(pstrName, maxPathLength);
		if (fileName == null) return -1;
		
		int fd = allocateFD();
		if (fd == -1) return -1;
	
		OpenFile openFile = UserKernel.fileSystem.open(fileName, false);
		if (openFile == null) {
			deallocateFD(fd);	// in case of future change
			return -1;
		}
		
		openFiles[fd] = openFile;
		return fd;
	}

	private int handleRead(int fd, int pBuffer, int count) {
		if (fd < 0 || fd >= maxNumFileDescriptors){
			return -1;
		}
		if (openFiles[fd] == null) {
			return -1;
		}
		
		byte[] buffer = new byte[count];
		int numRead = openFiles[fd].read(buffer, 0, count); // count < 0 is ok
		if (numRead < 0) {
			/* error */
			return -1;
		}
		if (numRead > 0) {
			if (writeVirtualMemory(pBuffer, buffer, 0, numRead) != numRead) {
				return -1;
			};
		}
		return numRead;
	}

	private int handleWrite(int fd, int pBuffer, int count) {
		if (fd < 0 || fd >= maxNumFileDescriptors) {
			return -1;
		}
		if (openFiles[fd] == null) {
			return -1;
		}
		if (count < 0) {
			return -1;
		}
		
		byte[] buffer = new byte[count];
		if (readVirtualMemory(pBuffer, buffer, 0, count) != count) {
			return -1;
		}
		
		int actualWritten = openFiles[fd].write(buffer, 0, count);
		if (actualWritten != count) {
			/* error when writing */
			return -1;
		}
		return count; // or actualWritten 
	}
	
	private int handleClose(int fd) {
		return deallocateFD(fd);
	}
	
	private int handleUnlink(int pstrName) {
		String fileName = readVirtualMemoryString(pstrName, maxPathLength);
		if (fileName == null) {
			return -1;
		}
		
		boolean successful = UserKernel.fileSystem.remove(fileName);
		if (!successful) {
			return -1;
		}
		
		return 0;
	}
	private static final int syscallHalt = 0, syscallExit = 1, syscallExec = 2,
			syscallJoin = 3, syscallCreate = 4, syscallOpen = 5,
			syscallRead = 6, syscallWrite = 7, syscallClose = 8,
			syscallUnlink = 9;

	/**
	 * Handle a syscall exception. Called by <tt>handleException()</tt>. The
	 * <i>syscall</i> argument identifies which syscall the user executed:
	 * 
	 * <table>
	 * <tr>
	 * <td>syscall#</td>
	 * <td>syscall prototype</td>
	 * </tr>
	 * <tr>
	 * <td>0</td>
	 * <td><tt>void halt();</tt></td>
	 * </tr>
	 * <tr>
	 * <td>1</td>
	 * <td><tt>void exit(int status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>2</td>
	 * <td><tt>int  exec(char *name, int argc, char **argv);
     * 								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>3</td>
	 * <td><tt>int  join(int pid, int *status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>4</td>
	 * <td><tt>int  creat(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>5</td>
	 * <td><tt>int  open(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>6</td>
	 * <td><tt>int  read(int fd, char *buffer, int size);
     *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>7</td>
	 * <td><tt>int  write(int fd, char *buffer, int size);
     *								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>8</td>
	 * <td><tt>int  close(int fd);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>9</td>
	 * <td><tt>int  unlink(char *name);</tt></td>
	 * </tr>
	 * </table>
	 * 
	 * @param syscall
	 *            the syscall number.
	 * @param a0
	 *            the first syscall argument.
	 * @param a1
	 *            the second syscall argument.
	 * @param a2
	 *            the third syscall argument.
	 * @param a3
	 *            the fourth syscall argument.
	 * @return the value to be returned to the user.
	 */
	public int handleSyscall(int syscall, int a0, int a1, int a2, int a3) {
		String msg;
		switch (syscall) {
		case syscallHalt:
			return handleHalt();
		
		case syscallExit:
			return handleExit(a0);
			
		case syscallExec:
			return handleExec(a0, a1, a2);
		
		case syscallJoin:
			return handleJoin(a0, a1);
			
		case syscallCreate:
			return handleCreate(a0);
			
		case syscallOpen:
			return handleOpen(a0);
			
		case syscallRead:
			return handleRead(a0, a1, a2);
			
		case syscallWrite:
			return handleWrite(a0, a1, a2);
			
		case syscallClose:
			return handleClose(a0);
			
		case syscallUnlink:
			return handleUnlink(a0);

		default:
			msg = "Unknown syscall " + syscall;
			Lib.debug(dbgProcess, msg);
			
		}
		
		UThread currentThread = (UThread) UThread.currentThread();

		/* msg_buf is NULL-terminated */
		byte[] msg_buf = SynchConsole.bytesFromString(msg + "\n");
		OpenFile stdout = currentThread.process.openFiles[STDOUT_FILENO];
		if (stdout != null) {
//			stdout.write(msg_buf, 0, msg_buf.length - 1);
		}
		UThread.exit(currentThread, Processor.exceptionSyscall, false);
		
		Lib.assertNotReached();
		return 0;
	}

	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 * @param badVAddr
	 * 			  the badVAddr register
	 */
	public void handleException(int cause, int badVAddr) {
		exceptionHandled = false;
		
		String msg = null;
		switch (cause) {
		case Processor.exceptionSyscall:
			inSysCall = true;
			int result = handleSyscall(UserKernel.processor.readRegister(Processor.regV0),
					UserKernel.processor.readRegister(Processor.regA0), UserKernel.processor
							.readRegister(Processor.regA1), UserKernel.processor
							.readRegister(Processor.regA2), UserKernel.processor
							.readRegister(Processor.regA3));
			UserKernel.processor.writeRegister(Processor.regV0, result);
			UserKernel.processor.advancePC();
			exceptionHandled = true;
			inSysCall = false;
			break;
			
		case Processor.exceptionAddressError:
		case Processor.exceptionBusError:
		case Processor.exceptionPageFault:
		case Processor.exceptionReadOnly:
		case Processor.exceptionTLBMiss:
			/* these exceptions has badVAddr */
			msg = "Unexpected exception: " + Processor.exceptionNames[cause] + "@0x" + Lib.toHexString(badVAddr, 8);
			break;
		default:
			msg = "Unexpected exception: " + Processor.exceptionNames[cause];
		}
		
		if (!exceptionHandled) {
			Lib.debug(dbgProcess, msg);
			
			if (!inSysCall) {
				/* unhandled exception in user program. Terminate the process.*/
				UThread currentThread = (UThread) UThread.currentThread();
				/* msg_buf is NULL-terminated */
				byte[] msg_buf = SynchConsole.bytesFromString(msg + "\n");
				OpenFile stdout = currentThread.process.openFiles[STDOUT_FILENO];
				if (stdout != null) {
//					stdout.write(msg_buf, 0, msg_buf.length - 1);
				}

				UThread.exit(currentThread, cause, false);
			}
			
			/* a process in syscall does not terminate abruptly. Leave it to the syscall to decide. */
		}
		
		else if (signaledHalt) {
			/* if !exceptionHandled and inSyscall, then the syscall will eventually set exceptionHandled to true */
			UThread.exit(0, false);
		}
	}
	
	
	/**
	 * Initializes openFiles to all null.
	 */
	protected void initializeFD() {
		openFiles = new OpenFile[maxNumFileDescriptors];
		Arrays.fill(openFiles, null);
	}
	
	/**
	 * Allocate the minimum available FD.
	 * 
	 * @return	the allocated fd, or -1 when there's no available fd
	 */
	protected int allocateFD() {
		int fd = 0;
		while (fd < maxNumFileDescriptors && openFiles[fd] != null) ++fd;
		if (fd == maxNumFileDescriptors) fd = -1;
		return fd;
	}
	
	/**
	 * Deallocate the fd and close the file if no other fd refers to the same file.
	 * 
	 * @param fd	the fd to close
	 * @return		0 on success, -1 if fd is out of range or fd is not open
	 */
	protected int deallocateFD(int fd) {
		if (fd >= 0 && fd < maxNumFileDescriptors && openFiles[fd] != null) {
			/* Currently there's no way to associate multiple fd's to the same file.
			 * So deallocating fd must lead to closing the file. */
			openFiles[fd].close();
			openFiles[fd] = null;
			return 0;
		}
		return -1;
	}
	
	/**
	 * Closes all opened files.
	 */
	protected void closeAllFD() {
		if (openFiles == null) return ;
		for (int i = 0; i != maxNumFileDescriptors; ++i) {
			if (openFiles[i] != null) {
				openFiles[i].close();
				openFiles[i] = null;
			}
		}
	}
	
	/**
	 * Clear and release this process's resources.
	 */
	protected void clearProcess() {
		closeAllFD();
		if (pageTable != null) {
			for (TranslationEntry entry : pageTable) {
				if (entry != null && entry.valid)
					UserKernel.memMap.releaseMem(entry);
			}
			pageTable = null;
		}
	}
	
	/** The program being run by this process. */
	protected Coff coff;

	/** This process's page table. The page table is simply an array for there are no vm is supported and the address space,
	 * is at most the whole physical address space. */
	protected TranslationEntry[] pageTable;
	/** The number of contiguous pages occupied by the program. */
	public int numPages;

	/** The number of pages in the program's stack. */
	public final int stackPages = Config.getInteger("Processor.numStackPages", 8);

	public int initialPC;

	public int initialSP;
	public int argc, argv;

	/** The maximum number of file descriptors a process can have concurrently. */
	public static final int maxNumFileDescriptors = 16;
	
	public static final int STDIN_FILENO = 0;
	public static final int STDOUT_FILENO = 1;
	//private static final int STDERR_FILENO = 2;
	
	/** The associated file to each file descriptor. */
	public OpenFile openFiles[];

	/** The set of child processes. */
	public java.util.HashMap<Integer, UThread> childProcess;
	
	/** Whether now the process is in a syscall. */
	public boolean inSysCall;
	
	/** Whether the last exception was successfully handled. */
	public boolean exceptionHandled;
	
	public static boolean signaledHalt = false;
	
	public static final int pageSize = Processor.pageSize;
	public static final char dbgProcess = 'a';
	public static final char dbgProcessor = 'p';
	
	/** The maximum length of file name (not including the NULL terminator). See syscall.h. */
	public static final int maxFileNameLength = nachos.filesys.FileStat.FILE_NAME_MAX_LEN - 1;
	
	public static final int maxPathLength = nachos.filesys.NachosFS.MAX_PATH_LENGTH;
	
}
