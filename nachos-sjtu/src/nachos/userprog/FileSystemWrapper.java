package nachos.userprog;

import java.util.HashMap;

import nachos.machine.FileSystem;
import nachos.machine.OpenFile;
import nachos.threads.Lock;

/**
 * The abstract class of FileSystem wrappers.
 *
 */
public abstract class FileSystemWrapper implements FileSystem {

	/**
	 * Constructs a new FileSystemWrapper.
	 * 
	 * @param fs	the underlying unwrapped FileSystem
	 */
	public FileSystemWrapper(FileSystem fs) {
		this.fs = fs;
		this.fileInfo = new HashMap<String, FileInfo>();
		this.fileInfoLock = new Lock();
	}
	
	@Override
	public OpenFile open(String name, boolean truncate) {
		if (name == null) return null;
		
		FileInfo info = acquireFileInfo(name);
		
		/* only one thread can operate on the same file simultaneously. */
		info.sem.P();
		
		OpenFileWrapper of_wrapper = open_impl(info, truncate);
		
		/* signal waiting threads to continue */
		info.sem.V();
		
		if (of_wrapper == null) releaseFileInfo(info);
		
		return of_wrapper;
	}

	@Override
	public boolean remove(String name) {
		if (name == null) return false;
		
		FileInfo info = acquireFileInfo(name);
		
		/* only one thread can operate on the same file simultaneously. */
		info.sem.P();
		
		boolean result = remove_impl(info);
		
		/* signal waiting threads to continue */
		info.sem.V();
		
		releaseFileInfo(info);
		
		return result;
	}
	

	/**
	 * Acquires a the FileInfo object associated to the file of the name.
	 * 
	 * @param name	the name of the file
	 * @return		the FileInfo object that is associated to the file of the name 
	 */
	public FileInfo acquireFileInfo(String name) {
		fileInfoLock.acquire();
		
		FileInfo info = fileInfo.get(name);
		if (info == null) {
			info = newFileInfo(name);
			fileInfo.put(name, info);
		}
		++info.holdCount;
		
		fileInfoLock.release();
		
		return info;
	}

	/**
	 * Releases the FileInfo object. When holdCount drops to 0,
	 * the default behaviour is that the FileInfo object will
	 * be removed from fileInfo. 
	 * 
	 * @param info
	 */
	public void releaseFileInfo(FileInfo info) {
		fileInfoLock.acquire();
		
		--info.holdCount;
		if (info.holdCount == 0) {
			fileInfo.remove(info.name);
		}
		
		fileInfoLock.release();
	}

	/**
	 * Returns the underlying file system.
	 * 
	 * @return	the underlying file system
	 */
	public FileSystem underlyingFileSystem() {
		return fs;
	}

	/**
	 * Returns a new FileInfo object or an object of subclasses of FileInfo 
	 * used by this FileSystemWrapper.
	 * 
	 * @param name	the name of the associated file
	 * @return		the new FileInfo object	
	 */
	protected abstract FileInfo newFileInfo(String name);
	
	/**
	 * Returns a new OpenFileWrapper used by this file system wrapper.
	 * 
	 * @param file	the underlying OpenFile object
	 * @param info	the associated FileInfo object
	 * @param fs	the FileSystemWrapper that provides the OpenFileWrapper
	 * @return
	 */
	protected abstract OpenFileWrapper newOpenFileWrapper(OpenFile file, FileInfo info, FileSystemWrapper fs);

	/**
	 * The unsynchronised implementation of open. Only for internal use.
	 * 
	 * @param name
	 * @param truncate
	 * @return
	 */
	protected abstract OpenFileWrapper open_impl(FileInfo info, boolean truncate);

	/**
	 * The unsynchronised implementation of remove. Only for internal use.
	 * 
	 * @param name
	 * @param truncate
	 * @return
	 */
	protected abstract boolean remove_impl(FileInfo info);

	/** The underlying file system. */
	protected FileSystem fs;
	
	/** The map from file name to associated FileInfo object. */
	protected HashMap<String, FileInfo> fileInfo;
	
	/** The lock that has to be acquired before accessing fileInfo. */
	protected Lock fileInfoLock;
}
