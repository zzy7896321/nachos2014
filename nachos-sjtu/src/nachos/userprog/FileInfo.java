package nachos.userprog;

import nachos.threads.Semaphore;

/**
 * The base class of FileInfo.
 * 
 */
public class FileInfo {
	
	/**
	 * Constructs a new FileInfo Object.
	 * 
	 * @param name
	 */
	public FileInfo(String name) {
		this.name = name;
		holdCount = 0;
		sem = new Semaphore(1);
	}
		
	/** the name of the associated file */
	public String name;
	
	/** the number of references to this FileInfo object. When holdCount drops to 0,
	 * it can be removed from fileInfo table. */
	public int holdCount;
	
	/** the semaphore that threads should signal when it's going call file system methods. */
	public Semaphore sem;
}
