package nachos.userprog;

import nachos.machine.FileSystem;
import nachos.machine.OpenFile;

/**
 * This is the wrapper class for nachos.machine.StubFileSystem, which violates all specification and fails
 * to recognize a UNIX-style path.
 * 
 * @author ZZy
 *
 */
public class StubFileSystemWrapper extends FileSystemWrapper {
	public StubFileSystemWrapper(FileSystem fs) {
		super(fs);
	}
	
	@Override
	protected nachos.userprog.OpenFileWrapper open_impl(nachos.userprog.FileInfo info_base, boolean truncate) {
		FileInfo info = (FileInfo) info_base;
		
		if (info.unlinking) {
			/* unlinking the file, reject the open request */
			return null;
		}
		
		/* This is a long operation lasting for more than 1000 ticks. */
		OpenFile openFile = fs.open(info.name, truncate);
		
		if (openFile == null) {
			/* open failed */
			return null;
		}
	
		/* transfer the ownership of info to the wrapper */
		OpenFileWrapper of_wrapper = (OpenFileWrapper) newOpenFileWrapper(openFile, info, this);
		++info.openCount;
		
		return of_wrapper;
	}

	@Override
	protected boolean remove_impl(nachos.userprog.FileInfo info_base) {
		FileInfo info = (FileInfo) info_base;
		boolean result;
		
		if (info.openCount > 0) {
			/* cannot unlink now */
			info.unlinking = true;
			result = true;
		}
		else {
			/* long operation */
			result = fs.remove(info.name);
		}
		
		return result;
	}

	@Override
	protected nachos.userprog.FileInfo newFileInfo(String name) {
		return new StubFileSystemWrapper.FileInfo(name);
	}

	@Override
	protected nachos.userprog.OpenFileWrapper newOpenFileWrapper(
			OpenFile openFile, 
			nachos.userprog.FileInfo info, 
			FileSystemWrapper fs) {
		return new StubFileSystemWrapper.OpenFileWrapper(openFile, info, fs);
	}
	
	public static class FileInfo extends nachos.userprog.FileInfo {
		protected FileInfo(String name) {
			super(name);
			openCount = 0;
			unlinking = false;
		}
		
		/** the number of not-closed access to this file */
		public int openCount;
		
		/** whether the file is to be unlinked or not. */
		public boolean unlinking;	
	}
	
	public static class OpenFileWrapper extends nachos.userprog.OpenFileWrapper {
		protected OpenFileWrapper(
				OpenFile openFile, 
				nachos.userprog.FileInfo info, 
				FileSystemWrapper fs) {
			super(openFile, info, fs);
		}

		@Override
		protected void close_impl() {
			FileInfo info = (FileInfo) this.info;
			
			super.close_impl();
			--info.openCount;
			
			if (info.openCount == 0 && info.unlinking) {
				fs.underlyingFileSystem().remove(info.name);
				info.unlinking = false;
			}
		}
	}

}
