package nachos.userprog;

import nachos.machine.*;
import nachos.threads.*;

/**
 * A kernel that can support multiple user processes.
 */
public class UserKernel extends ThreadedKernel {
	/**
	 * Allocate a new user kernel.
	 */
	public UserKernel() {
		super();
	}

	/**
	 * Initialize this kernel. Creates a synchronized console and sets the
	 * processor's exception handler.
	 */
	public void initialize(String[] args) {
		super.initialize(args);

		stats = new Stats();
		
		console = new SynchConsole(Machine.console());

		Machine.processor().setExceptionHandler(new Runnable() {
			public void run() {
				exceptionHandler();
			}
		});
		
		/* processor info */
		processor = Machine.processor();	// since the processor has only one instance
		usingTLB = this instanceof nachos.vm.VMKernel; // use the same way that processor set this variable
		tlbSize = (usingTLB) ? processor.getTLBSize() : 0;	// to avoid assertion failure
		numPhysPages = processor.getNumPhysPages();
		
		/* the managed file system */
		if (!no_fs_init) 
			fileSystem = FileSystemManager.createFileSystemConformingToSpecification(ThreadedKernel.fileSystem);
		/* else fileSystem must be set by subclasses */
		
		/* MemMap of the physical memory */
		if (!no_memmap_init)
			memMap = new SimpleMemMap(numPhysPages);
		/* else memMap is wrapped by subclasses, or not used */
		
		/* thread table */
		threadTableLock = new Lock();
		threadTable = new java.util.HashMap<Integer, UThread>();
		lastPid = 0;	// pids are required to be positive 
		root = null;
	}

	/**
	 * Test the console device.
	 */
	public void selfTest() {
//		super.selfTest();
//
//		System.out.println("Testing the console device. Typed characters");
//		System.out.println("will be echoed until q is typed.");
//
//		int c;
//		alarm.waitUntil(0);
//		do {
//			c = console.readByte(true);
//			console.writeByte((char) c);
//		} while (c != 'q');
//		
//		System.out.println("");
	}

	/**
	 * Returns the current process.
	 * 
	 * @return the current process, or <tt>null</tt> if no process is current.
	 */
	public static UserProcess currentProcess() {
		if (!(KThread.currentThread() instanceof UThread))
			return null;

		return ((UThread) KThread.currentThread()).process;
	}

	/**
	 * The exception handler. This handler is called by the processor whenever a
	 * user instruction causes a processor exception.
	 * 
	 * <p>
	 * When the exception handler is invoked, interrupts are enabled, and the
	 * processor's cause register contains an integer identifying the cause of
	 * the exception (see the <tt>exceptionZZZ</tt> constants in the
	 * <tt>Processor</tt> class). If the exception involves a bad virtual
	 * address (e.g. page fault, TLB miss, read-only, bus error, or address
	 * error), the processor's BadVAddr register identifies the virtual address
	 * that caused the exception.
	 */
	public void exceptionHandler() {
		Lib.assertTrue(KThread.currentThread() instanceof UThread);

		UserProcess process = ((UThread) KThread.currentThread()).process;
		int cause = processor.readRegister(Processor.regCause);
		int badVAddr = processor.readRegister(Processor.regBadVAddr);
		process.handleException(cause, badVAddr);
	}

	/**
	 * Start running user programs, by creating a process and running a shell
	 * program in it. The name of the shell program it must run is returned by
	 * <tt>Machine.getShellProgramName()</tt>.
	 * 
	 * @see nachos.machine.Machine#getShellProgramName
	 */
	public void run() {
		super.run();

		UserProcess process = UserProcess.newUserProcess();

		String shellProgram = Machine.getShellProgramName();
		if (process.execute(shellProgram, new String[] {shellProgram}) == null) {
//		if (process.execute("many.coff", new String[] {"many.coff", "matmult_2", "3"}) == null) {
			System.out.println("Error occured when loading shell.");
			nachos.machine.Machine.terminate();
		}

		KThread.finish();
	}

	@Override
	protected void finish() {
		super.finish();
		
		if (!threadTableLock.isHeldByCurrentThread()) 
			threadTableLock.acquire();
		
		for (UThread thread : threadTable.values()) {
			thread.process.clearProcess();	// release resources
		}
		
		/* no need to release the lock */
		
		stats.print();
	}

	/** Globally accessible reference to the synchronised console. */
	public static SynchConsole console;
	
	/** Globally accessible reference to the managed file system. */
	public static FileSystem fileSystem;
	public static boolean no_fs_init = false;
	
	/** Globally accessible reference to the physical memory map. */
	public static MemMap memMap;
	public static boolean no_memmap_init = false;
	
/* Global thread info. */
	/** Lock for the thread table and lastPid. */
	public static Lock threadTableLock;
	
	/** The global table of running threads . */
	public static java.util.HashMap<Integer, UThread> threadTable;
	
	/** The last pid allocated. */
	public static int lastPid;
	
	/** Root process. Accessing root process does not require a lock. */
	public static UThread root;
	
	/**
	 * Allocates a new pid and register the thread in the global table.
	 * 
	 * @return	the new pid, or -1 if no pid is available
	 */
	public static int registerNewThread(UThread thread) {
		boolean isAcquiredInThisCall = !threadTableLock.isHeldByCurrentThread();
		if (isAcquiredInThisCall) threadTableLock.acquire();
		
		/* No more pid available. */
		if (lastPid == Integer.MAX_VALUE) {
			threadTableLock.release();
			return -1;
		}
		
		int pid = ++lastPid;
		threadTable.put(pid, thread);
		
		if (root == null) root = thread;
		
		if (isAcquiredInThisCall) threadTableLock.release();
		
		return pid;
	}
	
	/**
	 * Releases the pid and remove entry in the global thread table.
	 * 
	 * @param pid	the pid to release
	 */
	public static void releasePID(int pid) {
		boolean isAcquiredInThisCall = !threadTableLock.isHeldByCurrentThread();
		
		if (isAcquiredInThisCall) threadTableLock.acquire();
		threadTable.remove(pid);
		if (isAcquiredInThisCall) threadTableLock.release();
	}
	
/* Processor Info (thanks to kernel code not running on the MIPS processor) */
	public static Processor processor;
	
	public static boolean usingTLB;
	
	public static int tlbSize;
	
	public static int numPhysPages;

/* stats */
	public static Stats stats;
}
