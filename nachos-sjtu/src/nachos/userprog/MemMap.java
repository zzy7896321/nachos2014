package nachos.userprog;

import nachos.machine.TranslationEntry;

/**
 * Classes that manages physical memory should implement this interface.
 * 
 */
public interface MemMap {
	
	/**
	 * Allocates count pages of physical memory and map virtual mem pages from vpn to vpn + count -1
	 * to the allocated physical mem pages. Either count pages or nothing will be allocated.
	 * <p>
	 * The operation cannot be interrupted even if the thread did not call <tt>requireUninterruptible</tt>.
	 * However, the thread may block when another process is operating on the memory map or has acquired
	 * the lock by calling <tt>requireUninterruptible</tt>.
	 * 
	 * @param pageTable		the page table to fill
	 * @param vpn			the starting vpn of vm page
	 * @param count			the number of pages to allocate
	 * @return				the number of pages successfully allocated, or -1 on failure
	 */
	public int allocateMem(TranslationEntry[] pageTable, int vpn, int count);

	/**
	 * Allocates a single page of physical memory and map virtual mem page vpn to it.
	 * <p>
	 * The operation cannot be interrupted even if the thread did not call <tt>requireUninterruptible</tt>.
	 * However, the thread may block when another process is operating on the memory map or has acquired
	 * the lock by calling <tt>requireUninterruptible</tt>.
	 * 
	 * @param entry			the page table entry to fill 
	 * @param vpn			the vpn of the vm page
	 * @return				1 on success, or -1 on failure 
	 */
	public int allocateMem(TranslationEntry entry, int vpn);
	
	/**
	 * Release count pages of physical memory mapped to pageTable[vpn .. vpn + count - 1].
	 * <p>
	 * The operation cannot be interrupted even if the thread did not call <tt>requireUninterruptible</tt>.
	 * However, the thread may block when another process is operating on the memory map or has acquired
	 * the lock by calling <tt>requireUninterruptible</tt>.
	 * 
	 * @param pageTable		the page table to fill
	 * @param vpn			the starting vpn of vm pages to release
	 * @param count			the number of pages to release
	 */
	public void releaseMem(TranslationEntry[] pageTable, int vpn, int count);
	
	/**
	 * Release the page mapped in the entry.
	 * <p>
	 * The operation cannot be interrupted even if the thread did not call <tt>requireUninterruptible</tt>.
	 * However, the thread may block when another process is operating on the memory map or has acquired
	 * the lock by calling <tt>requireUninterruptible</tt>.
	 * 
	 * @param entry 		the entry to fill
	 */
	public void releaseMem(TranslationEntry entry);
	
	/**
	 * Maps pageTable[vpn .. vpn + count - 1] to the same page to which oldPageTable[old_vpn .. old_vpn + count - 1] are
	 * mapped.
	 * <p>
	 * The operation cannot be interrupted even if the thread did not call <tt>requireUninterruptible</tt>.
	 * However, the thread may block when another process is operating on the memory map or has acquired
	 * the lock by calling <tt>requireUninterruptible</tt>.
	 * 
	 * @param pageTable		the new page table
	 * @param oldTable		the old page table
	 * @param vpn			the starting vpn in the new page table
	 * @param old_vpn		the starting vpn in the old page table
	 * @param count			the number of pages to map
	 * @return				count on success, -1 on failure
	 */
	public int reuseMem(TranslationEntry[] pageTable, TranslationEntry[] oldPageTable, int vpn, int old_vpn, int count);
	
	/**
	 * Maps the vm page in entry to the same page to which vm page in oldEntry is mapped.
	 * <p>
	 * The operation cannot be interrupted even if the thread did not call <tt>requireUninterruptible</tt>.
	 * However, the thread may block when another process is operating on the memory map or has acquired
	 * the lock by calling <tt>requireUninterruptible</tt>.
	 * 
	 * @param entry			the new page table
	 * @param oldEntry		the old page table
	 * @return				1 on success, -1 on failure
	 */
	public int reuseMem(TranslationEntry entry, TranslationEntry oldEntry);
	
	/**
	 * Requires the consecutive operations on the MemoryMap is not interruptible.
	 * <p>
	 * If the MemoryMap does support this feature, only <tt>true</tt> should be returned.
	 * If the caller successfully acquired the lock, <tt>true</tt> is returned. Otherwise,
	 * it blocks until it can acquire the lock.
	 * <p>
	 * If this feature is not supported, this function always returns <tt>false</tt>
	 * immediately.
	 * 
	 * @return <tt>true</tt> if the lock is acquired
	 */
	public boolean requireUninterruptible();

	/**
	 * Release the lock acquired by calling <tt>requireUninterruptible</tt> and continue
	 * the execution of one of the processes blocked on <tt>requireUninterruptible</tt> if 
	 * there are any.
	 */
	public void releaseUninterruptible();
	
	/**
	 * Checks whether the memory map support uninterruptible operations.
	 * 
	 * @return	<tt>true</tt> if the implementation support this feature
	 */
	public boolean supportUninterruptible();

	/**
	 * The number of pages available.
	 * <p>
	 * This method does not use lock. Acquire the lock by calling <tt>requireUninterruptible</tt> beforehand.
	 * 
	 * @return		the number of pages available
	 */
	public int pagesAvailable();
}
