package nachos.userprog;

import nachos.machine.*;
import nachos.threads.*;

/**
 * A UThread is KThread that can execute user program code inside a user
 * process, in addition to Nachos kernel code.
 */
public class UThread extends KThread {
	/**
	 * Allocate a new UThread.
	 * @throws PIDOverflow 
	 */
	public UThread(UserProcess process, String name) throws PIDOverflow {
		super();

		setTarget(new Runnable() {
			public void run() {
				runProgram();
			}
		});

		this.process = process;
		this.pid = UserKernel.registerNewThread(this);
		if (this.pid == -1) {
			throw new PIDOverflow();
		}
		
		setName(name + "(" + this.pid + ")");
	}

	private void runProgram() {
		/* first try to restore the states, then set the right value of registers. */
		process.restoreState();
		
		/* we actually write arguments here if virtual memory is enabled. */
		process.initRegisters();

		UserKernel.processor.run();

		Lib.assertNotReached();
	}

	/**
	 * Exits the current thread with the exit code.
	 * 
	 * @param exitCode	the exit code
	 */
	public static void exit(int exitCode, boolean normalExit) {
		exit((UThread) currentThread(), exitCode, normalExit);
	}
	
	/**
	 * Exits the thread with the exit code.
	 * 
	 * @param thread
	 * @param exitCode
	 * @param normalExit
	 */
	public static void exit(UThread thread, int exitCode, boolean normalExit) {
		/* sets up exit code and releases the resources */
		thread.exitCode = exitCode;
		thread.normalExit = normalExit;
		thread.process.clearProcess();
		
		/* Guard the global thread table with the lock */
		UserKernel.threadTableLock.acquire();
		
		UserKernel.releasePID(thread.pid);
		boolean isLastProcess = UserKernel.threadTable.isEmpty();
		
		UserKernel.threadTableLock.release();
		
		if (isLastProcess) {
			/* shutdown. Inform the kernel to clean up resources. */
			UserKernel.kernel.terminate();
		}
		else {
			KThread.finish();
		}
	}

	/**
	 * Save state before giving up the processor to another thread.
	 */
	protected void saveState() {
		process.saveState();

		for (int i = 0; i < Processor.numUserRegisters; i++)
			userRegisters[i] = UserKernel.processor.readRegister(i);

		super.saveState();
	}

	/**
	 * Restore state before receiving the processor again.
	 */
	protected void restoreState() {
//		Lib.debug(UserProcess.dbgProcess, "context switch to " + this.getName());
		
		super.restoreState();

		for (int i = 0; i < Processor.numUserRegisters; i++)
			UserKernel.processor.writeRegister(i, userRegisters[i]);

		process.restoreState();
	
		/* If we are signalled halt in the middle of an exception handler, we have to wait
		 * until the exception handling complete. Otherwise, we may encounter deadlock or
		 * file system inconsistency. */
		if (UserProcess.signaledHalt && process.exceptionHandled) {
			exit(0, false);
		}
	}
	
	/**
	 * Storage for the user register set.
	 * 
	 * <p>
	 * A thread capable of running user code actually has <i>two</i> sets of CPU
	 * registers: one for its state while executing user code, and one for its
	 * state while executing kernel code. While this thread is not running, its
	 * user state is stored here.
	 */
	public int userRegisters[] = new int[Processor.numUserRegisters];

	/**
	 * The process to which this thread belongs.
	 */
	public UserProcess process;
	
	/** PID. */
	protected int pid;
	
	/** The exit code. Only meaningful when the UThread's state is statusFinished. */
	protected int exitCode;
	
	/** Indicates whether the process has exited normally. */
	protected boolean normalExit;
}
