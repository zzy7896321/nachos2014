package nachos.userprog;

import nachos.machine.Processor;
import nachos.machine.Lib;

/**
 * Processor.MipsException with public visibility. This is thrown when
 * kernel code encounters an exception.
 */
@SuppressWarnings("serial")
public class MipsException extends Exception {
	
	public MipsException(int cause) {
		Lib.assertTrue(cause >= 0 && cause < Processor.exceptionNames.length);

		this.cause = cause;
		this.hasBadVAddr = false;
	}

	public MipsException(int cause, int badVAddr) {
		this(cause);

		hasBadVAddr = true;
		this.badVAddr = badVAddr;
	}
	
	public void handle() {
		UserKernel.processor.writeRegister(Processor.regCause, cause);

		if (hasBadVAddr)
			UserKernel.processor.writeRegister(Processor.regBadVAddr, badVAddr);

		if (Lib.test(dbgDisassemble) || Lib.test(dbgFullDisassemble)) {
			if (hasBadVAddr)
				System.out.println("exception: " + Processor.exceptionNames[cause] + "@0x" + Lib.toHexString(badVAddr, 8));
			else
				System.out.println("exception: " + Processor.exceptionNames[cause]);
		}
		
		/* don't need to finishLoad since we are not running kernel code on the MIPS processor */
		//finishLoad();
		
		Runnable exceptionHandler = UserKernel.processor.getExceptionHandler();
		Lib.assertTrue(exceptionHandler != null);

		/* no way to inform the autograder since we don't have the privilege */
		/*if (!nachos.machine.Machine.autoGrader().exceptionHandler(privilege))
			return; */

		exceptionHandler.run();
	}
		
	private boolean hasBadVAddr;
	private int cause, badVAddr;
	
	private static final char dbgDisassemble = 'm';
	private static final char dbgFullDisassemble = 'M';
}
