package nachos.userprog;

public class PIDOverflow extends Throwable {

	private static final long serialVersionUID = 1L;

	public PIDOverflow() {
		super("PID overflow");
	}
}
