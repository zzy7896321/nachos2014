package nachos.filesys;

import nachos.machine.Lib;

/**
 * struct DirectoryHeader;
 *
 */
public class DirectoryHeader implements POD {

	/** 0x0 ~ 0x1 */
	public short num_entries;

	/** magic number: 2:0 ~ 2:4, the first part of reserved 0: 2:4~2:7 */
	public byte byte1 = MAGIC_NUMBER;

	/** the second part of reserved 0: 0x3 */
	public byte byte2 = 0;

	/** 0x4 ~ 0x5 */
	public short my_inode_no;
	
	/** 0x6 ~ 0x7 */
	public short parent_inode_no;

	/** 0x8 ~ 0x9 */
	public short ext_filename_inode;

	/** 0xa ~ 0xf */
	public byte[] reserved2 = new byte[6];
	
	@Override
	public boolean toBytes(byte[] data, int offset) {
		if (offset + 0x10 > data.length) return false;
	
		Lib.bytesFromShort(data, offset + 0x0, num_entries);
		data[offset + 0x2] = byte1;
		data[offset + 0x3] = byte2;
		Lib.bytesFromShort(data, offset + 0x4, my_inode_no);
		Lib.bytesFromShort(data, offset + 0x6, parent_inode_no);
		Lib.bytesFromShort(data, offset + 0x8, ext_filename_inode);
		System.arraycopy(reserved2, 0, data, offset + 0xa, 6);
		
		return true;
	}

	@Override
	public boolean fromBytes(byte[] data, int offset) {
		if (offset + 0x10 > data.length) return false;
		
		num_entries = Lib.bytesToShort(data, offset + 0x0);
		byte1 = data[offset + 0x2];
		byte2 = data[offset + 0x3];
		my_inode_no = Lib.bytesToShort(data, offset + 0x4);
		parent_inode_no = Lib.bytesToShort(data, offset + 0x6);
		ext_filename_inode = Lib.bytesToShort(data, offset + 0x8);
		System.arraycopy(data, offset + 0xa, reserved2, 0, 6);
		
		return true;
	}

	public static final byte MAGIC_NUMBER = 0x5;
	
	public int num_entries() {
		return NachosFS.unsigned_short_to_int(num_entries);
	}
	
	public int ext_filename_inode() {
		return NachosFS.unsigned_short_to_int(ext_filename_inode);
	}
	
	public int my_inode_no() {
		return NachosFS.unsigned_short_to_int(my_inode_no);
	}
	
	public int parent_inode_no() {
		return NachosFS.unsigned_short_to_int(parent_inode_no);
	}

	@Override
	public int struct_size() {
		// TODO Auto-generated method stub
		return 0x10;
	}
}
