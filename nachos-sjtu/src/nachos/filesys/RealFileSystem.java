package nachos.filesys;

import nachos.machine.FileSystem;
import nachos.machine.Machine;
import nachos.machine.OpenFile;

/**
 * RealFileSystem provide necessary methods for filesystem syscall.
 * The FileSystem interface already define two basic methods, you should implement your own to adapt to your task.
 * <p>
 * We have to maintain this interface since the nachos virtual machine will
 * be configured to construct this class. However, the implementation is moved to NachosFS class.
 * 
 * @author starforever
 */
public class RealFileSystem implements FileSystem {

	/**
	 * Constructs a new RealFileSystem.
	 */
	public RealFileSystem() {
	}

	/** import from stub filesystem */
	public void importStub ()
	{
		/* the importing process is not modified */
		FileSystem stubFS = Machine.stubFileSystem();
		FileSystem realFS = FilesysKernel.realFileSystem;
		String[] file_list = Machine.stubFileList();
		for (int i = 0; i < file_list.length; ++i)
		{
			if (!file_list[i].endsWith(".coff"))
				continue;
			OpenFile src = stubFS.open(file_list[i], false);
			if (src == null)
			{
				continue;
			}
			OpenFile dst = realFS.open(file_list[i], true);
			int size = src.length();
			byte[] buffer = new byte[size];
			src.read(0, buffer, 0, size);
			dst.write(0, buffer, 0, size);
			src.close();
			dst.close();
		}
	}

	@Override
	public OpenFile open (String name, boolean create)
	{
		int cwd_inode_no = FilesysProcess.get_cwd_inode_no();
		
		OpenFile of = null;
		if (create) {
			of = FilesysKernel.nachosFS.open(cwd_inode_no, name, NachosFS.OPEN_CREATE | NachosFS.OPEN_TRUNC);
		}
		else {
			of = FilesysKernel.nachosFS.open(cwd_inode_no, name, 0);
		}
		
		return of;
	}

	@Override
	public boolean remove (String name)
	{
		int cwd_inode_no = FilesysProcess.get_cwd_inode_no();
		
		boolean result = FilesysKernel.nachosFS.unlink(cwd_inode_no, name);
		
		return result;
	}

	public int getFreeSize() {
		
		return FilesysKernel.nachosFS.numFreeSectors();
	}

	public int getSwapFileSectors() {
		
		return FilesysKernel.nachosFS.numSwapSectors();
	}
	
	
}
