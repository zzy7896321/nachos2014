package nachos.filesys;

import nachos.machine.Config;
import nachos.machine.Machine;
import nachos.threads.ThreadedKernel;
import nachos.userprog.UserKernel;
import nachos.vm.VMKernel;

/**
 * A kernel with file system support.
 * 
 * @author starforever
 */
public class FilesysKernel extends VMKernel {

	public FilesysKernel () {
		super();
	}

	public void initialize (String[] args) {
		/* don't use VMFileSystemWrapper */
		VMKernel.no_vmfs_init = true;
		
		/* we will provide the NachosFSSwapFile */
		VMKernel.no_swap_file_init = true;
		swapFile = new NachosFSSwapFile();

		/* do initialisation of super classes */
		super.initialize(args);
	
		/* threaded kernel should have constructed the RealFileSystem */
		if (!(ThreadedKernel.fileSystem instanceof RealFileSystem)) {
			/* not likely, just replace it */
			ThreadedKernel.fileSystem = new RealFileSystem();
		}
		/* the NachosFS will do the management itself */
		UserKernel.fileSystem = ThreadedKernel.fileSystem;
		/* we don't use vmfs wrapper */
		VMKernel.vmfs = null;	
		realFileSystem = (RealFileSystem) ThreadedKernel.fileSystem;
		
		boolean format = Config.getBoolean("FilesysKernel.format");
		/* It is weird that the NachosFS gets initialised after the 
		 * RealFileSystem interface is constructed. However, we have to
		 * make sure the ThreadedKernel is up, or the locks used
		 * by nachos.machine.SynchDisk will cause trouble. */
		nachosFS = new NachosFS();
		if (!nachosFS.initialise(format)) {
			/* what if we failed? */
			terminate();
		}
		
		if (nachosFS.suggestImportStub) {
			realFileSystem.importStub();
		}
	}

	public void selfTest () {
//		super.selfTest();
	}

	@Override
	protected void finish() {
		super.finish();
	
		/* at this point all files are closed */
		nachosFS.finish();
	}
	  
	public static final char dbgFS = 'f';

	public static RealFileSystem realFileSystem;
	
	public static NachosFS nachosFS;
}
