package nachos.filesys;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Predicate;

import nachos.machine.Disk;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.SynchDisk;
import nachos.threads.Condition2;
import nachos.threads.KThread;
import nachos.threads.Lock;
import nachos.threads.LotteryScheduler;
import nachos.threads.PriorityScheduler;
import nachos.threads.RoundRobinScheduler;
import nachos.threads.ThreadedKernel;
import nachos.userprog.UThread;
import nachos.vm.EmbeddedLinkedList;

/**
 * The Nachos File System (NachosFS). 
 * <p>
 * The file system is implemented with 3 layers:
 * <ul>
 * <li> NachosFS service: formatting routine, syscall handling, I/O to swap partition
 * <li> Disk cache: configurable disk cache; We have two separate cache for the swap partition and the main partition.
 * <li> IO scheduler: IO request scheduling; Currently a C-LOOK scheduler is implemented.
 * </ul>
 * <p>
 * The NachosFS is not designed to handle I/O failure. I/O failure may result in data loss, or file system corruption.
 * <p>
 * This implementation is not fully tested.
 * 
 * <p>
 * See doc/NachosFS.md for detailed documentation on the NachosFS.
 */
@SuppressWarnings("unused")
public class NachosFS {

/* interface */
	
	/**
	 * Constructs a new NachosFS object.
	 * Initialisation is done by initialise().
	 */
	public NachosFS() {
		NachosFSInitialised = false;
		Arrays.fill(zeros, (byte) 0);
	}

	/**
	 * Initialises the NachosFS. 
	 * <p>
	 * If format is set to <tt>true</tt>, the disk will be formated no matter
	 * whether a NachosFS has already existed on the Disk or
	 * not, which will lead to data loss.
	 * However, the disk will be formatted if no NachosFS exists on the disk.
	 * <p>
	 * If a NachosFS is in a broken state, a disk check will be conducted (not implemented).
	 * 
	 * @param format		whether to enforce formatting
	 * @return				whether the initialisation is successful
	 */
	public boolean initialise(boolean format) {
		
		initialiseDiskInfo();
		
		initialiseDiskScheduler();
	
		/* don't need to synchronise swap cache on exit */
		initialiseCache(2, new int[]{swapCacheSize, fsCacheSize}, new boolean[] {false, true} );
		
		initialiseNachosFS(format);
		
		return NachosFSInitialised;
	}

	/**
	 * Clears the NachosFS and releases resources.
	 * 
	 */
	public void finish() {
		
		finishNachosFS();
		
		finishCache();
		
		finishDiskScheduler();
	}
	
/* logical file system */

	/**
	 * Initialises the NachosFS service. This must be initialised after scheduler and cache.
	 * <p>
	 * On success, NachosFSInitialised is set to <tt>true</tt>. 
	 * 
	 * @param format	whether to enforce a formatting
	 */
	public void initialiseNachosFS(boolean format) {
		int io_result;
	
		int check_result = NACHOSFS_HEADER_FATAL;
		
		/* if formatting is enforced, there's no need to check the existing file system */
		if (!format) {
			/* read partition table and NachosFS header */
			if ((io_result = readPOD(FS_CACHE, nachosfs_header_sector_no, nachosfs_header)) != SUCCESS) {
				Lib.debug(dbgFS, "error occurs when reading NachosFS header: " + error_msg[io_result]);
				return ;
			}
		
			check_result = checkNachosFSHeader();
			if (check_result == NACHOSFS_HEADER_WARNING) {
				Lib.debug(dbgFS, "nachos fs header: there may be minor bugs that affects disk utilisation");
			}
			
		}
		
		if (check_result == NACHOSFS_HEADER_FATAL) {
			if (!format()) {
				return ;
			}
			suggestImportStub = true;
		}
		
		else if (check_result == NACHOSFS_HEADER_FSCK) {
			if (!fsck()) {
				return ;
			}
			suggestImportStub = true;
		}
		
		/* initialises file system configurations */
		
		/* inodes */
		num_inodes = unsigned_short_to_int(nachosfs_header.inode_count);
		num_inodes_per_block = sector_size / INODE_SIZE;
		if (num_inodes_per_block == 0) {
			/* fatal */
			return ;
		}
		num_free_inodes = unsigned_short_to_int(nachosfs_header.num_free_inodes);
	
		/* data blocks */
		num_data_blocks = unsigned_short_to_int(nachosfs_header.block_count);
		num_free_data_blocks = unsigned_short_to_int(nachosfs_header.num_free_blocks);
		
		/* block groups */
		block_group_offset = unsigned_short_to_int(nachosfs_header.block_group_offset);
		block_group_size = unsigned_short_to_int(nachosfs_header.block_group_size);
		num_block_groups = unsigned_short_to_int(nachosfs_header.num_block_groups);
		
		last_block_group_has_inode = (num_inodes / num_inodes_per_block) == num_block_groups;
		
		/* bitmap */
		bitmap_offset = unsigned_short_to_int(nachosfs_header.bitmap_offset);
		num_bitmap_blocks = unsigned_short_to_int(nachosfs_header.num_bitmap_blocks);
		num_bits_per_block = sector_size << 3;
		
		/* swap partition */
		swap_offset = unsigned_short_to_int(nachosfs_header.swap_offset);
		num_swap_sectors = unsigned_short_to_int(nachosfs_header.swap_length);
		
		/* nachosfs partition */
		nachosfs_offset = unsigned_short_to_int(nachosfs_header.nachosfs_offset);
		
		/* root inode no. */
		root_inode_no = unsigned_short_to_int(nachosfs_header.root_inode_number);
		
		/* sets the fs_state bit */
		nachosfs_header.fs_state = 1;
		if ((io_result = writePartial(FS_CACHE, nachosfs_header_sector_no, 0x16, new byte[]{nachosfs_header.fs_state}, 0, 1)) != SUCCESS) {
			Lib.debug(dbgFS,  "error occurs when writing NachosFS header: " + error_msg[io_result]);
			/* this is fatal */
			return ;
		}
		if ((io_result = sync(FS_CACHE, nachosfs_header_sector_no)) != SUCCESS) {
			Lib.debug(dbgFS, "error occurs when synchronising NachosFS header: " + error_msg[io_result]);
			/* fatal error */
			return ;
		}

		num_block_ptr_per_block = sector_size / 2;
		num_directory_entry_per_block = sector_size / DIRECTORY_ENTRY_SIZE;
		num_ext_filename_entry_per_block = sector_size / EXT_FILENAME_ENTRY_SIZE;
		
		/* nachosfs lock */
		NachosFSLock = new Lock();
		
		/* initialises bitmap cache */
		bitmapCache = new byte[num_bitmap_blocks][];
//		bitmapCacheInIO = new boolean[num_bitmap_blocks];
//		bitmapCacheIOWaiting = new Condition2[num_bitmap_blocks];
		for (int i = 0; i < num_bitmap_blocks; ++i) {
//			bitmapCacheIOWaiting[i] = new Condition2(NachosFSLock);
			
			/* bring in the bitmaps */
			byte[] buffer = new byte[sector_size];
			if ((io_result = readBlock(FS_CACHE, bitmap_block_addr(i), buffer, 0)) != SUCCESS) {
				Lib.debug(dbgFS, "error occurs when caching bitmaps: " + error_msg[io_result]);
				/* fatal error */
				return ;
			}
			
			bitmapCache[i] = buffer;
		}
		
		/* initialises global open file table */
		globalOpenFileTable = new TreeMap<Integer, GlobalOpenFile>();
		freeGlobalOpenFile = new EmbeddedLinkedList();
		numGlobalOpenFile = 0;
		
		/* from this point, the file system is successfully initialised */
		NachosFSInitialised = true;
	}

	/**
	 * Finishes the NachosFS service after flushing everything back to disk.
	 */
	public void finishNachosFS() {
		if (!NachosFSInitialised) return ;
		
		nachosfs_header.num_free_blocks = (short) num_free_data_blocks;
		nachosfs_header.num_free_inodes = (short) num_free_inodes;
		nachosfs_header.fs_state = 0;
		
		writePOD(FS_CACHE, nachosfs_header_sector_no, nachosfs_header);
	}
	

	/**
	 * Formats the disk. NOTE: this routine is not synchronised.
	 * <p>
	 * Formatting parameters can be configured by setting the static variables.
	 * 
	 * @return	<tt>true</tt> on success
	 */
	private boolean format() {
		NachosFSHeader header = nachosfs_header;
		int io_result;
		Lib.debug(dbgFS, "formating the disk");

		/* this header must be in sector 0 */
		int nachosfs_header_block = 0;
		
		/* disk info */
		int sector_size = this.sector_size;
		Lib.debug(dbgFS, "sector size = " + sector_size);
		if (sector_size != 512) {
			Lib.debug(dbgFS, "[error] sector size != 512, not supported!");
			return false;
		}
		
		int num_sectors = this.num_sectors;
		Lib.debug(dbgFS, "number of sectors = " + num_sectors);
		if (num_sectors < NACHOSFS_MINIMAL_DISK_SECTORS) {
			Lib.debug(dbgFS, "[error] cannot format the disk: too few sectors");
			return false;
		}
	
		/* swap partition
		 * the priority is to allocate enough space for the swap */
		int swap_length = 0;
		if (configNumSwapSectors + NACHOSFS_MINIMAL_DISK_SECTORS <= num_sectors) {
			swap_length = configNumSwapSectors;
		}
		else {
			swap_length = num_sectors - NACHOSFS_MINIMAL_DISK_SECTORS;
			if ((swap_length & 1) == 1) {
				/* make sure that the number of swap sectors is even, 
				 * since we are using 1KB pages while each sector is 512 bytes */
				--swap_length;
			}
			
			if (swap_length < 0) {
				/* not good */
				swap_length = 0;
			}
		}
		Lib.debug(dbgFS, "number of swap sectors = " + swap_length);
		if (swap_length < configNumSwapSectors) {
			Lib.debug(dbgFS, "[warning] swap parition is smaller than the configured size: " + configNumSwapSectors + " sectors");
		}
		int swap_offset = nachosfs_header_block + 1;
		if (swap_length > 0) {
			Lib.debug(dbgFS, "swap partition offset = " + swap_offset);
		}
		
		/* main partition */
		int nachosfs_length = num_sectors - swap_length - 1; /* 1 for the header */
		Lib.debug(dbgFS, "number of main partition sectors = " + nachosfs_length);
		if (nachosfs_length < configMinimalMainSectors) {
			Lib.debug(dbgFS, "[warning] main partition is smaller than the configured minimal size: " + configMinimalMainSectors + " sectors");
		}
		int nachosfs_offset = swap_offset + swap_length;
		Lib.debug(dbgFS, "main partition offset = " + nachosfs_offset);
		
		/* NachosFS info: inodes, bitmaps, data blocks */
		int inode_count;
		int block_count;
		int block_group_offset;
		int block_group_size;
		int num_block_groups;
		int bitmap_offset;
		int num_bitmap_blocks;
		{
			int alpha = configBlockGroupSize;		/* block group size (1 Inode group and size - 1 data blocks) */
			int beta = sector_size / INODE_SIZE;	/* #inodes in a block */
			int T = nachosfs_length;				/* #blocks in the main partition */
			int gamma = sector_size << 3;			/* #bits per block */
			
			int D;									/* #data blocks */
			int I;									/* #inode blocks */
			int B;									/* #bitmap blocks */
			int B_hat;								/* upper bound of B */
			int G;									/* # complete block groups */
			int R;									/* # blocks in the last block group */
			int G_prime; 							/* # block groups */
			int b;									/* # bits in the bitmap */
		
			/* #bits needed in the bitmap for a single block group is beta + alpha - 1.
			 * This bound is fairly tight due to gamma >> beta + alpha - 1. */
			B_hat = unsigned_div_ceil(unsigned_div_ceil(T, alpha) * (beta + alpha - 1), gamma);
			
			if (B_hat + 2 > T) {
				Lib.debug(dbgFS, "[error] cannot fit NachosFS on the disk");
				return false;
			}
		
			/* complete block groups */
			G = unsigned_div_floor(T - B_hat, alpha);
		
			R = T - B_hat - G * alpha;
			G_prime = G + ((R == 0) ? 0 : 1);
			
			if (G_prime >= alpha / 2) {
				/* allocate an additional inode block */
				D = G * (alpha - 1) + R - 1;
			}
			
			else {
				D = G * (alpha - 1) + R;
			}
			
			I = T - B_hat - D;
		
			/* #bits needed */
			b = D + beta * I;
			B = unsigned_div_ceil(b, gamma);
			
			if (B < B_hat) {
				Lib.debug(dbgFS, "[warning] wasted bitmap blocks = " + (B_hat - B));
			}
			
			inode_count = I * beta;
			block_count = D;
			block_group_offset = B_hat;
			block_group_size = alpha;
			num_block_groups = G_prime;
			bitmap_offset = 0;
			num_bitmap_blocks = B;
		}
		
		Lib.debug(dbgFS, "inode_count = " + inode_count);
		Lib.debug(dbgFS, "block_count = " + block_count);
		Lib.debug(dbgFS, "block_group_offset = " + block_group_offset);
		Lib.debug(dbgFS, "block_group_size = " + block_group_size);
		Lib.debug(dbgFS, "num_block_groups = " + num_block_groups);
		Lib.debug(dbgFS, "bitmap offset = " + bitmap_offset);
		Lib.debug(dbgFS, "num_bitmap_blocks = " + num_bitmap_blocks);			
		
		Lib.debug(dbgFS, String.format("available space = %d Bytes (%.1f MB)", block_count * sector_size,
				((double) (block_count * sector_size)) / (1024.0 * 1024)));
		
		if (block_count < configMinimalMainDataSectors) {
			Lib.debug(dbgFS, "[warning] too few data sectors");
		}
		
		if (inode_count < configMinimalMainFiles) {
			Lib.debug(dbgFS, "[warning] too few inodes");
		}
		
		int root_inode_number = 1;
		Lib.debug(dbgFS, "root inode = " + root_inode_number);
		
		/* nachos_header */
		header.magic_number = NACHOSFS_HEADER_MAGIC_NUMBER;
		header.sector_size = (short) sector_size;
		header.num_sectors = (short) num_sectors;
		header.swap_offset = (short) swap_offset;
		header.swap_length = (short) swap_length;
		header.nachosfs_offset = (short) nachosfs_offset;
		header.nachosfs_length = (short) nachosfs_length;
		header.fs_state = 0;
		Arrays.fill(header.reserved0, 0, 15, (byte) 0);
	
		header.inode_count = (short) inode_count;
		header.block_count = (short) block_count;
		header.num_free_inodes = (short) inode_count;
		header.num_free_blocks = (short) block_count;
		header.block_group_offset = (short) block_group_offset;
		header.block_group_size = (short) block_group_size;
		header.num_block_groups = (short) num_block_groups;
		header.bitmap_offset = (short) bitmap_offset;
		header.num_bitmap_blocks = (short) num_bitmap_blocks;
		header.root_inode_number = (short) root_inode_number;
		Arrays.fill(header.reserved1, 0, 460, (byte) 0);
		
		/* write header */
		if ((io_result = writePOD(FS_CACHE, nachosfs_header_sector_no, header)) != SUCCESS) {
			Lib.debug(dbgFS, "[error] io error when writing header: " + error_msg[io_result]);
			return false;
		}
	
		/* initialise bitmaps */
		byte[] buffer = new byte[sector_size];
		Arrays.fill(buffer, 0, sector_size, (byte) 0);

		/* write from end to start in order to improve disk cache performance,
		 * and the first one will be initialised when root directory is created */
		for (int i = num_bitmap_blocks - 1; i >= 0;  --i) {
			int sector_no = nachosfs_offset + bitmap_offset + i;
			if ((io_result = writeBlock(FS_CACHE, sector_no, buffer, 0)) != SUCCESS) {
				Lib.debug(dbgFS, "[error] io error when initialising bitmaps: " + error_msg[io_result]);
				return false;
			}
		}
		
		/* initialises inodes; from the end to start */
		int num_inode_blocks = inode_count / (sector_size / INODE_SIZE);
		for (int i = num_inode_blocks ; i >= 0; --i) {
			int sector_no = nachosfs_offset + block_group_offset + (i - 1) * block_group_size;
			if ((io_result = writeBlock(FS_CACHE, sector_no, buffer, 0)) != SUCCESS) {
				Lib.debug(dbgFS, "[error] io error when initialising inodes: " + error_msg[io_result]);
				return false;
			}
		}
		
		/* initialise root directory */
		{
			int num_inodes_per_block = sector_size / INODE_SIZE;
			int root_inode_group_no = (root_inode_number - 1) / num_inodes_per_block;
			int root_inode_group_index = (root_inode_number - 1) % num_inodes_per_block;
			int root_inode_index = root_inode_group_no * (num_inodes_per_block + block_group_size - 1) + root_inode_group_index;
			int root_inode_sector_no = nachosfs_offset + block_group_offset + root_inode_group_no * block_group_size;
			int root_inode_offset = root_inode_group_index * INODE_SIZE;
		
			/* allocate an inode for root */
			buffer[root_inode_index >>> 3] |= (1 << (root_inode_index & 0x7));
			--header.num_free_inodes;
		
			int root_data_block_no = 0;
			int root_data_group_no = 0;
			int root_data_group_index = 0;
			int root_data_index = num_inodes_per_block;	/* group 0, block 0 */
			int root_data_sector_no = nachosfs_offset + block_group_offset + 1 + 0;
		
			/* allocate a data block for root */
			buffer[root_data_index >>> 3] |= (1 << (root_data_index & 0x7));
			--header.num_free_blocks;
			
			int root_extfn_inode_no = root_inode_number + 1;
			int root_extfn_inode_group_no = (root_extfn_inode_no - 1) / num_inodes_per_block;
			int root_extfn_inode_group_index = (root_extfn_inode_no - 1) % num_inodes_per_block;
			int root_extfn_inode_index = root_extfn_inode_group_no * (num_inodes_per_block + block_group_size - 1) + root_extfn_inode_group_index;
			int root_extfn_inode_sector_no = nachosfs_offset + block_group_offset + root_extfn_inode_group_no * block_group_size;
			int root_extfn_inode_offset = root_extfn_inode_group_index * INODE_SIZE;
			
			/* allocate an inode for root ext. filename */
			buffer[root_extfn_inode_index >>> 3] |= (1 << (root_extfn_inode_index & 0x7));
			--header.num_free_inodes;
		
			/* writes bitmap */
			if ((io_result = writeBlock(FS_CACHE, nachosfs_offset + bitmap_offset, buffer, 0)) != SUCCESS) {
				Lib.debug(dbgFS, "[error] io error when initialising bitmap block 0: " + error_msg[io_result]);
				return false;
			}
			
			/* root inode */
			Inode root_inode = new Inode();
			
			root_inode.allocated(true);
			root_inode.filetype(Inode.FILETYPE_DIR);
			root_inode.inlined(false);
			root_inode.num_allocated_sectors = 1;
			root_inode.size = DIRECTORY_HEADER_SIZE;	/* an empty directory */
			root_inode.ref_count = 1;
			root_inode.direct_ptr[0] = (short) root_data_block_no;
			
			if ((io_result = writePODPartial(FS_CACHE, root_inode_sector_no, root_inode_offset, root_inode)) != SUCCESS) {
				Lib.debug(dbgFS, "[error] io error when creating root inode: " + error_msg[io_result]);
				return false;
			} 
			
			/* root ext. filename inode */
			Inode root_extfn_inode = new Inode();
			
			root_extfn_inode.allocated(true);
			root_extfn_inode.filetype(Inode.FILETYPE_NORMAL);
			root_extfn_inode.inlined(false);
			root_extfn_inode.num_allocated_sectors = 0;
			root_extfn_inode.size = 0;
			root_extfn_inode.ref_count = 1;
		
			if ((io_result = writePODPartial(FS_CACHE, root_extfn_inode_sector_no, root_extfn_inode_offset, root_extfn_inode)) != SUCCESS) {
				Lib.debug(dbgFS, "[error] io error when creating root ext. filename inode: " + error_msg[io_result]);
				return false;
			}
			
			/* root directory header */
			DirectoryHeader root_header = new DirectoryHeader();
			root_header.num_entries = 0;
			root_header.ext_filename_inode = (short) root_extfn_inode_no;
			root_header.my_inode_no = (short) root_inode_number;
			root_header.parent_inode_no = (short) root_inode_number;
		
			Arrays.fill(buffer, 0, sector_size, (byte) 0);
			if (!root_header.toBytes(buffer, 0)) {
				Lib.debug(dbgFS, "[error] error when creating root directory header");
				return false;
			}
			
			if ((io_result = writeBlock(FS_CACHE, root_data_sector_no, buffer, 0)) != SUCCESS) {
				Lib.debug(dbgFS, "[error] io error when creating root directory header: " + error_msg[io_result]);
				return false;
			}
		}
	
		/* ok, we are done. Have to synchronise the data with the disk. */
		syncAll(FS_CACHE);
		
		return true;
	}

	/**
	 * fsck routine. NOT implemented!!
	 * 
	 * @return	<tt>true<tt> on success
	 */
	private boolean fsck() {
		/* TODO implement fsck */
		Lib.debug(dbgFS, "[warning] fsck is not implemented, the disk will be formated");
		return format();
	}

	/**
	 * Checks the partition table and NachosFS header.
	 * 
	 * @return	<ul>
	 * <li> NACHOSFS_HEADER_NORMAL if the header is Okay 
	 * <li> NACHOSFS_HEADER_FSCK if a abnormal shutdown is detected
	 * <li> NACHOSFS_HEADER_FATAL if the file system is corrupt 
	 * </ul>
	 */
	@SuppressWarnings("unchecked")
	private int checkNachosFSHeader() {
		NachosFSHeader header = nachosfs_header;
		int result = NACHOSFS_HEADER_NORMAL;
		int cmp;
	
		/* partition table */
		
		if (header.magic_number != NACHOSFS_HEADER_MAGIC_NUMBER) {
			return NACHOSFS_HEADER_FATAL;
		}
		
		if (header.sector_size != sector_size) {
			/* sector size changed? may be this file system was created on a different disk */
			return NACHOSFS_HEADER_FATAL;
		}
	
		cmp = unsigned_compare(header.num_sectors, num_sectors);
		if (cmp > 0) {
			/* header.num_sectors > num_sectors, invalid configuration */
			return NACHOSFS_HEADER_FATAL;
		}
		
		if (cmp < 0) {
			/* unused space, but why ? */
			result = NACHOSFS_HEADER_WARNING;
		}
	
		if (header.swap_length != 0) {
			/* have swap partition */
			int swap_length = unsigned_short_to_int(header.swap_length);
			int swap_offset = unsigned_short_to_int(header.swap_offset);
		
			cmp = unsigned_compare(swap_offset, header.num_sectors);
			if (cmp >= 0) {
				/* swap partition is out of range */
				return NACHOSFS_HEADER_FATAL;
			}
			
			cmp = unsigned_compare(swap_offset + swap_length, header.num_sectors);
			if (cmp > 0) {
				/* swap partition is out of range */
				return NACHOSFS_HEADER_FATAL;
			}
		}
	
		if (header.nachosfs_length == 0) {
			/* no main partition? */
			return NACHOSFS_HEADER_FATAL;
		}
		
		{
			int nachosfs_length = unsigned_short_to_int(header.nachosfs_length);
			int nachosfs_offset = unsigned_short_to_int(header.nachosfs_offset);
			
			cmp = unsigned_compare(nachosfs_offset, header.num_sectors);
			
			if (cmp >= 0) {
				/* main partition is out of range */
				return NACHOSFS_HEADER_FATAL;
			}
			
			cmp = unsigned_compare(nachosfs_length + nachosfs_offset, header.num_sectors);
			if (cmp > 0) {
				/* main partition is out of range */
				return NACHOSFS_HEADER_FATAL;
			}
		}
		
		if (header.fs_state != 0) {
			/* the machine was not properly shutdown in the previous run,
			 * need to run fsck */
			result = NACHOSFS_HEADER_FSCK;
		}
		
		/* nachos fs header */
		if (header.inode_count == 0) {
			/* no inode? */
			return NACHOSFS_HEADER_FATAL;
		}
		
		if (header.block_count == 0) {
			/* no data block? */
			return NACHOSFS_HEADER_FATAL;
		}
		
		cmp = unsigned_compare(header.num_free_inodes, header.inode_count);
		if (cmp > 0) {
			/* more free inodes than total ? */
			return NACHOSFS_HEADER_FATAL;
		}
		
		cmp = unsigned_compare(header.num_free_blocks, header.block_count);
		if (cmp > 0) {
			/* more free data blocks than total? */
			return NACHOSFS_HEADER_FATAL;
		}
		
		cmp = unsigned_compare(header.block_group_offset, header.nachosfs_length);
		if (cmp >= 0) {
			/* block groups out of range */
			return NACHOSFS_HEADER_FATAL;
		}
	
		cmp = unsigned_compare(header.block_group_size, 1);
		if (cmp <= 0) {
			/* no data blocks? */
			return NACHOSFS_HEADER_FATAL;
		}
		
		cmp = unsigned_compare(header.bitmap_offset, header.nachosfs_length);
		if (cmp >= 0) {
			/* bitmap_offset is out of range */
			return NACHOSFS_HEADER_FATAL;
		} 

		/* total number of inode and data blocks */
		int inode_data_length = unsigned_short_to_int(header.inode_count) / (sector_size / INODE_SIZE)
								+ unsigned_short_to_int(header.block_count);
		cmp = unsigned_compare(
				unsigned_div_ceil(inode_data_length, header.block_group_size), 
				header.num_block_groups);
		if (cmp != 0) {
			/* incorrect # block groups */
			return NACHOSFS_HEADER_FATAL;
		}
	
		if (header.root_inode_number == INODE_NULL) {
			/* root is null? */
			return NACHOSFS_HEADER_FATAL;
		}
		
		/* overlapping check */
		if (check_overlap(new Pair[] {
			/* empty interval that guards the lower boundary of nachos fs*/
			get_interval((short) 0, 0),
			
			/* bitmap */
			get_interval(header.bitmap_offset, header.num_bitmap_blocks),
			
			/* block groups */
			get_interval(header.block_group_offset, inode_data_length),
			
			/* empty interval that guards the upper boundary of nachos fs */
			get_interval(header.nachosfs_length, unsigned_short_to_int(header.nachosfs_length)),	
		})) {
			
			/* having overlapping intervals */
			return NACHOSFS_HEADER_FATAL;
		}
		
		return result;
	}

	/**
	 * Opens a file for read/write. Open flags include:
	 * <ul>
	 * <li>	OPEN_CREATE: create the file if it does not exist
	 * <li> OPEN_TRUNC: truncate the file
	 * <li> OPEN_IF_NOT_EXIT: only opens the file if it does not exist, should be used together with OPEN_CREATE
	 * </ul>
	 * <p>
	 * By default, symlinks will be followed. Cannot open directory using this routine.
	 * 
	 * @param cwd_inode_no		the inode no. of the current working directory
	 * @param path				the path of the file
	 * @param flag				open flags
	 * @return					the OpneFile on success, <tt>null</tt> o.w.
	 */
	public OpenFile open(int cwd_inode_no, String path, int flag) {
		return open(cwd_inode_no, path, flag, Inode.FILETYPE_NORMAL);
	}

	/**
	 * Opens a file for read/write with file type specified if the file is to be created.
	 * 
	 * @see public OpenFile open(int, String, int)
	 * 
	 * @param cwd_inode_no		the inode no. of the current working directory
	 * @param path				the path of the file
	 * @param flag				open flags
	 * @param file_type			the file type of the file if it is to be created
	 * @return					the OpenFile on success, <tt>null</tt> o.w.
	 */
	private OpenFile open(int cwd_inode_no, String path, int flag, byte file_type) {
		if (path.endsWith("/")) {
			/* not a file */
			return null;
		}
	
		/* don't drop the lock on return, since we may want to operate on the directory atomically */
		Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry>
			query_result = walk_through_path(cwd_inode_no, path, true, false);
		
		if (query_result == null) return null;
		
		GlobalOpenFile dir = query_result.first;
		String name = query_result.second;
		
		if (name == null) {
			/* not likely? */
//			Lib.debug(dbgFS, "why here? in open: name == null");
			/* could be a symlink that links to another directory */
			/* the lock has been dropped */
			release_global_open_file(dir);
			return null;
		}
	
		/* dir.lock is not dropped */
		DirectoryEntry dir_entry = query_result.fourth;
		GlobalOpenFile of = null;
		
		if (dir_entry == null) {
			if ((flag & OPEN_CREATE) != 0) {
				of = create_new_file(dir.inode_no + 1, file_type, false);
				if (of == null) {
					dir.lock.release();
					release_global_open_file(dir);
					return null;
				}
				
				/* of.lock is not dropped */
				link_file(of, dir, name);
				/* TODO handle error */
				
				dir.lock.release();
				release_global_open_file(dir);
				
				of.lock.release();
			}
			else {
				dir.lock.release();
				release_global_open_file(dir);
				return null;
			}
		}
		
		else {
			if ((flag & OPEN_IF_NOT_EXIST) != 0) {
				dir.lock.release();
				release_global_open_file(dir);
				return null;
			}
			
			of = get_global_open_file(dir_entry.inode_no());
			if (of == null) {
				dir.lock.release();
				release_global_open_file(dir);	
				return null;
			}
			
			of.lock.acquire();
			
			if (of.inode.is_directory()) {
				/* a directory */
				of.lock.release();
				release_global_open_file(of);
				dir.lock.release();
				release_global_open_file(dir);
				return null;
			}
			
			if ((flag & OPEN_TRUNC) != 0) {
				truncate_file(of);
			}
			
			of.lock.release();
			
			dir.lock.release();
			release_global_open_file(dir);
		}
		
		OpenFile ret = new OpenFile(path, of, 0);
		
		return ret;
	}
	
	public static final int OPEN_CREATE = 0x1;
	public static final int OPEN_TRUNC = 0x2;
	public static final int OPEN_IF_NOT_EXIST = 0x4;

	/**
	 * Unlinks the file from the directory. The actual file is released only when the reference count
	 * drops to 0 and all open to the file is closed. However, new open to the file can be established
	 * using other link the same file.
	 * 
	 * @param cwd_inode_no		the inode no. of the current working directory
	 * @param path				the path of the file
	 * @return					<tt>true</tt> on success
	 */
	public boolean unlink(int cwd_inode_no, String path) {
		
		if (path.endsWith("/")) {
			/* not a file */
			return false;
		}
		
		/* don't drop the lock on return, since we may want to operate on the directory atomically */
		Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry>
			query_result = walk_through_path(cwd_inode_no, path, false, false);
		
		if (query_result == null) return false;
		
		GlobalOpenFile dir = query_result.first;
		String name = query_result.second;
		
		if (name == null) {
			/* not likely? */
			Lib.debug(dbgFS, "why here? in open: name == null");
			/* the lock has been dropped */
			release_global_open_file(dir);
			
			return false;
		}
	
		/* dir.lock is not dropped */
		DirectoryEntry dir_entry = query_result.fourth;
		GlobalOpenFile of = null;
		
		if (dir_entry == null) {
			dir.lock.release();
			release_global_open_file(dir);
			
			return false;
		}
		
		else {
			of = get_global_open_file(dir_entry.inode_no());
			Lib.assertTrue(of != null);
			of.lock.acquire();
		
			if (of.inode.is_directory()) {
				/* cannot unlink a directory */
				of.lock.release();
				release_global_open_file(of);
				dir.lock.release();
				release_global_open_file(dir);
				return false;
			}
			
			unlink_file(of, dir, query_result.third);
			
			of.lock.release();
			release_global_open_file(of);
			dir.lock.release();
			release_global_open_file(dir);
			
			return true;
		}
	}

	/**
	 * Creates a hard link.
	 * 
	 * @param cwd_inode_no		the inode no. of the current working directory
	 * @param old_path			
	 * @param new_path		
	 * @return					
	 */
	public boolean link(int cwd_inode_no, String old_path, String new_path) {
		if (old_path.endsWith("/")) {
			/* cannot link a directory */
			return false;
		}
		
		if (new_path.endsWith("/")) {
			/* not a file */
			return false;
		}
		
		Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry>
			query_old_path = walk_through_path(cwd_inode_no, old_path, false, false);
		if (query_old_path == null) {
			return false;
		}
		
		GlobalOpenFile dir = query_old_path.first;
		String name = query_old_path.second;
		
		if (name == null) {
			/* not likely? */
			Lib.debug(dbgFS, "why here? in open: name == null");
			/* the lock has been dropped */
			release_global_open_file(dir);
			
			return false;
		}
	
		/* dir.lock is not dropped */
		DirectoryEntry dir_entry = query_old_path.fourth;
		if (dir_entry == null) {
			/* file does not exist */
			dir.lock.release();
			release_global_open_file(dir);
			return false;
		}
		
		GlobalOpenFile of = get_global_open_file(dir_entry.inode_no());
		dir.lock.release();
		release_global_open_file(dir);
		
		if (of == null) {
			/* error */
			return false;
		}
		
		Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry>
			query_new_path = walk_through_path(cwd_inode_no, new_path, false, false);
		if (query_new_path == null) {
			release_global_open_file(of);
			return false;
		}
		
		GlobalOpenFile new_dir = query_new_path.first;
		String new_name = query_new_path.second;
		
		if (new_name == null) {
			/* why? */
			Lib.debug(dbgFS, "why here? in open: name == null");
			release_global_open_file(of);
			/* the lock has been dropped */
			release_global_open_file(new_dir);
			return false;
		}

		/* new_dir.lock is not dropped */
		DirectoryEntry new_dir_entry = query_new_path.fourth;
		
		if (new_dir_entry != null) {
			/* file already exists */
			new_dir.lock.release();
			release_global_open_file(new_dir);
			release_global_open_file(of);
			return false;
		}
		
		of.lock.acquire();
	
		link_file(of, new_dir, new_name);
		
		of.lock.release();
		
		new_dir.lock.release();
		release_global_open_file(new_dir);
		
		return true;
	}

	/**
	 * Creates symbolic link.
	 * 
	 * @param cwd_inode_no
	 * @param old_path
	 * @param new_path
	 * @return
	 */
	public boolean symlink(int cwd_inode_no, String old_path, String new_path) {
		String path;
		if (!old_path.startsWith("/")) {
			String cwd = getcwd(cwd_inode_no);
			if (cwd == null) {
				return false;
			}
			path = cwd + old_path;
		}
		else {
			path = old_path;
		}
		
		OpenFile of = open(cwd_inode_no, new_path, OPEN_IF_NOT_EXIST | OPEN_CREATE, Inode.FILETYPE_SYMLINK);
		if (of == null) {
			return false;
		}
		
		byte[] buffer = path.getBytes();
		int num_written = of.write(0, buffer, 0, buffer.length);
		of.close();
		
		return num_written == buffer.length;
	}

	/**
	 * Gets the path of the directory.
	 * 
	 * @param cwd_inode_no
	 * @return
	 */
	public String getcwd(int cwd_inode_no) {
		LinkedList<String> paths = resolve_path(cwd_inode_no);
		if (paths == null) {
			return null;
		}
		
		return getCanonicalPath(paths);
	}

	/**
	 * Creates a new directory.
	 * 
	 * @param cwd_inode_no
	 * @param path
	 * @return
	 */
	public boolean mkdir(int cwd_inode_no, String path) {
		path = removeTrailingSlash(path);
		if (path.equals("/")) {
			return false;
		}
		
		/* don't drop the lock on return, since we may want to operate on the directory atomically */
		Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry>
			query_result = walk_through_path(cwd_inode_no, path, false, false);
		
		if (query_result == null) return false;
		
		GlobalOpenFile dir = query_result.first;
		String name = query_result.second;
		
		if (name == null) {
			/* not likely? */
			Lib.debug(dbgFS, "why here? in open: name == null");
			/* the lock has been dropped */
			release_global_open_file(dir);
			return false;
		}
		
		/* dir.lock is not dropped */
		DirectoryEntry dir_entry = query_result.fourth;
		if (dir_entry != null) {
			/* file already exists */
			dir.lock.release();
			release_global_open_file(dir);
			return false;
		}
		
		GlobalOpenFile new_dir = create_dir(dir.inode_no, dir.inode_no);
		if (new_dir == null) {
			dir.lock.release();
			release_global_open_file(dir);
			return false;
		}
		
		link_file(new_dir, dir, name);
		
		new_dir.lock.release();
		release_global_open_file(new_dir);
		dir.lock.release();
		release_global_open_file(dir);
		
		return true;
	}

	/**
	 * Removes a directory. A directory can only be removed when it's empty.
	 * 
	 * @param cwd_inode_no
	 * @param path
	 * @return
	 */
	public boolean rmdir(int cwd_inode_no, String path) {
		path = removeTrailingSlash(path);
		if (path.equals("/")) {
			return false;
		}
		
		/* don't drop the lock on return, since we may want to operate on the directory atomically */
		Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry>
			query_result = walk_through_path(cwd_inode_no, path, false, false);
		
		if (query_result == null) return false;
		
		GlobalOpenFile dir = query_result.first;
		String name = query_result.second;
		
		if (name == null) {
			/* not likely? */
			Lib.debug(dbgFS, "why here? in open: name == null");
			/* the lock has been dropped */
			release_global_open_file(dir);
			return false;
		}
		
		/* dir.lock is not dropped */
		DirectoryEntry dir_entry = query_result.fourth;
		if (dir_entry == null) {
			dir.lock.release();
			release_global_open_file(dir);
			return false;
		}
		
		GlobalOpenFile subdir = get_global_open_file(dir_entry.inode_no());
		if (subdir == null) {
			/* why? */
			dir.lock.release();
			release_global_open_file(dir);
			return false;
		}
		
		subdir.lock.acquire();
		
		if (!subdir.inode.is_directory()) {
			subdir.lock.release();
			release_global_open_file(subdir);
			dir.lock.release();
			release_global_open_file(dir);
			return false;
		}
		
		DirectoryHeader header = read_dir_header(subdir);
		if (header == null) {
			subdir.lock.release();
			release_global_open_file(subdir);
			dir.lock.release();
			release_global_open_file(dir);
			return false;
		}
		
		if (header.num_entries() > 0) {
			/* non-empty directory */
			subdir.lock.release();
			release_global_open_file(subdir);
			dir.lock.release();
			release_global_open_file(dir);
			return false;
		}
		
		unlink_file(subdir, dir, query_result.third);
		
		subdir.lock.release();
		release_global_open_file(subdir);
		dir.lock.release();
		release_global_open_file(dir);
		
		return true;
	}

	/**
	 * Lists the names of the files in the directory.
	 * 
	 * @param cwd_inode_no
	 * @param path
	 * @return
	 */
	public String[] readdir(int cwd_inode_no, String path) {
		path = path + "/";	/* add / to force open the final directory */
		
		/* don't drop the lock on return, since we may want to operate on the directory atomically */
		Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry>
			query_result = walk_through_path(cwd_inode_no, path, true, false);
		
		if (query_result == null) return null;
		
		GlobalOpenFile dir = query_result.first;
		dir.lock.acquire();
		
		DirectoryHeader header = read_dir_header(dir);
		int num_entries = header.num_entries();
		
		String[] ret = new String[num_entries];
		for (int i = 0; i < num_entries; ++i) {
			DirectoryEntry dir_entry = read_dir_entry(dir, i);
			ret[i] = get_file_name(dir, dir_entry);
			if (ret[i] == null) {
				/* ?? why? */
				dir.lock.release();
				release_global_open_file(dir);
				return null;
			}
		}
		
		dir.lock.release();
		release_global_open_file(dir);
		
		return ret;	
	}

	/**
	 * Gets the stat info of the given file.
	 * 
	 * @param cwd_inode_no
	 * @param path
	 * @return
	 */
	public FileStat stat(int cwd_inode_no, String path) {
		path = removeTrailingSlash(path);
		
		int inode_no;
		String name;
		if (path.equals("/")) {
			inode_no = root_inode_no;
			name = "/";
		}
		
		else {
			/* don't drop the lock on return, since we may want to operate on the directory atomically */
			Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry>
				query_result = walk_through_path(cwd_inode_no, path, false, false);
			
			if (query_result == null) return null;
			
			GlobalOpenFile dir = query_result.first;
			name = query_result.second;
			
			if (name == null) {
				/* not likely? */
				Lib.debug(dbgFS, "why here? in open: name == null");
				/* the lock has been dropped */
				release_global_open_file(dir);
				return null;
			}
			
			/* dir.lock is not released */
			DirectoryEntry dir_entry = query_result.fourth;
			
			if (dir_entry == null) {
				/* file does not exists */
				dir.lock.release();
				release_global_open_file(dir);
			}
			
			inode_no = dir_entry.inode_no();
			
			dir.lock.release();
			release_global_open_file(dir);
		}
		
		GlobalOpenFile of = get_global_open_file(inode_no);
		if (of == null) {
			return null;
		}
		
		of.lock.acquire();
		
		Inode inode = of.inode;
		
		FileStat stat = new FileStat();
		stat.name = name;
		stat.size = inode.size();
		stat.sectors = inode.num_allocated_sectors();
		if (inode.is_directory()) {
			stat.type = FileStat.DIR_FILE_TYPE;
		}
		else if (inode.is_symlink()) {
			stat.type = FileStat.LinkFileType;
		}
		else {
			stat.type = FileStat.NORMAL_FILE_TYPE;
		}
		stat.inode = inode_no;
		stat.links = inode.ref_count();
		
		of.lock.release();
		release_global_open_file(of);
		
		return stat;
	}

	/**
	 * Gets the inode no. of the given directory.
	 * 
	 * @param cwd_inode_no
	 * @param path
	 * @return
	 */
	public int chdir(int cwd_inode_no, String path) {
		path = path + "/";
		
		/* don't drop the lock on return, since we may want to operate on the directory atomically */
		Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry>
			query_result = walk_through_path(cwd_inode_no, path, true, false);
		
		if (query_result == null) return INODE_NULL;
		
		GlobalOpenFile dir = query_result.first;
		dir.lock.acquire();
		
		int ret = dir.inode_no;
		
		dir.lock.release();
		release_global_open_file(dir);
		
		return ret;	
	}

	/**
	 * Number of free sectors.
	 * 
	 * @return
	 */
	public int numFreeSectors() {
		NachosFSLock.acquire();
		int ret = num_free_data_blocks;
		NachosFSLock.release();
		
		return ret;
	}

	/**
	 * Number of sectors in the swap partition.
	 * 
	 * @return
	 */
	public int numSwapSectors() {
		return num_swap_sectors;
	}

	/**
	 * Swap partition offset.
	 * 
	 * @return
	 */
	public int swapOffset() {
		return swap_offset;
	}

	/**
	 * Root directory inode no.
	 * 
	 * @return
	 */
	public int rootInodeNo() {
		return root_inode_no;
	}

	/**
	 * Gets a global open file to a file. Inode may be loaded before the GlobalOpenFile is 
	 * returned to the caller. If the total number of global open files exceeds the limit, 
	 * open will fail.
	 * <p>
	 * This routine involves NachosFSLock and file lock.
	 * 
	 * @param inode_no		the inode no. of the file to be opened
	 * @return			 	the GlobalOpenFile on success, <tt>null</tt> o.w.
	 */
	private GlobalOpenFile get_global_open_file(int inode_no) {
		GlobalOpenFile of = null;
		
		NachosFSLock.acquire();
		
		of = __find_global_open_file(inode_no, true);
		of.open_count += 1;
		
		NachosFSLock.release();
	
		/* checks whether we need to load the inode */
		of.lock.acquire();
	
		int io_result = SUCCESS;
		if (of.inode == null) {
			of.inode = new Inode();
			Pair<Integer, Integer> addr = inode_addr(inode_no);
			io_result = readPODPartial(FS_CACHE, addr.first, addr.second, of.inode);
		}
		else if (of.inode.allocated() && of.inode.ref_count() == 0) {
			/* this file is to be removed */
			io_result = ERROR_IO_ERROR;
		}
		
		of.lock.release();
		
		if (io_result != SUCCESS) {
			release_global_open_file(of);
			of = null;
		}
		
		return of;
	}

	/**
	 * Releases the global open file and releases the disk space of the file if 
	 * both reference count and open count drop to 0.
	 * <p>
	 * This routine involves NachosFSLock and the file lock of the GlobalOpenFile.
	 * 
	 * @param of
	 */
	private void release_global_open_file(GlobalOpenFile of) {
		NachosFSLock.acquire();
		
		--of.open_count;
		if (of.open_count == 0) {
			__remove_file(of);
			of.insert_after(freeGlobalOpenFile);
		}
		
		NachosFSLock.release();
	}
	
	private GlobalOpenFile __find_global_open_file(int inode_no, boolean new_open_file_insert_into_table) {
		GlobalOpenFile of = null;
		
		of = globalOpenFileTable.get(inode_no);
		if (of == null) {
			if (!new_open_file_insert_into_table) {
				of = new GlobalOpenFile();
				of.inode_no = inode_no;
				of.inode = null;
			}
			else
			if (numGlobalOpenFile < maxOpenFileCount) {
				/* create a new global open file */
				++numGlobalOpenFile;
				of = new GlobalOpenFile();
				of.inode_no = inode_no;
				of.inode = null;
				
				globalOpenFileTable.put(inode_no, of);
			}
			
			else {
				if (freeGlobalOpenFile.list_prev == freeGlobalOpenFile) {
					/* maxOpenFileCount reached */
					NachosFSLock.release();
					return null;
				}
				
				of = (GlobalOpenFile) freeGlobalOpenFile.list_prev;
				of.remove_from_list();
				
				globalOpenFileTable.remove(of.inode_no);
				
				of.inode_no = inode_no;
				of.inode = null;
				globalOpenFileTable.put(inode_no, of);
			}
		}
		
		return of;
	}
	
	private void __remove_file(GlobalOpenFile of) {
		Inode inode = of.inode;
		Lib.assertTrue(of.open_count == 0, "open_count != 0");
		
		if (!inode.allocated()) {
			of.inode = null;
			return ;
		}
		if (inode.ref_count > 0) {
			return ;
		}
		if (inode.is_directory()) {
			__remove_directory(of);
		}
		else {
			__remove_normal_file(of);
		}
		
		of.inode = null;
	}
	
	private void __remove_directory(GlobalOpenFile of) {
		DirectoryHeader header = read_dir_header(of);
		
		int ext_filename_inode_no = header.ext_filename_inode();
		if (ext_filename_inode_no != INODE_NULL) {
			GlobalOpenFile extfn = __find_global_open_file(ext_filename_inode_no, false);
			Lib.assertTrue(extfn.open_count == 0);
			if (extfn.inode == null) {
				extfn.inode = new Inode();
				Pair<Integer, Integer> addr = inode_addr(ext_filename_inode_no);
				readPODPartial(FS_CACHE, addr.first, addr.second, extfn.inode);
			}

			extfn.inode.ref_count -= 1;
			Lib.assertTrue(extfn.inode.ref_count == 0);
			__remove_normal_file(extfn);
			extfn.inode = null;
		}
		
		__remove_normal_file(of);
	}
	
	private void __remove_normal_file(GlobalOpenFile of) {
		/* don't need to acquire the lock, since open_count == 0 */
		
		int num_sectors = of.inode.num_allocated_sectors();
		if (num_sectors <= NUM_DIRECT_PTR) {
			for (int i = 0; i < num_sectors; ++i) {
				__release_data_block(of.inode.direct_ptr(i));
			}
		}
		
		else if (num_sectors <= NUM_DIRECT_PTR + num_block_ptr_per_block) {
			for (int i = 0; i < NUM_DIRECT_PTR; ++i) {
				__release_data_block(of.inode.direct_ptr(i));
			}
			
			
			byte[] buffer = new byte[sector_size];
			int single_ind_ptr = of.inode.single_ind_ptr();
			int single_ind_sector_no = data_block_addr(single_ind_ptr);
			readBlock(FS_CACHE, single_ind_sector_no, buffer, 0);
			
			int num_single_ind_ptrs = num_sectors - NUM_DIRECT_PTR;
			for (int i = 0; i < num_single_ind_ptrs; ++i) {
				int ptr = unsigned_short_to_int(Lib.bytesToShort(buffer, i << 1));
				__release_data_block(ptr);
			}
			__release_data_block(single_ind_ptr);
		}
		
		else {
			for (int i = 0; i < NUM_DIRECT_PTR; ++i) {
				__release_data_block(of.inode.direct_ptr(i));
			}
			
			byte[] buffer = new byte[sector_size];
			int single_ind_ptr = of.inode.single_ind_ptr();
			int single_ind_sector_no = data_block_addr(single_ind_ptr);
			readBlock(FS_CACHE, single_ind_sector_no, buffer, 0);
			
			for (int i = 0; i < num_block_ptr_per_block; ++i) {
				int ptr = unsigned_short_to_int(Lib.bytesToShort(buffer, i << 1));
				__release_data_block(ptr);
			}
			__release_data_block(single_ind_ptr);
			
			byte[] buffer2 = new byte[sector_size];
			int rem = num_sectors - NUM_DIRECT_PTR - num_block_ptr_per_block;
			int first_level_ind_ptr = of.inode.double_ind_ptr();
			int first_level_sector_no = data_block_addr(first_level_ind_ptr);
			readBlock(FS_CACHE, first_level_sector_no, buffer, 0);
			
			for (int first_ind = 0; first_ind < num_block_ptr_per_block && rem > 0; ++first_ind) {
				int second_level_ind_ptr = unsigned_short_to_int(Lib.bytesToShort(buffer, first_ind << 1));
				int second_level_sector_no = data_block_addr(second_level_ind_ptr);
				readBlock(FS_CACHE, second_level_sector_no, buffer2, 0);
				
				for (int second_ind = 0; second_ind < num_block_ptr_per_block && rem > 0; ++second_ind) {
					int ptr = unsigned_short_to_int(Lib.bytesToShort(buffer, second_ind << 1));
					__release_data_block(ptr);
					--rem;
				}
				__release_data_block(second_level_ind_ptr);
			}
			__release_data_block(first_level_ind_ptr);
		}
	
		of.inode.allocated(false);
		of.inode.num_allocated_sectors = 0;
		write_inode(of);
		
		__release_inode(of.inode_no);
	}

	/**
	 * Reads a symbolic link. This function is not synchronised.
	 * 
	 * @param file	the symlink file
	 * @return		the path stored in the symlink
	 */
	private String read_symlink(GlobalOpenFile file) {
		int length = file.inode.size();
		byte[] buffer = new byte[length];
		int num_read = read_file(file, 0, length, buffer, 0);
		if (num_read != length) {
			return null;
		}
		String ret = bytesToString(buffer, 0, length);
		return ret;
	}
	
	/**
	 * Writes to a symbolic link. This function is not synchronised.
	 * 
	 * @param file	the symlink file
	 * @param str	the path to write to
	 * @return		<tt>true</tt> on success
	 */
	private boolean write_symlink(GlobalOpenFile file, String str) {
		byte[] buffer = str.getBytes();
		int length = buffer.length;
		int num_written = write_file(file, 0, length, buffer, 0);
		if (num_written != length) {
			return false;
		}
		return true;
	}

	/**
	 * Reads from a file. This function is not synchronised.
	 * 
	 * @param file			the file
	 * @param offset		the offset from which to read
	 * @param length		number of bytes to read
	 * @param buffer		the storage buffer
	 * @param buf_offset	the offset from which to store the bytes read
	 * @return				the amount of bytes read, or -1 on fatal error, in which case errno is set
	 */
	private int read_file(GlobalOpenFile file, int offset, int length, byte[] buffer, int buf_offset) {
		if (buffer == null || buf_offset < 0 || length < 0 || buf_offset + length > buffer.length) {
			set_errno(ERROR_INVALID_ARGUMENT);
			return -1;
		}
		
		if (file == null) {
			set_errno(ERROR_INVALID_ARGUMENT);
			return -1;
		}
		
		if (length == 0) return 0;
		
		int size = file.inode.size();
		if (offset < 0 || offset >= size) {
			/* offset out of range */
			set_errno(ERROR_INVALID_ARGUMENT);
			return -1;
		}
		
		/* in case the last characters lying out of range */
		length = Math.min(length, size - offset);

		if (file.inode.inlined()) {
			if (size > FILE_INLINE_THRES) {
				set_errno(ERROR_CORRUPT_FS);
				return -1;
			}
			
			/* the file is inlined */
			byte[] inline_buffer = new byte[FILE_INLINE_THRES];
			file.inode.get_inlined_data(inline_buffer, 0);
			
			System.arraycopy(inline_buffer, offset, buffer, buf_offset, length);
			return length;
		}
		
		/* the file is not inlined; bring in data one block a time */
		int amount = 0;
		while (amount < length) {
			
			int file_block_no = unsigned_div_floor(offset + amount, sector_size);
			int num_to_read = Math.min(length - amount, (file_block_no + 1) * sector_size - (offset + amount));
		
			int block_no = get_block_ptr(file, file_block_no);
			if (get_errno() != SUCCESS) {
				return -1;
			}
			int sector_no = data_block_addr(block_no);
			
			int io_result;
			if (num_to_read != sector_size) {
				io_result = readPartial(FS_CACHE, sector_no, offset + amount - file_block_no * sector_size, buffer, buf_offset + amount, num_to_read);
			}
			
			else {
				io_result = readBlock(FS_CACHE, sector_no, buffer, buf_offset + amount);
			}
			
			if (io_result != SUCCESS) {
				set_errno(ERROR_IO_ERROR);
				return amount;
			}
			
			amount += num_to_read;
		}
		
		return amount;
	}

	/**
	 * Writes to a file. This function is not synchronised.
	 * 
	 * @param file			the file to write to
	 * @param offset		the offset from which to write
	 * @param length		the number of bytes to write
	 * @param buffer		the buffer holding the bytes to write
	 * @param buf_offset	the offset from which in the buffer to write
	 * @return				the amount of bytes written, or -1 on fatal error, in which case errno is set
	 */
	private int write_file(GlobalOpenFile file, int offset, int length, byte[] buffer, int buf_offset) {
		if (buffer == null || buf_offset < 0 || length < 0|| buf_offset + length > buffer.length) {
			set_errno(ERROR_INVALID_ARGUMENT);
			return -1;
		}
		
		if (file == null) {
			set_errno(ERROR_INVALID_ARGUMENT);
			return -1;
		}
		
		if (length == 0) return 0;
	
		if (offset < 0) {
			set_errno(ERROR_INVALID_ARGUMENT);
			return -1;
		}
		
		int old_size = file.inode.size();
		int new_size = Math.max(offset + length, old_size);
		int block_no = -1;
	
		byte[] inlined_data = null;
		if (file.inode.inlined()) {
			if (file.inode.size() > FILE_INLINE_THRES) {
				set_errno(ERROR_CORRUPT_FS);
				return -1;
			}
	
			inlined_data = new byte[FILE_INLINE_THRES];
			file.inode.get_inlined_data(inlined_data, 0);
			if (new_size <= FILE_INLINE_THRES) {
				/* the file is still inlined */
				System.arraycopy(buffer, buf_offset, inlined_data, offset, length);
				file.inode.set_inlined_data(inlined_data, 0);
				
				/* write through */
				boolean io_result = write_inode(file);
				if (!io_result) {
					return -1;
				}
				
				return length;
			}
			
			else {
				/* the file is no longer inlined */ 
				file.inode.inlined(false);
				
				/* creates the first block */
				block_no = append_new_block(file, true);	/* inode written through */
				if (block_no < 0) {
					/* failed */
					file.inode.set_inlined_data(inlined_data, 0);
					file.inode.inlined(true);
					return -1;
				}
				
				if (old_size > 0) {
					int sector_no = data_block_addr(block_no);
					int io_result = writePartial(FS_CACHE, sector_no, 0, inlined_data, 0, old_size);
					if (io_result != SUCCESS) {
						release_data_block(block_no);
						release_block_ptr(file);
						
						file.inode.set_inlined_data(inlined_data, 0);
						file.inode.inlined(true);
						write_inode(file);
						
						set_errno(io_result);
						return -1;
					}
				}
			}
		}
		
		/* in the following part we do not need to cancel the allocation done above */
		int amount = 0;
		int old_highest = file.inode.num_allocated_sectors() - 1;
		int written_lowest = unsigned_div_floor(offset, sector_size);
		int written_highest = unsigned_div_floor(offset + length - 1, sector_size);
	
		/* (old_highest .. written_lowest) needs to be allocated and zeroed out */
		for (int file_block_no = old_highest + 1; file_block_no < written_lowest; ++file_block_no) {
			if (block_no == -1)
				block_no = append_new_block(file, true);
			else
				block_no = append_new_block(file, block_no, true);
			
			if (block_no < 0) {
				/* failed */
				return -1;
			}
			
			int size_before = file.inode.size();
			file.inode.size = (file_block_no + 1) * sector_size;
			if (!write_inode(file)) {
				/* fail */
				file.inode.size = size_before;
				return -1;
			}
		}
		
		/* [written_lowest .. written_highest] */
		for (int file_block_no = written_lowest; file_block_no <= written_highest; ++file_block_no) {
			int num_to_write = Math.min(length - amount, sector_size);
			
			if (file_block_no >= file.inode.num_allocated_sectors()) {
				/* allocates a new one */
				if (block_no == -1)
					block_no = append_new_block(file, num_to_write != sector_size);
				else
					block_no = append_new_block(file, block_no, num_to_write != sector_size);

				if (block_no < 0) {
					/* failed */
					return amount;
				}
			}
			else {
				block_no = get_block_ptr(file, file_block_no);
				if (get_errno() != SUCCESS) {
					return amount;
				}
			}
		
			int sector_no = data_block_addr(block_no);
			int in_block_offset = unsigned_mod(offset + amount, sector_size);
			
			int io_result;
			if (num_to_write == sector_size) {
				io_result = writeBlock(FS_CACHE, sector_no, buffer, buf_offset + amount);
			}
			else {
				io_result = writePartial(FS_CACHE, sector_no, in_block_offset, buffer, buf_offset + amount, num_to_write);
			}
			
			if (io_result != SUCCESS) {
				return amount;
			}
			else {
				int size_before = file.inode.size();
				
				amount += num_to_write;
			}
		}
	
		file.inode.size = new_size;
		write_inode(file);
		
		set_errno(SUCCESS);
		return amount;
	}

	/**
	 * Truncates the file. This routine is not synchronised.
	 * 
	 * @param file
	 * @return
	 */
	private boolean truncate_file(GlobalOpenFile file) {
		return set_length(file, 0, false, false);
	}

	/**
	 * Resolves the path of the file.
	 * <p>
	 * This routine invokes get_global_open_file, which involves NachosFSLock and the file lock.
	 * 
	 * @param inode_no	the inode no. of the file
	 * @return			a linked list of parts of the path, the list is empty if it is the root
	 */
	private LinkedList<String> resolve_path(int inode_no) {
		LinkedList<String> paths = new LinkedList<String>();
		if (inode_no == root_inode_no) {
			return paths;
		}
		
		GlobalOpenFile dir = get_global_open_file(inode_no);
		if (dir == null) {
			return null;
		}
		dir.lock.acquire();
		
		while (inode_no != root_inode_no) {
			
			if (!dir.inode.is_directory()) {
				dir.lock.release();
				release_global_open_file(dir);
				return null;
			}
			
			DirectoryHeader header = read_dir_header(dir);
			if (header == null) {
				dir.lock.release();
				release_global_open_file(dir);
				return null;
			}
			int parent_inode_no = header.parent_inode_no();
			dir.lock.release();
			release_global_open_file(dir);
			
			dir = get_global_open_file(parent_inode_no);
			if (dir == null) {
				return null;
			}
			
			dir.lock.acquire();
			
			Pair<Integer, DirectoryEntry> query_result = search_for_entry(dir, inode_no);
			if (query_result == null) {
				dir.lock.release();
				release_global_open_file(dir);
				return null;
			}
			
			String name = get_file_name(dir, query_result.second);
			if (name == null) {
				dir.lock.release();
				release_global_open_file(dir);
			}
			paths.addFirst(name);
			inode_no = parent_inode_no;
		}
		
		dir.lock.release();
		release_global_open_file(dir);
		
		return paths;
	}

	/**
	 * Sets the length of a file. This routine is not synchronised. If new_length < file.length, the file will be truncated.
	 * 
	 * @param file
	 * @param new_length
	 * @param zeros
	 * @param allow_inline	
	 * @return
	 */
	private boolean set_length(GlobalOpenFile file, int new_length, boolean zeros, boolean allow_inline) {
		Lib.assertTrue(!allow_inline, "inline is not supported!");
		
		int old_length = file.inode.size();
		int old_num_sectors = file.inode.num_allocated_sectors();
		int new_num_sectors = unsigned_div_ceil(new_length, sector_size);
		
		if (new_num_sectors < old_num_sectors) {
			for (int file_block_no = old_num_sectors - 1; file_block_no >= new_num_sectors; --file_block_no) {
				file.inode.size = file_block_no * sector_size;
				
				int block_no = get_block_ptr(file, file_block_no);
				if (get_errno() != SUCCESS) {
					/* TODO roll back */
					return false;
				}
				
				if (!release_block_ptr(file)) {
					/* TODO roll back */
					return false;
				}
				
				release_data_block(block_no);
			}
			
			if (new_length < file.inode.size) {
				file.inode.size = new_length;
			}
			
			if (!write_inode(file)) {
				/* TODO roll back? */
				return false;
			}
		}
		
		else if (new_num_sectors == old_num_sectors) {
			/* just resize it */
			file.inode.size = new_length;
			if (!write_inode(file)) {
				/* roll back */
				file.inode.size = old_length;
				return false;
			}
			
			/* zeros out the content */
			if (zeros && new_length > old_length) {
				int file_block_no = new_num_sectors - 1;
				int block_no = get_block_ptr(file, file_block_no);
				int sector_no = data_block_addr(block_no);
				if (get_errno() != SUCCESS) {
					return false;
				}
				
				int from = unsigned_mod(old_length, sector_size);
				int to = unsigned_mod(new_length, sector_size);
				int io_result = writePartial(FS_CACHE, sector_no, from, NachosFS.zeros, from, to - from);
				if (io_result != SUCCESS) {
					set_errno(io_result);
					return false;
				}
			}
		}
		
		else {
			/* allocates new blocks */
			int file_block_no = old_num_sectors - 1;
			int block_no;
			int sector_no;
		
			file.inode.size = (file_block_no + 1) * sector_size;
			
			if (zeros) {
				block_no = get_block_ptr(file, file_block_no);
				if (get_errno() != SUCCESS) {
					/* TODO handle error */
					return false;
				}
				sector_no = data_block_addr(block_no);
				
				int from = unsigned_mod(old_length, sector_size);
				int io_result = writePartial(FS_CACHE, sector_no, from, NachosFS.zeros, from, sector_size - from);
				if (io_result != SUCCESS) {
					/* TODO error */
					set_errno(io_result);
					return false;
				}
			}
			
			for (file_block_no++; file_block_no < new_num_sectors; ++file_block_no) {
				if ((block_no = append_new_block(file, false)) < 0) {
					/* TODO do something? */
					write_inode(file);
					return false;
				}
				
				file.inode.size = (file_block_no + 1) * sector_size;
				if (file.inode.size > new_length) {
					file.inode.size = new_length;
				}
				
				if (zeros) {
					sector_no = data_block_addr(block_no);
					
					int io_result = writeBlock(FS_CACHE, sector_no, NachosFS.zeros, 0);
					if (io_result != SUCCESS) {
						/* TODO error */
						set_errno(io_result);
						return false;
					}
				}
			}
			
			if (!write_inode(file)) {
				/* TODO handle */
				return false;
			}
		}
		
		set_errno(SUCCESS);
		return true;
	}

	/**
	 * Creates a new file. This routine invokes get_global_open_file which involves 
	 * NachosFSLock and the file lock.
	 * file.lock is not released on return.
	 * <p>
	 * The reference count is 0 initially.
	 * 
	 * @param inode_hint		where to start the search for a free inode
	 * @param file_type			the type of the file to be created
	 * @param inlined			whether to inline the file
	 * @return					the GlobalOpenFile to the new file on success, null o.w.
	 */
	private GlobalOpenFile create_new_file(int inode_hint, byte file_type, boolean inlined) {
		int inode_no = allocate_inode(inode_hint);
		if (inode_no == INODE_NULL) {
			return null;
		}
	
		GlobalOpenFile file = get_global_open_file(inode_no);
		if (file == null) {
			return null;
		}
		
		file.lock.acquire();
		
		Inode inode = file.inode;
		inode.allocated(true);
		inode.filetype(file_type);
		inode.inlined(false);
		inode.num_allocated_sectors = 0;
		inode.size = 0;
		inode.ref_count = 0;
		
//		file.lock.release();
		return file;
	}

	/**
	 * Links the file in the directory and increases the reference count of the file.
	 * This routine is not synchronised.
	 * 
	 * @param file		the file to be linked
	 * @param dir		the directory where the file is to be linked
	 * @param name		the name of the file
	 * @return			<tt>true</tt> on success
	 */
	private boolean link_file(GlobalOpenFile file, GlobalOpenFile dir, String name) {
		/* FIXME what if link failed? */
		file.inode.ref_count++;
		insert_dir_entry(dir, name, file.inode_no);
		
		write_inode(file);
		return true;
	}

	/**
	 * Unlinks the file from the directory and decreases the reference count of the file.
	 * This routine is not synchronised.
	 * 
	 * @param file		the file to be unlinked
	 * @param dir		the dircetory that the file is in
	 * @param index		the index of the DirectoryEntry that links the file
	 * @return			<tt>true</tt> on success
	 */
	private boolean unlink_file(GlobalOpenFile file, GlobalOpenFile dir, int index) {
		/* FIXME what if remove failed? */
		file.inode.ref_count--;
		remove_dir_entry(dir, index);
		
		write_inode(file);
		
		return true;
	}

	/**
	 * Resolves the path. 
	 * <p>
	 * If the path starts with '/', the starting_inode_no is ignored and resolution starts at
	 * root directory. Otherwise, resolution starts at the directory of which the inode no. is
	 * starting_inode_no. Any part except the final part of the path should be a directory or
	 * a symbolic link that finally links to a directory. Whether to follow the final part that is
	 * a symbolic link depends on the value of follow_symlink, as its name suggests.
	 * <p>
	 * The quadruple returned consists of four components:
	 * <ol>
	 * <li> GlobalOpenFile of the parent directory
	 * <li> the name of the final part
	 * <li> the index of the final part in the directory
	 * <li> the directory entry of the final part
	 * </ol>
	 * <p>
	 * Return value varies depending on the given path:
	 * <ul>
	 * <li> If error occurs during the resolution, <tt>null</tt> is returned.
	 * <li> If the path has a trailing '/', the final part of the path must be a directory
	 * or a symbolic link that finally links to a directory. The open file of the last part
	 * is returned as the first component of the quadruple. The other three components are <tt>null</tt>.
	 * In this case, drop_dir_lock is ignored and the lock of the directory is nevertheless dropped.
	 * <li> Otherwise, the returned quadruple has at least its first two components non-null. The third and
	 * fourth components are <tt>null</tt> if the entry of the last part in the directory does not exist. Otherwise,
	 * the index and the entry is returned as the third and fourth component. drop_dir_lock decides whether the lock
	 * of the directory (the first component) is dropped on return.
	 * </ul>
	 * <p>
	 * 
	 * 
	 * @param starting_inode_no		the inode no of the starting directory if the path is relative
	 * @param path					the path
	 * @param follow_symlink		whether to follow the final part that is a symbolic link
	 * @param drop_dir_lock			whether to drop the directory lock
	 * @return						a quadruple as described above
	 */
	private Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry> walk_through_path(
			int starting_inode_no, String path, boolean follow_symlink, boolean drop_dir_lock) {
		return __walk_through_path(starting_inode_no, path, follow_symlink, new IntPtr(0), drop_dir_lock);
	}

	private Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry> __walk_through_path(
			int starting_inode_no, String path, boolean follow_symlink, IntPtr num_symlink_followed, boolean drop_dir_lock) {
		
		int p = 0;
		int cur_inode_no = starting_inode_no;
		if (path.startsWith("/")) {
			/* absolute path */
			p = 1;
			cur_inode_no = root_inode_no;
		}
		GlobalOpenFile of = get_global_open_file(cur_inode_no);
		if (of == null) {
			set_errno(ERROR_IO_ERROR);
			return null;
		}
		
		if (p > path.length()) {
			return new Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry>(of, null, null, null);
		}
		
		while (true) {
			int np = path.indexOf('/', p);
			if (np == -1) {
				/* last part */
				
				String name = path.substring(p);
				
				of.lock.acquire();
				Pair<Integer, DirectoryEntry> query_result = search_for_entry(of, name);
				Integer entry_no = null;
				DirectoryEntry dir_entry = null;
				if (query_result != null) {
					entry_no = query_result.first;
					dir_entry = query_result.second;
				}
				else {
					if (get_errno() != ERROR_FILE_NOT_FOUND) {
						of.lock.release();
						/* errno */
						return null;
					}
				}
				
				if (follow_symlink && dir_entry != null) {
					int next_inode_no = dir_entry.inode_no();
//					of.lock.release();
					
					GlobalOpenFile next_of = get_global_open_file(next_inode_no);
					if (next_of == null) {
						/* error */
						of.lock.release();
						set_errno(ERROR_IO_ERROR);
						return null;
					}
					
					next_of.lock.acquire();
				
					if (next_of.inode.allocated() && next_of.inode.ref_count != 0 & next_of.inode.is_symlink()) {
						/* read sym link */
						of.lock.release();
						release_global_open_file(of);
						
						++num_symlink_followed.x;
						if (num_symlink_followed.x > MAX_SYMLINK_FOLLOWED) {
							next_of.lock.release();
							release_global_open_file(next_of);
							set_errno(ERROR_LOOP);
							return null;
						}
						
						String linked_path = read_symlink(next_of);
						
						next_of.lock.release();
						release_global_open_file(next_of);
						
						if (linked_path == null) {
							/* use the same errno as read_symlink */
							return null;
						}
						
						return __walk_through_path(root_inode_no, linked_path, true, num_symlink_followed, drop_dir_lock);
					}
					
					next_of.lock.release();
					release_global_open_file(next_of);
				}
				
				if (drop_dir_lock) {
					of.lock.release();
				}
				
				set_errno(SUCCESS);
				return new Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry>(of, name, entry_no, dir_entry); 
			}
			
			else if (p == np) {
				/* empty part; skip it */
				p = np + 1;
				if (p >= path.length()) {
					/* path with trailing '/' */
					of.lock.acquire();
					of = __walk_into_directory(of, num_symlink_followed);
					if (of == null) {
						/* of has already been released */
						return null;
					}
					of.lock.release();
					set_errno(SUCCESS);
					return new Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry>(of, null, null, null);
				}
			}
			
			else {
				/* middle part */
			
				/* file access is exclusive */
				of.lock.acquire();	/* don't forget to release the lock */
				
				if (!of.inode.allocated() || of.inode.ref_count == 0) {
					/* file does not exists */
					of.lock.release();
					release_global_open_file(of);
					
					set_errno(ERROR_FILE_NOT_FOUND);
					return null;
				}
			
				of = __walk_into_directory(of, num_symlink_followed);
				if (of == null) {
					/* already released */
					return null;
				}
				
				String name = path.substring(p, np);
				int next_inode_no = cur_inode_no;
			
				if (name.equals(".")) {
					/* stay here */
					of.lock.release();
					
				}
				
				else if (name.equals("..")) {
					/* go up to parent directory */
					/* .. of root is itself */
				
					DirectoryHeader header = read_dir_header(of);
					if (header == null) {
						/* IO ERROR */
						of.lock.release();
						release_global_open_file(of);
						/* use error code returned by read_dir_header */
						return null;
					}
				
					next_inode_no = header.parent_inode_no();
					of.lock.release();
				}
				
				else {
					/* search for the entry */
					
					Pair<Integer, DirectoryEntry> query_result = search_for_entry(of, name);
					if (query_result == null) {
						/* error */
						of.lock.release();
						release_global_open_file(of);
						/* errno */
						return null;
					}
					
					DirectoryEntry dir_entry = query_result.second;
					next_inode_no = dir_entry.inode_no();
					of.lock.release();
				}
				
				if (next_inode_no != cur_inode_no) {
					release_global_open_file(of);
				
					of = get_global_open_file(next_inode_no);
					if (of == null) {
						set_errno(ERROR_IO_ERROR);
						return null;
					}
					
					cur_inode_no = next_inode_no;
				}
				
				p = np + 1;
				
				if (p >= path.length()) {
					/* got a trailing '/' */
					of.lock.acquire();
					of = __walk_into_directory(of, num_symlink_followed);
					if (of == null) {
						/* of has already been released */
						return null;
					}
					of.lock.release();
					set_errno(SUCCESS);
					return new Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry>(of, null, null, null);
				}
			}
		}
	}
	
	private GlobalOpenFile __walk_into_directory(GlobalOpenFile of, IntPtr num_symlink_followed) {
		while (of.inode.is_symlink()) {
			/* follow the symlink */

			num_symlink_followed.x += 1;
			if (num_symlink_followed.x > MAX_SYMLINK_FOLLOWED) {
				of.lock.release();
				release_global_open_file(of);

				set_errno(ERROR_LOOP);
				return null;
			}

			String new_path = read_symlink(of);
			of.lock.release();
			release_global_open_file(of);
			of = null;

			if (new_path == null) {
				/* use the same errno as read_symlink */
				return null;
			}

			new_path += "/";	/* add trailing '/' to force the walking enter the final directory */

			Quadruple<GlobalOpenFile, String, Integer, DirectoryEntry> walk_result = 
					__walk_through_path(root_inode_no, new_path, true, num_symlink_followed, true);

			if (walk_result == null) {
				return null;
			}

			of = walk_result.first;
			of.lock.acquire();
		}

		if (!of.inode.is_directory()) {
			/* the file is not a directory */
			of.lock.release();
			release_global_open_file(of);

			set_errno(ERROR_NOT_DIR);
			return null;
		}
		
		set_errno(SUCCESS);
		return of;
	}

	/**
	 * Creates a new directory. Locks are all dropped on return.
	 * 
	 * @param inode_hint		where to start searching for the new inode
	 * @param parent_inode_no	the parent directory inode no.
	 * @return					the GlobalOpenFile of the directory on success, <tt>null</tt> o.w.
	 */
	private GlobalOpenFile create_dir(int inode_hint, int parent_inode_no) {
		GlobalOpenFile dir = create_new_file(inode_hint, Inode.FILETYPE_DIR, false);
		if (dir == null) {
			return null;
		}
		
		/* dir.lock is not released */
	
		/* extended file name is just an anonymous normal file */
		GlobalOpenFile extfn = create_new_file(dir.inode_no + 1, Inode.FILETYPE_NORMAL, false);
		if (extfn == null) {
			dir.lock.release();
			release_global_open_file(dir);
		}
		
		/* extfn.lock is not released */
		DirectoryHeader header = new DirectoryHeader();
		header.num_entries = 0;
		header.my_inode_no = (short) dir.inode_no;
		header.parent_inode_no = (short) parent_inode_no;
		header.ext_filename_inode = (short) extfn.inode_no;
		
		if (!write_dir_header(dir, header)) {
			/* TODO handle error */
			extfn.lock.release();
			release_global_open_file(extfn);
			dir.lock.release();
			release_global_open_file(dir);
			return null;
		}
		
		extfn.inode.ref_count += 1;
		write_inode(extfn);
		
		extfn.lock.release();
		release_global_open_file(extfn);
		
		return dir;
	}

	/**
	 * Gets the file name in the directory entry. This routine is not synchronised. This routine
	 * may need to open the extended filename file, so the caller should not have already acquired the lock
	 * of that file.
	 * 
	 * @param dir		the directory
	 * @param entry		the directory entry
	 * @return			the file name, or <tt>null</tt> if error occurs
	 */
	private String get_file_name(GlobalOpenFile dir, DirectoryEntry entry) {
		
		int length = entry.file_name_length();
		byte[] buffer = new byte[length];
		
		if (length <= HAVE_EXT_FILENAME_ENTRY_THRES) {
			/* primary part */
			System.arraycopy(entry.primary_file_name, 0, buffer, 0, length);
		}
		
		else if (length <= HAVE_EXT_FILENAME_ENTRY_THRES2) {
			DirectoryHeader header = read_dir_header(dir);
			if (header == null) {
				set_errno(ERROR_IO_ERROR);
				return null;
			}
			
			/* primary part */
			System.arraycopy(entry.primary_file_name, 0, buffer, 0, PRIMARY_FILENAME_CAPACITY);
			
			/* ext. 0 */
			GlobalOpenFile extfn = get_global_open_file(header.ext_filename_inode());
			extfn.lock.acquire();
			
			int fnb = entry.ext_file_name_block_no();
			int ind = entry.ext_file_name_entry_index0();
			ExtFileNameEntry extfn_entry = read_extfn_entry(extfn, fnb, ind);
			
			System.arraycopy(extfn_entry.content, 0, buffer, PRIMARY_FILENAME_CAPACITY,
					length - PRIMARY_FILENAME_CAPACITY);
			
			extfn.lock.release();
			release_global_open_file(extfn);
		}
		
		else {
			DirectoryHeader header = read_dir_header(dir);
			if (header == null) {
				set_errno(ERROR_IO_ERROR);
				return null;
			}
			
			/* primary part */
			System.arraycopy(entry.primary_file_name, 0, buffer, 0, PRIMARY_FILENAME_CAPACITY);
			
			/* ext. 0 */
			GlobalOpenFile extfn = get_global_open_file(header.ext_filename_inode());
			extfn.lock.acquire();
			
			int fnb = entry.ext_file_name_block_no();
			int ind = entry.ext_file_name_entry_index0();
			ExtFileNameEntry extfn_entry = read_extfn_entry(extfn, fnb, ind);
			
			System.arraycopy(extfn_entry.content, 0, buffer, PRIMARY_FILENAME_CAPACITY,
					EXT_FILENAME_ENTRY_CAPACITY1);
			
			int fnb1 = extfn_entry.next_block_no();
			int ind1 = extfn_entry.ext_file_name_index();
			ExtFileNameEntry extfn_entry1 = read_extfn_entry(extfn, fnb1, ind1);
			
			System.arraycopy(extfn_entry1.content, 0, buffer, PRIMARY_FILENAME_CAPACITY + EXT_FILENAME_ENTRY_CAPACITY1, 
					length - PRIMARY_FILENAME_CAPACITY - EXT_FILENAME_ENTRY_CAPACITY1);
			
			extfn.lock.release();
			release_global_open_file(extfn);
		}
		
		return new String(buffer);
	}

	/**
	 * Searches for the directory entry that is linked to the given inode. This routine is not synchronised.
	 * 
	 * @param directory		the directory to search in 
	 * @param inode_no		the inode no.
	 * @return				a pair of the index no. and the directory entry if such an entry is found; <tt>null</tt> o.w.
	 */
	private Pair<Integer, DirectoryEntry> search_for_entry(GlobalOpenFile directory, int inode_no) {
		
		DirectoryHeader header = read_dir_header(directory);
		if (header == null) {
			set_errno(ERROR_IO_ERROR);
			return null;
		}
		
		int num_entries = header.num_entries();
		for (int i = 0; i < num_entries; ++i) {
			DirectoryEntry dir_entry = read_dir_entry(directory, i);
			
			if (dir_entry.inode_no() == inode_no) {
				set_errno(SUCCESS);
				return new Pair<Integer, DirectoryEntry>(i, dir_entry);
			}
		}
		
		set_errno(ERROR_FILE_NOT_FOUND);
		return null;
	}

	/**
	 * Searches for the directory entry given the name of the file. This routine is not synchronised. This routine may
	 * need to open the extended filename file so the caller should not have already acquired the lock of that file.
	 * 
	 * @param directory		the directory to search in
	 * @param name			the file name
	 * @return				a pair of the index and the directory entry if such an entry is found; <tt>null</tt> o.w.
	 */
	private Pair<Integer, DirectoryEntry> search_for_entry(GlobalOpenFile directory, String name) {
		
		DirectoryHeader header = read_dir_header(directory);
		if (header == null) {
			set_errno(ERROR_IO_ERROR);
			return null;
		}
		
		GlobalOpenFile ext_filename_file = null;
		
		int num_entries = header.num_entries();
		for (int i = 0; i < num_entries; ++i) {
			DirectoryEntry dir_entry = read_dir_entry(directory, i);
			
			int length = dir_entry.file_name_length();
			if (name.length() != length) {
				continue;
			}
			
			if (length <= HAVE_EXT_FILENAME_ENTRY_THRES) {
				/* only primary part */
				
				if (str_match(dir_entry.primary_file_name, 0, length, name, 0)) {
					/* a match record */
					if (ext_filename_file != null) {
						ext_filename_file.lock.release();
						release_global_open_file(ext_filename_file);
					}
					set_errno(SUCCESS);
					return new Pair<Integer, DirectoryEntry>(i, dir_entry);
				} else {
					continue;
				}
			}
			else {
				/* has ext. file name entry */
				
				/* primary part */
				if (!str_match(dir_entry.primary_file_name, 0, PRIMARY_FILENAME_CAPACITY, name, 0)) {
					continue;
				}
			
				if (ext_filename_file == null) {
					/* opens the ext filename file for the first time */
					int ext_filename_inode_no = header.ext_filename_inode();
					ext_filename_file = get_global_open_file(ext_filename_inode_no);
					if (ext_filename_file == null) {
						set_errno(ERROR_IO_ERROR);
						return null;
					}
					ext_filename_file.lock.acquire();
				}

				int extfn_block_no = dir_entry.ext_file_name_block_no();
				int extfn_index = dir_entry.ext_file_name_entry_index0();
				
				
				ExtFileNameEntry extfn_entry = read_extfn_entry(ext_filename_file, extfn_block_no, extfn_index);
				if (extfn_entry == null) {
					ext_filename_file.lock.release();
					release_global_open_file(ext_filename_file);
					return null;
				}
				
				if (name.length() <= HAVE_EXT_FILENAME_ENTRY_THRES2 ) {
					if (!str_match(extfn_entry.content, 0, name.length() - PRIMARY_FILENAME_CAPACITY, 
							name, PRIMARY_FILENAME_CAPACITY)) {
						continue;
					}
				}
				
				else {
					if (!str_match(extfn_entry.content, 0, EXT_FILENAME_ENTRY_CAPACITY1,
							name, PRIMARY_FILENAME_CAPACITY)) {
						continue;
					}
					
					extfn_block_no = extfn_entry.next_block_no();
					extfn_index = extfn_entry.ext_file_name_index();
					
					extfn_entry = read_extfn_entry(ext_filename_file, extfn_block_no, extfn_index);
					if (extfn_entry == null) {
						ext_filename_file.lock.release();
						release_global_open_file(ext_filename_file);
						return null;
					}
					
					if (!str_match(extfn_entry.content, 0, name.length() - PRIMARY_FILENAME_CAPACITY - EXT_FILENAME_ENTRY_CAPACITY1,
							name, PRIMARY_FILENAME_CAPACITY + EXT_FILENAME_ENTRY_CAPACITY1)) {
						continue;
					}
				}
				
				ext_filename_file.lock.release();
				release_global_open_file(ext_filename_file);
				return new Pair<Integer, DirectoryEntry>(i, dir_entry);
				
			}
		}
		
		if (ext_filename_file != null) {
			ext_filename_file.lock.release();
			release_global_open_file(ext_filename_file);
		}
	
		set_errno(ERROR_FILE_NOT_FOUND);
		return null;
	}

	/**
	 * Reads the directory header. NOT synchornised.
	 * 
	 * @param directory
	 * @return
	 */
	private DirectoryHeader read_dir_header(GlobalOpenFile directory) {
		int header_block_no = directory.inode.direct_ptr(0);
		int header_sector_no = data_block_addr(header_block_no);
		
		DirectoryHeader header = new DirectoryHeader();
		int io_result = readPODPartial(FS_CACHE, header_sector_no, 0, header);
		
		if (io_result != SUCCESS) {
			set_errno(io_result);
			return null;
		}
		
		set_errno(SUCCESS);
		return header;		
	}

	/**
	 * Writes the directory header. NOT synchronised.
	 * 
	 * @param directory
	 * @param header
	 * @return
	 */
	private boolean write_dir_header(GlobalOpenFile directory, DirectoryHeader header) {
		byte[] buffer = new byte[header.struct_size()];
		header.toBytes(buffer, 0);
		
		write_file(directory, 0, header.struct_size(), buffer, 0);
		
		set_errno(SUCCESS);
		return true;
	}

	/**
	 * Reads the directory entry. NOT synchronised.
	 * 
	 * @param directory
	 * @param index
	 * @return
	 */
	private DirectoryEntry read_dir_entry(GlobalOpenFile directory, int index) {
	
		DirectoryEntry dir_entry = new DirectoryEntry();
		
		Pair<Integer, Integer> entry_addr = get_directory_entry_addr(index);
		int file_block_no = entry_addr.first;
		int in_block_offset = entry_addr.second * DIRECTORY_ENTRY_SIZE;
		
		int block_no = get_block_ptr(directory, file_block_no);
		if (get_errno() != SUCCESS) {
			return null;
		}
		int sector_no = data_block_addr(block_no);
		
		int io_result = readPODPartial(FS_CACHE, sector_no, in_block_offset, dir_entry);
		if (io_result != SUCCESS) {
			set_errno(io_result);
			return null;
		}
		
		set_errno(SUCCESS);
		return dir_entry;
	}

	/**
	 * Writes the directory entry. NOT synchronised.
	 * 
	 * @param directory
	 * @param index
	 * @param dir_entry
	 * @return
	 */
	private boolean write_dir_entry(GlobalOpenFile directory, int index, DirectoryEntry dir_entry) {
		byte[] buffer = new byte[dir_entry.struct_size()];
		dir_entry.toBytes(buffer, 0);
	
		Pair<Integer, Integer> entry_addr = get_directory_entry_addr(index);
		int file_block_no = entry_addr.first;
		int in_block_offset = entry_addr.second * DIRECTORY_ENTRY_SIZE;
		int offset = file_block_no * sector_size + in_block_offset;
		
		write_file(directory, offset, dir_entry.struct_size(), buffer, 0);
		
		set_errno(SUCCESS);
		return true;
	}

	/**
	 * Inserts a directory entry. NOT synchronised. May need to open the ext. filename file.
	 * 
	 * @param directory
	 * @param name
	 * @param inode_no
	 * @return
	 */
	private boolean insert_dir_entry(GlobalOpenFile directory, String name, int inode_no) {
		
		return insert_dir_entry(directory, name.getBytes(), inode_no);
	}
	
	/**
	 * Inserts a directory entry. NOT synchronised. May need to open the ext. filename file.
	 * 
	 * @param directory
	 * @param name
	 * @param inode_no
	 * @return
	 */
	private boolean insert_dir_entry(GlobalOpenFile directory, byte[] name, int inode_no) {
		if (name.length >= MAX_FILENAME_LENGTH) {
			return false;
		}
		
		DirectoryHeader header = read_dir_header(directory);
		if (header == null) {
			return false;
		}
	
		int my_index = header.num_entries();
		if (my_index >= Short.MAX_VALUE) {
			return false;
		}
		
		DirectoryEntry dir_entry = new DirectoryEntry();
		dir_entry.inode_no = (short) inode_no;
		dir_entry.file_name_length = (byte) name.length;
		
		if (name.length <= HAVE_EXT_FILENAME_ENTRY_THRES) {
			/* file name fits in the directory entry */
			System.arraycopy(name, 0, dir_entry.primary_file_name, 0, name.length);
		}
		
		else if (name.length <= HAVE_EXT_FILENAME_ENTRY_THRES2){
			/* 1 directory entry + 1 ext. filename entry */
			
			/* primary name part */
			System.arraycopy(name, 0, dir_entry.primary_file_name, 0, PRIMARY_FILENAME_CAPACITY);
		
			/* ext. filename entry 0 */
			ExtFileNameEntry ext_filename_entry = new ExtFileNameEntry();
			ext_filename_entry.parent_entry_no(my_index);
			
			System.arraycopy(name, PRIMARY_FILENAME_CAPACITY, 
					ext_filename_entry.content, 0, 
					name.length - PRIMARY_FILENAME_CAPACITY);
			
			GlobalOpenFile extfn = get_global_open_file(header.ext_filename_inode());
			extfn.lock.acquire();
			
			add_ext_filename_entry(extfn, dir_entry, ext_filename_entry);
			
			extfn.lock.release();
			release_global_open_file(extfn);
		}
		
		else {
			/* 1 directory_entry + 2 ext. filename entry */
			
			/* primary name part */
			System.arraycopy(name, 0, dir_entry.primary_file_name, 0, PRIMARY_FILENAME_CAPACITY);
		
			/* ext. filename entry 0 */
			ExtFileNameEntry ext_filename_entry = new ExtFileNameEntry();
			ext_filename_entry.parent_entry_no(my_index);
			
			
			System.arraycopy(name, PRIMARY_FILENAME_CAPACITY, 
					ext_filename_entry.content, 0, 
					EXT_FILENAME_ENTRY_CAPACITY1);
			
			/* ext. filename entry 1 */
			ExtFileNameEntry ext_filename_entry1 = new ExtFileNameEntry();
			
			System.arraycopy(name, PRIMARY_FILENAME_CAPACITY + EXT_FILENAME_ENTRY_CAPACITY1, 
					ext_filename_entry1.content, 0, 
					name.length - PRIMARY_FILENAME_CAPACITY - EXT_FILENAME_ENTRY_CAPACITY1);
			
			GlobalOpenFile extfn = get_global_open_file(header.ext_filename_inode());
			extfn.lock.acquire();
			
			add_ext_filename_entry(extfn, dir_entry, ext_filename_entry, ext_filename_entry1);
			/* TODO error */
				
			extfn.lock.release();
			release_global_open_file(extfn);
		}
		
		write_dir_entry(directory, my_index, dir_entry);
		
		header.num_entries++;
		write_dir_header(directory, header);
		
		set_errno(SUCCESS);
		return true;
	}

	/**
	 * Reads the ext. filename entry. NOT synchronised.
	 * 
	 * @param extfn
	 * @param file_block_no
	 * @param index
	 * @return
	 */
	private ExtFileNameEntry read_extfn_entry(GlobalOpenFile extfn, int file_block_no, int index) {
		int block_no = get_block_ptr(extfn, file_block_no);
		/* TODO handle error */
		
		int sector_no = data_block_addr(block_no);
		ExtFileNameEntry extfn_entry = new ExtFileNameEntry();
		int io_result = readPODPartial(FS_CACHE, sector_no, index * EXT_FILENAME_ENTRY_SIZE, extfn_entry);
		if (io_result != SUCCESS) {
			set_errno(io_result);
			return null;
		}
		
		return extfn_entry;
	}

	/**
	 * Writes the ext. filename entry. NOT synchronised.
	 * 
	 * @param extfn
	 * @param file_block_no
	 * @param index
	 * @param extfn_entry
	 * @return
	 */
	private boolean write_extfn_entry(GlobalOpenFile extfn, int file_block_no, int index, ExtFileNameEntry extfn_entry) {
		byte[] buffer = new byte[extfn_entry.struct_size()];
		extfn_entry.toBytes(buffer, 0);
		
		int num_written = write_file(extfn, file_block_no * sector_size + index * EXT_FILENAME_ENTRY_SIZE, 
				extfn_entry.struct_size(), buffer, 0);
		
		return num_written == extfn_entry.struct_size();
	}

	/**
	 * Appends an ext. filename entry. NOT synchronised.
	 * <p>
	 * The links from the directory entry to the ext. filename entry is set in this routine. The caller
	 * needs to write back the directory entry to complete the update.
	 * 
	 * @param extfn
	 * @param dir_entry
	 * @param entry
	 * @return
	 */
	private boolean add_ext_filename_entry(GlobalOpenFile extfn, DirectoryEntry dir_entry, ExtFileNameEntry entry) {
		int size = extfn.inode.size;
		int file_block_no = unsigned_div_floor(size, sector_size);
		int index = unsigned_div_floor(unsigned_mod(size, sector_size), EXT_FILENAME_ENTRY_SIZE);
	
		entry.is_head(true);
		dir_entry.ext_file_name_block_no(file_block_no);
		dir_entry.ext_file_name_entry_no(index);
		
		return write_extfn_entry(extfn, file_block_no, index, entry);
	}

	/**
	 * Appends two ext. filename entry. NOT synchronised.
	 * <p>
	 * The links from the directory entry to entry 0 and between entry 0 and entry 1 is set in this routine. The caller
	 * needs to write back the directory entry to complete the update.
	 * 
	 * @param extfn
	 * @param dir_entry
	 * @param entry0
	 * @param entry1
	 * @return
	 */
	private boolean add_ext_filename_entry(GlobalOpenFile extfn, DirectoryEntry dir_entry, ExtFileNameEntry entry0, ExtFileNameEntry entry1) {
		int size = extfn.inode.size;
		int file_block_no0 = unsigned_div_floor(size, sector_size);
		int index0 = unsigned_div_floor(unsigned_mod(size, sector_size), EXT_FILENAME_ENTRY_SIZE);
		int index1 = index0 + 1;
		int file_block_no1 = file_block_no0;
		if (index1 == num_ext_filename_entry_per_block) {
			index1 = 0;
			file_block_no1++;
		}
	
		dir_entry.ext_file_name_block_no(file_block_no0);
		dir_entry.ext_file_name_entry_no(index0);
		
		entry0.is_head(true);
		entry0.next_block_no(file_block_no1);
		entry0.ext_file_name_index(index1);
		
		entry1.is_head(false);
		entry1.parent_entry_no(file_block_no0);
		entry1.ext_file_name_index(index0);
	
		write_extfn_entry(extfn, file_block_no0, index0, entry0);
		write_extfn_entry(extfn, file_block_no1, index1, entry1);
		
		return true;
	}

	private boolean __replace_ext_filename_entry(GlobalOpenFile dir, GlobalOpenFile extfn, int sfbn, int sind,
			int tfbn, int tind) {
		
		ExtFileNameEntry extfn_entry = read_extfn_entry(extfn, sfbn, sind);
		write_extfn_entry(extfn, tfbn, tind, extfn_entry);

		if (extfn_entry.is_head()) {
			int parent_no = extfn_entry.parent_entry_no();
			DirectoryEntry parent_entry = read_dir_entry(dir, parent_no);
			parent_entry.ext_file_name_block_no(tfbn);
			parent_entry.ext_file_name_entry_no(tind);
			write_dir_entry(dir, parent_no, parent_entry);
		}

		else {
			int parent_no = extfn_entry.parent_entry_no();
			int parent_index = extfn_entry.ext_file_name_index();
			ExtFileNameEntry parent_entry = read_extfn_entry(extfn, parent_no, parent_index);
			parent_entry.next_block_no(tfbn);
			parent_entry.ext_file_name_index(tind);
			write_extfn_entry(extfn, parent_no, parent_index, parent_entry);
		}
			
		return true;
	}
	
	private boolean __extfn_entry_no_equal(int fbn, int ind, int fbn1, int ind1) {
		return fbn == fbn1 && ind == ind1;
	}

	/**
	 * Removes an ext. filename entry. NOT synchronised.
	 * 
	 * @param dir
	 * @param extfn
	 * @param file_block_no
	 * @param index
	 * @return
	 */
	private boolean remove_ext_filename_entry(GlobalOpenFile dir, GlobalOpenFile extfn, int file_block_no, int index) {
		int size = extfn.inode.size();
		Lib.assertTrue(size >= EXT_FILENAME_ENTRY_SIZE);
		int last_file_block_no = unsigned_div_floor(size - EXT_FILENAME_ENTRY_SIZE, sector_size);
		int last_index = unsigned_div_floor(unsigned_mod(size - EXT_FILENAME_ENTRY_SIZE, sector_size), EXT_FILENAME_ENTRY_SIZE);
		
		if (!__extfn_entry_no_equal(last_file_block_no, last_index, file_block_no, index)) {
			__replace_ext_filename_entry(dir, extfn, last_file_block_no, last_index, file_block_no, index);
		}
		
		set_length(extfn, size - EXT_FILENAME_ENTRY_SIZE, false, false);
		
		set_errno(SUCCESS);
		return true;
	}

	/**
	 * Removes two filename entries. NOT synchornised.
	 * 
	 * @param dir
	 * @param extfn
	 * @param fbn0		the file block no. of the first entry
	 * @param ind0		the index of the first entry
	 * @param fbn1		the file block no. of the second entry
	 * @param ind1		the index of the second entry
	 * @return
	 */
	private boolean remove_ext_filename_entry(GlobalOpenFile dir, GlobalOpenFile extfn, int fbn0, int ind0, int fbn1, int ind1) {
		int size = extfn.inode.size();
		Lib.assertTrue(size >= 2 * EXT_FILENAME_ENTRY_SIZE);
		int last_fbn = unsigned_div_floor(size - EXT_FILENAME_ENTRY_SIZE, sector_size);
		int last_ind = unsigned_div_floor(unsigned_mod(size - EXT_FILENAME_ENTRY_SIZE, sector_size), EXT_FILENAME_ENTRY_SIZE);
		int ls_fbn = unsigned_div_floor(size - EXT_FILENAME_ENTRY_SIZE * 2, sector_size);
		int ls_ind = unsigned_div_floor(unsigned_mod(size - EXT_FILENAME_ENTRY_SIZE * 2, sector_size), EXT_FILENAME_ENTRY_SIZE);
		
		boolean e0l = __extfn_entry_no_equal(last_fbn, last_ind, fbn0, ind0);
		boolean e1l = __extfn_entry_no_equal(last_fbn, last_ind, fbn1, ind1);
		boolean e0s = __extfn_entry_no_equal(ls_fbn, ls_ind, fbn0, ind0);
		boolean e1s = __extfn_entry_no_equal(ls_fbn, ls_ind, fbn1, ind1);
		
		if (!e0l && !e1l) {
			if (!e0s) {
				__replace_ext_filename_entry(dir, extfn, last_fbn, last_ind, fbn0, ind0);
			}
			else {
				__replace_ext_filename_entry(dir, extfn, last_fbn, last_ind, fbn1, ind1);
			}
		}
		
		if (!e0s && !e1s) {
			if (!e0l) {
				__replace_ext_filename_entry(dir, extfn, ls_fbn, ls_ind, fbn0, ind0);
			}
			else {
				__replace_ext_filename_entry(dir, extfn, ls_fbn, ls_ind, fbn1, ind1);
			}
		}
		
		set_length(extfn, size - EXT_FILENAME_ENTRY_SIZE * 2, false, false);
		
		set_errno(SUCCESS);
		return true;
	}

	/**
	 * Removes a directory entry. NOT synchronised. May need to open the ext. filename file.
	 * 
	 * @param directory
	 * @param index
	 * @return
	 */
	private boolean remove_dir_entry(GlobalOpenFile directory, int index) {
		
		DirectoryHeader header = read_dir_header(directory);
		if (header == null) {
			return false;
		}
		
		DirectoryEntry entry = read_dir_entry(directory, index);
		int filename_length = entry.file_name_length();
		if (filename_length <= HAVE_EXT_FILENAME_ENTRY_THRES) {
			/* done */
		}
		
		else if (filename_length <= HAVE_EXT_FILENAME_ENTRY_THRES2) {
			/* 1 ext. filename entry */
			int extfn_file_block_no = entry.ext_file_name_block_no();
			int extfn_index = entry.ext_file_name_entry_index0();
			
			GlobalOpenFile extfn = get_global_open_file(header.ext_filename_inode());
			extfn.lock.acquire();
			
			remove_ext_filename_entry(directory, extfn, extfn_file_block_no, extfn_index);
			
			extfn.lock.release();
			release_global_open_file(extfn);
		}
		
		else {
			/* 2 ext. filename entry */
			int extfn_fbn0 = entry.ext_file_name_block_no();
			int extfn_index0 = entry.ext_file_name_entry_index0();
		
			GlobalOpenFile extfn = get_global_open_file(header.ext_filename_inode());
			extfn.lock.acquire();
			
			ExtFileNameEntry extfn_entry0 = read_extfn_entry(extfn, extfn_fbn0, extfn_index0);
			
			int extfn_fbn1 = extfn_entry0.next_block_no();
			int extfn_index1 = extfn_entry0.ext_file_name_index();
			
			remove_ext_filename_entry(directory, extfn, extfn_fbn0, extfn_index0,
					extfn_fbn1, extfn_index1);
			
			extfn.lock.release();
			release_global_open_file(extfn);
		}
		
		int num_entries = header.num_entries();
		if (index < num_entries - 1) {
			/* substitute the last to here */
			
			DirectoryEntry last_entry = read_dir_entry(directory, num_entries - 1);
			/* FIXME last_entry == null? */
			
			int last_entry_filename_length = last_entry.file_name_length();
			
			if (last_entry_filename_length <= HAVE_EXT_FILENAME_ENTRY_THRES) {
				/* no ext. entries */
			}
			
			else {
				/* update the first entry */
				GlobalOpenFile extfn = get_global_open_file(header.ext_filename_inode());
				extfn.lock.acquire();
			
				int extfn_fbn0 = last_entry.ext_file_name_block_no();
				int extfn_ind0 = last_entry.ext_file_name_entry_index0();
				
				ExtFileNameEntry extfn_entry0 = read_extfn_entry(extfn, extfn_fbn0, extfn_ind0);
				/* FIXME extfn_entry0 == null? */
			
				extfn_entry0.parent_entry_no(index);
				write_extfn_entry(extfn, extfn_fbn0, extfn_ind0, extfn_entry0);
				
				extfn.lock.release();
				release_global_open_file(extfn);
			}
			
			/* do the move */
			write_dir_entry(directory, index, last_entry);
		}
		
		set_length(directory, directory.inode.size() - DIRECTORY_ENTRY_SIZE, false, false);
		/* FIXME fail ? */
		
		--header.num_entries;
		write_dir_header(directory, header);
		/* FIXME fail ? */
		
		set_errno(SUCCESS);
		return true;
	}
	
	/**
	 * Allocates a new data block and appends it the the file. NOT synchronised.
	 * Need to acquire NachosFSLock.
	 * 
	 * @param file
	 * @param zeros		whether to fill the new block with zero
	 * @return			the block no. of the new block
	 */
	private int append_new_block(GlobalOpenFile file, boolean zeros) {
		int M = unsigned_div_floor(file.inode_no - 1, num_inodes_per_block);
		int start_from = M * (block_group_size - 1);
		
		return append_new_block(file, start_from, zeros);
	}
	
	/**
	 * Allocates a new data block and appends it the the file. NOT synchronised.
	 * Need to acquire NachosFSLock.
	 * 
	 * @param file
	 * @param hint		where to start the search for the new data block
	 * @param zeros		whether to fill the new block
	 * @return			the block no. of the new block
	 */
	private int append_new_block(GlobalOpenFile file, int hint, boolean zeros) {
		
		int block_no = allocate_data_block(hint);
		if (get_errno() != SUCCESS) {
			return -1;
		}
		
		int file_block_no = file.inode.num_allocated_sectors();
		if (!set_block_ptr(file, file_block_no, block_no)) {
			release_data_block(block_no);
			return -1;
		}
		
		/* write through */
		if (!write_inode(file)) {
			release_block_ptr(file);
			release_data_block(block_no);
			return -1;
		}
		
		if (zeros) {
			int sector_no = data_block_addr(block_no);
			int io_result = writeBlock(FS_CACHE, sector_no, NachosFS.zeros, 0);
			if (io_result != SUCCESS) {
				/* fail */
				release_block_ptr(file);
				release_data_block(block_no);
				write_inode(file);
				return -1;
			}
		}
		
		set_errno(SUCCESS);
		return block_no;
	}
	

	/**
	 * Gets the block no. of the (file_block_no)th block in the file. NOT synchronised.
	 * 
	 * @param file
	 * @param file_block_no	in-file block number
	 * @return				logical block number
	 */
	private int get_block_ptr(GlobalOpenFile file, int file_block_no) {
		if (file_block_no < NUM_DIRECT_PTR) {
			set_errno(SUCCESS);
			return file.inode.direct_ptr(file_block_no);
		}
	
		file_block_no -= NUM_DIRECT_PTR;
		
		if (file_block_no < num_block_ptr_per_block) {
			/* access the single indirection block */
			int single_ind_ptr = file.inode.single_ind_ptr();
			int first_level_sector_no = data_block_addr(single_ind_ptr);
			int first_level_index = file_block_no;
		
			ShortPtr ptr = new ShortPtr();
			int io_result = readPODPartial(FS_CACHE, first_level_sector_no, first_level_index << 1, ptr);
			
			set_errno(io_result);
			return unsigned_short_to_int(ptr.x);
		}
		
		file_block_no -= num_block_ptr_per_block;
		
		if (file_block_no < num_block_ptr_per_block * num_block_ptr_per_block) {
			/* access the first indirection block */
			int double_ind_ptr = file.inode.double_ind_ptr();
			int first_level_sector_no = data_block_addr(double_ind_ptr);
			int first_level_index = unsigned_div_floor(file_block_no, num_block_ptr_per_block);
			
			ShortPtr ptr = new ShortPtr();
			int io_result = readPODPartial(FS_CACHE, first_level_sector_no, first_level_index << 1, ptr);
			if (io_result != SUCCESS) {
				set_errno(io_result);
				return 0;
			}
			
			/* access the second indirection block */
			int second_level_block_no = unsigned_short_to_int(ptr.x);
			int second_level_sector_no = data_block_addr(second_level_block_no);
			int second_level_index = unsigned_mod(file_block_no, num_block_ptr_per_block);
			
			io_result = readPODPartial(FS_CACHE, second_level_sector_no, second_level_index << 1, ptr);
			set_errno(io_result);
			return unsigned_short_to_int(ptr.x);
		}
		
		set_errno(ERROR_INVALID_ARGUMENT);
		return 0;
	}

	/**
	 * Sets the block no. of the (file_block_no)th block in the file. NOT synchronised.
	 * <p>
	 * If file_block_no is 1 greater than the number of blocks allocated to the file, 
	 * new indirection blocks are allocated when necessary and the number of blocks allocated
	 * is increased. However, if file_block_no is at least 2 greater the number of blocks
	 * allocated to the file, this routine will report error.
	 * 
	 * @param file
	 * @param file_block_no		the in-file block no.
	 * @param block_no			the block no.
	 * @return
	 */
	private boolean set_block_ptr(GlobalOpenFile file, int file_block_no, int block_no) {
		/* TODO remove range check */
		if (file_block_no >= file.inode.num_allocated_sectors() + 1) {
			set_errno(ERROR_INVALID_ARGUMENT);
			return false;
		}
		
		if (file_block_no < NUM_DIRECT_PTR) {
			/* direct pointer */
			file.inode.direct_ptr[file_block_no] = (short) block_no;
			if (file_block_no >= file.inode.num_allocated_sectors()) {
				++file.inode.num_allocated_sectors;
			}
			
			set_errno(SUCCESS);
			return true;
		}
		
		file_block_no -= NUM_DIRECT_PTR;
		
		if (file_block_no < num_block_ptr_per_block) {
			/* set to the single indirection block */
		
			int single_ind_ptr;
			if (file.inode.num_allocated_sectors() <= NUM_DIRECT_PTR) {
				/* this is the first time that we access the single indirection pointer */
				/* need to allocate the block first */
				
				single_ind_ptr = allocate_data_block(block_no + 1);
				if (single_ind_ptr < 0) {
					/* failed */
					set_errno(ERROR_INSUFFICIENT_SPACE);
					return false;
				}
				
				file.inode.single_ind_ptr = (short) single_ind_ptr;
			}
			else {
				single_ind_ptr = file.inode.single_ind_ptr();
			}
			
			int first_level_sector_no = data_block_addr(single_ind_ptr);
			int first_level_index = file_block_no;
			
			ShortPtr ptr = new ShortPtr((short) block_no);
			int io_result = writePODPartial(FS_CACHE, first_level_sector_no, first_level_index << 1, ptr);
			if (io_result != SUCCESS) {
				
				if (file.inode.num_allocated_sectors() <= NUM_DIRECT_PTR) {
					/* rollback */
					release_data_block(single_ind_ptr);
				}
				set_errno(io_result);
				return false;
			}
			
			if (file_block_no + NUM_DIRECT_PTR >= file.inode.num_allocated_sectors()) {
				++file.inode.num_allocated_sectors;
			}
			set_errno(SUCCESS);
			return true;
		}
		
		file_block_no -= num_block_ptr_per_block;
		
		if (file_block_no < num_block_ptr_per_block * num_block_ptr_per_block) {
			/* set to the double indirection block */
		
			/* parameters */
			int first_level_index = unsigned_div_floor(file_block_no, num_block_ptr_per_block);
			int second_level_index = unsigned_mod(file_block_no, num_block_ptr_per_block);
			
			int double_ind_ptr ;
			int first_level_sector_no;
			int second_level_block_no;
			int second_level_sector_no;
			
			boolean allocate_first_level = file.inode.num_allocated_sectors() <= NUM_DIRECT_PTR + num_block_ptr_per_block;
			boolean allocate_second_level = unsigned_mod(file.inode.num_allocated_sectors() - NUM_DIRECT_PTR - num_block_ptr_per_block, 
					num_block_ptr_per_block) == 0;
			
			/* access first level */
			if (allocate_first_level) {
				
				/* allocate first level indirection block */
				double_ind_ptr = allocate_data_block(block_no + 1);
				if (double_ind_ptr < 0) {
					/* failed */
					set_errno(ERROR_INSUFFICIENT_SPACE);
					return false;
				}
				
				file.inode.double_ind_ptr = (short) double_ind_ptr;
			}
			else {
				double_ind_ptr = file.inode.double_ind_ptr(); 
			}
	
			first_level_sector_no = data_block_addr(double_ind_ptr);
			
			/* access second level */
			ShortPtr ptr = new ShortPtr();
		
			if (allocate_second_level) {
				
				/* allocate second level indirection block , as near as possible to double_ind_ptr */
				second_level_block_no = allocate_data_block(double_ind_ptr + 1);
				if (second_level_block_no < 0) {
					/* failed */
					if (allocate_first_level) {
						/* rollback */
						release_data_block(double_ind_ptr);
					}
					
					set_errno(ERROR_INSUFFICIENT_SPACE);
					return false;
				}
				
				ptr.x = (short) second_level_block_no;
				int io_result = writePODPartial(FS_CACHE, first_level_sector_no, first_level_index << 1, ptr);
				if (io_result != SUCCESS) {
					/* io error */
					
					if (allocate_first_level) {
						/* rollback */
						release_data_block(double_ind_ptr);
					}
					
					set_errno(io_result);
					return false;
				}
			}
			
			else {
				
				int io_result = readPODPartial(FS_CACHE, first_level_sector_no, first_level_index << 1, ptr);
				if (io_result != SUCCESS) {
					/* failed */
					/* assert(!allocate_first_level); */
					set_errno(io_result);
					return false;
				}
				second_level_block_no = unsigned_short_to_int(ptr.x);
			}

			second_level_sector_no = data_block_addr(second_level_block_no);
			
			ptr.x = (short) block_no;
			int io_result = writePODPartial(FS_CACHE, second_level_sector_no, second_level_index << 1, ptr);
			if (io_result != SUCCESS) {
				/* failed */
			
				/* rollback */
				if (allocate_second_level) {
					release_data_block(second_level_block_no);
					
					if (allocate_first_level) {
						release_data_block(double_ind_ptr);
					}
				}
				
				set_errno(io_result);
				return false;
			}
			
			if (file_block_no + NUM_DIRECT_PTR + num_block_ptr_per_block >= file.inode.num_allocated_sectors()) {
				++file.inode.num_allocated_sectors;
			}
			set_errno(SUCCESS);
			return true;
		}
		
		set_errno(ERROR_INVALID_ARGUMENT);
		return false;
	}

	/**
	 * Removes the last block of the file (but the block is not returned to the free space; it is the caller's
	 * responsibility to release it). 
	 * If some indirection block is no longer used after releasing
	 * the last, the indirection block is also released. This routine is not synchronised. The caller
	 * may need to write back the inode to complete the update.
	 * 
	 * @param file
	 * @return
	 */
	private boolean release_block_ptr(GlobalOpenFile file) {
		int num_allocated = file.inode.num_allocated_sectors();
		if (num_allocated == 0) return true;
		
		--num_allocated;
		--file.inode.num_allocated_sectors;
		
		if (num_allocated < NUM_DIRECT_PTR) {
			return true;
		}
		
		else if (num_allocated == NUM_DIRECT_PTR) {
			/* release single indirection block  */
			
			int single_ind_ptr = file.inode.single_ind_ptr();
			release_data_block(single_ind_ptr);
			
			return true;
		}
		
		num_allocated -= NUM_DIRECT_PTR;
		if (num_allocated < num_block_ptr_per_block) {
			return true;
		}
		
		num_allocated -= num_block_ptr_per_block;
		
		int first_level_index = unsigned_div_ceil(num_allocated, num_block_ptr_per_block);
		int second_level_index = unsigned_mod(num_allocated, num_block_ptr_per_block);
		boolean release_first_level = num_allocated <= NUM_DIRECT_PTR + num_block_ptr_per_block * num_block_ptr_per_block;
		boolean release_second_level = second_level_index == 0;
		
		if (release_second_level) {
			
			int double_ind_ptr = file.inode.double_ind_ptr();
			int first_level_sector_no = data_block_addr(double_ind_ptr);
		
			ShortPtr ptr = new ShortPtr();
			int io_result = readPODPartial(FS_CACHE, first_level_sector_no, first_level_index << 1, ptr); 
			if (io_result != SUCCESS) {
				set_errno(io_result);
				return false;
			}
			
			int second_level_block_no = unsigned_short_to_int(ptr.x);
			release_data_block(second_level_block_no);
			
			if (release_first_level) {
				release_data_block(double_ind_ptr);
			}
		}
		
		return true;
	}

	/**
	 * Writes the inode of the file. NOT synchronised.
	 * 
	 * @param of
	 * @return
	 */
	private boolean write_inode(GlobalOpenFile of) {
	
		int inode_no = of.inode_no;
		Inode inode = of.inode;
		
		Pair<Integer, Integer> iaddr = inode_addr(inode_no);
		int sector_no = iaddr.first;
		int in_block_offset = iaddr.second;
		
		int io_result = writePODPartial(FS_CACHE, sector_no, in_block_offset, inode);
		if (io_result != SUCCESS) {
			set_errno(io_result);
			return false;
		}
		
		return true;
	}
	

	private static void set_errno(int errno) {
		KThread.set_errno(errno);
	}
	
	private static int get_errno() {
		return KThread.get_errno();
	}

	/**
	 * Process OpenFile objects.
	 *
	 */
	private class OpenFile extends nachos.machine.OpenFile {	
		private OpenFile(String path, GlobalOpenFile of, int initial_offset) {
			super(FilesysKernel.realFileSystem, path);
			
			this.cursor = initial_offset;
			this.of = of;
		}

		public int read(int pos, byte[] buf, int offset, int length) {
			of.lock.acquire();
			int num_read = read_file(of, pos, length, buf, offset);
			of.lock.release();
			
			if (Machine.interrupt().disabled()) {
				/* force the cpu to tick so that StrictReadFile won't complain about 0 time read */
				Machine.interrupt().enable();
				Machine.interrupt().disable();
			}
			
			return num_read;
		}

		@Override
		public int write(int pos, byte[] buf, int offset, int length) {
			of.lock.acquire();
			int num_read = write_file(of, pos, length, buf, offset);
			of.lock.release();
			
			return num_read;
		}

		@Override
		public int length() {
			of.lock.acquire();
			int size = of.inode.size();
			of.lock.release();
			
			return size;
		}

		@Override
		public void close() {
			release_global_open_file(of);
		}

		@Override
		public void seek(int pos) {
			cursor = pos;
		}

		@Override
		public int tell() {
			return cursor;
		}

		@Override
		public int read(byte[] buf, int offset, int length) {
			int num_read = read(cursor, buf, offset, length);
			if (num_read >= 0) {
				cursor += num_read;
			}
			return num_read;
		}

		@Override
		public int write(byte[] buf, int offset, int length) {
			int num_written = write(cursor, buf, offset, length);
			if (num_written >= 0) {
				cursor += num_written;
			}
			return num_written;
		}
		
		private int cursor;
		private GlobalOpenFile of;
	}
	
	
	/**
	 * Allocates a new data block.
	 * 
	 * @param search_from
	 * @return		the block no. of the new block, -1 if no block is available
	 */
	private int allocate_data_block(int search_from) {
		NachosFSLock.acquire();
		int ret = __allocate_data_block(search_from);
		NachosFSLock.release();
		
		return ret;
	}

	/**
	 * Allocates n new data blocks.
	 * 
	 * @param search_from
	 * @param block_no
	 * @param n
	 * @return		the number of data blocks actually allocated
	 */
	private int allocate_data_blocks(int search_from, int[] block_no, int n) {
		int num_allocated = 0;
		
		NachosFSLock.acquire();
		
		while (num_allocated < n) {
			block_no[num_allocated] = __allocate_data_block(search_from);
			if (block_no[num_allocated] < 0) {
				break;
			}
			search_from = block_no[num_allocated++] + 1;
		}
		
		NachosFSLock.release();
		
		return num_allocated;
	}

	/**
	 * Implementation of allocate_data_block. NOT synchronised.
	 * 
	 * @param search_from
	 * @return
	 */
	private int __allocate_data_block(int search_from) {
		if (search_from >= num_data_blocks) {
			search_from = 0;
		}
		int block_no = search_from;
		int index = data_block_index(block_no);
	
		if (num_free_data_blocks == 0) {
			return -1;
		}
		
		do {
			int v = access_bitmap(index);
			if (v == BITMAP_FALSE) {
				--num_free_data_blocks;
				int io_result = set_bitmap(index, BITMAP_TRUE);
				if (io_result != BITMAP_SET_SUCCESS) {
					++num_free_data_blocks;
					block_no = -1;
				}
				
				return block_no;
			}
			
			block_no = (block_no + 1) % num_data_blocks;
			index = data_block_index(block_no);
		} while (block_no != search_from);

		return -1;
	}

	/**
	 * Releases a data block.
	 * 
	 * @param block_no
	 */
	private void release_data_block(int block_no) {
		NachosFSLock.acquire();
		__release_data_block(block_no);
		NachosFSLock.release();
	}

	/**
	 * Releases n data blocks.
	 * 
	 * @param block_no
	 * @param n
	 */
	private void release_data_blocks(int[] block_no, int n) {
		NachosFSLock.acquire();
		for (int i = 0; i < n; ++i) {
			__release_data_block(block_no[i]);
		}
		NachosFSLock.release();
	}

	/**
	 * Implementation of release_data_block. NOT synchronised.
	 * 
	 * @param block_no
	 */
	private void __release_data_block(int block_no) {
		int index = data_block_index(block_no);
		
		if (set_bitmap(index, BITMAP_FALSE) != BITMAP_SET_SUCCESS) {
			/* fatal error */
			return ;
		}
		++num_free_data_blocks;
	}

	/**
	 * Allocates a new inode.
	 * 
	 * @param start_from
	 * @return
	 */
	private int allocate_inode(int start_from) {
		if (start_from >= num_inodes) {
			start_from = 1;
		}
		
		int inode_no = start_from;
		int index = inode_index(inode_no);
		
		NachosFSLock.acquire();
		
		if (num_free_inodes == 0) {
			NachosFSLock.release();
			return INODE_NULL;
		}
		
		do {
			int v = access_bitmap(index);
			if (v == BITMAP_FALSE) {
				--num_free_inodes;
				int io_result = set_bitmap(index, BITMAP_TRUE);
				if (io_result != BITMAP_SET_SUCCESS) {
					++num_free_inodes;
					inode_no = INODE_NULL;
				}
				
				NachosFSLock.release();
				return inode_no;
			}
			inode_no = (inode_no + 1) % num_inodes;
			index = inode_index(inode_no);
		} while (inode_no != start_from);
		
		NachosFSLock.release();
		
		return INODE_NULL;	
	}

	/**
	 * Releases an inode.
	 * 
	 * @param inode_no
	 */
	private void release_inode(int inode_no) {
		
		NachosFSLock.acquire();
		__release_inode(inode_no);
		NachosFSLock.release();
	}

	/**
	 * Implementation of release_inode. NOT synchronised.
	 * 
	 * @param inode_no
	 */
	private void __release_inode(int inode_no) {
		int index = inode_index(inode_no);
		
		if (set_bitmap(index, BITMAP_FALSE) != BITMAP_SET_SUCCESS) {
			/* fatal error */
		}
		else {
			++num_free_inodes;
		}
	}

	/**
	 * Access the ith bit in the bitmap. NOT synchronised.
	 * 
	 * @param index
	 * @return
	 */
	private int access_bitmap(int index) {
		int block_no = unsigned_div_floor(index, num_bits_per_block);
		int block_index = unsigned_mod(index, num_bits_per_block);

		/* TODO disable checking */
		if (block_no >= num_bitmap_blocks) {
			return BITMAP_INDEX_OUT_OF_BOUND;
		}
		
		/* read can always be serviced immediately */
		if ((bitmapCache[block_no][block_index >>> 3] & (1 << (block_index & 0x7))) != 0) {
			return BITMAP_TRUE; 
		}
		else {
			return BITMAP_FALSE;
		}
	}

	/**
	 * Sets the ith bit in the bitmap. NOT synchronised.
	 * 
	 * @param index
	 * @param value
	 * @return
	 */
	private int set_bitmap(int index, int value) {
		int block_no = unsigned_div_floor(index, num_bits_per_block);
		int block_index = unsigned_mod(index, num_bits_per_block);
		int block_index_main = block_index >> 3;
		int block_index_secondary = block_index & 0x7;
	
		/* TODO disable checking */
		if (block_no >= num_bitmap_blocks) {
			return BITMAP_INDEX_OUT_OF_BOUND;
		}
		
		if (value > 1) {
			return BITMAP_INVALID_ARGUMENT;
		}
	
		int old_value = (bitmapCache[block_no][block_index_main] >>> block_index_secondary) & 1;
		
		if (value == BITMAP_FALSE) {
			bitmapCache[block_no][block_index_main] &= ~(1 << (block_index_secondary));
		}
		else {
			bitmapCache[block_no][block_index_main] |= (1 << (block_index_secondary));
		}
		
		int io_result = writePartial(FS_CACHE, bitmap_block_addr(block_no), block_index_main,
				bitmapCache[block_no], block_index_main, 1);
		
		if (io_result != SUCCESS) {
			/* recover value */
			if (old_value == BITMAP_FALSE) {
				bitmapCache[block_no][block_index_main] &= ~(1 << (block_index_secondary));
			}
			else {
				bitmapCache[block_no][block_index_main] |= (1 << (block_index_secondary));
			}
			
			return BITMAP_IO_ERROR;
		}
		return BITMAP_SET_SUCCESS;
	}
	
	private static final int BITMAP_FALSE = 0;
	private static final int BITMAP_TRUE = 1;
	private static final int BITMAP_INDEX_OUT_OF_BOUND = 2;
	private static final int BITMAP_IO_ERROR = 4;
	private static final int BITMAP_INVALID_ARGUMENT = 8;
	private static final int BITMAP_SET_SUCCESS = 0x10;

	/**
	 * Sectors no. of the bitmap block.
	 * 
	 * @param block_no		the block no.
	 * @return				the sector no.
	 */
	private int bitmap_block_addr(int block_no) {
		return nachosfs_offset + bitmap_offset + block_no;
	}

	/**
	 * Address of the inode.
	 * 
	 * @param inode_no		the inode no.
	 * @return				a pair of sector no. and in-block offset of the inode
	 */
	private Pair<Integer, Integer> inode_addr(int inode_no) {
		int M = unsigned_div_floor(inode_no - 1, num_inodes_per_block);
		int Ig = unsigned_mod(inode_no - 1, num_inodes_per_block);
		
		return new Pair<Integer, Integer>(nachosfs_offset + block_group_offset + M * block_group_size,
										INODE_SIZE * Ig);
	}

	/**
	 * Bitmap index of the inode
	 * 
	 * @param inode_no		the inode no.
	 * @return				the index in the bitmap of the indoe
	 */
	private int inode_index(int inode_no) {
		int M = unsigned_div_floor(inode_no - 1, num_inodes_per_block);
		int Ig = unsigned_mod(inode_no - 1, num_inodes_per_block);
		
		return M * (num_inodes_per_block + block_group_size - 1) + Ig;
	}

	/**
	 * Sector no. of the data block.
	 * 
	 * @param block_no		the block no.
	 * @return				its sector no.
	 */
	private int data_block_addr(int block_no) {
		int M = unsigned_div_floor(block_no, block_group_size - 1);
		int Ig = unsigned_mod(block_no, block_group_size - 1);
		
		if (block_no < num_block_groups || last_block_group_has_inode) {
			return nachosfs_offset + block_group_offset + M * block_group_size + 1 + Ig;
		}
		
		else {
			return nachosfs_offset + block_group_offset + M * block_group_size + Ig;
		}
	}

	/**
	 * Bitmap index of the data block.
	 * 
	 * @param block_no		the block no.
	 * @return				the index in the bitmap of the data block
	 */
	private int data_block_index(int block_no) {
		int M = unsigned_div_floor(block_no, block_group_size - 1);
		int Ig = unsigned_mod(block_no, block_group_size - 1);
		
		if (block_no < num_block_groups || last_block_group_has_inode) {
			return M * (num_inodes_per_block + block_group_size - 1) + num_inodes_per_block + Ig;
		}
		
		else {
			return M * (num_inodes_per_block + block_group_size - 1) + Ig;
		}
	}

	/**
	 * Address of the directory entry.
	 * 
	 * @param index		the index of the directory entry
	 * @return			a pair of the in-file block no. and in-block index of the directory entry
	 */
	private Pair<Integer, Integer> get_directory_entry_addr(int index) {
		return new Pair<Integer, Integer> (
				(index + FIRST_DIRECTORY_ENTRY_OFFSET) / num_directory_entry_per_block,
				(index + FIRST_DIRECTORY_ENTRY_OFFSET) % num_directory_entry_per_block);
	}
	
/* NachosFS runtime */

	/** Whehter NachosFS has been successfully initialised. */
	public boolean NachosFSInitialised;

	/** Whether kernel is suggested to import files in the test directory. */
	public boolean suggestImportStub = false;

	/** The NachosFS lock that guards bitmaps and globalOpenFileTable. */
	private Lock NachosFSLock;

	/** The in-memory bitmap cache, which is fairly small on the Nachos VM.
	 *  Use write-through updating policy. */
	private byte[][] bitmapCache;

	/** The global open file table, which is a mapping from inode numbers to global open file objects. */
	private TreeMap<Integer, GlobalOpenFile> globalOpenFileTable;

	/** The list of free global open file objects. */
	private EmbeddedLinkedList freeGlobalOpenFile;

	/** The total number global open file ever created. */
	private int numGlobalOpenFile;
	
	/**
	 * The global open file structure, which contains its inode (write-through on update),
	 * inode number, open count, and other meta-data.
	 * 
	 */
	private static class GlobalOpenFile extends EmbeddedLinkedList {
		private GlobalOpenFile() {
			lock = new Lock();
		}
	
		private Lock lock;
		
		private int open_count;
		
		private int inode_no;
		private Inode inode;
	}
	
/* cache configuration */
	/** swap partition cache */
	public static final int SWAP_CACHE = 0;
	
	/** main partition cache */
	public static final int FS_CACHE = 1;
	
	/** swap partition cache size */
	public static final int swapCacheSize = 64;
	
	/** main partition cache size */
	public static final int fsCacheSize = 96;

	/** the maximum number of global open file that can be created */
	public static final int maxOpenFileCount = 256 * 16;

/* file system configuration */
	/** sector number of the partition table and nachosfs header, fixed to 0 */
	private int nachosfs_header_sector_no = 0;

	/** the partition table and the nachosfs header. This struct is loaded when the
	 * NachosFS service is initialised. Subsequent updates are not written back to it,
	 * until the system properly shutdown (invoking finishNachosFS()). */
	private NachosFSHeader nachosfs_header = new NachosFSHeader();

	/* The following are initialised after nachosfs_header is loaded or the disk is formated. */	
	
	/** The total number of inodes in the main partition. */
	private int num_inodes;

	/** The number of inodes per block. */
	private int num_inodes_per_block;

	/** The number of free inodes on the disk. */
	private int num_free_inodes;

	/** The total number of data blocks in the main partition. */
	private int num_data_blocks;

	/** The total number of free data blocks in the main partition. */
	private int num_free_data_blocks;

	/** The offset of the first block group relative to the start of the main partition. */
	private int block_group_offset;

	/** Number of blocks in a block group (including possibly an inode block). */
	private int block_group_size;

	/** The total number of block groups in the main partition. */
	private int num_block_groups;

	/** Whether the last block group has inode. See NachosFS.md. */
	private boolean last_block_group_has_inode;

	/** The offset of the first block of the bitmap relative to the start of the main partition. */
	private int bitmap_offset;

	/** The total number of bitmap blocks in the main partition. */
	private int num_bitmap_blocks;

	/** The number of bits in a block. */
	private int num_bits_per_block;

	/** The offset of the swap partition relative to the start of the disk. */
	private int swap_offset;

	/** The number of sectors in the swap partition. */
	private int num_swap_sectors;

	/** The offset of the main parition relative to the start of the disk. */
	private int nachosfs_offset;

	/** The inode no. of the root directory. Fixed to 1. */
	private int root_inode_no;

	/** The number of block pointers that a data block can hold. */
	private int num_block_ptr_per_block;

	/** The number of directory entries that a data block can hold. */
	private int num_directory_entry_per_block;

	/** The number of extended filename entry that a data block can hold. */
	private int num_ext_filename_entry_per_block;
	
/* file system constants */
	public static final int NACHOSFS_HEADER_MAGIC_NUMBER = 0x3A8EFF89;
	
	public static final int NACHOSFS_HEADER_NORMAL = 0;
	public static final int NACHOSFS_HEADER_WARNING = 1;
	public static final int NACHOSFS_HEADER_FSCK = 2;
	public static final int NACHOSFS_HEADER_FATAL = 3;
	
	public static final int INODE_NULL = 0;
	public static final int INODE_SIZE = 32;
	public static final int NUM_DIRECT_PTR = 8;
	
	public static final int DIRECTORY_HEADER_SIZE = 16;
	public static final int DIRECTORY_ENTRY_SIZE = 16;
	public static final int EXT_FILENAME_ENTRY_SIZE = 128;
	public static final int FIRST_DIRECTORY_ENTRY_OFFSET = 1;
	
	public static final int HAVE_EXT_FILENAME_ENTRY_THRES = 12;
	public static final int EXT_FILENAME_ENTRY_CAPACITY1 = 123;
	public static final int PRIMARY_FILENAME_CAPACITY = 10;
	public static final int HAVE_EXT_FILENAME_ENTRY_THRES2 = 10 + 123;
	public static final int EXT_FILENAME_ENTRY_CAPACITY2 = 125;
	
	public static final int MAX_FILENAME_LENGTH = 255;
	
	public static final int MAX_PATH_LENGTH = 65536;
	public static final int MAX_SYMLINK_FOLLOWED = 128;
	
	public static final int FILE_INLINE_THRES = 20;

	/** The minimum size of disk space to hold a NachosFS. If this limit is reached,
	 * nothing other than the meta-data and an empty root directory can be stored on the disk. */
	public static final int NACHOSFS_MINIMAL_DISK_SECTORS = 66;

	/** The number of sectors to be allocated to the swap partition. This is priorly satisfied if there's
	 * no enough space on the disk. */
	public static final int configNumSwapSectors = 256; /* 128 KB */
	
	/** The minimal number of sectors to be allocated to the main partition. A warning will be prompted if 
	 * the number of sectors allocated to the main partition falls below this number. */
	public static final int configMinimalMainSectors = 3036;	/* 1.5 MB */

	/** An inode is created for every configNumSectorPerIndoe sectors. */
	public static final int configBlockGroupSize = 64;

	/** The minimal number of data sectors to support. A warning will be prompted if 
	 * the number of data sectors falls below this number. */
	public static final int configMinimalMainDataSectors = 2568; /* 1.25 MB */

	/** The minimal number of files to support. A warning will be prompted if 
	 * the number of supported files falls below this number. */
	public static final int configMinimalMainFiles = 210;
	
/* IO cache layer */	
/* currently the block size is identical to the sector size */

	/** 
	 * Initialises the IO cache. This must be initialised after the disk scheduler is up.
	 * Multiple separate cache can be created. IO request should be posted with its cache
	 * number. IO requests of the same sector with different cache number will result in
	 * different results depending on the current status of the cache (, which is highly
	 * recommended not to do so).
	 * 
	 * @param num_caches		the number of caches to create
	 * @param cache_size		the cache size of the caches
	 * @param sync_on_finish	whether the cache should be synchronised with the disk when finishing the cache
	 */
	public void initialiseCache(int num_caches, int[] cache_size, boolean[] sync_on_finish) {
		cacheLock = new Lock();
		
		numCaches = num_caches;
		
		cacheLRUList = new EmbeddedLinkedList[num_caches];
		cacheWaiting = new Condition2[num_caches];
		
		for (int i = 0; i < num_caches; ++i) {
			cacheLRUList[i] = new EmbeddedLinkedList();
			cacheWaiting[i] = new Condition2(cacheLock);
		}
		
		cacheMap = new TreeMap<Integer, BlockCache>();
		
		cacheMaxSize = cache_size.clone();
		cacheSyncOnFinish = sync_on_finish.clone();
		cacheSize = new int[num_caches];
		Arrays.fill(cacheSize, 0);
	}

	/**
	 * Clears up the resource of the cache after required synchronisations are done.
	 * 
	 */
	public void finishCache() {
	
		for (int cache_no = 0; cache_no < numCaches; ++cache_no) {
			if (cacheSyncOnFinish[cache_no]) {
				syncAll(cache_no);
			}
		}
		
	}

	/**
	 * Writes a block.
	 * 
	 * @param cache_no		the cache no. of the block
	 * @param sector_no		the sector no. of the first sector in the block
	 * @param buffer		the bytes to be written
	 * @param offset		offset in the buffer array from which bytes are to be written to the cache
	 * @return				error no. indicating whether the operation is successful
	 */
	public int writeBlock(int cache_no, int sector_no, byte[] buffer, int offset) {
		if (offset < 0 || offset + sector_size > buffer.length) {
			return ERROR_INVALID_ARGUMENT;
		}
	
		int result = __writeBlock(cache_no, sector_no, (byte[] cache) -> {
			System.arraycopy(buffer, offset, cache, 0, sector_size);
			return true;
		});
		
		return result;
	}
	
	/**
	 * Reads a block.
	 * 
	 * @param cache_no		the cache no. of the block
	 * @param sector_no		the sector no. of the first sector in the block
	 * @param buffer		the buffer to hold the bytes read
	 * @param offset		offset in the buffer array from which bytes read are to be stored
	 * @return				error no. indicating whether the operation is successful
	 */
	public int readBlock(int cache_no, int sector_no, byte[] buffer, int offset) {
		if (offset < 0 || offset + sector_size > buffer.length) {
			return ERROR_INVALID_ARGUMENT;
		}
		
		int result = __readBlock(cache_no, sector_no, (byte[] cache) -> {
			System.arraycopy(cache, 0, buffer, offset, sector_size);
			return true;
		});
		
		return result;
	}

	/**
	 * Writes a POD. Java does not provide C-structure like facility so that we
	 * have to use a separate interface to provide the same functionality without
	 * introduce memcpy overhead. This POD should be of size of an entire block.
	 * 
	 * @param cache_no		the cache no. of the block
	 * @param sector_no		the sector no. of the first sector in the block
	 * @param pod			the pod to be written to the cache
	 * @return				error no. indicating whether the operation is successful
	 */
	public int writePOD(int cache_no, int sector_no, POD pod) {
		if (pod == null) return ERROR_INVALID_ARGUMENT;
		
		int result = __writeBlock(cache_no, sector_no, (byte[] cache) -> {
			return pod.toBytes(cache, 0);
		});
		
		return result;
	}
	
	/**
	 * Reads a POD. Java does not provide C-structure like facility so that we
	 * have to use a separate interface to provide the same functionality without
	 * introduce memcpy overhead. This POD shoule of size of an entire block.
	 * 
	 * @param cache_no		the cache no. of the block
	 * @param sector_no		the sector no. of the first sector in the block
	 * @param pod			the pod to be read from the cache
	 * @return				error no. indicating whether the operation is successful
	 */
	public int readPOD(int cache_no, int sector_no, POD pod) {
		if (pod == null) return ERROR_INVALID_ARGUMENT;
		
		int result = __readBlock(cache_no, sector_no, (byte[] cache) -> {
			return pod.fromBytes(cache, 0);
		});
		
		return result;
	}

	/**
	 * Reads part of a block. This routine can reduce the overhead of transferring the entrie buffer when reading
	 * a small object.
	 * 
	 * @param cache_no		the cache no. of the block
	 * @param sector_no		the sector no. of the first sector in the block
	 * @param offset_in_block  the offset in the block from which bytes are to be read
	 * @param buffer		the buffer to hold the bytes read
	 * @param offset_in_buffer	offset in the buffer array from which the read bytes are to be stored
	 * @param length		the number of bytes to be read
	 * @return				error no. indicating whether the operation is successful
	 */
	public int readPartial(int cache_no, int sector_no, int offset_in_block, byte[] buffer, int offset_in_buffer, int length) {
		if (offset_in_block < 0 || offset_in_block + length > sector_size) {
			return ERROR_INVALID_ARGUMENT;
		}
		
		if (offset_in_buffer < 0 || offset_in_buffer + length > buffer.length) {
			return ERROR_INVALID_ARGUMENT;
		}
		
		int result = __readBlock(cache_no, sector_no, (byte[] cache) -> {
			System.arraycopy(cache, offset_in_block, buffer, offset_in_buffer, length);
			return true;
		});
		
		return result;
	}
	
	/**
	 * Writes part of a block. This routine can reduce the overhead of transferring the entrie buffer when writing
	 * a small object.
	 * 
	 * @param cache_no		the cache no. of the block
	 * @param sector_no		the sector no. of the first sector in the block
	 * @param offset_in_block  the offset in the block from which bytes are to be written
	 * @param buffer		the buffer to hold the bytes to be written 
	 * @param offset_in_buffer	offset in the buffer array from which bytes are to be written
	 * @param length		the number of bytes to be written
	 * @return				error no. indicating whether the operation is successful
	 */
	public int writePartial(int cache_no, int sector_no, int offset_in_block, byte[] buffer, int offset_in_buffer, int length) {
		if (offset_in_block < 0 || offset_in_block + length > sector_size) {
			return ERROR_INVALID_ARGUMENT;
		}
		
		if (offset_in_buffer < 0 || offset_in_buffer + length > buffer.length) {
			return ERROR_INVALID_ARGUMENT;
		}
		
		/* read and update; cannot use writeBlock_impl or readBlock_impl; */
		cacheLock.acquire();

		/* must bring in the block first, otherwise we may lose the part that's not updated */
		Pair<Integer, BlockCache> result = getBlockCache(cache_no, sector_no, false);
		if (result.first != SUCCESS) {
			cacheLock.release();
			return result.first;
		}
		BlockCache cache = result.second;
		
		System.arraycopy(buffer, offset_in_buffer, cache.buffer, offset_in_block, length);
		cache.status |= BLOCK_CACHE_DIRTY;
		insertToCacheHead(cache_no, cache);
		
		cacheLock.release();
		
		return SUCCESS;
	}

	/**
	 * Reads a POD that does not occupy an entire block. This routine can reduce the overhead of tranffering the entire block
	 * when reading a small object.
	 * 
	 * @param cache_no		the cache no. of the block
	 * @param sector_no		the sector no. of the first sector in the block
	 * @param offset_in_block  the offset in the block from which bytes are to be read
	 * @param pod			the POD to be read from the cache
	 * @return				error no. indicating whether the operation is successful
	 */
	public int readPODPartial(int cache_no, int sector_no, int offset_in_block, POD pod) {
		if (offset_in_block < 0) {
			return ERROR_INVALID_ARGUMENT;
		}
		
		int result = __readBlock(cache_no, sector_no, (byte[] cache) -> {
			return pod.fromBytes(cache, offset_in_block);
		});
		
		return result;
	}
	
	/**
	 * Writes a POD that does not occupy an entire block. This routine can reduce the overhead of tranffering the entire block
	 * when writing a small object.
	 * 
	 * @param cache_no		the cache no. of the block
	 * @param sector_no		the sector no. of the first sector in the block
	 * @param offset_in_block  the offset in the block from which bytes are to be written
	 * @param pod			the POD to be written to the cache
	 * @return				error no. indicating whether the operation is successful
	 */
	public int writePODPartial(int cache_no, int sector_no, int offset_in_block, POD pod) {
		if (offset_in_block < 0) {
			return ERROR_INVALID_ARGUMENT;
		}
		
		/* read and update; cannot use writeBlock_impl or readBlock_impl; */
		cacheLock.acquire();

		/* must bring in the block first, otherwise we may lose the part that's not updated */
		Pair<Integer, BlockCache> result = getBlockCache(cache_no, sector_no, false);
		if (result.first != SUCCESS) {
			cacheLock.release();
			return result.first;
		}
		BlockCache cache = result.second;
	
		boolean write_result = pod.toBytes(cache.buffer, offset_in_block);
		cache.status |= BLOCK_CACHE_DIRTY;
		insertToCacheHead(cache_no, cache);
		
		cacheLock.release();
	
		if (!write_result) return ERROR_JAVA_FAIL;
		return SUCCESS;
	}
	
	private int __writeBlock(int cache_no, int sector_no, Predicate<byte[]> dataWriter) {
		if (cache_no >= numCaches) {
			return ERROR_INVALID_ARGUMENT;
		}
	
		cacheLock.acquire();
	
		Pair<Integer, BlockCache> result = getBlockCache(cache_no, sector_no, true);
		if (result.first != SUCCESS) {
			cacheLock.release();
			return result.first;
		}
		BlockCache cache = result.second;
		
		boolean write_result = dataWriter.test(cache.buffer);
		cache.status |= BLOCK_CACHE_DIRTY;
		insertToCacheHead(cache_no, cache);
		
		cacheLock.release();
		
		if (!write_result) return ERROR_JAVA_FAIL;
		return SUCCESS;
	}
	
	private int __readBlock(int cache_no, int sector_no, Predicate<byte[]> dataReader) {
		if (cache_no >= numCaches) {
			return ERROR_INVALID_ARGUMENT;
		}
		
		cacheLock.acquire();
	
		Pair<Integer, BlockCache> result = getBlockCache(cache_no, sector_no, false);
		if (result.first != SUCCESS) {
			cacheLock.release();
			return result.first;
		}
		BlockCache cache = result.second;
		
		boolean read_result = dataReader.test(cache.buffer);
		insertToCacheHead(cache_no, cache);
		
		cacheLock.release();
		
		if (!read_result) return ERROR_JAVA_FAIL;
		return SUCCESS;
	}

	/**
	 * Gets the cache of a specific block. If it is a block to be written to, this routine will not
	 * actually load the cache from the disk if it is not present.
	 * 
	 * @param cache_no		the cache no.
	 * @param sector_no		the sector no. of the first sector in the block
	 * @param write			whether the cache is to be written to or not
	 * @return				a pair of errno and block cache on success
	 */
	private Pair<Integer, BlockCache> getBlockCache(int cache_no, int sector_no, boolean write) {
		BlockCache cache = null;
		int errno = 0;
		
		while (true) {
			/* looks up in the cached blocks */
			cache = cacheMap.get(sector_no);

			if (cache != null) {
				/* the block is found in the cache */
				
				if (cache.cache_no != cache_no) {
					/* error, this block does not belong to the declared cache */
					cache = null;
					errno = ERROR_INVALID_ARGUMENT;
					break;
				}

				if (cache.valid()) {
					/* this block is okay */
					if (cache.synchronising() && write) {
						/* have to wait */
						cache.waiting.sleep();
						
						/* when we are waken up , the cache may be invalid,
						 * so we have to re-check the block in the cache */
						continue;
					}
					
					else {
						/* read is okay when synchronising the block,
						 * but write must wait until synchronising completes */
						errno = SUCCESS;
						break;
					}
				}

				else {
					/* this block is being evicted or loaded, have to wait */
					cache.waiting.sleep();
					
					/* re-check */
					continue;
				}
				
			}
		
			/* cache == null */
			else {
				/* this block is not in cache */
				
				if (cacheSize[cache_no] == cacheMaxSize[cache_no]) {
					/* already full, evict one cached block */

					cache = prevBlockCache(cacheLRUList[cache_no]);
					if (cache != null) {
						
						cache.remove_from_list();
						boolean was_valid = cache.valid();
						cache.status &= ~BLOCK_CACHE_VALID;
						
						/* two block share one entry in the block. 
						 * If the old block is being written back to the disk, 
						 * accessing to the old block has to wait. */
						cacheMap.put(sector_no, cache);
						
						if (was_valid) {
							/* evict the block */
							
							if (cache.dirty()) {
								/* write back */
								cache.status |= BLOCK_CACHE_SYNC;
								
								cacheLock.release();
								int result = writeSector(cache.sector_no, cache.buffer, 0);
								cacheLock.acquire();
								
								cache.status &= ~BLOCK_CACHE_SYNC;
								cache.waiting.wakeAll();
								
								if (result != SUCCESS) {
									/* io error */
									cache.status |= BLOCK_CACHE_VALID;
									cacheMap.remove(sector_no);
									insertToCacheHead(cache_no, cache);
									cache = null;
									errno = result;
									break;
								}
							}
							
							cacheMap.remove(cache.sector_no);
						}
						
						cache.sector_no = sector_no;
						
						if (write) {
							cache.status = BLOCK_CACHE_VALID;
							errno = SUCCESS;
							break;
						}
						
						else {
							/* load the block */
							cache.status = BLOCK_CACHE_SYNC;
							
							cacheLock.release();
							int result = readSector(sector_no, cache.buffer, 0);
							cacheLock.acquire();
							
							cache.status &= ~BLOCK_CACHE_SYNC;
							cache.waiting.wakeAll();
							
							if (result != SUCCESS) {
								cacheMap.remove(sector_no);
								insertToCacheTail(cache_no, cache);
								cache = null;
								errno = result;
								break;
							}
							
							else {
								cache.status = BLOCK_CACHE_VALID;
								errno = result;
								break;
							}
						}
					}
					
					else {
						/* no cached block can be evicted; wait for an available one */
						cacheWaiting[cache_no].sleep();
						
						/* re-check */
						continue;
					}
				}

				else {
					/* creates a new cached block */

					cacheSize[cache_no]++;
					cache = new BlockCache(cacheLock, cache_no, sector_size);
					cache.sector_no = sector_no;
					cacheMap.put(sector_no, cache);
					
					if (write) {
						cache.status = BLOCK_CACHE_VALID;
						errno = SUCCESS;
						break;
					}
					
					else {
						/* load the block */
						cache.status = BLOCK_CACHE_SYNC;
						
						cacheLock.release();
						int result = readSector(cache.sector_no, cache.buffer, 0);
						cacheLock.acquire();
						
						cache.status &= ~BLOCK_CACHE_SYNC;
						cache.waiting.wakeAll();
						
						if (result != SUCCESS) {
							/* IO error, put it to the end of the lru list */
							cacheMap.remove(sector_no);
							cache.insert_before(cacheLRUList[cache_no]);
							insertToCacheTail(cache_no, cache);
							cache = null;
							errno = result;
							break;
						}
						
						else {
							/* successful */
							cache.status = BLOCK_CACHE_VALID;
							errno = SUCCESS;
							break;
						}
					}
				}
			}
		}
		
		Pair<Integer, BlockCache> ret = new Pair<Integer, BlockCache>(errno, cache);
		return ret;
	}

	/**
	 * Synchronise a block with the disk.
	 * 
	 * @param cache_no		the cache no.
	 * @param sector_no		the sector no. of the first sector in the block
	 * @return				errno
	 */
	public int sync(int cache_no, int sector_no) {
		if (cache_no >= numCaches) {
			return ERROR_INVALID_ARGUMENT;
		}
		
		cacheLock.acquire();
		
		BlockCache cache = cacheMap.get(sector_no);
		if (cache != null && cache.cache_no != cache_no) {
			cacheLock.release();
			return ERROR_INVALID_ARGUMENT;
		}
		
		int result = __sync(cache, true);
		
		cacheLock.release();
	
		return result;
	}

	/**
	 * Syncrhonises the entire cache. This call will block all subsequent IO request until synchronising
	 * completes even if the request does not involve the cache that is being synchronised.
	 * 
	 * @param cache_no		the cache no.
	 */
	private void syncAll(int cache_no) {
		
		cacheLock.acquire();
		
		BlockCache[] cacheList = cacheMap.values().toArray(new BlockCache[0]);
		for (BlockCache cache : cacheList) {
			if (cache.cache_no == cache_no) {
				/* don't release the lock when synchronising all blocks; o.w. new block may be written to when
				 * synchronising has not been completed */
				__sync(cache, false);
			}
		}
		
		cacheLock.release();
	}
	
	private int __sync(BlockCache cache, boolean dropLock) {
	
		if (cache != null) {
			if (cache.valid() && cache.dirty() && !cache.synchronising()) {
				
				cache.status |= BLOCK_CACHE_SYNC;
			
				if (dropLock) {
					cache.remove_from_list();
					cacheLock.release();
				}
				int result = writeSector(cache.sector_no, cache.buffer, 0);
				if (dropLock) {
					cacheLock.acquire();
					insertToCacheHead(cache.cache_no, cache);
				}
				
				cache.status &= ~BLOCK_CACHE_SYNC;
				
				if (result != SUCCESS) {
					return ERROR_IO_ERROR;
				}
				else {
					cache.status &= ~BLOCK_CACHE_DIRTY;
				}
			}
		}
		
		return SUCCESS;
	}
	
	private void insertToCacheHead(int cache_no, BlockCache cache) {
		cache.insert_after(cacheLRUList[cache_no]);
		cacheWaiting[cache_no].wake();
	}
	
	private void insertToCacheTail(int cache_no, BlockCache cache){
		cache.insert_before(cacheLRUList[cache_no]);
		cacheWaiting[cache_no].wake();
	}
	
	private static BlockCache nextBlockCache(EmbeddedLinkedList head) {
		EmbeddedLinkedList next = head.list_next;
		if (next instanceof BlockCache) return (BlockCache) next;
		return null;
	}
	
	private static BlockCache prevBlockCache(EmbeddedLinkedList head) {
		EmbeddedLinkedList prev = head.list_prev;
		if (prev instanceof BlockCache) return (BlockCache) prev;
		return null;
	}
	
	static class BlockCache extends EmbeddedLinkedList {
		BlockCache(Lock cacheLock, int cache_no, int block_size) {
			this.cache_no = cache_no;
			status = 0x0;
			waiting = new Condition2(cacheLock);
			
			this.buffer = new byte[block_size];
		}
	
		final boolean valid() {
			return (status & BLOCK_CACHE_VALID) != 0;
		}
		
		final boolean dirty() {
			return (status & BLOCK_CACHE_DIRTY) != 0;
		}
		
		final boolean synchronising() {
			return (status & BLOCK_CACHE_SYNC) != 0;
		}
		
		int cache_no;
		
		int sector_no;
		
		byte[] buffer;
		
		int status;
		
		Condition2 waiting;
	}
	
	static final int BLOCK_CACHE_VALID = 0x1;
	static final int BLOCK_CACHE_DIRTY = 0x2;
	static final int BLOCK_CACHE_SYNC = 0x4;

	/** Cache lock that guards IO caches. */
	private Lock cacheLock;

	/** The number of caches. */
	private int numCaches;

	/** LRU list. */
	private EmbeddedLinkedList[] cacheLRUList;

	/** waiting for an evictable cache entry */
	private Condition2[] cacheWaiting;

	/** mapping from sector no. to block cache */
	private TreeMap<Integer, BlockCache> cacheMap;

	/** maximum sizes of the caches */
	private int[] cacheMaxSize;

	/** whether to synchronise the cache when finishing */
	private boolean[] cacheSyncOnFinish;

	/** the current size of the caches */
	private int[] cacheSize;
	
/* Low-level disk operations and scheduling */

	/**
	 * Initialises the disk scheduler.
	 * 
	 */
	public void initialiseDiskScheduler() {
		/* have to use SynchDisk since we have no access to the underlying Disk */
		disk = Machine.synchDisk();

		diskSchedulerLock = new Lock();
		
		IORequestQueue = new TreeMap<Integer, IORequest>();
		IORequestPool = new LinkedList<IORequest>();
		
		schedulerActive = false;
		suspendScheduler = new Condition2(diskSchedulerLock);
		
		finishScheduler = false;
		
		waitingForSchedulerToHalt = new Condition2(diskSchedulerLock);
		
		/* fork the disk scheduler daemon */
		KThread diskSchedulerDaemon = new KThread(()->{CLOOK_scheduler();}).setName("dskschd");
		
		if (! (ThreadedKernel.scheduler instanceof RoundRobinScheduler)) {
			
			/* we want the disk scheduler run at a high priority */
			boolean intStatus = Machine.interrupt().disable();
			
			if (ThreadedKernel.scheduler instanceof PriorityScheduler) {
				ThreadedKernel.scheduler.setPriority(diskSchedulerDaemon, PriorityScheduler.priorityMaximum);
			}
			
			else if (ThreadedKernel.scheduler instanceof LotteryScheduler) {
				/* don't set too high a priority otherwise the value may overflow */
				ThreadedKernel.scheduler.setPriority(diskSchedulerDaemon, 128);
			}
			
			Machine.interrupt().restore(intStatus);
		}
		/* but with round-robin scheduler, there's no way to set the priority */
		
		diskSchedulerDaemon.fork();
	}

	/**
	 * Releases all resources after all IO request in the queue are serviced.
	 * 
	 */
	public void finishDiskScheduler() {
		diskSchedulerLock.acquire();
		
		finishScheduler = true;
		
		if (schedulerActive) {
			waitingForSchedulerToHalt.sleep();
		}
		
		diskSchedulerLock.release();
	}

	/**
	 * Writes a sector to the disk.
	 * 
	 * @param sector_no		the sector no.
	 * @param buffer		the bytes to be written
	 * @param offset		offset in the buffer
	 * @return				errno
	 */
	public int writeSector(int sector_no, byte[] buffer, int offset) {
		diskSchedulerLock.acquire();
		
		if (IORequestQueue.containsKey(sector_no)) {
			return ERROR_IO_BUSY;
		}
		
		IORequest request = getIORequest(sector_no, buffer, offset, true);
		postIORequest(request);
		
		diskSchedulerLock.release();
		
		return SUCCESS;
	}
	
	/**
	 * Reads a sector from the disk.
	 * 
	 * @param sector_no		the sector no.
	 * @param buffer		the bytes to hold the bytes read
	 * @param offset		offset in the buffer
	 * @return				errno
	 */
	public int readSector(int sector_no, byte[] buffer, int offset) {
		diskSchedulerLock.acquire();
		
		if (IORequestQueue.containsKey(sector_no)) {
			return ERROR_IO_BUSY;
		}
		
		IORequest request = getIORequest(sector_no, buffer, offset, false);
		postIORequest(request);
		
		diskSchedulerLock.release();
		
		return SUCCESS;
	}

	/**
	 * Gets an IO request object from the pool and initialises it.
	 * 
	 * @param sector_no
	 * @param buffer
	 * @param offset
	 * @param is_write
	 * @return
	 */
	private IORequest getIORequest(int sector_no, byte[] buffer, int offset, boolean is_write) {
		IORequest request = null;
		if (IORequestPool.isEmpty()) {
			request = new IORequest(diskSchedulerLock);
		}
		else {
			request = IORequestPool.poll();
		}
		request.initialise(sector_no, buffer, offset, is_write);
		return request;
	}

	/**
	 * Posts an IO request. This call will block until the request is serviced.
	 * 
	 * @param request
	 */
	private void postIORequest(IORequest request) {
		IORequestQueue.put(request.sector_no, request);
		Lib.debug(dbgDiskScheduler, "[disk scheduler] post io request " + request.sector_no);
		
		if (!schedulerActive) {
			schedulerActive = true;
			suspendScheduler.wake();
		}
		request.waiting.sleep();
	
		request.buffer = null;	/* let java GC be able to reclaim the space */
		IORequestPool.add(request);
	}

	/**
	 * C-LOOK scheduler.
	 * 
	 */
	private void CLOOK_scheduler() {
		diskSchedulerLock.acquire();
		
		schedulerActive = true;
		int cur_sector = 0;
		IORequest request = null;
		
		while (true) {
			if (IORequestQueue.isEmpty()) {
				
				if (finishScheduler) {
					/* finish the scheduler */
					break;
				}
				
				else {
					/* waiting for new request */
					schedulerActive = false;
					suspendScheduler.sleep();
				}
			}

			Integer next_to_service = IORequestQueue.ceilingKey(cur_sector);
			if (next_to_service == null) {
				/* back to sector 0*/
				cur_sector = 0;
				next_to_service = IORequestQueue.ceilingKey(cur_sector);
			}
			
			request = IORequestQueue.remove(next_to_service);
		
			/* service the request */
			if (request.is_write) {
				diskSchedulerLock.release();
				disk.writeSector(request.sector_no, request.buffer, request.offset);
				diskSchedulerLock.acquire();
			}
			
			else {
				diskSchedulerLock.release();
				disk.readSector(request.sector_no, request.buffer, request.offset);
				diskSchedulerLock.acquire();
			}
			request.waiting.wake();
			
			/* scan next */
			cur_sector = request.sector_no + 1;
			if (cur_sector >= num_sectors) {
				cur_sector = 0;
			}
		}
		
		schedulerActive = false;
		
		/* Notifies the kernel that we are done. */
		waitingForSchedulerToHalt.wake();
		
		diskSchedulerLock.release();
	}
	
	static class IORequest {
		public IORequest(Lock diskSchedulerLock) {
			waiting = new Condition2(diskSchedulerLock);
		}
		
		void initialise(int sector_no, byte[] buffer, int offset, boolean is_write) {
			this.sector_no = sector_no;
			this.buffer = buffer;
			this.offset = offset;
			this.is_write = is_write;
		}
		
		/** The number of the block to be written to or read from. */
		int sector_no;
		
		/** The buffer. */
		byte[] buffer;
		
		/** From the offset. */
		int offset;
		
		/** Write or read request. */
		boolean is_write;
	
		Condition2 waiting;
	}
	
	/** Access to the Nachos virtual machine disk. */
	private SynchDisk disk;

	/** The lock of the disk scheduler. */
	private Lock diskSchedulerLock;

	/** The IO request queue. */
	private TreeMap<Integer, IORequest> IORequestQueue;

	/** The unused IO request pool. */
	private LinkedList<IORequest> IORequestPool;

	/** Whether the scheduler thread is active. */
	private boolean schedulerActive;

	/** The disk scheduler should sleep on this condition when there's no request to service. */
	private Condition2 suspendScheduler;
	
	/** Whether the scheduler should finish after the IO request queue becomes empty. */
	private boolean finishScheduler;

	/** Waiting for the scheduler to halt and signal me. */
	private Condition2 waitingForSchedulerToHalt;
	
	public static final char dbgDiskScheduler = 'd';

/* Disk Info */
	
	private void initialiseDiskInfo() {
		sector_size = Disk.SectorSize;
		num_sectors = Disk.NumSectors;
		
		/* Disk.DiskSize includes the magic number, don't use it */
		raw_disk_size = sector_size * num_sectors;
	}
	
	private int sector_size;
	
	private int num_sectors;
	
	private int raw_disk_size;
	
/* ERROR */
	public static final int SUCCESS = 0;
	
	public static final int ERROR_IO_BUSY = 1;
	
	public static final int ERROR_INVALID_ARGUMENT = 2;

	public static final int ERROR_IO_ERROR = 3;
	
	public static final int ERROR_JAVA_FAIL = 4;
	
	public static final int ERROR_LOOP = 5;
	
	public static final int ERROR_PATH_TOO_LONG = 6;
	
	public static final int ERROR_NOT_DIR = 7;
	
	public static final int ERROR_FILE_NOT_FOUND = 8;
	
	public static final int ERROR_CORRUPT_FS = 9;
	
	public static final int ERROR_INSUFFICIENT_SPACE = 10;
	
	public static final String error_msg[] = {
		"success",
		"io busy",
		"invalid argument",
		"io error",
		"java error",
		
		"too many symlink followed",
		"path too long",
		"not a directory or symlink",
		"file or directory not found",
		"corrupt file system",
		"insufficient space",
	};
	
/* Miscellaneous */
	
	public static final char dbgFS = FilesysKernel.dbgFS;
	
	public static class Pair<T, V> {
		T first;
		V second;
		
		Pair(T t, V v) {
			first = t;
			second = v;
		}
	}
	
	public static class Quadruple<T, V, U, W> {
		T first;
		V second;
		U third;
		W fourth;
		
		Quadruple(T t, V v, U u, W w) {
			first = t;
			second = v;
			third = u;
			fourth = w;
		}
	}
	
	public static int unsigned_compare(short x, short y) {
		if (x == y) return 0;
		int xx = ((int) x) & 0xFFFF;
		int yy = ((int) y) & 0xFFFF;
		return (xx < yy) ? -1 : 1;
	}
	
	public static int unsigned_compare(int x, int y) {
		if (x == y) return 0;
		long xx = ((long) x) & 0xffffffffL;
		long yy = ((long) y) & 0xffffffffL;
		return (xx < yy) ? -1 : 1;
	}
	
	public static int unsigned_compare(short x, int y) {
		long xx = ((long) x) & 0xffffL;
		long yy = ((long) y) & 0xffffffffL;
		
		return (xx < yy) ? -1 : ((xx == yy) ? 0 : 1);
	}
	
	public static int unsigned_compare(int x, short y) {
		return -unsigned_compare(y, x);
	}
	
	public static int unsigned_short_to_int(short x) {
		return ((int) x) & 0xffff;
	}
	
	public static int unsigned_div_ceil(int x, int y) {
		long xl = ((long) x) & 0xffffffffl;
		long yl = ((long) y) & 0xffffffffl;
		return (int)((xl / yl) + ((xl % yl == 0) ? 0 : 1));
	}
	
	public static int unsigned_div_floor(int x, int y) {
		long xl = ((long) x) & 0xffffffffl;
		long yl = ((long) y) & 0xffffffffl;
		return (int)(xl / yl);
	}
	
	public static int unsigned_mod(int x, int y) {
		long xl = ((long) x) & 0xffffffffl;
		long yl = ((long) y) & 0xffffffffl;
		return (int)(xl % yl);
	}
	
	/**
	 * Checks if the intervals overlap.
	 * <p>
	 * The array may be modified.
	 * 
	 * @param intervals		an array of pair <x, y> which represents interval [x, y)
	 * @return				<tt>false</tt> if there's no overlap, <tt>true</tt> o.w.
	 */
	public static boolean check_overlap(Pair<Integer, Integer>[] intervals) {
	
		Arrays.sort(intervals, (Pair<Integer, Integer> int1, Pair<Integer, Integer> int2) -> {
			if (int1.first == int2.first) return 0;
			return (int1.first < int2.first) ? -1 : 1;
		});
		
		for (int i = 0; i < intervals.length - 1; ++i) {
			if (intervals[i].second > intervals[i+1].first) return true;
		}
		
		return false;
	}
	
	public static Pair<Integer, Integer> get_interval(short offset, int length) {
		int ioff = unsigned_short_to_int(offset);
		return new Pair<Integer, Integer>(ioff, ioff + length);
	}

	
	public static String getCanonicalPath(String cur_path, String path) {
		if (path.startsWith("/")) {
			return getCanonicalPath(path);
		}
		
		else {
			if (cur_path.endsWith("/")) {
				return getCanonicalPath(cur_path + path);
			}
			
			else {
				return getCanonicalPath(cur_path + "/" + path);
			}
		}
	}
	
	public static String getCanonicalPath(String path) {
		if (!path.startsWith("/")) return null;
		
		LinkedList<String> paths = new LinkedList<String>();
		int p = 1, np;
		
		while (p < path.length()) {
			np = path.indexOf('/', p);
			if (np == -1) np = path.length();
			
			if (np > p) {
				String name = path.substring(p, np);
				if (name.equals(".")) {
					/* stay here */
				}
				else if (name.equals("..")){
					paths.pollLast();
				}
				else {
					paths.add(name);
				}
			}
			
			p = np + 1;
		}
	
		if (paths.isEmpty()) return "/";
		
		StringBuilder builder = new StringBuilder();
		
		for (String name : paths) {
			builder.append("/").append(name);
		}
		
		return builder.toString();
	}
	
	static class IntPtr implements POD {
		IntPtr() { this.x = 0; }
		IntPtr(int x) {this.x = x;}
		int x;
		@Override
		public boolean toBytes(byte[] data, int offset) {
			if (offset + 4 > data.length) return false;
			Lib.bytesFromInt(data, offset, x);
			return true;
		}
		@Override
		public boolean fromBytes(byte[] data, int offset) {
			if (offset + 4 > data.length) return false;
			x = Lib.bytesToInt(data, offset);
			return true;
		}
		@Override
		public int struct_size() {
			return 0x4;
		}
	}
	
	static class ShortPtr implements POD {
		ShortPtr() { this.x = 0; }
		ShortPtr(short x) { this.x =x; }
		short x;
		@Override
		public boolean toBytes(byte[] data, int offset) {
			if (offset + 2 > data.length) return false;
			Lib.bytesFromShort(data, offset, x);
			return true;
		}
		@Override
		public boolean fromBytes(byte[] data, int offset) {
			if (offset + 2 > data.length) return false;
			x = Lib.bytesToShort(data, offset);
			return true;
		}
		@Override
		public int struct_size() {
			return 0x2;
		}
	}
	
	static class BytePtr implements POD {
		BytePtr() { this.x = 0; }
		BytePtr(byte x) { this.x =x; }
		byte x;
		@Override
		public boolean toBytes(byte[] data, int offset) {
			if (offset + 1 > data.length) return false;
			data[offset] = x;
			return true;
		}
		@Override
		public boolean fromBytes(byte[] data, int offset) {
			if (offset + 1 > data.length) return false;
			x = data[offset];
			return true;
		}
		@Override
		public int struct_size() {
			return 0x1;
		}
	}
	
	static public boolean str_match(byte[] str1, int start, int end, String str2, int start2) {
		while (start != end) {
			if (str1[start] != str2.charAt(start2)) return false;
			++start; ++start2;
		}
		return true;
	}
	
	static String bytesToString(byte[] buffer, int offset, int length) {
		return new String(buffer, offset, length);
	}

	static String removeTrailingSlash(String path) {
		int i = path.length() - 1;
		while (path.charAt(i) == '/' && i >= 0) --i;
		if (i < 0) return "/";
		else return path.substring(0, i + 1);
	}
	
	static byte[] zeros = new byte[Disk.SectorSize];
	
	static String getCanonicalPath(LinkedList<String> paths) {
		if (paths.isEmpty()) return "/";
		
		int sz = 0;
		for (String n : paths) sz += n.length();
		sz += paths.size();
		
		StringBuilder builder = new StringBuilder();
		builder.ensureCapacity(sz);
		for (String n : paths) builder.append("/").append(n);
		return builder.toString();
	}
}
