package nachos.filesys;

import nachos.machine.Lib;

public class Inode implements POD {

	/** allocation status: 0:0, file type: 0:1 ~ 0:2, inlined: 0:3,
	 * first part of reserved0: 0:4 ~ 0:7 */
	public byte byte0 = 0;
	
	/** second part of reserved0: 0x1 ~ 0x1 */
	public byte byte1 = 0;

	/** 0x2 ~ 0x3 */
	public short num_allocated_sectors;

	/** 0x4 ~ 0x7 */
	public int size;

	/** 0x8 ~ 0x9 */
	public short ref_count;

	/** 0xa ~ 0xb */
	public short reserved1 = 0;

	/** 0xc ~ 0x1b */
	public short[] direct_ptr = new short[8];

	/** 0x1c ~ 0x1d */
	public short single_ind_ptr;

	/** 0x1e ~ 0x1f */
	public short double_ind_ptr;
	
	
	
	@Override
	public boolean toBytes(byte[] data, int offset) {
		if (offset + 0x20 > data.length) {
			return false;
		}
	
		data[offset + 0x0] = byte0;
		data[offset + 0x1] = byte1;
		Lib.bytesFromShort(data, offset + 0x2, num_allocated_sectors);
		Lib.bytesFromInt(data, offset + 0x4, size);
		Lib.bytesFromShort(data, offset + 0x8, ref_count);
		Lib.bytesFromShort(data, offset + 0xa, reserved1);
		
		for (int i = 0; i < 8; ++i) {
			Lib.bytesFromShort(data, offset + 0xc + (i << 1), direct_ptr[i]);
		}
		Lib.bytesFromShort(data, offset + 0x1c, single_ind_ptr);
		Lib.bytesFromShort(data, offset + 0x1e, double_ind_ptr);
		
		return true;
	}

	@Override
	public boolean fromBytes(byte[] data, int offset) {
		if (offset + 0x20 > data.length) {
			return false;
		}
		
		byte0 = data[offset + 0x0];
		byte1 = data[offset + 0x1];
		num_allocated_sectors = Lib.bytesToShort(data, offset + 0x2);
		size = Lib.bytesToInt(data, offset + 0x4);
		ref_count = Lib.bytesToShort(data, offset + 0x8);
		reserved1 = Lib.bytesToShort(data, offset + 0xa);
		
		for (int i = 0; i < 8; ++i) {
			direct_ptr[i] = Lib.bytesToShort(data, offset + 0xc + (i << 1));
		}
		single_ind_ptr = Lib.bytesToShort(data, offset + 0x1c);
		double_ind_ptr = Lib.bytesToShort(data, offset + 0x1e);
		
		return true;
	}

	public static final byte ALLOC_STATUS_MASK = 0x1;
	public static final byte ALLOCATED = 0x1;
	public static final byte FREE = 0X0;

	public static final byte FILETYPE_MASK = 0x6;
	public static final byte FILETYPE_NORMAL = 0x0;
	public static final byte FILETYPE_DIR = 0X2;
	public static final byte FILETYPE_SYMLINK = 0X4;
	
	public static final byte INLINE_MASK = 0X8;
	public static final byte INLINED = 0x8;
	public static final byte NOT_INLINED = 0x0;
	
	public boolean allocated() {
		return (byte0 & ALLOC_STATUS_MASK) == ALLOCATED;
	}
	
	public void allocated(boolean allocation_status) {
		if (allocation_status) {
			byte0 = (byte) (byte0 & ~ALLOC_STATUS_MASK | ALLOCATED); 
		} 
		else {
			byte0 = (byte) (byte0 & ~ALLOC_STATUS_MASK);
		}
	}
	
	public byte filetype() {
		return (byte) (byte0 & FILETYPE_MASK);
	}
	
	public void filetype(byte type) {
		byte0 = (byte) (byte0 & ~FILETYPE_MASK | type);
	}
	
	public boolean is_normal_file() {
		return filetype() == FILETYPE_NORMAL;
	}
	
	public boolean is_directory() {
		return filetype() == FILETYPE_DIR;
	}
	
	public boolean is_symlink() {
		return filetype() == FILETYPE_SYMLINK;
	}
	
	public boolean inlined() {
		return (byte0 & INLINE_MASK) == INLINED;
	}
	
	public void inlined(boolean inlined) {
		if (inlined) {
			byte0 = (byte) (byte0 & ~INLINE_MASK | INLINED);
		}
		else {
			byte0 = (byte) (byte0 & ~INLINE_MASK);
		}
	}
	
	public int num_allocated_sectors() {
		return NachosFS.unsigned_short_to_int(num_allocated_sectors);
	}
	
	public int size() {
		return size;
	}
	
	public int ref_count() {
		return NachosFS.unsigned_short_to_int(ref_count);
	}
	
	public int direct_ptr(int index) {
		return NachosFS.unsigned_short_to_int(direct_ptr[index]);
	}
	
	public int single_ind_ptr() {
		return NachosFS.unsigned_short_to_int(single_ind_ptr);
	}
	
	public int double_ind_ptr() {
		return NachosFS.unsigned_short_to_int(double_ind_ptr);
	}
	
	public boolean get_inlined_data(byte[] buffer, int offset) {
		if (offset + 20 > buffer.length) return false;
	
		for (int i = 0; i < 8; ++i) {
			Lib.bytesFromShort(buffer, offset + (i << 1), direct_ptr[i]);
		}
		
		Lib.bytesFromShort(buffer, offset + 16, single_ind_ptr);
		Lib.bytesFromShort(buffer, offset + 18, double_ind_ptr);
		
		return true;
	}
	
	public boolean set_inlined_data(byte[] buffer, int offset) {
		if (offset + 20 > buffer.length) return false;
		
		for (int i = 0; i < 8; ++i) {
			direct_ptr[i] = Lib.bytesToShort(buffer, offset + (i << 1));
		}
		single_ind_ptr = Lib.bytesToShort(buffer, offset + 16);
		double_ind_ptr = Lib.bytesToShort(buffer, offset + 18);
		
		return true;
	}

	@Override
	public int struct_size() {
		// TODO Auto-generated method stub
		return 0x20;
	}
}
