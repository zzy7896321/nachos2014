package nachos.filesys;

import nachos.machine.Lib;

/**
 * struct extended_filename_entry;
 *
 */
public class ExtFileNameEntry implements POD {

	/** 0x0 ~ 0X1 */
	public short parent_entry_no;

	/** magic number: 2:0 ~ 2:3, ext_file_name_index: 2:4 ~ 2:5, type: 2:6 ~ 2:7 */
	public byte byte2 = MAGIC_NUMBER;

	/** 0x3 ~ 0X7f, 0x7e ~ 0x7f is next block no. if type == head */
	public byte[] content = new byte[125];
	
	public int parent_entry_no() {
		return NachosFS.unsigned_short_to_int(parent_entry_no);
	}
	
	public void parent_entry_no(int no) {
		parent_entry_no = (short) no;
	}
	
	public boolean is_head() {
		return (byte2 & TYPE_MASK) == TYPE_HEAD;
	}
	
	public void is_head(boolean h) {
		if (h) {
			byte2 = (byte) (byte2 & ~TYPE_MASK | TYPE_HEAD);
		}
		else {
			byte2 = (byte) (byte2 & ~TYPE_MASK | TYPE_TAIL);
		}
	}
	
	public int ext_file_name_index() {
		return (byte2 >>> 4) & 0x3;
	} 
	
	public void ext_file_name_index(int index) {
		byte2 = (byte) (byte2 & ~(0x3 << 4) | ((index & 0x3) << 4));
	}
	
	public int next_block_no() {
		return NachosFS.unsigned_short_to_int(Lib.bytesToShort(content, 0x7e));
	}
	
	public void next_block_no(int no) {
		Lib.bytesFromShort(content, 0x7e, (short) no);
	}
	
	@Override
	public boolean toBytes(byte[] data, int offset) {
		if (offset + 0x80 > data.length) return false;
	
		Lib.bytesFromShort(data, offset + 0x0, parent_entry_no);
		data[offset + 0x2] = byte2;
		System.arraycopy(content, 0, data, offset + 0x3, 125);
		
		return true;
	}

	@Override
	public boolean fromBytes(byte[] data, int offset) {
		if (offset + 0x80 > data.length) return false;
		
		parent_entry_no = Lib.bytesToShort(data, offset + 0x0);
		byte2 = data[offset + 0x2];
		System.arraycopy(data, offset + 0x3, content, 0, 125);
		
		return true;
	}

	public static final byte MAGIC_NUMBER = 0xb;
	
	public static final byte TYPE_MASK = 0xc;
	public static final byte TYPE_HEAD = 0x0;
	public static final byte TYPE_TAIL = 0xc;

	@Override
	public int struct_size() {
		// TODO Auto-generated method stub
		return 0x80;
	}
}
