package nachos.filesys;

import nachos.machine.Lib;

public class NachosFSHeader implements POD {

	/** 0x0 ~ 0x3 */
	public int magic_number;

	/** 0x4 ~ 0x5 */
	public short sector_size;
	
	/** 0x6 ~ 0x7 */
	public short num_sectors;

	/** 0x8 ~ 0x9 */
	public short swap_offset;

	/** 0xa ~ 0xb */
	public short swap_length;

	/** 0xc ~ 0xd */
	public short nachosfs_offset;

	/** 0xe ~ 0xf */
	public short nachosfs_length;

	/** 0x10 */
	public byte fs_state;

	/** 0x11 ~ 0x1f */
	public byte[] reserved0 = new byte[15];

	/** 0x20 ~ 0x21 */
	public short inode_count;

	/** 0x22 ~ 0x23 */
	public short block_count;

	/** 0x24 ~ 0x25 */
	public short num_free_inodes;

	/** 0x26 ~ 0x27 */
	public short num_free_blocks;

	/** 0x28 ~ 0x29 */
	public short block_group_offset;
	
	/** 0x2a ~ 0x2b */
	public short block_group_size;

	/** 0x2c ~ 0x2d */
	public short num_block_groups;

	/** 0x2e ~ 0x2f */
	public short bitmap_offset;

	/** 0x30 ~ 0x31 */
	public short num_bitmap_blocks;
	
	/** 0x32 ~ 0x33 */
	public short root_inode_number;

	/** 0x34 ~ 0x1ff */
	public byte[] reserved1 = new byte[460];

	@Override
	public boolean toBytes(byte[] data, int offset) {
		if (offset + 0x200 > data.length) return false;

		/* partition table */
		Lib.bytesFromInt(data, offset + 0x0, magic_number);
		Lib.bytesFromShort(data, offset + 0x4, sector_size);
		Lib.bytesFromShort(data, offset + 0x6, num_sectors);
		Lib.bytesFromShort(data, offset + 0x8, swap_offset);
		Lib.bytesFromShort(data, offset + 0xa, swap_length);
		Lib.bytesFromShort(data, offset + 0xc, nachosfs_offset);
		Lib.bytesFromShort(data, offset + 0xe, nachosfs_length);
		data[offset + 0x10] = fs_state;

		/* reserved 0 */
		System.arraycopy(reserved0, 0, data, offset + 0x11, 0x20 - 0x11);
		
		/* nachos fs header */
		Lib.bytesFromShort(data, offset + 0x20, inode_count);
		Lib.bytesFromShort(data, offset + 0x22, block_count);
		Lib.bytesFromShort(data, offset + 0x24, num_free_inodes);
		Lib.bytesFromShort(data, offset + 0x26, num_free_blocks);
		Lib.bytesFromShort(data, offset + 0x28, block_group_offset);
		Lib.bytesFromShort(data, offset + 0x2a, block_group_size);
		Lib.bytesFromShort(data, offset + 0x2c, num_block_groups);
		Lib.bytesFromShort(data, offset + 0x2e, bitmap_offset);
		Lib.bytesFromShort(data, offset + 0x30, num_bitmap_blocks);
		Lib.bytesFromShort(data, offset + 0x32, root_inode_number);
		
		/* reserved 1 */
		System.arraycopy(reserved1, 0, data, offset + 0x34, 0x200 - 0x34);
	
		return true;
	}

	@Override
	public boolean fromBytes(byte[] data, int offset) {
		if (offset + data.length > 0x200) return false;
	
		/* partition table */
		magic_number = Lib.bytesToInt(data, offset + 0x0);
		sector_size = Lib.bytesToShort(data, offset + 0x4);
		num_sectors = Lib.bytesToShort(data, offset + 0x6);
		swap_offset = Lib.bytesToShort(data, offset + 0x8);
		swap_length = Lib.bytesToShort(data, offset + 0xa);
		nachosfs_offset = Lib.bytesToShort(data, offset + 0xc);
		nachosfs_length = Lib.bytesToShort(data, offset + 0xe);
		fs_state = data[offset + 0x10];
	
		/* reserved 0 */
		System.arraycopy(data, offset + 0x11, reserved0, 0, 0x20 - 0x11);
	
		/* nachos fs header */
		inode_count = Lib.bytesToShort(data, offset + 0x20);
		block_count = Lib.bytesToShort(data, offset + 0x22);
		num_free_inodes = Lib.bytesToShort(data, offset + 0x24);
		num_free_blocks = Lib.bytesToShort(data, offset + 0x26);
		block_group_offset = Lib.bytesToShort(data, offset + 0x28);
		block_group_size = Lib.bytesToShort(data, offset + 0x2a);
		num_block_groups = Lib.bytesToShort(data, offset + 0x2c);
		bitmap_offset = Lib.bytesToShort(data, offset + 0x2e);
		num_bitmap_blocks = Lib.bytesToShort(data, offset + 0x30);
		root_inode_number = Lib.bytesToShort(data, offset + 0x32);
	
		/* reserved 1 */
		System.arraycopy(data, offset + 0x34, reserved1, 0, 0x200 - 0x34);
		
		return true;
	}

	@Override
	public int struct_size() {
		return 0x200;
	}
}
