package nachos.filesys;

import nachos.machine.Lib;

/**
 * struct DirectoryEntry;
 * 
 */
public class DirectoryEntry implements POD {

	/** 0x0 ~ 0x1 */
	public short inode_no;

	/** magic number: 2:0 ~ 2:3, ext. file entry index 0 : 2:4 ~ 2:5, reserved0: 2:6 ~ 2:7 */
	public byte byte2 = MAGIC_NUMBER;

	/** 0x3 */
	public byte file_name_length;

	/** 0x4 ~ 0xf, 0xe ~ 0xf may be ext. file name block no. 0 if file_name_length > 12 */
	public byte[] primary_file_name = new byte[12];

	public int inode_no() {
		return NachosFS.unsigned_short_to_int(inode_no);
	}
	
	public int ext_file_name_entry_index0() {
		return (byte2 >>> 4) & 0x3;
	}
	
	public void ext_file_name_entry_no(int no) {
		byte2 = (byte) (byte2 & ~(0x3 << + 4) | ((no & 0x3) << 4));
	}
	
	public int ext_file_name_block_no() {
		return ((int) (Lib.bytesToShort(primary_file_name, 10))) & 0xffff;
	}
	
	public void ext_file_name_block_no(int no) {
		Lib.bytesFromShort(primary_file_name, 10, (short) no);
	}
	
	public int file_name_length() {
		return ((int) file_name_length) & 0xff;
	}
	
	@Override
	public boolean toBytes(byte[] data, int offset) {
		if (offset + 0x10 > data.length) return false;
	
		Lib.bytesFromShort(data, offset + 0x0, inode_no);
		data[offset + 0x2] = byte2;
		data[offset + 0x3] = file_name_length;
		System.arraycopy(primary_file_name, 0, data, offset + 0x4, 12);
		
		return true;
	}

	@Override
	public boolean fromBytes(byte[] data, int offset) {
		if (offset + 0x10 > data.length) return false;
		
		inode_no = Lib.bytesToShort(data, offset + 0x0);
		byte2 = data[offset + 0x2];
		file_name_length = data[offset + 0x3];
		System.arraycopy(data, offset + 0x4, primary_file_name, 0, 12);
		
		return true;
	}

	public static final byte MAGIC_NUMBER = 0x7;

	@Override
	public int struct_size() {
		return 0x10;
	}
}
