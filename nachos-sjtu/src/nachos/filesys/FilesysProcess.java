package nachos.filesys;

import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.threads.KThread;
import nachos.userprog.UThread;
import nachos.vm.VMProcess;

/**
 * FilesysProcess is used to handle syscall and exception through some callback methods.
 * 
 * @author starforever
 */
public class FilesysProcess extends VMProcess {

	public static int get_cwd_inode_no() {
		int cwd_inode_no;
		KThread kthread = KThread.currentThread();
		FilesysProcess cur_process = null;
		if (kthread instanceof UThread) {
			UThread uthread = (UThread) kthread;
			if (uthread.process instanceof FilesysProcess) {
				cur_process = (FilesysProcess) uthread.process;
			}
		}
		
		if (cur_process == null) {
			cwd_inode_no = FilesysKernel.nachosFS.rootInodeNo();
		}
		else {
			cwd_inode_no = cur_process.cwd_inode_no;
		}
		
		return cwd_inode_no;
	}
	
	@Override
	public UThread execute(String name, String[] args) {
		cwd_inode_no = get_cwd_inode_no();
		
		return super.execute(name, args);
	}
	
	public int handleMkdir(int pstrName) {
		String fileName = readVirtualMemoryString(pstrName, maxPathLength);
		if (fileName == null) return -1;
		
		boolean result = FilesysKernel.nachosFS.mkdir(cwd_inode_no, fileName);
		if (result) {
			return 0;
		}
		else {
			return -1;
		}
	}
	
	public int handleRmdir(int pstrName) {
		String fileName = readVirtualMemoryString(pstrName, maxPathLength);
		if (fileName == null) return -1;
		
		boolean result = FilesysKernel.nachosFS.rmdir(cwd_inode_no, fileName);
		if (result) {
			return 0;
		}
		else {
			return -1;
		}
	}
	
	public int handleChdir(int pstrName) {
		String fileName = readVirtualMemoryString(pstrName, maxPathLength);
		if (fileName == null) return -1;
		
		int new_wd = FilesysKernel.nachosFS.chdir(cwd_inode_no, fileName);
		if (new_wd == NachosFS.INODE_NULL) {
			return -1;
		}
		else {
			cwd_inode_no = new_wd;
			return 0;
		}
	}
	
	public int handleGetcwd(int pstrBuffer, int size) {
		String cwd = FilesysKernel.nachosFS.getcwd(cwd_inode_no);
		
		if (cwd == null) {
			return -1;
		}
		
		byte[] buf = cwd.getBytes();
		if (buf.length + 1 > size) {
			return -1;
		}
		
		int result = writeNullTerminatedString(pstrBuffer, buf);
		if (result != buf.length + 1) {
			return -1;
		}
		else {
			return buf.length;
		}
	}
	
	public int handleReaddir(int pstrDirName, int pstrBuffer, int size, int namesize) {
		String fileName = readVirtualMemoryString(pstrDirName, maxPathLength);
		if (fileName == null) return -1;
		
		String[] buffer = FilesysKernel.nachosFS.readdir(cwd_inode_no, fileName);
		if (buffer == null) {
			return -1;
		}
		
		if (buffer.length > size) {
			return -1;
		}
		
		for (int i = 0; i < buffer.length; ++i) {
			byte[] buf = buffer[i].getBytes();
			if (buf.length + 1 > namesize) {
				return -1;
			}
			
			int result = writeNullTerminatedString(pstrBuffer + i * namesize, buf);
			if (result != buf.length + 1) {
				return -1;
			}
		}
		
		return buffer.length;
	}
	
	public int handleStat(int pstrName, int pStat) {
		String fileName = readVirtualMemoryString(pstrName, maxPathLength);
		if (fileName == null) return -1;
		
		FileStat stat = FilesysKernel.nachosFS.stat(cwd_inode_no, fileName);
		if (stat == null) {
			return -1;
		}
		
		byte[] name = stat.name.getBytes();
		if (name.length + 1 > FileStat.FILE_NAME_MAX_LEN) {
			/* ?? */
			return -1;
		}
		
		int num_written = writeNullTerminatedString(pStat + 0, name);
		if (num_written != name.length + 1) {
			return -1;
		}
		
		try {
			sw(pStat + 256, stat.size);
			sw(pStat + 260, stat.sectors);
			sw(pStat + 264, stat.type);
			sw(pStat + 268, stat.inode);
			sw(pStat + 272, stat.links);
		} catch (LoadStoreFailure e) {
			return -1;
		}
		
		return 0;
	}
	
	public int handleLink(int pstrOldName, int pstrNewName) {
		String oldName = readVirtualMemoryString(pstrOldName, maxPathLength);
		if (oldName == null) return -1;
		
		String newName = readVirtualMemoryString(pstrNewName, maxPathLength);
		if (newName == null) return -1;
	
//		System.err.printf("link %s %s\n", oldName, newName);
		boolean result = FilesysKernel.nachosFS.link(cwd_inode_no, oldName, newName);
		if (result) {
			return 0;
		}
		else {
			return -1;
		}
		
	}
	
	public int handleSymlink(int pstrOldName, int pstrNewName) {
		String oldName = readVirtualMemoryString(pstrOldName, maxPathLength);
		if (oldName == null) return -1;
		
		String newName = readVirtualMemoryString(pstrNewName, maxPathLength);
		if (newName == null) return -1;
		
		boolean result = FilesysKernel.nachosFS.symlink(cwd_inode_no, oldName, newName);
		if (result) {
			return 0;
		}
		else {
			return -1;
		}
	}

	/**
	 * Writes the buffer to the memory with a NULL appended to it.
	 * 
	 * @param vaddr
	 * @param buffer
	 * @return
	 */
	private int writeNullTerminatedString(int vaddr, byte[] buffer) {
		int num_written = writeVirtualMemory(vaddr, 1, buffer, 0, buffer.length);
		if (num_written != buffer.length) {
			return num_written;
		}
		try {
			sb(vaddr + buffer.length, (byte) 0);
			++num_written;
		} catch (LoadStoreFailure e) {
		}
		
		return num_written;
	}
	
	
	@Override
	public int handleSyscall (int syscall, int a0, int a1, int a2, int a3) {
		switch (syscall) {
		case SYSCALL_MKDIR:
			return handleMkdir(a0);

		case SYSCALL_RMDIR:
			return handleRmdir(a0);

		case SYSCALL_CHDIR:
			return handleChdir(a0);

		case SYSCALL_GETCWD:
			return handleGetcwd(a0, a1);

		case SYSCALL_READDIR:
			return handleReaddir(a0, a1, a2, a3);

		case SYSCALL_STAT:
			return handleStat(a0, a1);

		case SYSCALL_LINK:
			return handleLink(a0, a1);
			
		case SYSCALL_SYMLINK: 
			return handleSymlink(a0, a1);
			
		default:
			return super.handleSyscall(syscall, a0, a1, a2, a3);
		}
	}
	
	protected int cwd_inode_no;
	
	/* no need to override handleException */
	
	protected static final int SYSCALL_MKDIR = 14;
	protected static final int SYSCALL_RMDIR = 15;
	protected static final int SYSCALL_CHDIR = 16;
	protected static final int SYSCALL_GETCWD = 17;
	protected static final int SYSCALL_READDIR = 18;
	protected static final int SYSCALL_STAT = 19;
	protected static final int SYSCALL_LINK = 20;
	protected static final int SYSCALL_SYMLINK = 21;
}
