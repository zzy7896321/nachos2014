package nachos.filesys;

/**
 * This object can initialised from or converted to a byte array.
 *
 */
public interface POD {

	/**
	 * Converts the structure to a byte sequence.
	 * 
	 * @param data	the buffer
	 * @param offset	the offset from which to write
	 * @return		<tt>true</tt> on success, <tt>false</tt> if the buffer is too small
	 */
	public boolean toBytes(byte[] data, int offset);

	/**
	 * Initialised the structure with the given byte sequence.
	 * 
	 * @param data	the bytes
	 * @param offset	the offset from which to read
	 * @return		<tt>true</tt> on success, <tt>false</tt> if 
	 * 				the data is too short or invalid
	 */
	public boolean fromBytes(byte[] data, int offset);

	/**
	 * Sizeof this struct.
	 * 
	 * @return	the size of this struct (in bytes)
	 */
	public int struct_size();
}
