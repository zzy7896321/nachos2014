package nachos.filesys;

import nachos.machine.Disk;
import nachos.vm.SwapFile;

/**
 * SwapFile implementation using NachosFS. Whenever the swap partition
 * has space, the swap partition is used. When swap partition is used up,
 * a swap file will be opened to use.
 * 
 */
public class NachosFSSwapFile extends SwapFile {

	/**
	 * Constructs a NachosFSSwapFile object.
	 * The object is not initialised until initialise() is called;
	 */
	public NachosFSSwapFile() {
		/* do nothing */
		super();
	}

	/**
	 * Initialises the swap file. 
	 */
	public void initialise() {
		/* nothing to do */
		nachos.machine.Lib.assertTrue(pageSize % nachos.machine.Disk.SectorSize == 0);
	}
	
	@Override
	public boolean read(int spn, byte[] buffer, int pn) {
		if (spn < 0 || spn >= maxNumberPages() || buffer == null
				|| buffer.length != pageSize) {
			return false;
		}
		
		int sector_offset = FilesysKernel.nachosFS.swapOffset() + spn * sectors_per_page;
		
		for (int i = 0; i < sectors_per_page; ++i) {
			int io_result = FilesysKernel.nachosFS.readBlock(NachosFS.SWAP_CACHE,
					sector_offset + i, buffer, i * Disk.SectorSize);
			if (io_result != NachosFS.SUCCESS) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public boolean write(int spn, byte[] buffer, int pn) {
		if (spn < 0 || spn >= maxNumberPages() || buffer == null
				|| buffer.length != pageSize) {
			return false;
		}
		
		int sector_offset = FilesysKernel.nachosFS.swapOffset() + spn * sectors_per_page;
		
		for (int i = 0; i < sectors_per_page; ++i) {
			int io_result = FilesysKernel.nachosFS.writeBlock(NachosFS.SWAP_CACHE,
					sector_offset + i, buffer, i * Disk.SectorSize);
			if (io_result != NachosFS.SUCCESS) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public void close() {
		/* nothing to do */
	}

	@Override
	public int maxNumberPages() {
		return FilesysKernel.nachosFS.numSwapSectors();
	}
	
	static final protected int sectors_per_page = pageSize / nachos.machine.Disk.SectorSize;
}
