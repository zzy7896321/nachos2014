package nachos.threads;

import java.util.Arrays;
import nachos.machine.Lib;

/**
 * A priority queue. Objects are popped from the minimum to the maximum according to the comparator.
 * Handles to queue nodes are returned when inserting a new object. The node can be removed or inserted
 * to another PriorityQueue with the handles.
 * 
 * @author ZZy
 *
 */
public class PriorityQueue<T> implements Iterable<T> {
	
	static private class Node<T> {
		public T object;
		public int index;
	}
	
	/**
	 * A handle to priority queue node.
	 * 
	 * @author ZZy
	 *
	 */
	public class NodeHandle {
		/**
		 * Constructs a node handle to the node.
		 * @param node	the node
		 */
		private NodeHandle(Node<T> node) {
			this.node = node;
		}
		
		/**
		 * Retrieves the object in the node.
		 * 
		 * @return	the object in the node
		 */
		public T object() {
			return node.object;
		}
		
		/**
		 * Removes the node from the priority queue.
		 * Don't call this twice.
		 */
		public void remove() {
			Node<T> lastnode = data.remove(data.size() - 1);
			int i = node.index;
			if (i < data.size()) {
				lastnode.index = i;
				data.set(i, lastnode);
				int comp_result = comparator.compare(node.object, lastnode.object);
				if (comp_result > 0) {
					up(i);
				}
				else if (comp_result < 0) {
					down(i);
				}
			}
			node.index = -1;
		}
		
		/**
		 * Add the node to another priority queue. Call remove before call this.
		 * After the call, the old handle is no longer valid.
		 * 
		 * @param queue		the queue to add the node to
		 * @return			the new handle to the node
		 */
		public NodeHandle addTo(PriorityQueue<T> queue) {
			return queue.add(node);
		}
		
		/**
		 * Inform the queue that the object has been updated.
		 */
		public void updated() {
			if (up(node.index)) {
				down(node.index);
			}
		}
		
		/**
		 * Checks whether the node is in the queue.
		 * 
		 * @param queue		the queue
		 * @return			<tt>true</tt> if the node is in the queue, or <tt>false</tt> o.w.
		 */
		public boolean in(PriorityQueue<T> queue) {
			return node.index != -1 && PriorityQueue.this == queue;
		}
		
		private Node<T> node;
	}
	
	private class Iterator implements java.util.Iterator<T> {
		private Iterator() {
			iterator = PriorityQueue.this.data.iterator();
		}

		@Override
		public boolean hasNext() {
			return iterator.hasNext();
		}

		@Override
		public T next() {
			return iterator.next().object;
		}
		
		java.util.Iterator<Node<T>> iterator;
	}
	
	/**
	 * Constructs an empty priority queue with the comparator.
	 * The higher priority is given to the larger element according to the comparator.
	 * 
	 * @param comparator	the comparator
	 */
	public PriorityQueue(java.util.Comparator<? super T> comparator) {
		this.comparator = comparator;
	}
	
	/**
	 * Adds an object to the queue.
	 * 
	 * @param object	the object to add
	 * @return			a handle to the node
	 */
	public NodeHandle add(T object) {
		Node<T> node = new Node<T>();
		node.object = object;
		return add(node);
	}
	
	/**
	 * Adds the node to the queue.
	 * 
	 * @param node		the node to add
	 * @return			a handle to the node
	 */
	private NodeHandle add(Node<T> node) {
		node.index = data.size();
		data.add(node);
		up(node.index);
		return new NodeHandle(node);
	}
	
	/**
	 * Peeks the first object in the queue.
	 * 
	 * @return	the first object in the queue, or <tt>null</tt> o.w.
	 */
	public T peek() {
		if (data.isEmpty()) return null;
		return data.get(0).object;
	}
	
	/**
	 * Retrieves and removes the first object from the queue.
	 * 
	 * @return	the first object in the queue, or <tt>null</tt> o.w.
	 */
	public T poll() {
		if (data.isEmpty()) return null;
		Node<T> node = data.get(0);
		if (data.size() == 1) {
			data.clear();
		}
		else {
			Node<T> lastnode = data.remove(data.size() - 1);
			lastnode.index = 0;
			data.set(0, lastnode);
			down(0);
		}
		return node.object;
	}
	
	/**
	 * Checks whether the queue is empty
	 * 
	 * @return	<tt>true</tt> if the queue is empty, or <tt>false</tt> o.w.
	 */
	public boolean empty() {
		return data.isEmpty();
	}
	
	/**
	 * Try moving up the ith node along the tree.
	 * 
	 * @param i		the index of the node to be moved
	 * @return		<tt>true</tt> if no move is performed, or <tt>false</tt> o.w.
	 */
	private boolean up(int i) {
		Node<T> inode = data.get(i);
		while (i > 0) {
			int j = ((i + 1) >> 1) - 1;
			Node<T> jnode = data.get(j);
			if (comparator.compare(jnode.object, inode.object) <= 0) {
				break;
			}
			else {
				data.set(i, jnode);
				jnode.index = i;
				i = j;
			}
		}
		if (inode.index != i) {
			inode.index = i;
			data.set(i, inode);
			return false;
		}
		return true;
	}
	
	/**
	 * Try moving down the ith node along the tree.
	 * 
	 * @param i		the index of the node to be moved
	 * @return		<tt>true</tt> if no move is performed, or <tt>false</tt> o.w.
	 */
	private boolean down(int i) {
		Node<T> inode = data.get(i);
		int max_index = data.size() - 1;
		int j = (i << 1) + 1;
		while (j <= max_index) {
			Node<T> jnode = data.get(j);
			if (j < max_index) {
				Node<T> jp1node = data.get(j+1);
				if (comparator.compare(jnode.object, jp1node.object) > 0) {
					j = j + 1;
					jnode = jp1node;
				}
			}
			if (comparator.compare(inode.object, jnode.object) < 0) {
				break;
			}
			else {
				data.set(i, jnode);
				jnode.index = i;
				i = j;
				j = (i << 1) + 1;
			}
		}
		if (inode.index != i) {
			inode.index = i;
			data.set(i, inode);
			return false;
		}
		return true;
	}
	
	private java.util.ArrayList<Node<T>> data = new java.util.ArrayList<Node<T>>();
	private java.util.Comparator<? super T> comparator;
	
	@Override
	public java.util.Iterator<T> iterator() {
		return new Iterator();
	}
	
	public static void selfTest() {
		System.out.println("nachos.threads.PriorityQueue self test.");
		final int num_elements = 100000;
		final int range = 10000000;
		int[] A = new int[num_elements];
		PriorityQueue<Integer> pq = new PriorityQueue<Integer>((Integer x, Integer y) -> x - y);
		
		for (int i = 0; i < num_elements; ++i) {
			A[i] = Lib.random(range);
			pq.add(A[i]);
		}
		
		Arrays.sort(A);
		for (int i = 0; i < num_elements; ++i) {
			Lib.assertTrue(!pq.empty() && A[i] == pq.poll());
		}
		Lib.assertTrue(pq.empty());
		
		System.out.println("nachos.threads.PriorityQueue self test done.");
	}
	
}
