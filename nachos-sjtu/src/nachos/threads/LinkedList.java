package nachos.threads;

/**
 * A linked list. Objects can be put to the back of the list and retrieved from the front
 * of the list. Handles to list node will be returned on adding an element. The node can be
 * removed or added to another list with the handle.
 * 
 * @author ZZy
 *
 */
public class LinkedList<T> implements Iterable<T> {
	static private class Node<T> {
		public T object;
		public Node<T> prev;
		public Node<T> next;
	}
	
	private class Iterator implements java.util.Iterator<T>{
		private Iterator() {
			node = root;
		}
		
		@Override
		public boolean hasNext() {
			return node.next != root;
		}

		@Override
		public T next() {
			node = node.next;
			return node.object;
		}
		
		Node<T> node;
	}

	/**
	 * A handle to linked list node.
	 * 
	 * @author ZZy
	 *
	 */
	public class NodeHandle {
		/**
		 * Constructs a NodeHandle to the node.
		 * 
		 * @param node	the node to which the NodeHandle is set 
		 */
		private NodeHandle(Node<T> node) {
			this.node = node;
		}

		/**
		 * Retrieves the object in the node.
		 * 
		 * @return	the object
		 */
		public T object() {
			return node.object;
		}

		/**
		 * Removes the node from the associated linked list. 
		 * Don't call this function twice.
		 */
		public void remove() {
			LinkedList.this.remove(node);
			node.next = node.prev = null;
		}

		/**
		 * Adds the node to another linked list.
		 * Call remove before call this.
		 * The old handle is no longer valid after the call.
		 * 
		 * @param list	the list to which the node is added
		 * @return		the new handle to the node
		 */
		public NodeHandle addTo(LinkedList<T> list) {
			return list.add(node);
		}

		/**
		 * Checks whether the node is in the list.
		 * 
		 * @param list		the list to check 
		 * @return			<tt>true</tt> if the node is in the given list, or <tt>false</tt> o.w.
		 */
		public boolean in(LinkedList<T> list) {
			return node.next != null && LinkedList.this == list;
		}

		private Node<T> node;
	}

	/**
	 * Constructs an empty list.
	 */
	public LinkedList() {
		root = new Node<T>();
		root.object = null;
		root.prev = root.next = root;
	}

	/**
	 * Adds to the last of the list a thread.
	 * 
	 * @param thread	the thread to add to the list
	 * @return			a handle to the node that contains the thread 
	 */
	public NodeHandle add(T object) {
		Node<T> node = new Node<T>();
		node.object = object;
		return add(node);
	}

	/**
	 * Adds to the last of the list a node.
	 * 
	 * @param node	the node to add
	 */
	private NodeHandle add(Node<T> node) {
		node.prev = root.prev;
		node.next = root;
		root.prev.next = node;
		root.prev = node;
		return new NodeHandle(node);
	}

	/**
	 * Checks whether the list is empty.
	 * 
	 * @return	<tt>true</tt> if the list is empty, or <tt>false</tt> o.w.
	 */
	public boolean empty() {
		return root.next == root;
	}

	/**
	 * Removes the first node from the list. Don't call this when empty() == true.
	 * 
	 * @return	the T in the first node
	 */
	public T remove() {
		T object = root.next.object;
		remove(root.next);
		return object;
	}
	
	/**
	 * Retrieves the first object in the list.
	 * 
	 * @return		the first object in the list, or <tt>null</tt> if the list is empty
	 */
	public T peek() {
		return root.next.object;
	}

	/**
	 * Removes the node from the list. Don't call remove with root.
	 * 
	 * @param node	the node to be removed from the list
	 */
	private void remove(Node<T> node) {
		node.prev.next = node.next;
		node.next.prev = node.prev;
	}

	private Node<T> root;

	@Override
	public java.util.Iterator<T> iterator() {
		return new Iterator();
	}
}
