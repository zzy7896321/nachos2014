package nachos.threads;

import nachos.machine.*;

/**
 * A KThread is a thread that can be used to execute Nachos kernel code. Nachos
 * allows multiple threads to run concurrently.
 * 
 * To create a new thread of execution, first declare a class that implements
 * the <tt>Runnable</tt> interface. That class then implements the <tt>run</tt>
 * method. An instance of the class can then be allocated, passed as an argument
 * when creating <tt>KThread</tt>, and forked. For example, a thread that
 * computes pi could be written as follows:
 * 
 * <p>
 * <blockquote>
 * 
 * <pre>
 * class PiRun implements Runnable {
 * 	public void run() {
 *         // compute pi
 *         ...
 *     }
 * }
 * </pre>
 * 
 * </blockquote>
 * <p>
 * The following code would then create a thread and start it running:
 * 
 * <p>
 * <blockquote>
 * 
 * <pre>
 * PiRun p = new PiRun();
 * new KThread(p).fork();
 * </pre>
 * 
 * </blockquote>
 */
public class KThread {
	/**
	 * Get the current thread.
	 * 
	 * @return the current thread.
	 */
	public static KThread currentThread() {
		Lib.assertTrue(currentThread != null);
		return currentThread;
	}

	/**
	 * Allocate a new <tt>KThread</tt>. If this is the first <tt>KThread</tt>,
	 * create an idle thread as well.
	 */
	public KThread() {
		if (currentThread != null) {
			tcb = new TCB();
		} else {
			readyQueue = ThreadedKernel.scheduler.newThreadQueue(false);
			readyQueue.acquire(this);

			currentThread = this;
			tcb = TCB.currentTCB();
			name = "main";
			restoreState();

			createIdleThread();
		}
	}

	/**
	 * Allocate a new KThread.
	 * 
	 * @param target
	 *            the object whose <tt>run</tt> method is called.
	 */
	public KThread(Runnable target) {
		this();
		this.target = target;
	}

	/**
	 * Set the target of this thread.
	 * 
	 * @param target
	 *            the object whose <tt>run</tt> method is called.
	 * @return this thread.
	 */
	public KThread setTarget(Runnable target) {
		Lib.assertTrue(status == statusNew);

		this.target = target;
		return this;
	}

	/**
	 * Set the name of this thread. This name is used for debugging purposes
	 * only.
	 * 
	 * @param name
	 *            the name to give to this thread.
	 * @return this thread.
	 */
	public KThread setName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Get the name of this thread. This name is used for debugging purposes
	 * only.
	 * 
	 * @return the name given to this thread.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the full name of this thread. This includes its name along with its
	 * numerical ID. This name is used for debugging purposes only.
	 * 
	 * @return the full name given to this thread.
	 */
	public String toString() {
		return (name + " (#" + id + ")");
	}

	/**
	 * Deterministically and consistently compare this thread to another thread.
	 */
	public int compareTo(Object o) {
		KThread thread = (KThread) o;

		if (id < thread.id)
			return -1;
		else if (id > thread.id)
			return 1;
		else
			return 0;
	}

	/**
	 * Causes this thread to begin execution. The result is that two threads are
	 * running concurrently: the current thread (which returns from the call to
	 * the <tt>fork</tt> method) and the other thread (which executes its
	 * target's <tt>run</tt> method).
	 */
	public void fork() {
		Lib.assertTrue(status == statusNew);
		Lib.assertTrue(target != null);

		Lib.debug(dbgThread, "Forking thread: " + toString() + " Runnable: "
				+ target);

		boolean intStatus = Machine.interrupt().disable();

		tcb.start(new Runnable() {
			public void run() {
				runThread();
			}
		});

		ready();

		Machine.interrupt().restore(intStatus);
	}

	private void runThread() {
		begin();
		target.run();
		finish();
	}

	private void begin() {
		Lib.debug(dbgThread, "Beginning thread: " + toString());

		Lib.assertTrue(this == currentThread);

		restoreState();

		Machine.interrupt().enable();
	}

	/**
	 * Finish the current thread and schedule it to be destroyed when it is safe
	 * to do so. This method is automatically called when a thread's
	 * <tt>run</tt> method returns, but it may also be called directly.
	 * 
	 * The current thread cannot be immediately destroyed because its stack and
	 * other execution state are still in use. Instead, this thread will be
	 * destroyed automatically by the next thread to run, when it is safe to
	 * delete this thread.
	 */
	public static void finish() {
		Lib.debug(dbgThread, "Finishing thread: " + currentThread.toString());

		Machine.interrupt().disable();
		
		currentThread.wake_join_queue();

		Machine.autoGrader().finishingCurrentThread();

		Lib.assertTrue(toBeDestroyed == null);
		toBeDestroyed = currentThread;

		currentThread.status = statusFinished;

		sleep();
	}

	/**
	 * Relinquish the CPU if any other thread is ready to run. If so, put the
	 * current thread on the ready queue, so that it will eventually be
	 * rescheuled.
	 * 
	 * <p>
	 * Returns immediately if no other thread is ready to run. Otherwise returns
	 * when the current thread is chosen to run again by
	 * <tt>readyQueue.nextThread()</tt>.
	 * 
	 * <p>
	 * Interrupts are disabled, so that the current thread can atomically add
	 * itself to the ready queue and switch to the next thread. On return,
	 * restores interrupts to the previous state, in case <tt>yield()</tt> was
	 * called with interrupts disabled.
	 */
	public static void yield() {
		Lib.debug(dbgThread, "Yielding thread: " + currentThread.toString());

		Lib.assertTrue(currentThread.status == statusRunning);

		boolean intStatus = Machine.interrupt().disable();

		currentThread.ready();

		runNextThread();

		Machine.interrupt().restore(intStatus);
	}

	/**
	 * Relinquish the CPU, because the current thread has either finished or it
	 * is blocked. This thread must be the current thread.
	 * 
	 * <p>
	 * If the current thread is blocked (on a synchronization primitive, i.e. a
	 * <tt>Semaphore</tt>, <tt>Lock</tt>, or <tt>Condition</tt>), eventually
	 * some thread will wake this thread up, putting it back on the ready queue
	 * so that it can be rescheduled. Otherwise, <tt>finish()</tt> should have
	 * scheduled this thread to be destroyed by the next thread to run.
	 */
	public static void sleep() {
		Lib.debug(dbgThread, "Sleeping thread: " + currentThread.toString());

		Lib.assertTrue(Machine.interrupt().disabled());

		if (currentThread.status != statusFinished)
			currentThread.status = statusBlocked;

		runNextThread();
	}

	/**
	 * Moves this thread to the ready state and adds this to the scheduler's
	 * ready queue.
	 */
	public void ready() {
		Lib.debug(dbgThread, "Ready thread: " + toString());

		Lib.assertTrue(Machine.interrupt().disabled());
		Lib.assertTrue(status != statusReady);

		status = statusReady;
		if (this != idleThread) {
			++numReady;
			readyQueue.waitForAccess(this);
		}

		Machine.autoGrader().readyThread(this);
	}

	/**
	 * Waits for this thread to finish. If this thread is already finished,
	 * return immediately. This method must only be called once; the second call
	 * is not guaranteed to return. This thread must not be the current thread.
	 */
	public void join() {
		Lib.debug(dbgThread, "Joining to thread: " + toString());

		Lib.assertTrue(this != currentThread);
		
		boolean intStatus = Machine.interrupt().disable();
	
		/* have to test status atomically for this thread could terminate before return (on real machine). */
		if (status == statusFinished) {
			Lib.debug(dbgThread, "Join returns immediately.");
			Machine.interrupt().restore(intStatus);
			return ;
		}
		
		/* initialize the join_queue only when needed */
		if (join_queue == null) {
			join_queue = ThreadedKernel.scheduler.newThreadQueue(true);
			join_queue.acquire(this);
		}
		
		join_queue.waitForAccess(currentThread);
		KThread.sleep();
		
		Machine.interrupt().restore(intStatus);
	}
	
	/**
	 * Puts back all the threads in the join queue to ready queue.
	 * Should be called by finish() with interrupt disabled.
	 * 
	 */
	private void wake_join_queue() {
		Lib.debug(dbgThread, "Waking up joined thread: " + toString());
		
		Lib.assertTrue(this == currentThread);
		Lib.assertTrue(Machine.interrupt().disabled());
		
		if (join_queue != null) {
			KThread thread = join_queue.nextThread();
			while (thread != null) {
				thread.ready();
				thread = join_queue.nextThread();
			}
		}
	}
	
	private ThreadQueue join_queue = null;

	/**
	 * Fork a thread and join it.
	 */
	public static void test_join() {
		final int long_time_to_wait = 10000000;
		final int yield_per_loops =   1000000;
		/* Test 1 */
		System.out.println("test_join #1: main joins forked.");
		KThread thread = new KThread(() -> {
			for (int i = 0; i < long_time_to_wait ; ++i) {
				if (i % yield_per_loops == 0) {
					System.out.println("I'm forked, and I'm yielding.");
					yield();
				}
			}
		});
		thread.setName("test_join_1");
		thread.fork();
		
		System.out.println("I'm main, and I'm joining to the forked.");
		thread.join();
		System.out.println("I'm main, and finally both of us are finished.");
		
		
		/* Test 2*/
		System.out.println();
		System.out.println("test_join #2: main joins forked which has finished.");
		thread = new KThread(() -> {
			System.out.println("I'm forked, and I finish immediately.");
		});
		thread.setName("test_join_2");
		thread.fork();
		
		while (thread.status != statusFinished) {
			System.out.println("I'm main, the forked has not finished yet.");
			yield();
		}
		thread.join();
		System.out.println("I'm main, and we are finshed.");
		
		/* Test 3 */
		System.out.println();
		System.out.println("test_join #3: two threads waits for one thread.");
		thread = new KThread(() -> {
			for (int i = 0; i < long_time_to_wait; ++i) {
				if (i % yield_per_loops == 0) {
					System.out.println("I'm forked 1, and I'm yielding.");
					yield();
				}
			}
		});
		thread.setName("test_join_3 #1");
		thread.fork();
		
		/* cannot use non-final local variable in lambda expression,
		 * have to use a wrapper class as a workaround. */
		class Test_join_3_runnable implements Runnable {
			public Test_join_3_runnable(KThread thread) {
				this.thread = thread;
			}
			public void run() {
				System.out.println("I'm forked 2, and I'm joining to forked 1.");
				thread.join();
				System.out.println("I'm forked 2, and I'm back from join().");
			}
			private KThread thread;
		}
		
		KThread thread_beta = new KThread(new Test_join_3_runnable(thread));
		thread_beta.setName("test_join_3 #2");
		thread_beta.fork();
		
		System.out.print("I'm main, and I'm joining to forked 1.");
		thread.join();
		
		System.out.println("I'm main, and I'm back from join().");
		thread_beta.join();	// just to make sure that thread_beta has finished
		System.out.println("I'm main, and three of us are done.");
		thread_beta = null;
		
		/* Test 4 */
		System.out.println();
		System.out.println("test_join #4: chained join");
		
		/* cannot use lambda either... */
		final int test_4_total_threads = 4;
		class Test_join_4_runnable implements Runnable {
			public Test_join_4_runnable() { thread_no = 0; }
			public Test_join_4_runnable(Test_join_4_runnable other) { thread_no = other.thread_no + 1; }
			private int thread_no;
			public void run() {
				if (thread_no < test_4_total_threads) {
					System.out.println("I'm thread " + (thread_no) + 
								", and I'm creating thread " + (thread_no + 1) + ".");
					KThread thread = new KThread(new Test_join_4_runnable(this));
					thread.setName("test_join_4 #" + (thread_no + 1));
					thread.fork();
					
					System.out.println("I'm thread " + (thread_no) + ", and I'm joining thread " + (thread_no + 1) + ".");
					thread.join();
					System.out.println("I'm thread " + (thread_no) + ", and I'm done.");
				}
				else {
					System.out.println("I'm thread " + (thread_no) + " and limit reached. I'll wait for a while and exit.");
					for (int i = 0; i < long_time_to_wait; ++i) {
						if (i % yield_per_loops == 0) {
							System.out.println("I'm thread " + (thread_no) + ", and I'm yielding.");
							yield();
						}
					}
				}
			}
		}
		
		System.out.println("I'm main, and I'll start the runnable. Thread 0 is myself.");
		new Test_join_4_runnable().run();
		System.out.println("I'm main, and all threads are finished.");
		
	}

	/**
	 * Create the idle thread. Whenever there are no threads ready to be run,
	 * and <tt>runNextThread()</tt> is called, it will run the idle thread. The
	 * idle thread must never block, and it will only be allowed to run when all
	 * other threads are blocked.
	 * 
	 * <p>
	 * Note that <tt>ready()</tt> never adds the idle thread to the ready set.
	 */
	private static void createIdleThread() {
		Lib.assertTrue(idleThread == null);

		idleThread = new KThread(new Runnable() {
			public void run() {
				while (true)
					yield();
			}
		});
		idleThread.setName("idle");

		Machine.autoGrader().setIdleThread(idleThread);

		idleThread.fork();
	}

	/**
	 * Determine the next thread to run, then dispatch the CPU to the thread
	 * using <tt>run()</tt>.
	 */
	public static void runNextThread() {
		KThread nextThread = ((ThreadedKernel) ThreadedKernel.kernel).pickNextThread();

		nextThread.run();
	}

	/**
	 * Dispatch the CPU to this thread. Save the state of the current thread,
	 * switch to the new thread by calling <tt>TCB.contextSwitch()</tt>, and
	 * load the state of the new thread. The new thread becomes the current
	 * thread.
	 * 
	 * <p>
	 * If the new thread and the old thread are the same, this method must still
	 * call <tt>saveState()</tt>, <tt>contextSwitch()</tt>, and
	 * <tt>restoreState()</tt>.
	 * 
	 * <p>
	 * The state of the previously running thread must already have been changed
	 * from running to blocked or ready (depending on whether the thread is
	 * sleeping or yielding).
	 * 
	 * @param finishing
	 *            <tt>true</tt> if the current thread is finished, and should be
	 *            destroyed by the new thread.
	 */
	private void run() {
		Lib.assertTrue(Machine.interrupt().disabled());

		Machine.yield();

		currentThread.saveState();

		Lib.debug(dbgThread, "Switching from: " + currentThread.toString()
				+ " to: " + toString());

		currentThread = this;

		tcb.contextSwitch();

		currentThread.restoreState();
	}

	/**
	 * Prepare this thread to be run. Set <tt>status</tt> to
	 * <tt>statusRunning</tt> and check <tt>toBeDestroyed</tt>.
	 */
	protected void restoreState() {
		Lib.debug(dbgThread, "Running thread: " + currentThread.toString());

		Lib.assertTrue(Machine.interrupt().disabled());
		Lib.assertTrue(this == currentThread);
		Lib.assertTrue(tcb == TCB.currentTCB());

		Machine.autoGrader().runningThread(this);

		status = statusRunning;

		if (toBeDestroyed != null) {
			toBeDestroyed.tcb.destroy();
			toBeDestroyed.tcb = null;
			toBeDestroyed = null;
		}
	}

	/**
	 * Prepare this thread to give up the processor. Kernel threads do not need
	 * to do anything here.
	 */
	protected void saveState() {
		Lib.assertTrue(Machine.interrupt().disabled());
		Lib.assertTrue(this == currentThread);
	}

	private static class PingTest implements Runnable {
		PingTest(int which) {
			this.which = which;
		}

		public void run() {
			for (int i = 0; i < 5; i++) {
				System.out.println("*** thread " + which + " looped " + i
						+ " times");
				KThread.yield();
			}
		}

		private int which;
	}

	/**
	 * Tests whether this module is working.
	 */
	public static void selfTest() {
		Lib.debug(dbgThread, "Enter KThread.selfTest");

		new KThread(new PingTest(1)).setName("forked thread").fork();
		new PingTest(0).run();
	}

	private static final char dbgThread = 't';

	/** Time by which the thread should be waken up. Used by Alarm. */
	protected long wake_time;
	
	/**
	 * Additional state used by schedulers.
	 * 
	 * @see nachos.threads.PriorityScheduler.ThreadState
	 */
	public Object schedulingState = null;

	public static final int statusNew = 0;
	public static final int statusReady = 1;
	public static final int statusRunning = 2;
	public static final int statusBlocked = 3;
	public static final int statusFinished = 4;

	/**
	 * The status of this thread. A thread can either be new (not yet forked),
	 * ready (on the ready queue but not running), running, or blocked (not on
	 * the ready queue and not running).
	 */
	public int status = statusNew;
	private String name = "(unnamed thread)";
	private Runnable target;
	private TCB tcb;

	/**
	 * Unique identifer for this thread. Used to deterministically compare
	 * threads.
	 */
	private int id = numCreated++;
	/** Number of times the KThread constructor was called. */
	private static int numCreated = 0;

	public static ThreadQueue readyQueue = null;
	public static int numReady = 0;
	
	private static KThread currentThread = null;
	private static KThread toBeDestroyed = null;
	public static KThread idleThread = null;

	public static int get_errno() {
		return currentThread().errno;
	}
	
	public static void set_errno(int errno) {
		currentThread().errno = errno;
	}
	
	public int errno;
}
