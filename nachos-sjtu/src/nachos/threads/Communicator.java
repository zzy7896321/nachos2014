package nachos.threads;

import java.util.LinkedList;

import nachos.machine.Lib;

/**
 * A <i>communicator</i> allows threads to synchronously exchange 32-bit
 * messages. Multiple threads can be waiting to <i>speak</i>, and multiple
 * threads can be waiting to <i>listen</i>. But there should never be a time
 * when both a speaker and a listener are waiting, because the two threads can
 * be paired off at this point.
 */
public class Communicator {
	/**
	 * Allocate a new communicator.
	 */
	public Communicator() {
		this.lock = new Lock();
		this.numWaiting = 0;
		this.waitings = new LinkedList<Callback>();
	}

	/**
	 * Wait for a thread to listen through this communicator, and then transfer
	 * <i>word</i> to the listener.
	 * 
	 * <p>
	 * Does not return until this thread is paired up with a listening thread.
	 * Exactly one listener should receive <i>word</i>.
	 * 
	 * @param word
	 *            the integer to transfer.
	 */
	public void speak(int word) {
		lock.acquire();
	
		if (numWaiting++ < 0) {
			Callback callback = waitings.poll();
			Lib.assertTrue(callback instanceof ListenerCallback);
			callback.callback(word);
		}
		
		else {
			Callback callback = new SpeakerCallback(word, lock);
			waitings.add(callback);
			callback.waitForCallback();
		}
		
		lock.release();
	}

	/**
	 * Wait for a thread to speak through this communicator, and then return the
	 * <i>word</i> that thread passed to <tt>speak()</tt>.
	 * 
	 * @return the integer transferred.
	 */
	public int listen() {
		int word;
		lock.acquire();
		
		Callback callback;
		if (numWaiting-- <= 0) {
			callback = new ListenerCallback(lock);
			waitings.add(callback);
			callback.waitForCallback();
			word = callback.getMsg();
		}
		else {
			callback = waitings.poll();
			Lib.assertTrue(callback instanceof SpeakerCallback);
			word = callback.callback(0);
		}
		
		lock.release();
		return word; 
	}

	static private abstract class Callback {
		public Callback(int msg, Lock lock) {
			this.msg = msg;
			this.condition = new Condition(lock);
		}
		
		public void waitForCallback() {
			this.condition.sleep();
		}
		
		abstract public int callback(int msg);
		
		public int getMsg() {
			return msg;
		}
		
		protected int msg;
		protected Condition condition;
	}
	
	static private class SpeakerCallback extends Callback {
		public SpeakerCallback(int msg, Lock lock) {
			super(msg, lock);
		}
		
		public int callback(int msg) {
			this.condition.wake();
			return this.msg;
		}
	}
	
	static private class ListenerCallback extends Callback {
		public ListenerCallback(Lock lock) {
			super(0, lock);
		}
		
		public int callback(int msg) {
			this.condition.wake();
			this.msg = msg;
			return 0;
		}
	}
	
	/** The lock. */
	private Lock lock;
	
	/** The.waiting callback queue. SpeakerCallbacks and ListenerCallbacks cannot appaer in the 
	 * queue at the same time. */
	private LinkedList<Callback> waitings;
	
	/** Number of waiting speaker or listener. Positive number indicates 
	 * speakers are waiting, while negative number indicates listeners are waiting. */
	private int numWaiting;
	
	public static void selfTest() {
		System.out.println();
		System.out.println("Communicator self test.");
		
		class Speaker implements Runnable {
			public Speaker(int num, Communicator communicator, int waitTicks) { 
				this.num = num;
				this.communicator = communicator; 
				this.waitTicks = waitTicks;
			}
			private int num;
			private Communicator communicator;
			private int waitTicks;
			public void run() {
				if (waitTicks > 0) {
					System.out.println("I'm speaker #" + num + ", and I'll wait for " + waitTicks + " ticks.");
					ThreadedKernel.alarm.waitUntil(waitTicks);
				}
				//while (nachos.machine.Lib.random(2) == 1) KThread.yield();
				System.out.println("I'm speaker #" + num + ", and I'm speaking to the communicator."); 
				communicator.speak(num);
			}
		}
		
		class Listener implements Runnable {
			public Listener(int num, Communicator communicator, int waitTicks) {
				this.num = num;
				this.communicator = communicator; 
				this.waitTicks = waitTicks;
			}
			private int num;
			private Communicator communicator;
			private int waitTicks;
			public void run() {
				if (waitTicks > 0) {
					System.out.println("I'm listener #" + num + ", and I'll wait for " + waitTicks + " ticks.");
					ThreadedKernel.alarm.waitUntil(waitTicks);
				}
				//while (nachos.machine.Lib.random(2) == 1) KThread.yield();
				System.out.println("I'm listener #" + num + ", and I'm litsening to the communicator.");
				int msg = communicator.listen();
				System.out.println("I'm listener #" + num + ", and I've heard from speaker #" + msg + ".");
			}
		}
		
		Communicator communicator = new Communicator();
		{
			System.out.println("Communicator test #1: one speaker waits for one listner.");
			KThread threads[] = new KThread[] {
				new KThread(new Speaker(1, communicator, 0)).setName("speaker #1"),
				new KThread(new Listener(1, communicator, 600)).setName("listener #1")
			};
			for (KThread thread : threads) {
				thread.fork();
			}
			for (KThread thread : threads) {
				thread.join();
			}
		}
		
		{
			System.out.println();
			System.out.println("Communicator test #2: speaker, listener, listner, speaker.");
			KThread threads[] = new KThread[] {
				new KThread(new Speaker(1, communicator, 0)).setName("speaker #1"),
				new KThread(new Listener(1, communicator, 600)).setName("listener #1"),
				new KThread(new Listener(2, communicator, 600)).setName("listener #2"),
				new KThread(new Speaker(2, communicator, 1200)).setName("speaker #2")
			};
			for (KThread thread : threads) {
				thread.fork();
			}
			for (KThread thread : threads) {
				thread.join();
			}
		}
		
		final int num_participants = 5;
		
		int priorityMaximum;
		int priorityMinimum;
		
		if (ThreadedKernel.scheduler instanceof LotteryScheduler) {
			priorityMaximum = (int) LotteryScheduler.priorityMaximum - 1;
			priorityMinimum = (int) LotteryScheduler.priorityMinimum;
		}
		else if (ThreadedKernel.scheduler instanceof PriorityScheduler) {
			priorityMaximum = PriorityScheduler.priorityMaximum;
			priorityMinimum = PriorityScheduler.priorityMinimum;
		}
		else {
			priorityMaximum = 1;
			priorityMinimum = 1;
		}
		
		{
			System.out.println();
			System.out.printf("Communicator test #3: speaker * %d, listener * %d\n", num_participants, num_participants);
			KThread threads[] = new KThread[num_participants * 2];
			for (int i = 0; i < num_participants; ++i) {
				threads[i] = new KThread(new Speaker(i+1, communicator, 0)).setName("speaker #" + (i+1));
				threads[i+num_participants] = new KThread(new Listener(i+1, communicator, 0)).setName("listener #" + (i+1));
				
				boolean intStatus = nachos.machine.Machine.interrupt().disable();
				
				int priority = nachos.machine.Lib.random(priorityMaximum - priorityMinimum + 1) + priorityMinimum;
				ThreadedKernel.scheduler.setPriority(threads[i], priority);
				System.out.println("Speaker #" + (i+1) + " is created with priority " + priority + ".");
				
				priority = nachos.machine.Lib.random(priorityMaximum - priorityMinimum + 1) + priorityMinimum;
				ThreadedKernel.scheduler.setPriority(threads[i+num_participants], priority);
				System.out.println("Listener #" + (i+1) + " is created with priority " + priority + ".");
				
				nachos.machine.Machine.interrupt().setStatus(intStatus);
			}
			for (KThread thread : threads) {
				thread.fork();
			}
			for (KThread thread : threads) {
				thread.join();
			}
			
		}
		
		{
			System.out.println();
			System.out.printf("Communicator test #4: listener * %d, speaker * %d\n", num_participants, num_participants);
			KThread threads[] = new KThread[num_participants * 2];
			for (int i = 0; i < num_participants; ++i) {
				threads[i+num_participants] = new KThread(new Speaker(i+1, communicator, 0)).setName("speaker #" + (i+1));
				threads[i] = new KThread(new Listener(i+1, communicator, 0)).setName("listener #" + (i+1));
				
				boolean intStatus = nachos.machine.Machine.interrupt().disable();
				
				int priority = nachos.machine.Lib.random(priorityMaximum - priorityMinimum + 1) + priorityMinimum;
				ThreadedKernel.scheduler.setPriority(threads[i], priority);
				System.out.println("Listener #" + (i+1) + " is created with priority " + priority + ".");
				
				priority = nachos.machine.Lib.random(priorityMaximum - priorityMinimum + 1) + priorityMinimum;
				ThreadedKernel.scheduler.setPriority(threads[i+num_participants], priority);
				System.out.println("Speaker #" + (i+1) + " is created with priority " + priority + ".");
				
				nachos.machine.Machine.interrupt().setStatus(intStatus);
			}
			for (KThread thread : threads) {
				thread.fork();
			}
			for (KThread thread : threads) {
				thread.join();
			}
			
		}
	}
}
