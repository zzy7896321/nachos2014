package nachos.threads;

import nachos.machine.*;
import java.util.PriorityQueue;
import java.util.Comparator;

/**
 * Uses the hardware timer to provide preemption, and to allow threads to sleep
 * until a certain time.
 */
public class Alarm {
	/**
	 * Allocate a new Alarm. Set the machine's timer interrupt handler to this
	 * alarm's callback.
	 * 
	 * <p>
	 * <b>Note</b>: Nachos will not function correctly with more than one alarm.
	 */
	public Alarm() {
		sleepingThreads = new PriorityQueue<KThread>(Comparator.comparingLong((KThread thread) -> thread.wake_time));
		
		Machine.timer().setInterruptHandler(new Runnable() {
			public void run() {
				timerInterrupt();
			}
		});
	}

	/**
	 * The timer interrupt handler. This is called by the machine's timer
	 * periodically (approximately every 500 clock ticks). Causes the current
	 * thread to yield, forcing a context switch if there is another thread that
	 * should be run.
	 */
	public void timerInterrupt() {		
		Lib.assertTrue(Machine.interrupt().disabled());
		long now_time = Machine.timer().getTime();
		
		KThread thread = sleepingThreads.peek();
		while (thread != null && now_time >= thread.wake_time) {
			sleepingThreads.poll();
			thread.ready();
			thread = sleepingThreads.peek();
		}
		
		KThread.yield();
	}

	/**
	 * Put the current thread to sleep for at least <i>x</i> ticks, waking it up
	 * in the timer interrupt handler. The thread must be woken up (placed in
	 * the scheduler ready set) during the first timer interrupt where
	 * 
	 * <p>
	 * <blockquote> (current time) >= (WaitUntil called time)+(x) </blockquote>
	 * 
	 * @param x
	 *            the minimum number of clock ticks to wait.
	 * 
	 * @see nachos.machine.Timer#getTime()
	 */
	public void waitUntil(long x) {
		boolean intStatus = Machine.interrupt().disable();
		
		long wakeTime = Machine.timer().getTime() + x;
		KThread.currentThread().wake_time = wakeTime;
		sleepingThreads.add(KThread.currentThread());
		KThread.sleep();
	
		Machine.interrupt().setStatus(intStatus);
	}
	
	private PriorityQueue<KThread> sleepingThreads;
	
	public static void selfTest() {
		System.out.println("Alarm self test: start a second thread, and sleep for a while.");
		
		KThread thread = new KThread(()->{ 
			System.out.println("I'm forked, and I'll sleep for 1000 ticks.");
			ThreadedKernel.alarm.waitUntil(1000);
			System.out.println("I'm forked, and I'm waken up.");
		}).setName("alarm_test_forked");
		thread.fork();
		
		System.out.println("I'm main, and I'll sleep for 2000 ticks.");
		ThreadedKernel.alarm.waitUntil(2000);
		System.out.println("I'm main, and I'm waken up.");
		
		thread.join();
		System.out.println("I'm main, and we are done.");
	}
}
