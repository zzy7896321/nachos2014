package nachos.threads;

import nachos.machine.Lib;
import java.util.ArrayList;
import nachos.threads.LotteryScheduler.ThreadState;

/**
 * Internal data structure of LotteryQueue.
 * <p>
 * Improve my document.
 * 
 * @author ZZy
 *
 */
public class LotteryQueueInternal implements Iterable<ThreadState> {
	
	static private class Node {
		public ThreadState threadState;
		public long subTreeSum;
		public int index;
	}
	
	public class NodeHandle {
		private NodeHandle(Node node) {
			this.node = node;
		}
		
		public ThreadState threadState() {
			return node.threadState;
		}
		
		public void updated() {
			LotteryQueueInternal.this.update(node);
		}
		
		public boolean in(LotteryQueueInternal queue) {
			return LotteryQueueInternal.this == queue;
		}
		
		private Node node;
	}
	
	private class Iterator implements java.util.Iterator<ThreadState> {
		private Iterator() {
			this.iterator = nodes.iterator();
		}
	
		@Override
		public boolean hasNext() {
			return iterator.hasNext();
		}
		
		@Override
		public ThreadState next() {
			return iterator.next().threadState;
		}
		
		java.util.Iterator<Node> iterator;
	}
	
	/**
	 * Constructs an empty complete binary tree.
	 */
	public LotteryQueueInternal() {
		nodes = new ArrayList<Node>();
	}
	
	public NodeHandle add(ThreadState threadState) {
		Node node = new Node();
		node.threadState = threadState;
		return add(node);
	}
	
	public ThreadState poll() {
		if (nodes.isEmpty()) return null;
		
		int index = 0;
		Node node = nodes.get(0);
		long rand_value = random(node.subTreeSum);
		while (true) {
			int left_index = leftIndex(index);
			if (left_index >= nodes.size()) break;
			Node left = nodes.get(left_index);
			
			if (rand_value < left.subTreeSum) {
				index = left_index;
				node = left;
				continue;
			}
			
			if (rand_value - left.subTreeSum < node.threadState.getEffectivePriorityL()) {
				break;
			}
			
			rand_value -= (left.subTreeSum + node.threadState.getEffectivePriorityL());
			index = rightIndex(index);
			node = nodes.get(index);
		}
		
		ThreadState ret = node.threadState;
		if (index == nodes.size() -1)  {
			nodes.remove(nodes.size() - 1);
			if (nodes.size() > 0) {
				update(nodes.get(parentIndex(nodes.size())));
			}
		}
		
		else {
			Node last = nodes.remove(nodes.size() - 1);	// nodes.size() >= 2
			int last_parent_index = parentIndex(last.index);
			update(nodes.get(last_parent_index));
			
			last.index = index;
			nodes.set(index, last);
			update(last);
		}
		
		return ret;
	}
	
	public long getSum() {
		if (nodes.isEmpty()) return 0;
		return nodes.get(0).subTreeSum;
	}
	
	private NodeHandle add(Node node) {
		nodes.add(node);
		node.index = nodes.size() - 1;
		update(node);
		return new NodeHandle(node);
	}
	
	private void update(Node node) {
		 // update to the root
		int index = node.index;
		updateSubTreeSum(node);
		while (index != 0) {
			index = parentIndex(index);
			node = nodes.get(index);
			updateSubTreeSum(node);
		}
	}
	
	private void updateSubTreeSum(Node node) {
		node.subTreeSum = node.threadState.getEffectivePriorityL();
		int left = leftIndex(node.index);
		if (left < nodes.size()) {
			node.subTreeSum += nodes.get(left).subTreeSum;
		}
		else return ;
		int right = rightIndex(node.index);
		if (right < nodes.size()) {
			node.subTreeSum += nodes.get(right).subTreeSum;
		}
	}
	
	public int size() {
		return nodes.size();
	}
	
	public boolean empty() {
		return nodes.isEmpty();
	}
	
	@Override
	public java.util.Iterator<ThreadState> iterator() {
		return new Iterator();
	}
	
	private static int leftIndex(int index) {
		return (index << 1) + 1;
	}
	
	private static int rightIndex(int index) {
		return (index << 1) + 2;
	}
	
	private static int parentIndex(int index) {
		return (index - 1) >> 1;
	}
	
	
	private static long randomNextLong() {
		return (((long) Lib.random((1 << 30))) << 33) + (((long) Lib.random(1 << 30)) << 3) + ((long) Lib.random(1 << 3));
	}
	
	private static long random(long range) {
		if ((range) < (long)Integer.MAX_VALUE) {
			return (long) Lib.random((int) range);
		}
		
		else {
			if ((range & -range) == range) {
				return randomNextLong() & (range - 1);
			}
			
			else {
				long upper = Long.highestOneBit(range);
				upper = (upper -1) + upper;
				long now;
				while ( (now = randomNextLong() & upper) >= range);
				return now;
			}
		}
	}
	
	private ArrayList<Node> nodes; 
}