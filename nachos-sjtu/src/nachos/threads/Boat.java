package nachos.threads;

import nachos.ag.BoatGrader;
import nachos.machine.Lib;

public class Boat {
	static BoatGrader bg;

	public static void selfTest() {
		BoatGrader b = new BoatGrader();

		System.out.println("\n ***Testing Boats with only 2 children***");
		b.startTest(0, 2);

		System.out.println("\n ***Testing Boats with 2 children, 1 adult***");
		b.startTest(1, 2);

		System.out.println("\n ***Testing Boats with 3 children, 3 adults***");
		b.startTest(3, 3);
		
		System.out.println("\n ***Testing Boats with 0 children, 1 adult***");
		b.startTest(1, 0);
		
		System.out.println("\n ***Testing Boats with 1 child, 0 adult***");
		b.startTest(0, 1);
	}
	
	/** the number of people on Oahu. Whenever an individual is on Oahu, he can find the correct value of this variable. */
	private static int OahuNumPeople;
	
	/** the number of people on Molokai. Whenever an individual is on Molokai, he can find the correct value of this variable.  */
	private static int MolokaiNumPeople;
	
	/** the number of people who has acquired an id */
	private static int numRegistered;
	
	private enum Location {
		Oahu, Molokai
	};
	
	/** the place where the boat is located */
	private static Location boat;
	
	/** the lock */
	private static Lock boatLock;
	
	/** The first child who acquired the lock becomes a captain, who should set caption to his/her id.
	 *  When transporting an adult, a captain should first transport his chief to Molokai and then
	 *  come back to Oahu alone. Once all adults are on Molokai, captain should row the boat with one child
	 *  each time to Molokai, and come back if there are still children in Oahu. The chief is seleted
	 *  arbitrarily from the remaining children when needed. */
	private static int captain;
	
	/* uses Condition2 to improve performance */
	
	/** The condition that the captain waits on. */
	private static Condition2 waitingCaptain;

	/** The condition that the chief officer waits on. */
	private static Condition2 waitingChief;
	
	/** the condition that the remaining children wait on. */
	private static Condition2 waitingChildren;
	
	/** the condition that the adults wait on */
	private static Condition2 waitingAdults;
	
	/** the number of adults. This is a common knowledge once everybody has acquired an id, 
	 *  since no one would commence his/her trip before that. */
	private static int numAdult;

	/**
	 *  Initializes all the global variables. 
	 * 
	 *  @param numPeople	the total number of people
	 */
	private static void initialize(int numPeople) {
		OahuNumPeople = numPeople;
		MolokaiNumPeople = 0;
		numRegistered = 0;
		boat = Location.Oahu;
		boatLock = new Lock();
		captain = 0;
		waitingCaptain = new Condition2(boatLock);
		waitingChief = new Condition2(boatLock);
		waitingChildren = new Condition2(boatLock);
		waitingAdults = new Condition2(boatLock);
		numAdult = 0;
	}
	
	/**
	 * Launch the threads and wait until they all terminate.
	 * 
	 * @param adults	the number of adult threads
	 * @param children	the number of child threads
	 */
	private static void launchThreads(int adults, int children) {
		KThread threads[] = new KThread[adults + children];
		
		for (int i = 0; i < children; ++i) {
			threads[i] = new KThread(()->{ChildItinerary();}).setName("Boat Child Thread #" + (i + 1));
			threads[i].fork();
		}
		
		for (int i = 0; i < adults; ++i) {
			threads[i + children] = new KThread(()->{AdultItinerary();}).setName("Boat Adult Thread #" + (i + 1));
			threads[i + children].fork();
		}
		
		for (KThread thread : threads) {
			thread.join();
		}
	}
	
	/** 
	 * Queries the number of people on the same side. Acquire boatLock before call this.
	 * 
	 * @param where		the island the querist is on
	 * @return			the number of people on the island the querist is on
	 */
	static private int accessNumPeople(Location where) {
		Lib.assertTrue(boatLock.isHeldByCurrentThread());
		if (where == Location.Oahu) return OahuNumPeople;
		return MolokaiNumPeople;
	}

	/**
	 * Registers the current thread. Acquire boatLock before call this.
	 * 
	 * @param isAdult		whether the caller is adult
	 * @return				the id
	 */
	static private int register(boolean isAdult) {
		Lib.assertTrue(boatLock.isHeldByCurrentThread());
		if (isAdult) ++numAdult;
		return ++numRegistered;
	}
	
	/**
	 * Volunteers to be the captain. This function should only be called exactly once by a child thread.
	 * Acquire boatLock before call this.
	 * 
	 * @param id		the id of the calling thread
	 */
	static private void volunteerToBeCaptain(int id) {
		Lib.assertTrue(boatLock.isHeldByCurrentThread());
		Lib.assertTrue(captain == 0);
		captain = id;
	}

	/** maintained by rowTheBoat and rideTheBoat. */
	static private int rideCapacity = 0;
	
	/**
	 * Rows the boat to the other side. Acquire boatLock before call this.
	 * 
	 * @param currentLocation	where the sailor is located
	 * @param isAdult	whether the caller is an adult thread
	 * @return			the destination
	 */
	static private Location rowTheBoat(Location currentLocation, boolean isAdult) {
		Lib.assertTrue(boatLock.isHeldByCurrentThread());
		Lib.assertTrue(currentLocation == boat, "eh, the boat is on the other side");
		if (isAdult) {
			rideCapacity = 0;
			if (boat == Location.Oahu) {
				boat = Location.Molokai;
				--OahuNumPeople;	// not checked here, for they are checked in bg calls
				++MolokaiNumPeople;
				bg.AdultRowToMolokai();
				return Location.Molokai;
			}
			else {
				boat = Location.Oahu;
				++OahuNumPeople;
				--MolokaiNumPeople;
				bg.AdultRowToOahu();
				return Location.Oahu;
			}
		}
		else {
			rideCapacity = 1;
			if (boat == Location.Oahu) {
				boat = Location.Molokai;
				--OahuNumPeople;
				++MolokaiNumPeople;
				bg.ChildRowToMolokai();
				return Location.Molokai;
			}
			else {
				boat = Location.Oahu;
				++OahuNumPeople;
				--MolokaiNumPeople;
				bg.ChildRowToOahu();
				return Location.Oahu;
			}
		}
		
	}

	/**
	 * Rides the boat to the other side. Acquire boatLock before call this.
	 * 
	 * @param currentLocation	where the rider is located
	 * @param isAdult			whether the caller is an adult thread
	 * @return					the destination
	 */
	static private Location rideTheBoat(Location currentLocation, boolean isAdult) {
		Lib.assertTrue(boatLock.isHeldByCurrentThread());
		Lib.assertTrue(!isAdult, "adult can not ride a boat");
		Lib.assertTrue(rideCapacity > 0, "the boat is already fully loaded");
		Lib.assertTrue(currentLocation != boat, "eh, where's the captain?");
		
		--rideCapacity;
		if (currentLocation == Location.Oahu) {
			--OahuNumPeople;
			++MolokaiNumPeople;
			bg.ChildRideToMolokai();
			return Location.Molokai;
		}
		
		else {
			++OahuNumPeople;
			--MolokaiNumPeople;
			bg.ChildRideToOahu();
			return Location.Oahu;
		}
	}
	
	public static void begin(int adults, int children, BoatGrader b) {
		// Store the externally generated autograder in a class
		// variable to be accessible by children.
		bg = b;

		// Instantiate global variables here
		initialize(adults + children);
		
		// Create threads here. See section 3.4 of the Nachos for Java
		// Walkthrough linked from the projects page.
		launchThreads(adults, children);
	}

	static void AdultItinerary() {
		boatLock.acquire();
		@SuppressWarnings("unused")
		int id = register(true);
		Location location = Location.Oahu;
		int num_people = accessNumPeople(location);
		int num_child;
		
		if (num_people == numRegistered) {
			num_child = num_people - numAdult;
			if (num_child == 0 && numAdult == 1) {
				/* I'm the only one. I'll row the boat myself. */
				location = rowTheBoat(location, true);
				return ;
			}
			
			Lib.assertTrue(num_child >= 2, "no solution");
			waitingCaptain.wake();
		}
		waitingAdults.sleep();
		
		/* I can now row the boat to Oahu. Once I get there, I'm done. */
		location = rowTheBoat(location, true);
		waitingChief.wake();
		
		boatLock.release();
	}

	static void ChildItinerary() {
		boatLock.acquire();
		int id = register(false);
		Location location = Location.Oahu;
		int num_people = accessNumPeople(location);
		int num_child;
		
		if (captain == 0){
			/* I volunteer to be the captain. */
			volunteerToBeCaptain(id);
			
			if (num_people == numRegistered) {
				/* I'm the last one. */
				num_child = num_people - numAdult;
				Lib.assertTrue(num_child >= 2 || numAdult == 0, "no solution");
			}
			else {
				/* wait for the last registered person to wake me up. */
				waitingCaptain.sleep();
				num_child = num_people - numAdult;
			}
			
			while (accessNumPeople(location) > num_child) {
				/* There are still adults on Oahu. */
				/* Step1: row the boat with a chief officer to Oahu */
				location = rowTheBoat(location, false);
				waitingChildren.wake();
				waitingCaptain.sleep();
				
				/* Step2: go back to Oahu. */
				location = rowTheBoat(location, false);
				waitingAdults.wake();
				waitingCaptain.sleep();
				
				/* Now, with an adult transported to Molokai, 
				 * other people just stay where they are. */
			}
			
			/* Only children left, I'll be the boatman. */
			while (true) {
				location = rowTheBoat(location, false);
				if (accessNumPeople(location) == num_people) {
					/* Okay, everybody is on Molokai. Done.*/
					break;
				}
				waitingChildren.wake();
				waitingCaptain.sleep();
				
				/* Re-check whether I should go back to Oahu to pick up more peers. */
				if (accessNumPeople(location) == num_people) {
					/* Done. */
					break;
				}
				location = rowTheBoat(location, false);
			}
		}
		
		else {
			/* I'll take turn to be chief officer. */
			if (num_people == numRegistered) {
				num_child = num_people - numAdult;
				Lib.assertTrue(num_child >= 2 || numAdult == 0, "no solution");
				
				waitingCaptain.wake();
			}
			waitingChildren.sleep();
			num_child = num_people - numAdult;
			
			while (accessNumPeople(location) + 1 > num_child) {
				/* I'll help the captain to transport an adult to Molokai. */
				location = rideTheBoat(location, false);
				waitingCaptain.wake();
				waitingChief.sleep();
				
				/* I'll row the boat back to Oahu. */
				location = rowTheBoat(location, false);
				waitingCaptain.wake();
				waitingChildren.sleep();
			}
			
			/* This time, I'm the passenger. Once I get to Molokai, I'm done. */
			location = rideTheBoat(location, false);
			waitingCaptain.wake();
		}
		
		boatLock.release();
	}

	static void SampleItinerary() {
		// Please note that this isn't a valid solution (you can't fit
		// all of them on the boat). Please also note that you may not
		// have a single thread calculate a solution and then just play
		// it back at the autograder -- you will be caught.
		System.out
				.println("\n ***Everyone piles on the boat and goes to Molokai***");
		bg.AdultRowToMolokai();
		bg.ChildRideToMolokai();
		bg.AdultRideToMolokai();
		bg.ChildRideToMolokai();
	}

}
