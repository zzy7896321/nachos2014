package nachos.threads;

import nachos.machine.Lib;
import nachos.machine.Machine;

/**
 * A scheduler that chooses threads based on their priorities.
 * 
 * <p>
 * A priority scheduler associates a priority with each thread. The next thread
 * to be dequeued is always a thread with priority no less than any other
 * waiting thread's priority. Like a round-robin scheduler, the thread that is
 * dequeued is, among all the threads of the same (highest) priority, the thread
 * that has been waiting longest.
 * 
 * <p>
 * Essentially, a priority scheduler gives access in a round-robin fashion to
 * all the highest-priority threads, and ignores all other threads. This has the
 * potential to starve a thread if there's always a thread waiting with higher
 * priority.
 * 
 * <p>
 * A priority scheduler must partially solve the priority inversion problem; in
 * particular, priority must be donated through locks, and through joins.
 */
public class PriorityScheduler extends Scheduler {
	/**
	 * Allocate a new priority scheduler.
	 */
	public PriorityScheduler() {
	}

	/**
	 * Allocate a new priority thread queue.
	 * 
	 * @param transferPriority
	 *            <tt>true</tt> if this queue should transfer priority from
	 *            waiting threads to the owning thread.
	 * @return a new priority thread queue.
	 */
	public ThreadQueue newThreadQueue(boolean transferPriority) {
		return new PriorityQueue(transferPriority);
	}

	public int getPriority(KThread thread) {
		Lib.assertTrue(Machine.interrupt().disabled());

		return getThreadState(thread).getPriority();
	}

	public int getEffectivePriority(KThread thread) {
		Lib.assertTrue(Machine.interrupt().disabled());

		return getThreadState(thread).getEffectivePriority();
	}

	public void setPriority(KThread thread, int priority) {
		Lib.assertTrue(Machine.interrupt().disabled());

		Lib.assertTrue(priority >= priorityMinimum
				&& priority <= priorityMaximum);

		getThreadState(thread).setPriority(priority);
	}

	public boolean increasePriority()
	{
		boolean intStatus = Machine.interrupt().disable();

		KThread thread = KThread.currentThread();

		int priority = getPriority(thread);
		if (priority == priorityMaximum)
		{
		  Machine.interrupt().restore(intStatus); // bug identified by Xiao Jia @ 2011-11-04
			return false;
		}

		setPriority(thread, priority + 1);

		Machine.interrupt().restore(intStatus);
		return true;
	}

	public boolean decreasePriority()
	{
		boolean intStatus = Machine.interrupt().disable();

		KThread thread = KThread.currentThread();

		int priority = getPriority(thread);
		if (priority == priorityMinimum)
		{
		  Machine.interrupt().restore(intStatus); // bug identified by Xiao Jia @ 2011-11-04
			return false;
		}

		setPriority(thread, priority - 1);

		Machine.interrupt().restore(intStatus);
		return true;
	}

	/**
	 * The default priority for a new thread. Do not change this value.
	 */
	public static final int priorityDefault = 1;
	/**
	 * The minimum priority that a thread can have. Do not change this value.
	 */
	public static final int priorityMinimum = 0;
	/**
	 * The maximum priority that a thread can have. Do not change this value.
	 */
	public static final int priorityMaximum = 7;

	/**
	 * Return the scheduling state of the specified thread.
	 * 
	 * @param thread
	 *            the thread whose scheduling state to return.
	 * @return the scheduling state of the specified thread.
	 */
	protected ThreadState getThreadState(KThread thread) {
		if (thread.schedulingState == null)
			thread.schedulingState = new ThreadState(thread);

		return (ThreadState) thread.schedulingState;
	}
	
	
	/**
	 * A <tt>ThreadQueue</tt> that sorts threads by priority.
	 * 
	 */
	protected class PriorityQueue extends ThreadQueue {
		PriorityQueue(boolean transferPriority) {
			this.transferPriority = transferPriority;

			this.waitingThreads = new nachos.threads.PriorityQueue<ThreadState>(
			(PriorityTimeTag lhs, PriorityTimeTag rhs) -> {
				if (lhs.effectivePriority == rhs.effectivePriority) {
					return (lhs.enQueueTime < rhs.enQueueTime) ? -1 : (lhs.enQueueTime == rhs.enQueueTime) ? 0 : 1;
				}
				return rhs.effectivePriority - lhs.effectivePriority;	// descending effective priority order
			});
			
			this.owner = null;
			this.myNodeHandle = null;
		}

		public void waitForAccess(KThread thread) {
			Lib.assertTrue(Machine.interrupt().disabled());
			
			ThreadState thread_state = getThreadState(thread);
//			Lib.assertTrue(thread_state.waitingOn == null, "already waiting in another queue.");
			
			int eff_priority = thread_state.getEffectivePriority();
			int old_max_priority = maxPriority();
			thread_state.waitingOn = this;
			/* don't use Machine.timer().getTime() since the enqueue events could happen during the same tick
			 * on Nachos virtual machine */
			thread_state.enQueueTime = ++lastTimeTag;
			thread_state.waitingOnHandle = waitingThreads.add(thread_state);
			
			if (eff_priority > old_max_priority){
				updateOwner();
			}
		}

		public void acquire(KThread thread) {
			Lib.assertTrue(Machine.interrupt().disabled());
			
//			Lib.assertTrue(waitingThreads.empty(), "cannot acquire access when waiting queue is not empty");
			
			if (transferPriority) {
				ThreadState thread_state = getThreadState(thread);
				switchOwner(thread_state);
			}
		}

		public KThread nextThread() {
			Lib.assertTrue(Machine.interrupt().disabled());
			
			if (waitingThreads.empty()) return null;
			
			ThreadState thread_state = waitingThreads.poll();
			thread_state.waitingOn = null;
			thread_state.waitingOnHandle = null;
			
			if (transferPriority) {
				switchOwner(thread_state);
			}
			
			return thread_state.thread;
		}

		/**
		 * Return the next thread that <tt>nextThread()</tt> would return,
		 * without modifying the state of this queue.
		 * 
		 * @return the next thread that <tt>nextThread()</tt> would return.
		 */
		protected ThreadState pickNextThread() {
			if (waitingThreads.empty()) return null;
			
			return waitingThreads.peek(); 
		}
		
		private void switchOwner(ThreadState thread_state) {
			dropOwner();
			owner = thread_state;
			myNodeHandle = owner.acquired.add(this);
			updateOwner();
		}
		
		private void updateOwner() {
			if (owner == null) return ;
			if (owner.waitingOn == null) return ;
			
			myNodeHandle.updated();
			if (owner.updateEffectivePriority()) {
				owner.waitingOn.updateOwner();
			}
			
		}
		
		private void dropOwner() {
			if (this.owner == null) return;
			ThreadState owner = this.owner;
			this.owner = null;
			this.myNodeHandle.remove();
			this.myNodeHandle = null;
			
			if (owner.updateEffectivePriority()) {
				owner.waitingOn.updateOwner();
			}
		}
		
		private int maxPriority() {
			if (waitingThreads.empty()) {
				return priorityMinimum - 1;
			}
			return waitingThreads.peek().effectivePriority;
		}

		public void print() {
			Lib.assertTrue(Machine.interrupt().disabled());
		
			boolean printed = false;
			for (ThreadState thread_state : waitingThreads) {
				if (!printed) {
					System.out.print(thread_state.thread);
					printed = true;
				}
				else {
					System.out.print(" ");
					System.out.print(thread_state.thread);
				}
			}
			System.out.println();
		}

		/**
		 * <tt>true</tt> if this queue should transfer priority from waiting
		 * threads to the owning thread.
		 */
		public boolean transferPriority;
	
		/** the priority queue of threads waiting on the queue */
		private nachos.threads.PriorityQueue<ThreadState> waitingThreads;
		
		/** the thread state of the last thread that acquired access from this queue */
		private ThreadState owner;
		
		/** the handle to the acquired priority queue node of 
		 * the last process that acquired access from this */
		private nachos.threads.PriorityQueue<PriorityScheduler.PriorityQueue>.NodeHandle myNodeHandle;
		
		/** the time tag of the last enqueued thread */
		private long lastTimeTag = Long.MAX_VALUE;	// +1 goes to Long.MIN_VALUE
		
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			
			for (ThreadState thread_state : waitingThreads) {
				builder.append(thread_state).append('\n');
			}
			return builder.toString();
		}
	}

	/**
	 * This class is created only for convenience. It is used when PriorityQueue may want to query
	 * a thread with specific priority and enQueueTime without constructing a complete ThreadState.
	 * 
	 * @author ZZy
	 */
	private class PriorityTimeTag {
		/** The priority of the associated thread. */
		protected int priority;
		
		/** The cached effective priority of the associated thread. 
		 * (Need to be updated whenever priority or acquired queue changes) */
		protected int effectivePriority;
	
		/** the machine time of the time when the associated thread was enqueued */
		protected long enQueueTime;
	}
	
	/**
	 * The scheduling state of a thread. This should include the thread's
	 * priority, its effective priority, any objects it owns, and the queue it's
	 * waiting for, if any.
	 * 
	 * @see nachos.threads.KThread#schedulingState
	 */
	protected class ThreadState extends PriorityTimeTag {
		/**
		 * Allocate a new <tt>ThreadState</tt> object and associate it with the
		 * specified thread.
		 * 
		 * @param thread
		 *            the thread this state belongs to.
		 */
		public ThreadState(KThread thread) {
			this.thread = thread;
			this.priority = priorityDefault;
			this.effectivePriority = this.priority;
		
			this.acquired = new nachos.threads.PriorityQueue<PriorityScheduler.PriorityQueue>(
					(PriorityScheduler.PriorityQueue q1, PriorityScheduler.PriorityQueue q2) ->
					q2.maxPriority() - q1.maxPriority());
			this.waitingOn = null;
			this.waitingOnHandle = null;
		}

		/**
		 * Return the priority of the associated thread.
		 * 
		 * @return the priority of the associated thread.
		 */
		public int getPriority() {
			return priority;
		}

		/**
		 * Return the effective priority of the associated thread.
		 * 
		 * @return the effective priority of the associated thread.
		 */
		public int getEffectivePriority() {
			
			return effectivePriority;
		}
	
		/**
		 * Updates the effective priority and returns the old effective priority.
		 * If the thread is waiting on a queue, the queue's internal data structure
		 * will be properly updated. However, the owner of the queue may need further update.
		 * 
		 * @return	<tt>true</tt> if the owner of the queue this is waiting on needs to
		 * 			be updated
		 */
		private boolean updateEffectivePriority() {
			int new_eff_priority = priority;
			if (!acquired.empty()) {
				int acquired_priority = acquired.peek().maxPriority();
				if (acquired_priority > new_eff_priority)
					new_eff_priority = acquired_priority;
			}
			
			if (new_eff_priority == effectivePriority) return false;
			if (waitingOn == null) {
				effectivePriority = new_eff_priority;
				return false;
			}
			
			int waitingOn_old_priority = waitingOn.maxPriority();
			effectivePriority = new_eff_priority;
			waitingOnHandle.updated();
			return effectivePriority > waitingOn_old_priority;
		}

		/**
		 * Set the priority of the associated thread to the specified value.
		 * 
		 * @param priority
		 *            the new priority.
		 */
		public void setPriority(int priority) {
			if (this.priority == priority)
				return;
			
			this.priority = priority;
			if (updateEffectivePriority()) {
				this.waitingOn.updateOwner();
			}
		}

		/**
		 * Called when <tt>waitForAccess(thread)</tt> (where <tt>thread</tt> is
		 * the associated thread) is invoked on the specified priority queue.
		 * The associated thread is therefore waiting for access to the resource
		 * guarded by <tt>waitQueue</tt>. This method is only called if the
		 * associated thread cannot immediately obtain access.
		 * 
		 * @param waitQueue
		 *            the queue that the associated thread is now waiting on.
		 * 
		 * @see nachos.threads.ThreadQueue#waitForAccess
		 */
		public void waitForAccess(PriorityQueue waitQueue) {
			Lib.assertNotReached("ThreadState.waitForAccess cannot be called from outside PriorityScheduler.");
		}

		/**
		 * Called when the associated thread has acquired access to whatever is
		 * guarded by <tt>waitQueue</tt>. This can occur either as a result of
		 * <tt>acquire(thread)</tt> being invoked on <tt>waitQueue</tt> (where
		 * <tt>thread</tt> is the associated thread), or as a result of
		 * <tt>nextThread()</tt> being invoked on <tt>waitQueue</tt>.
		 * 
		 * @see nachos.threads.ThreadQueue#acquire
		 * @see nachos.threads.ThreadQueue#nextThread
		 */
		public void acquire(PriorityQueue waitQueue) {
			Lib.assertNotReached("ThreadState.acquire cannot be called from outside PriorityScheduler.");
		}

		/** The thread with which this object is associated. */
		protected KThread thread;
		
		/** The priority queue in which are the scheduler queues of which ownership is the associated thread. */
		private nachos.threads.PriorityQueue<PriorityScheduler.PriorityQueue> acquired;
		
		/** The scheduler queue that the associated thread is waiting on. */
		private PriorityScheduler.PriorityQueue waitingOn;
		
		/** The handle to the node in the scheduler queue that the associated thread is waiting on. */
		private nachos.threads.PriorityQueue<ThreadState>.NodeHandle waitingOnHandle;
		
		@Override
		public String toString() {
			return thread.toString() + String.format("[p=%d, ep=%d]", priority, effectivePriority);
		}
	}
	
	static public void selfTest() {
		{
			System.out.println();
			System.out.println("Priority scheduler self test #1: concurrent threads with priority priorityMinimum to priorityMaximum.");
			System.out.println("Output should in the descending order, if PriorityScheduler is used.");
			
			boolean intStatus = Machine.interrupt().disable();
			KThread thread[] = new KThread[priorityMaximum + 1];
			for (int i = priorityMinimum; i <= priorityMaximum; ++i) {
				thread[i] = new KThread(()->{
					System.out.println("I'm " + KThread.currentThread().getName() + ".");
				}).setName("PrioritySceduler test 1 #" + i);
				ThreadedKernel.scheduler.setPriority(thread[i], i);
				thread[i].fork();
			}
			Machine.interrupt().restore(intStatus);
			
			ThreadedKernel.alarm.waitUntil(0);	// wait until next timer interrupt
		}
		
		{
			System.out.println();
			System.out.println("Priority scheduler self test #2: Priority donation.");
			boolean intStatus = Machine.interrupt().disable();
			KThread thread1 = new KThread(()->{
				System.out.println("I'm " + KThread.currentThread().getName() + ", and I'm should run before #2.");
			}).setName("PriorityScheduler test 2 #1");
			KThread thread2 = new KThread(()->{
				System.out.println("I'm " + KThread.currentThread().getName() + ".");
			}).setName("PriorityScheduler test 2 #2");
			
			thread2.fork();
			thread1.fork();
			
			ThreadedKernel.scheduler.setPriority(thread1, 0);
			ThreadedKernel.scheduler.setPriority(thread2, 1);
			ThreadedKernel.scheduler.setPriority(2);
			
			Machine.interrupt().setStatus(intStatus);
			
			System.out.println("I'm main, and I'm joining #1 so that he acquires my priority.");
			thread1.join();
			ThreadedKernel.alarm.waitUntil(0);
		}
	
		{
			System.out.println();
			System.out.println("Priority scheduler self test #3: semaphore and condition.");
			
			// SynchList uses condition to implement waiting on empty queue and condition uses semaphore. 
			// SynchList also contains a lock, which uses a priority-donation-enabled queue
			final SynchList<Integer> synchList = new SynchList<Integer>();	
			
			KThread threads[] = new KThread[] {
				new KThread(() -> {
					System.out.println("I'm " + KThread.currentThread().getName() + 
							" with priority 5, and I'm trying to get a msg from the list.");
					int msg = synchList.removeFirst();
					System.out.println("I'm " + KThread.currentThread().getName() + ", and I've got msg " + msg + ".");
				}).setName("PriorityScheduler test 3 #1"),
				new KThread(() -> {
					System.out.println("I'm " + KThread.currentThread().getName() + 
							" with priority 4, and I'm trying to get a msg from the list.");
					int msg = synchList.removeFirst();
					System.out.println("I'm " + KThread.currentThread().getName() + ", and I've got msg " + msg + ".");
				}).setName("PriorityScheduler test 3 #2"),
				new KThread(() -> {
					System.out.println("I'm " + KThread.currentThread().getName() + 
							" with priority 2, and I'm trying to send a msg to the list.");
					synchList.add(3);
					System.out.println("I'm " + KThread.currentThread().getName() + ", and I've sent msg 3.");
				}).setName("PriorityScheduler test 3 #3"),
				new KThread(() -> {
					System.out.println("I'm " + KThread.currentThread().getName() + 
							" with priority 1, and I'm trying to send a msg to the list.");
					synchList.add(4);
					System.out.println("I'm " + KThread.currentThread().getName() + ", and I've sent msg 4.");
				}).setName("PriorityScheduler test 3 #4"),
				
			};
			
			boolean intStatus = Machine.interrupt().disable();
			
			ThreadedKernel.scheduler.setPriority(threads[0], 5);
			ThreadedKernel.scheduler.setPriority(threads[1], 4);
			ThreadedKernel.scheduler.setPriority(threads[2], 2);
			ThreadedKernel.scheduler.setPriority(threads[3], 1);
			
			for (KThread thread : threads) {
				thread.fork();
			}
			
			Machine.interrupt().setStatus(intStatus);
			
			ThreadedKernel.alarm.waitUntil(0);
		}
		
//		{
//			System.out.println();
//			System.out.println("Priority scheduler self test #4: More priority donation.");
//			boolean intStatus = Machine.interrupt().disable();
//			ThreadedKernel.scheduler.setPriority(0);
//			
//			Machine.interrupt().setStatus(intStatus);
//		}
	}
}
