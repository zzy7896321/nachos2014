package nachos.threads;

/**
 * A <i>communicator</i> allows threads to synchronously exchange 32-bit
 * messages. Multiple threads can be waiting to <i>speak</i>, and multiple
 * threads can be waiting to <i>listen</i>. But there should never be a time
 * when both a speaker and a listener are waiting, because the two threads can
 * be paired off at this point.
 * 
 * <p>
 * Communicator2 uses only a 32-bit buffer. Threads that are interrupting a communication
 * channel has to busy wait. Busy waiting leads to two undesired behaviour:
 * <ol>
 * <li> Speakers and Listeners could wait simultaneously.
 * <li> The time complexity is O(n^2) in worst case.
 * </ol>
 * 
 * <p>
 * Hence, Communicator2 should SELDOM be needed unless O(|speaker|) space complexity is
 * unacceptable.
 */
@Deprecated
public class Communicator2 {
	/**
	 * Allocate a new communicator.
	 */
	public Communicator2() {
		this.lock = new Lock();
		this.waitingThreads = new Condition(lock);
		this.wakerThread = new Condition(lock);
		this.interruptingThreads = new Condition(lock);
		this.numWaiting = 0;
		this.inCommunication = false;
	}

	/**
	 * Wait for a thread to listen through this communicator, and then transfer
	 * <i>word</i> to the listener.
	 * 
	 * <p>
	 * Does not return until this thread is paired up with a listening thread.
	 * Exactly one listener should receive <i>word</i>.
	 * 
	 * @param word
	 *            the integer to transfer.
	 */
	public void speak(int word) {
		lock.acquire();
		
		/* I'm interrupting an on-going communication. I should wait them to finish. */
		while (inCommunication) {
			interruptingThreads.sleep();
		}
		
		if (numWaiting >= 0) {
			/* no one's waiting or at least one speaker's waiting. */
			++numWaiting;
			waitingThreads.sleep();	// Wait for a listener to wake me up.
			--numWaiting;
			
			message = word;
			wakerThread.wake();	// wake the listener up
		}
		
		else {
			/* at least one listener's waiting. */
			inCommunication = true;
			waitingThreads.wake();
			message = word;
		}
		
		lock.release();
	}

	/**
	 * Wait for a thread to speak through this communicator, and then return the
	 * <i>word</i> that thread passed to <tt>speak()</tt>.
	 * 
	 * @return the integer transferred.
	 */
	public int listen() {
		int word;
		lock.acquire();

		/* have to re-check for other interrupting threads waken up before current could again set inCommunication to true */
		while (inCommunication) {
			interruptingThreads.sleep();
		}
		
		if (numWaiting > 0) {
			/* at least one speaker's waiting. */
			inCommunication = true;
			waitingThreads.wake();	// wake up one speaker
			wakerThread.sleep();	// wait for the speaker to wake me up
		}
		
		else {
			/* no one's waiting or at least one listener's waiting */
			--numWaiting;
			waitingThreads.sleep();		// Wait for a speaker to wake me up.
			++numWaiting;
		}
		
		word = message;
		inCommunication = false;
		interruptingThreads.wakeAll();	// wake up all the interrupting threads
		lock.release();
		return word;
	}

	/** The lock. */
	private Lock lock;
	
	/** Threads that are waiting for counterparts to start the communication. */
	private Condition waitingThreads;

	/** One thread that is waiting its counterparts to send the message. 
	 *  This added only for performance consideration, for o.w. the listener has
	 *  to wait on interruptingThreads condition, and will be waken up along
	 *  with the interrupting threads. They will have to check twice whether 
	 *  a communication is on-going. */
	private Condition wakerThread;
	
	/** Threads that are interrupting the on-going communication. */
	private Condition interruptingThreads;
	
	/** The message buffer. */
	private int message;
	
	/** Number of waiting speaker or listener. Positive number indicates 
	 * speakers are waiting, while negative number indicates listeners are waiting. */
	private int numWaiting;
	
	/** Whether there are two threads communicating. */
	private boolean inCommunication;
	
	public static void selfTest() {
		System.out.println();
		System.out.println("Communicator2 self test.");
		
		class Speaker implements Runnable {
			public Speaker(int num, Communicator2 communicator, int waitTicks) { 
				this.num = num;
				this.communicator = communicator; 
				this.waitTicks = waitTicks;
			}
			private int num;
			private Communicator2 communicator;
			private int waitTicks;
			public void run() {
				if (waitTicks > 0) {
					System.out.println("I'm speaker #" + num + ", and I'll wait for " + waitTicks + " ticks.");
					ThreadedKernel.alarm.waitUntil(waitTicks);
				}
				System.out.println("I'm speaker #" + num + ", and I'm speaking to the communicator."); 
				communicator.speak(num);
			}
		}
		
		class Listener implements Runnable {
			public Listener(int num, Communicator2 communicator, int waitTicks) {
				this.num = num;
				this.communicator = communicator; 
				this.waitTicks = waitTicks;
			}
			private int num;
			private Communicator2 communicator;
			private int waitTicks;
			public void run() {
				if (waitTicks > 0) {
					System.out.println("I'm listener #" + num + ", and I'll wait for " + waitTicks + " ticks.");
					ThreadedKernel.alarm.waitUntil(waitTicks);
				}
				System.out.println("I'm listener #" + num + ", and I'm litsening to the communicator.");
				int msg = communicator.listen();
				System.out.println("I'm listener #" + num + ", and I've heard from speaker #" + msg + ".");
			}
		}
		
		Communicator2 communicator = new Communicator2();
		{
			System.out.println("Communicator2 test #1: one speaker waits for one listner.");
			KThread threads[] = new KThread[] {
				new KThread(new Speaker(1, communicator, 0)).setName("speaker #1"),
				new KThread(new Listener(1, communicator, 600)).setName("listener #1")
			};
			for (KThread thread : threads) {
				thread.fork();
			}
			for (KThread thread : threads) {
				thread.join();
			}
		}
		
		{
			System.out.println();
			System.out.println("Communicator2 test #2: speaker, listener, listner, speaker.");
			KThread threads[] = new KThread[] {
				new KThread(new Speaker(1, communicator, 0)).setName("speaker #1"),
				new KThread(new Listener(1, communicator, 600)).setName("listener #1"),
				new KThread(new Listener(2, communicator, 600)).setName("listener #2"),
				new KThread(new Speaker(2, communicator, 1200)).setName("speaker #2")
			};
			for (KThread thread : threads) {
				thread.fork();
			}
			for (KThread thread : threads) {
				thread.join();
			}
		}
		
		final int num_participants = 5;
		
		{
			System.out.println();
			System.out.printf("Communicator2 test #3: speaker * %d, listener * %d\n", num_participants, num_participants);
			KThread threads[] = new KThread[num_participants * 2];
			for (int i = 0; i < num_participants; ++i) {
				threads[i] = new KThread(new Speaker(i+1, communicator, 0)).setName("speaker #" + (i+1));
				threads[i+num_participants] = new KThread(new Listener(i+1, communicator, 600)).setName("listener #" + (i+1));
			}
			for (KThread thread : threads) {
				thread.fork();
			}
			for (KThread thread : threads) {
				thread.join();
			}
			
		}
		
		{
			System.out.println();
			System.out.printf("Communicator2 test #4: listener * %d, speaker * %d\n", num_participants, num_participants);
			KThread threads[] = new KThread[num_participants * 2];
			for (int i = 0; i < num_participants; ++i) {
				threads[i+num_participants] = new KThread(new Speaker(i+1, communicator, 0)).setName("speaker #" + (i+1));
				threads[i] = new KThread(new Listener(i+1, communicator, 600)).setName("listener #" + (i+1));
			}
			for (KThread thread : threads) {
				thread.fork();
			}
			for (KThread thread : threads) {
				thread.join();
			}
			
		}
	}
}
