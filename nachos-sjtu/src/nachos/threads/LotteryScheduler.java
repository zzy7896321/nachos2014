package nachos.threads;

import nachos.machine.Lib;
import nachos.machine.Machine;

/**
 * A scheduler that chooses threads using a lottery.
 * 
 * <p>
 * A lottery scheduler associates a number of tickets with each thread. When a
 * thread needs to be dequeued, a random lottery is held, among all the tickets
 * of all the threads waiting to be dequeued. The thread that holds the winning
 * ticket is chosen.
 * 
 * <p>
 * Note that a lottery scheduler must be able to handle a lot of tickets
 * (sometimes billions), so it is not acceptable to maintain state for every
 * ticket.
 * 
 * <p>
 * A lottery scheduler must partially solve the priority inversion problem; in
 * particular, tickets must be transferred through locks, and through joins.
 * Unlike a priority scheduler, these tickets add (as opposed to just taking the
 * maximum).
 */
public class LotteryScheduler extends PriorityScheduler { 
	// why does LotteryScheduler extend PriorityScheduler ?? 
	// almost all code from PriorityScheduler cannot be used without modification
	// (including code from the original framework)
	/**
	 * Allocate a new lottery scheduler.
	 */
	public LotteryScheduler() {
		/* It's lucky that I don't initialize anything in PriorityScheduler. */
	}

	@Override
	public ThreadQueue newThreadQueue(boolean transferPriority) {
		return new LotteryQueue(transferPriority);
	}
	
	/**
	 * Use getPriorityL instead.
	 */
	@Override
	@Deprecated
	public int getPriority(KThread thread) {
		Lib.assertTrue(Machine.interrupt().disabled());

		return getThreadStateL(thread).getPriority();
	}
	
	/**
	 * Use getEffectivePriorityL instead;
	 */
	@Override
	@Deprecated
	public int getEffectivePriority(KThread thread) {
		Lib.assertTrue(Machine.interrupt().disabled());

		return getThreadStateL(thread).getEffectivePriority();
	}

	@Override
	public void setPriority(KThread thread, int priority) {
		Lib.assertTrue(Machine.interrupt().disabled());

		Lib.assertTrue(priority >= priorityMinimum
				&& priority <= priorityMaximum);

		getThreadStateL(thread).setPriority(priority);
	}
	
	@Override
	public boolean increasePriority()
	{
		boolean intStatus = Machine.interrupt().disable();

		KThread thread = KThread.currentThread();

		long priority = getPriorityL(thread);
		if (priority == priorityMaximum)
		{
		  Machine.interrupt().restore(intStatus); // bug identified by Xiao Jia @ 2011-11-04
			return false;
		}

		setPriorityL(thread, priority + 1);

		Machine.interrupt().restore(intStatus);
		return true;
	}

	@Override
	public boolean decreasePriority()
	{
		boolean intStatus = Machine.interrupt().disable();

		KThread thread = KThread.currentThread();

		long priority = getPriorityL(thread);
		if (priority == priorityMinimum)
		{
		  Machine.interrupt().restore(intStatus); // bug identified by Xiao Jia @ 2011-11-04
			return false;
		}

		setPriorityL(thread, priority - 1);

		Machine.interrupt().restore(intStatus);
		return true;
	}
	
	/**
	 * Get the priority of the specified thread. Must be called with interrupts
	 * disabled.
	 * 
	 * @param thread
	 *            the thread to get the priority of.
	 * @return the thread's priority.
	 */
	public long getPriorityL(KThread thread) {
		Lib.assertTrue(Machine.interrupt().disabled());

		return getThreadStateL(thread).getPriorityL();
	}
	
	/**
	 * Get the priority of the current thread. Equivalent to
	 * <tt>getPriority(KThread.currentThread())</tt>.
	 * 
	 * @return the current thread's priority.
	 */
	public long getPriorityL() {
		return getPriorityL(KThread.currentThread());
	}
	
	/**
	 * Get the effective priority of the specified thread. Must be called with
	 * interrupts disabled.
	 * 
	 * <p>
	 * The effective priority of a thread is the priority of a thread after
	 * taking into account priority donations.
	 * 
	 * <p>
	 * For a priority scheduler, this is the maximum of the thread's priority
	 * and the priorities of all other threads waiting for the thread through a
	 * lock or a join.
	 * 
	 * <p>
	 * For a lottery scheduler, this is the sum of the thread's tickets and the
	 * tickets of all other threads waiting for the thread through a lock or a
	 * join.
	 * 
	 * @param thread
	 *            the thread to get the effective priority of.
	 * @return the thread's effective priority.
	 */
	public long getEffectivePriorityL(KThread thread) {
		Lib.assertTrue(Machine.interrupt().disabled());
		
		return getThreadStateL(thread).getEffectivePriorityL();
	}
	
	/**
	 * Get the effective priority of the current thread. Equivalent to
	 * <tt>getEffectivePriority(KThread.currentThread())</tt>.
	 * 
	 * @return the current thread's priority.
	 */
	public long getEffectivePriorityL() {
		return getEffectivePriorityL(KThread.currentThread());
	}
	
	/**
	 * Set the priority of the specified thread. Must be called with interrupts
	 * disabled.
	 * 
	 * @param thread
	 *            the thread to set the priority of.
	 * @param priority
	 *            the new priority.
	 */
	public void setPriorityL(KThread thread, long priority) {
		Lib.assertTrue(Machine.interrupt().disabled());

		Lib.assertTrue(priority >= priorityMinimum
				&& priority <= priorityMaximum);

		getThreadStateL(thread).setPriorityL(priority);
	}
	
	/**
	 * Set the priority of the current thread. Equivalent to
	 * <tt>setPriority(KThread.currentThread(), priority)</tt>.
	 * 
	 * @param priority
	 *            the new priority.
	 */
	public void setPriorityL(long priority) {
		setPriorityL(KThread.currentThread(), priority);
	}

	/**
	 * Same as <tt>increasePriority</tt>.
	 * @see increasePriority
	 */
	public boolean increasePriorityL() {
		return increasePriority();
	}
	
	/**
	 * Same as <tt>decreasePriority</tt>.
	 * @see decreasePriority
	 */
	public boolean decreasePriorityL() {
		return decreasePriority();
	}
	/**
	 * The thread queue pops a thread according to the categorical distribution
	 * with the number of tickets of all threads as the weights.
	 */
	protected class LotteryQueue extends ThreadQueue {
		
		LotteryQueue(boolean transferPriority) {
			this.transferPriority = transferPriority;
			
			this.waitingThreads = new LotteryQueueInternal();
			this.owner = null;
		}

		@Override
		public void waitForAccess(KThread thread) {
			Lib.assertTrue(Machine.interrupt().disabled());
			
			ThreadState thread_state = getThreadStateL(thread);
//			Lib.assertTrue(thread_state.waitingOn == null, "already waiting in another queue.");
			
			thread_state.waitingOn = this;
			thread_state.waitingOnHandle = waitingThreads.add(thread_state);
			
			if (transferPriority) {
				updateOwner(thread_state.getEffectivePriorityL());
			}
		}

		@Override
		public KThread nextThread() {
			Lib.assertTrue(Machine.interrupt().disabled());
			
			if (waitingThreads.empty()) return null;
			
			if (transferPriority) {
				/* drop the previous owner */
				updateOwner(-waitingThreads.getSum());
				owner = null;
			}
			
			ThreadState thread_state = waitingThreads.poll();
			thread_state.waitingOn = null;
			thread_state.waitingOnHandle = null;
			
			if (transferPriority) {
				owner = thread_state;
				updateOwner(waitingThreads.getSum());
			}
			
			return thread_state.thread;
		}

		@Override
		public void acquire(KThread thread) {
			Lib.assertTrue(Machine.interrupt().disabled());
			
//			Lib.assertTrue(waitingThreads.empty(), "cannot acquire access when waiting queue is not empty");
			
			if (transferPriority) {
				ThreadState thread_state = getThreadStateL(thread);
				owner = thread_state;
			}
		}

		@Override
		public void print() {
			System.out.print(toString());
		}
		
		private void updateOwner(long delta) {
			if (delta == 0) return ;
			ThreadState owner = this.owner;
			while (owner != null) {
				owner.effectivePriority += delta;
				if (owner.waitingOn != null) {
					owner.waitingOnHandle.updated();
					owner = owner.waitingOn.owner;
				}
				else {
					owner = null;
				}
			}
		}
		
		/* pick next thread is meaningless */
		
		/**
		 * <tt>true</tt> if this queue should transfer priority from waiting
		 * threads to the owning thread.
		 */
		public boolean transferPriority;
		
		/** the queue of waiting threads */
		private LotteryQueueInternal waitingThreads;
		
		/** the thread state of the last thread that acquired access from this queue */
		private ThreadState owner;
		
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			
			for (ThreadState thread_state : waitingThreads) {
				builder.append(thread_state).append('\n');
			}
			return builder.toString();
		}
	}

	/**
	 * Don't use this, or an assertion failure will occur.
	 */
	@Override
	@Deprecated
	protected PriorityScheduler.ThreadState getThreadState(KThread thread) {
		Lib.assertNotReached("LotteryScheduler.ThreadState is not compatible with PriorityScheduler.ThreadState");
		return null;
	}

	/**
	 * Returns the scheduling state of the specific thread.
	 * 
	 * @param thread	the thread
	 * @return			the scheduling state of the thread
	 */
	protected ThreadState getThreadStateL(KThread thread) {
		if (thread.schedulingState == null)
			thread.schedulingState = new ThreadState(thread);
		
		return (ThreadState) thread.schedulingState;
	}

	/**
	 * Lottery scheduler uses long instead of int as the type of priority.
	 */
	protected class ThreadState {
		
		/**
		 * Constructs a new ThreadState object and associate it with the
		 * specific thread. The priority is set to default.
		 * 
		 * @param thread	the thread this state is associated with
		 */
		public ThreadState(KThread thread) {
			this.thread = thread;
			this.priority = this.effectivePriority = priorityDefault;
			
			this.waitingOn = null;
			this.waitingOnHandle = null;	
		}
		
		/**
		 * Returns the priority of the associated thread.
		 * Use getPriorityL to get the real priority.
		 * This method exists only for backward compatibility.
		 * 
		 * @return	the priority
		 */
		@Deprecated
		public int getPriority() {
			return (int) priority;
		}
		
		/**
		 * Returns the effective priority of the associated thread.
		 * Use getEffectivePriorityL to retrieve the real effective priority.
		 * This method exists for backward compatibility.
		 * 
		 * @return	the effective priority
		 */
		@Deprecated
		public int getEffectivePriority() {
			return (int) this.effectivePriority;
		}
		
		/**
		 * Returns the priority of the associated thread.
		 * 
		 * @return	the priority
		 */
		public long getPriorityL() {
			return this.priority;
		}
		
		/**
		 * Returns the effective priority of the associated thread.
		 * 
		 * @return	the effective priority
		 */
		public long getEffectivePriorityL() {
			return this.effectivePriority;
		}
		
		/**
		 * Sets the priority of the associated thread.
		 * 
		 * @param priority	the new priority
		 */
		public void setPriority(int priority) {
			setPriorityL((long) priority);
		}
		
		/**
		 * Sets the priority of the associated thread.
		 * 
		 * @param priority	the new priority
		 */
		public void setPriorityL(long priority) {
			if (this.priority == priority)
				return ;
			
			long delta = priority - this.priority;
			this.priority = priority;
			this.effectivePriority += delta;
			if (waitingOn != null) {
				waitingOnHandle.updated();
				waitingOn.updateOwner(delta);
			}
			
		}
		
		/* waitForAccess and acquire are not available since they are useless */
		
		/** The thread this object is associated with. */
		protected KThread thread;
		
		/** The priority of the associated thread. */
		protected long priority;
		
		/** The cached effective priority. (Need to be updated whenever possible priority or acquired queue changes.) */
		protected long effectivePriority;
		
		/** The queue that this thread is waiting on. */
		protected LotteryQueue waitingOn;
		
		/** The handle of this object in the tree of waitingOn. */
		protected LotteryQueueInternal.NodeHandle waitingOnHandle;
		
		@Override
		public String toString() {
			return thread.toString() + String.format("[p=%d, ep=%d]", priority, effectivePriority);
		}
	}
	
	/** The minimum number of tickets to be assigned to a single thread. */
	public static final long priorityMinimum = 1;
	
	/** The maximum number of tickets to be assigned to a single thread. */
	public static final long priorityMaximum = Integer.MAX_VALUE;
	
	/** The default number of tickets to be assigned to a signle thread. */
	@SuppressWarnings("unused")
	public static final long priorityDefault = 
			(PriorityScheduler.priorityDefault >= priorityMinimum && PriorityScheduler.priorityDefault <= priorityMaximum) ?
					PriorityScheduler.priorityDefault : 1;
	
	public static void selfTest() {
		
		{
			System.out.println();
			System.out.println("Lottery scheduler self test #1: threads with equal priority");
			
			boolean intStatus = Machine.interrupt().disable();
			KThread threads[] = new KThread[5];
			for (int i = 0; i < threads.length; ++i) {
				threads[i] = new KThread(() -> {
					System.out.println("I'm" + KThread.currentThread().getName() + ".");
				}).setName("LotterySchedduler test 1 #" + i);
				threads[i].fork();
			}
			Machine.interrupt().restore(intStatus);
			
			ThreadedKernel.alarm.waitUntil(0);
		}
		
		{
			System.out.println();
			System.out.println("Lottery scheduler self test #2: threads with priority 1, 10, 100, 1000, 100000, 1000000");
			
			
			boolean intStatus = Machine.interrupt().disable();
			int priorities[] = {1, 10, 100, 1000, 100000, 1000000};
			KThread threads[] = new KThread[priorities.length];
			for (int i = 0; i < threads.length; ++i) {
				threads[i] = new KThread(() -> {
					System.out.println("I'm" + KThread.currentThread().getName() + ".");
				}).setName("LotterySchedduler test 2 #" + i + "[p=" + priorities[i] + "]");
				ThreadedKernel.scheduler.setPriority(threads[i], priorities[i]);
				threads[i].fork();
			}
			Machine.interrupt().restore(intStatus);
			
			ThreadedKernel.alarm.waitUntil(0);
		}
		
		{
			System.out.println();
			System.out.println("Lottery scheduler self test #3: priority transfer");
			System.out.println("#0[p=1], #1[p=1000000] join #0, #2[p=1000]");
			
			boolean intStatus = Machine.interrupt().disable();
			KThread threads[] = new KThread[3];
			threads[0] = new KThread(()->{
				ThreadedKernel.alarm.waitUntil(0);
				System.out.println("I'm #0, and I'm done.");
			}).setName("LotteryScheduler test 2 #0");
			ThreadedKernel.scheduler.setPriority(threads[0], 1);
			
			threads[1] = new KThread(()->{
				threads[0].join();
				System.out.println("I'm #1, and I'm done.");
			}).setName("LotteryScheduler test 2 #1");
			ThreadedKernel.scheduler.setPriority(threads[1], 1000000);
			
			threads[2] = new KThread(()->{
				ThreadedKernel.alarm.waitUntil(0);
				System.out.println("I'm #2, and I'm done.");
			}).setName("LotteryScheduler test 2 #2");
			ThreadedKernel.scheduler.setPriority(threads[2], 1000);
			
			for (int i = 0; i < threads.length; ++i) {
				threads[i].fork();
			}
			
			Machine.interrupt().restore(intStatus);
			
			ThreadedKernel.alarm.waitUntil(500);
		}
		
	}
}
