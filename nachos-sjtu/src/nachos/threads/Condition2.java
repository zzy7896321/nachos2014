package nachos.threads;

import nachos.machine.*;

//TODO implement condition2

/**
 * An implementation of condition variables that disables interrupt()s for
 * synchronization.
 * 
 * <p>
 * You must implement this.
 * 
 * @see nachos.threads.Condition
 */
public class Condition2 {
	/**
	 * Allocate a new condition variable.
	 * 
	 * @param conditionLock
	 *            the lock associated with this condition variable. The current
	 *            thread must hold this lock whenever it uses <tt>sleep()</tt>,
	 *            <tt>wake()</tt>, or <tt>wakeAll()</tt>.
	 */
	public Condition2(Lock conditionLock) {
		this.conditionLock = conditionLock;
		
		this.waitQueue = ThreadedKernel.scheduler.newThreadQueue(false);
	}

	/**
	 * Atomically release the associated lock and go to sleep on this condition
	 * variable until another thread wakes it using <tt>wake()</tt>. The current
	 * thread must hold the associated lock. The thread will automatically
	 * reacquire the lock before <tt>sleep()</tt> returns.
	 */
	public void sleep() {
		Lib.assertTrue(conditionLock.isHeldByCurrentThread());
		
		boolean intStatus = Machine.interrupt().disable();
		this.waitQueue.waitForAccess(KThread.currentThread());
		
		conditionLock.release();
		KThread.sleep();
		conditionLock.acquire();
		
		Machine.interrupt().setStatus(intStatus);
	}

	/**
	 * Wake up at most one thread sleeping on this condition variable. The
	 * current thread must hold the associated lock.
	 */
	public void wake() {
		Lib.assertTrue(conditionLock.isHeldByCurrentThread());
		
		boolean intStatus = Machine.interrupt().disable();
		
		KThread next_thread = this.waitQueue.nextThread();
		if (next_thread != null) {
			next_thread.ready();
		}
		
		Machine.interrupt().setStatus(intStatus);
	}

	/**
	 * Wake up all threads sleeping on this condition variable. The current
	 * thread must hold the associated lock.
	 */
	public void wakeAll() {
		Lib.assertTrue(conditionLock.isHeldByCurrentThread());
		
		/* Don't keep the interrupt disabled across the loop. */
		while (true) {
			boolean intStatus = Machine.interrupt().disable();
			KThread thread = this.waitQueue.nextThread();
			if (thread == null) break;
			thread.ready();
			Machine.interrupt().setStatus(intStatus);
		}
		
	}

	private Lock conditionLock;
	private ThreadQueue waitQueue;
	
	public static void selfTest() {
		/* the main and forked thread uses a condition2 to communicate with each other. */
		
		Lock lock = new Lock();
		Condition2 condition = new Condition2(lock);
		
		java.util.LinkedList<String> main_to_forked_msg = new java.util.LinkedList<String>();
		java.util.LinkedList<String> forked_to_main_msg = new java.util.LinkedList<String>(); 
		
		class Condition_test_runnable implements Runnable {
			public Condition_test_runnable(Lock lock, Condition2 condition, 
					java.util.LinkedList<String> main_to_forked_msg,
					java.util.LinkedList<String> forked_to_main_msg) {
				this.lock = lock;
				this.condition = condition;
				this.main_to_forked_msg = main_to_forked_msg;
				this.forked_to_main_msg = forked_to_main_msg;
			}
			private Lock lock;
			private Condition2 condition;
			private java.util.LinkedList<String> main_to_forked_msg, forked_to_main_msg;
			
			public void run() {
				System.out.println("I'm forked, and I'm acquiring the lock to receive from main the message.");
				
				lock.acquire();
				String msg = main_to_forked_msg.removeFirst();
				if (msg != null)
					System.out.println("I'm forked, and I've got the message: " + msg);
				else
					System.out.println("I'm forked, but strangely I got nothing from main.");
				
				msg = "Hello, main!";
				System.out.println("I'm forked, and whatever, I'm sending main a message: " + msg);
				forked_to_main_msg.add(msg);
				condition.wake();
				lock.release();
			}
		}
		
		KThread thread = new KThread(new Condition_test_runnable(lock, condition, main_to_forked_msg, forked_to_main_msg))
			.setName("condition2_selfTest_forked");
		
		lock.acquire();
		String msg = "Hello, forked!";
		System.out.println("I'm main. I have acquired the lock and I'm sending to forked a message: " + msg);
		main_to_forked_msg.add(msg);
		
		thread.fork();
		System.out.println("I'm main, and I'm waiting for the forked to reply.");
		condition.sleep();
		
		msg = forked_to_main_msg.removeFirst();
		if (msg != null)
			System.out.println("I'm main and I'm waken up. I've got the message: " + msg);
		else
			System.out.println("I'm main and I'm waken up. But things doesn't seem right, no message received!");
		lock.release();
		
		thread.join();
		System.out.println("I'm main, and we are all done.");
	}
}
