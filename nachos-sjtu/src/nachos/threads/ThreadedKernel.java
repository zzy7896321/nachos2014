package nachos.threads;

import nachos.machine.*;

/**
 * A multi-threaded OS kernel.
 */
public class ThreadedKernel extends Kernel {
	/**
	 * Allocate a new multi-threaded kernel.
	 */
	public ThreadedKernel() {
		super();
	}

	/**
	 * Initialize this kernel. Creates a scheduler, the first thread, and an
	 * alarm, and enables interrupts. Creates a file system if necessary.
	 */
	public void initialize(String[] args) {
		// set scheduler
		String schedulerName = Config.getString("ThreadedKernel.scheduler");
		scheduler = (Scheduler) Lib.constructObject(schedulerName);

		// set fileSystem
		String fileSystemName = Config.getString("ThreadedKernel.fileSystem");
		if (fileSystemName != null)
			fileSystem = (FileSystem) Lib.constructObject(fileSystemName);
		else if (Machine.stubFileSystem() != null)
			fileSystem = Machine.stubFileSystem();
		else
			fileSystem = null;

		// start threading
		new KThread(null);

		alarm = new Alarm();

		Machine.interrupt().enable();
	}

	/**
	 * Test this kernel. Test the <tt>KThread</tt>, <tt>Semaphore</tt>,
	 * <tt>SynchList</tt>, and <tt>ElevatorBank</tt> classes. Note that the
	 * autograder never calls this method, so it is safe to put additional tests
	 * here.
	 */
	public void selfTest() {
		KThread.selfTest();
		Semaphore.selfTest();
		System.out.println("Semaphore test done");
		SynchList.selfTest();
		System.out.println("SynchList test done");
		
		KThread.test_join();
		
		SynchList.selfTest();
		Condition2.selfTest();
		Alarm.selfTest();
		
		long time0 = Machine.timer().getTime();
		Communicator.selfTest();
		long time1 = Machine.timer().getTime() - time0;
		Communicator2.selfTest();
		long time2 = Machine.timer().getTime() - time1;
		System.out.printf("Communicator time = %d\n", time1); 
		System.out.printf("Communicator2 time = %d\n", time2);
		
		if (!(ThreadedKernel.scheduler instanceof LotteryScheduler)) {
			PriorityScheduler.selfTest();
		}
		else {
			LotteryScheduler.selfTest();
		}
		
		Boat.selfTest();
		
		PriorityQueue.selfTest();
		
	}

	/**
	 * A threaded kernel does not run user programs, so this method does
	 * nothing.
	 */
	public void run() {
	}

	/**
	 * Terminate this kernel. Never returns.
	 */
	public void terminate() {
		finish();
		
		Machine.halt();
	}

	/**
	 * Clear the kernel resources before halting the machine. 
	 */
	protected void finish() {
		
	}
	
	/**
	 * Pick the next threads. Invoked by KThread.runNextThread().
	 * 
	 * @return
	 */
	public KThread pickNextThread() {
		KThread nextThread = null;
		if (KThread.numReady > 0) {
			--KThread.numReady;
			nextThread = KThread.readyQueue.nextThread();
		}
		else {
			nextThread = KThread.idleThread;
		}
		
		return nextThread;
	}

	/** Globally accessible reference to the scheduler. */
	public static Scheduler scheduler = null;
	/** Globally accessible reference to the alarm. */
	public static Alarm alarm = null;
	/** Globally accessible reference to the raw file system. */
	public static FileSystem fileSystem = null;

}
