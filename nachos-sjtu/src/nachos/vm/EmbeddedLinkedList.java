package nachos.vm;

/**
 * An doubly-linked list which can be embedded in other data structures.
 * <p>
 * To embed the linked list into another data structure, the data structure
 * should be implemented in a class extending this class. However, with this
 * inheritance scheme prevents a multiple list or other embedded data structures
 * to be embedded into one simultaneously due to the single inheritance restriction
 * from Java.
 *
 */
public class EmbeddedLinkedList {

	/**
	 * Constructs a new embedded linked list node forming a single-node linked list.
	 */
	public EmbeddedLinkedList() {
		list_next = list_prev = this;
	}

	/**
	 * Removes this from a list.
	 * 
	 * @return	this
	 */
	public EmbeddedLinkedList remove_from_list() {
		list_prev.list_next = list_next;
		list_next.list_prev = list_prev;
		
		list_next = list_prev = this;
		return this;
	}

	/**
	 * Insert this before the head node. Must be called when list_next == list_prev == this.
	 * 
	 * @param head		the head node
	 * @return			head
	 */
	public EmbeddedLinkedList insert_before(EmbeddedLinkedList head) {
		head.list_prev.list_next = this;
		list_prev = head.list_prev;
		head.list_prev = this;
		list_next = head;
		return head;
	}
	
	/**
	 * Insert this after the head node. Must be called when list_next == list_prev == this.
	 * 
	 * @param head		the head node
	 * @return			head
	 */
	public EmbeddedLinkedList insert_after(EmbeddedLinkedList head) {
		head.list_next.list_prev = this;
		list_next = head.list_next;
		head.list_next = this;
		list_prev = head;
		return head;
	}
	
	/**
	 * Returns the next node.
	 * 
	 * @return	next
	 */
	public EmbeddedLinkedList next() {
		return list_next;
	}
	
	/**
	 * Returns the previous node.
	 * 
	 * @return	prev	
	 */
	public EmbeddedLinkedList prev() {
		return list_prev;
	}
	
	public EmbeddedLinkedList list_next, list_prev;
}
