package nachos.vm;

/**
 * Frame entry data structure which is used to book-keeping
 * physical memory usage and is referenced by vm page entries.
 *
 */
public class FrameEntry extends EmbeddedLinkedList {

	/**
	 * Constructs a frame entry with the specified physical frame number.
	 * 
	 * @param ppn 	the physical frame number
	 */
	public FrameEntry(int ppn) {
		this.ppn = ppn;
		vmPage = null;
		status = VM.FRAME_STATUS_NOT_IN_LIST;
	}
	
	/** The frame number. */
	public int ppn;
	
	/** Whether the frame is used. */
	public boolean used;
	
	/** The vm page that this frame is containing. <tt>null</tt> if
	 * this frame has never loaded with any page. */
	public VMPageEntry vmPage;
	
	/** The status can be one of the following: (VM constants)
	 * <ul>
	 * <li> FRAME_STATUS_NOT_IN_LIST: this frame is not in any list, 
	 * indicating this frame is being loaded 
	 * <li> FRAME_STATUS_FREE: this frame is free. If vmPage is not <tt>null</tt>,
	 * it is the last page loaded to this frame.
	 * <li> FRAME_STATUS_CLEAN: this frame is clean.
	 * <li> FRAME_STATUS_DIRTY: this frame is dirty.
	 * <li> FRAME_STATUS_FRESH: this frame is freshly loaded and is never 
	 * accessed since the last time the tlb entries are examined.
	 * </ul> 
	 * @see nachos.vm.VM
	 * */
	public int status;
	
	@Override
	public String toString() {
		if (vmPage == null) return "frame " + ppn + " : null";
		return "frame " + ppn + " : " + vmPage.toString();
	}
}
