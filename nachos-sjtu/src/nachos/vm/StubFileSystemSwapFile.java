package nachos.vm;

import nachos.machine.OpenFile;
import nachos.machine.Lib;

/**
 * Uses a file in stub file system as the swap file. Relies on VMKernel.vmfs.
 *
 */
public class StubFileSystemSwapFile extends SwapFile {

	public StubFileSystemSwapFile(String name) {
		swapFileName = name;
		
		VMFileSystemWrapper.FileInfo swapFileInfo = (VMFileSystemWrapper.FileInfo) VMKernel.vmfs.acquireFileInfo(swapFileName);
		openFile = VMKernel.fileSystem.open(swapFileName, true);
		
		swapFileInfo.sem.P();
		Lib.assertTrue(swapFileInfo.openCount == 1);
		swapFileInfo.setExclusive(true);
		swapFileInfo.sem.V();
		
		/* no longer need it as long as openFile is not closed */
		VMKernel.vmfs.releaseFileInfo(swapFileInfo);
	}

	@Override
	public boolean read(int spn, byte[] buffer, int pn) {
		return openFile.read(spn * pageSize, buffer, pn * pageSize, pageSize) == pageSize;
	}

	@Override
	public boolean write(int spn, byte[] buffer, int pn) {
		return openFile.write(spn * pageSize, buffer, pn * pageSize, pageSize) == pageSize;
	}

	@Override
	public void close() {
		openFile.close();
	}
	
	private String swapFileName;
	
	private OpenFile openFile;

	@Override
	public int maxNumberPages() {
		return Integer.MAX_VALUE;	// assume that the file can grow infinitely on the stub file system
	}
}
