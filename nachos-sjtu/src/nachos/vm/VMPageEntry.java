package nachos.vm;

/**
 * A virtual memory page entry. An entry may be shared by multiple processes, even in
 * the same page table (not now).
 *
 */
public class VMPageEntry extends EmbeddedLinkedList {

	/**
	 * Constructs a new virtual memory page entry.
	 */
	public VMPageEntry() {
		super();
		
		type = TYPE_INVALID;
		valid = false;
		storeInfo = null;
		ppn = -1;
		ref_count = 0;
	}
	
	/** The type of the VM page. */
	public int type;
	
	/** Whether the content in the frame is valid. */
	public boolean valid;
	
	/** Whether the VM page is read-only. */
	public boolean readOnly;

	/** Whether the VM page is executable. */
	public boolean executable;
	
	/** The physical memory frame that the VM page is loaded to. */
	public int ppn;
	
	/** Info of the backing store that contains the page, which varies depending on the type. 
	 * <p>
	 * <ul> When type ==
	 * <li> TYPE_OBJECT_FILE, storeInfo is of type VM.ObjectFilePage
	 * <li> TYPE_ANONYMOUS, storeInfo is <tt>null</tt> if this page
	 * was never swapped out, or of type VM.SwapFilePage if this page
	 * was once swapped out. SawpFilePage won't be released until the process
	 * exits.
	 * </ul>*/
	public Object storeInfo;
	
	/** The reference count to this VM page entry. */
	public int ref_count;

	/** The VM page entry is just initialised and contains nothing useful. */
	public static final int TYPE_INVALID = 0;
	
	/** The VM page entry maps a page from an object file. */
	public static final int TYPE_OBJECT_FILE = 1;
	
	/** The VM page entry maps an anonymous page (stack, heap, though heap is not supported now),
	 * <p>
	 * storeInfo is set to
	 * <ul>
	 * <li> <tt>null</tt> when the page is never loaded.
	 * <li> an SwapFilePage object
	 * </ul> */
	public static final int TYPE_ANONYMOUS = 2;
	
	public String name = "anonymous";
	
	@Override
	public String toString() {
		return name;
	}
}
