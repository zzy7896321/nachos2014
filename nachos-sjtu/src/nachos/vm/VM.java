package nachos.vm;

import java.io.EOFException;

import nachos.machine.Coff;
import nachos.machine.CoffSection;
import nachos.machine.Processor;
import nachos.machine.OpenFile;
import nachos.machine.Lib;
import nachos.threads.Condition2;
import nachos.threads.Lock;
import nachos.machine.TranslationEntry;

import java.util.Arrays;
import java.util.HashMap;

/**
 * The memory management subsystem.
 * <p>
 * This class manages the physical memory, the swap file and the virtual page entries.
 * 
 * <p>
 * This class must be initialised after VMFileSystemWrapper is ready.
 *
 */
public class VM {
	/**
	 * Constructs a VM.
	 * @see VM 
	 */
	public VM() {
		/* initialises frame entries */
		numFrames = VMKernel.processor.getNumPhysPages();
	
		freeFrames = new FrameList(FRAME_STATUS_FREE);
		cleanFrames = new FrameList(FRAME_STATUS_CLEAN);
		dirtyFrames = new FrameList(FRAME_STATUS_DIRTY);
		freshFrames = new FrameList(FRAME_STATUS_FRESH);
	
		frames = new FrameEntry[numFrames];
		for (int i = 0; i < numFrames; ++i) {
			frames[i] = new FrameEntry(i);
			freeFrames.addToTail(frames[i]);
		}
	
		/* the swap file */
		swapFile = VMKernel.swapFile;	// this is initialised during the kernel initialisation
		
		/* VM page info */
		executableFiles = new HashMap<String, ExecutableFile>();
		
		/* the lock */
		lock = new Lock();
	}

	/**
	 * Allocates virtual memory pages for the coff.
	 * <p>
	 * One page table section is created for each section in the coff.
	 * Initialised sections are marked as TYPE_OBJECT_FILE, while unitialised
	 * ones are marked as TYPE_ANONYMOUS.
	 * <p>
	 * This process may block since the page table lock has to be acquired to 
	 * look up shared pages.
	 * 
	 * @param process		the process to which virtual memory pages are to be allocated
	 * @param executable	the file name of the executable (directly passed to the file system)
	 * @return				<tt>true</tt> on success, <tt>false</tt> o.w.
	 */
	public boolean allocateVMPageForCoff(VMProcess process, String executable) {
		acquireLock();
		
		PageTable pageTable = process.pageTable;
	
		/* looks up the executable file in the file record. If no record exists,
		 * getExecutableFileRecord call will handle the opening of the coff file. */
		ExecutableFile exec_file = getExecutableFileRecord(executable);
		if (exec_file == null) {
			releaseLock();
			return false;
		}
		
		Coff coff = exec_file.coff;
	
		/* iterates through the sections and allocates pages */
		for (int section_no = 0; section_no < coff.getNumSections(); ++section_no) {
			CoffSection coffSection = coff.getSection(section_no);
			int start_pn = coffSection.getFirstVPN();
			int length = coffSection.getLength();
			
			if (length == 0) continue;
			
			if (!pageTable.addPageTableSection(start_pn, length)) {
				Lib.debug(dbgVM, "overlapping section in coff");
				releaseExecutableFileRecord(executable);
				
				/* release the lock */
				releaseLock();
				return false;
			}
			
			PageTableSection section = pageTable.getPageTableSection(start_pn);
			
			/* initialised section */
			if (coffSection.isInitialzed()) {
				
				/* read-only pages are shared */
				if (coffSection.isReadOnly()) {
					for (int i = 0; i < length; ++i) {
						section.vmPage[i] = exec_file.vmSections[section_no][i];
						section.vmPage[i].ref_count += 1;
					}
				}
				
				else {
					/* though not likely that this page is executable
					 * have to use such an awkward way to determine whether a page is executable
					 * since the executable variable is not exposed to outside nachos.machine */
					boolean isExecutable = coffSection.getName().contains("text");
					
					String page_name = exec_file.file.getName() + coffSection.getName();
					
					for (int i = 0; i < length; ++i) {
						VMPageEntry vmPage = new VMPageEntry();
						vmPage.type = VMPageEntry.TYPE_OBJECT_FILE;
						vmPage.name = page_name + "(" + i + ")";
						vmPage.valid = false;
						vmPage.readOnly = false;
						vmPage.executable = isExecutable;
						vmPage.storeInfo = new ObjectFilePage(coffSection, i);
						vmPage.ref_count += 1;
						
						section.vmPage[i] = vmPage;
					}
				}
			}
		
			/* uninitialised section */
			else {
				setAnonymousPages(section, exec_file.file.getName() + coffSection.getName());
				if (coffSection.isReadOnly()) {
					/* weird combination */
					for (int i = 0; i < length; ++i) {
						section.vmPage[i].readOnly = true;
					}
				}
			}
		}
		
		process.initialPC = coff.getEntryPoint();
		
		/* the process should hold the executable file record until it exits */
		process.executableFile = exec_file;
		
		/* release the lock */
		releaseLock();
		
		return true;
	}

	/**
	 * Gets the file record of an executable. If the record does not exist, 
	 * a new record will be created.
	 * 
	 * @param executable	the name of the executable
	 * @return				the record on sucess, <tt>null</tt> on failure
	 */
	private ExecutableFile getExecutableFileRecord(String executable) {
		ExecutableFile file = executableFiles.get(executable);
		
		if (file == null) {
			/* don't rely on VMKernel.vmfs here, since we may replace the file system with nachos.filsys.RealFileSystem in
			 * later phase */
			OpenFile openFile = VMKernel.fileSystem.open(executable, false);
			if (openFile == null) {
				Lib.debug(dbgVM, "open executable failed");
				return null;
			}
			
			Coff coff;
			try {
				coff = new Coff(openFile);
			} catch (EOFException e) {
				openFile.close();
				Lib.debug(dbgVM, "coff load failed");
				return null;
			}
			
			file = new ExecutableFile(openFile, coff);
			
			executableFiles.put(executable, file);
			
			file.vmSections = new VMPageEntry[coff.getNumSections()][];
			for (int section_no = 0; section_no < coff.getNumSections(); ++section_no) {
				CoffSection coffSection = coff.getSection(section_no);
				
				/* as explained before, we can neither rewrite the coff loader,
				 * nor access the executable variable in CoffSection */
				boolean isExecutable = coffSection.getName().contains("text");
			
				/* initialised read-only sections can be shared, so we need
				 * to keep a reference to the vm pages allocated for them */
				if (coffSection.isInitialzed() && coffSection.isReadOnly()) {
					String pageName = file.file.getName() + coffSection.getName();
					
					file.vmSections[section_no] = new VMPageEntry[coffSection.getLength()];
					for (int i = 0; i < coffSection.getLength(); ++i) {
						VMPageEntry vmPage = new VMPageEntry();
						vmPage.type = VMPageEntry.TYPE_OBJECT_FILE;
						vmPage.name = pageName + "(" + i + ")";
						vmPage.valid = false;
						vmPage.readOnly = true;
						vmPage.executable = isExecutable;
						vmPage.storeInfo = new ObjectFilePage(coffSection, i);
						vmPage.ref_count = 0;
						file.vmSections[section_no][i] = vmPage;
					}
				}
			}
		}
	
		/* increase the reference count, when it drops to 0, the file record can be released */
		file.ref_count += 1;
		
		return file;
	}

	/**
	 * Releases the file record of the executable. The record will be released
	 * as soon as reference count drops to 0.
	 * 
	 * @param executable	the name of the executable
	 * @return				<tt>true</tt> if the record does exist
	 */
	private boolean releaseExecutableFileRecord(String executable) {
		ExecutableFile file = executableFiles.get(executable);
		
		if (file == null) return false;
		
		file.ref_count -= 1;
		if (file.ref_count == 0) {
			file.coff.close();
			executableFiles.remove(executable);
		}
		
		return true;
	}
	
	/**
	 * Allocates and adds anonymous page section to the pageTable.
	 * The section starts from the page that process.lowestSP is in and is
	 * of numPages long.
	 * <p>
	 * This call may block when acquiring the lock.
	 
	 * @param process		the process to allocate stack pages for
	 * @param numPages		the number of stack pages to be allocated
	 * @return				<tt>true</tt> on success
	 */
	public boolean allocateStackPages(VMProcess process, int numPages) {
		acquireLock();
		
		int startSP = process.lowestSP;
		PageTable pageTable = process.pageTable;
		
		int end_pn = Processor.pageFromAddress(startSP);
		int start_pn = end_pn - numPages;
		if (start_pn < 0 || end_pn > max_num_pages) {
		
			/* release the lock */
			releaseLock();
			return false;
		}
		
		if (!pageTable.addPageTableSection(start_pn, numPages)) {
		
			/* release the lock */
			releaseLock();
			return false;
		}
		
		PageTableSection section = pageTable.getPageTableSection(start_pn);
		setAnonymousPages(section, "stack");
		process.lowestSP = Processor.makeAddress(start_pn, 0);
	
		releaseLock();
		
		return true;
	}

	/**
	 * Initialises anonymous pages in the page table section.
	 * 
	 * @param section
	 * @param page_name
	 */
	private void setAnonymousPages(PageTableSection section, String page_name) {
		for (int i = section.start_pn; i < section.end_pn; ++i) {
			VMPageEntry vmPage = new VMPageEntry();
			vmPage.type = VMPageEntry.TYPE_ANONYMOUS;
			vmPage.name = page_name + "@" + Lib.toHexString(Processor.makeAddress(i, 0));
			vmPage.valid = false;
			vmPage.storeInfo = null;
			vmPage.readOnly = false;
			vmPage.ref_count += 1;
			
			section.vmPage[i - section.start_pn] = vmPage;
		}
	}

	/**
	 * Releases all pages allocated to the process.
	 * <p>
	 * This call may block when acquiring the lock.
	 * 
	 * @param process
	 */
	public void releasePages(VMProcess process) {
		/* acquire the resource lock */
		acquireLock();
		
		/* releases vm pages */
		PageTable pageTable = process.pageTable;
		if (pageTable != null) {
			PageTableSection p_section = pageTable.first();

			while (p_section != null) {
				for (int i = 0, length = p_section.end_pn - p_section.start_pn; 
						i < length; ++i) {

					VMPageEntry vmPage = p_section.vmPage[i];
					vmPage.ref_count -= 1;
					
					switch (vmPage.type) {
					case VMPageEntry.TYPE_ANONYMOUS:
						if (vmPage.valid) {
							int ppn = vmPage.ppn;
							vmPage.valid = false;
							releaseFrame(frames[ppn]);
						}
						if (p_section.vmPage[i].storeInfo != null) {
							SwapFilePage storeInfo = (SwapFilePage) vmPage.storeInfo;
							swapFile.releaseSwapPage(storeInfo.spn);
						}
						break;
					case VMPageEntry.TYPE_OBJECT_FILE:
						if (vmPage.valid) {
							int ppn = vmPage.ppn;
							if (vmPage.ref_count == 0 || frames[ppn].status == FRAME_STATUS_FRESH) {
								vmPage.valid = false;
								releaseFrame(frames[ppn]);
							}
						}
						break;
					default:
						Lib.debug(dbgVM, "unknown vm page entry type " + p_section.vmPage[i].type);
					}
				}

				if (p_section.list_next instanceof PageTableSection) {
					p_section = (PageTableSection) p_section.list_next;
				}
				else {
					/* end of the list */
					p_section = null;
				}
			} 
		}
	
		/* releases executable file record */
		ExecutableFile exec_file = process.executableFile;
		if (exec_file != null) {
			releaseExecutableFileRecord(exec_file.file.getName());
			process.executableFile = null;
		}
		
		/* release the lock */
		releaseLock();
	}

	/**
	 * This routine removes the frame from the list it is currently in
	 * and inserts it back to free frame list. Resources need to be released
	 * prior to calling this routine.
	 * 
	 * @param frame
	 */
	private void releaseFrame(FrameEntry frame) {
		switch (frame.status) {
		case FRAME_STATUS_CLEAN:
			cleanFrames.removeFromList(frame, false);
			freeFrames.addToTail(frame);
			break;
			
		case FRAME_STATUS_DIRTY:
			dirtyFrames.removeFromList(frame, false);
			freeFrames.addToTail(frame);
			break;
		
		case FRAME_STATUS_FRESH:
			freshFrames.removeFromList(frame, false);
			freeFrames.addToTail(frame);
			break;
		}
	}

	/**
	 * The TLB miss handler. If a page fault is identified when the tlb miss
	 * is being handled, the page fault will also be handled, which may lead
	 * to blocking for a very long time.
	 * <p>
	 * This call may block when acquiring the lock.
	 * 
	 * @param process
	 * @param bad_vaddr
	 * @return
	 */
	public int handleTLBMiss(VMProcess process, int bad_vaddr) {
		PageTable pageTable = process.pageTable;
		int bad_pn = Processor.pageFromAddress(bad_vaddr);
	
		/* acquire the lock */
		acquireLock();	// possible context switch
		
		/* fetch ref bits first */
		fetchTLBRefBits(process);

		/* 1. look up the address in the page table.
		 * If there is a valid entry, just replace one of the TLB entry. */
		VMPageEntry vmPage = pageTable.getPageTableEntry(bad_pn);
			
		if (vmPage == null) {
			/* invalid virtual page address */
		
			releaseLock();	// no context switch
			return HANDLE_TLB_MISS_BAD_ADDRESS;				
		}
			
		if (vmPage.valid) {
			/* page found, just do the TLB replacement,
			 * tlb flush is handled by replaceTLBEntry */
			replaceTLBEntry(process, bad_pn, vmPage);
		
			releaseLock();	// possible context switch
			return HANDLE_TLB_MISS_NORMAL;
		}
		
		/* 2. if there is a page fault,
		 *	  call handlePageFault to deal with it. 
		 *	  Note that call to handlePageFault may block.
		 * */
		int handle_page_fault_result = handlePageFault(process, vmPage);
		switch (handle_page_fault_result) {
		case HANDLE_PAGE_FAULT_NORMAL:
			replaceTLBEntry(process, bad_pn, vmPage);
			/* release the lock */
			releaseLock();	// possible context switch

			return HANDLE_TLB_MISS_NORMAL;
			
		case HANDLE_PAGE_FAULT_NO_AVAILABLE_FRAME:
			evictMyFrame(process);
			
			releaseLock();
			((VMKernel) VMKernel.kernel).suspendThread();
			
			/* just pretend we have dealt with the problem,
			 * next instruction will raise tlb miss again */
			
			return HANDLE_TLB_MISS_NORMAL;
			
		case HANDLE_PAGE_FAULT_LOAD_ERROR:
			/* fatal error */
			releaseLock();
			return HANDLE_TLB_MISS_LOAD_ERROR;
			
		}

		/* release the lock */
		releaseLock();
	
		return HANDLE_TLB_MISS_UNKNOWN;
	}
	
	
	public static final int HANDLE_TLB_MISS_NORMAL = 0;
	public static final int HANDLE_TLB_MISS_BAD_ADDRESS = 1;
	public static final int HANDLE_TLB_MISS_LOAD_ERROR = 2;
	public static final int HANDLE_TLB_MISS_UNKNOWN = 3;
	
	public static final String HANDLE_TLB_MISS_ERROR_MSG[] = {
		"normal",
		"bad address",
		"load error",
		"unknown"
	};

	/**
	 * Replaces one of the process's TLB entry.
	 * 
	 * @param process
	 * @param vpn
	 * @param entry
	 */
	private void replaceTLBEntry(VMProcess process, int vpn, VMPageEntry entry) {
		/* clock algorithm */
	
		int index = process.tlbLastReplacement;
		int PC_pn = Processor.pageFromAddress(process.currentPC);
	
		for (int i = 0; i < VMKernel.tlbSize; ++i) {
			if (process.tlbEntries[i] == vpn) {
				/* no need to replace */
				/* flush outside */
//				writeTLBEntry(i, vpn, entry);
				return;
			}
		}
		
		int count = 0;
		while (true) {
			Lib.assertTrue(++count <= VMKernel.tlbSize * 2, "cannot find a tlb to replace");
			index = (index + 1) % VMKernel.tlbSize;
			
			int tlbEntry = process.tlbEntries[index];
			int ref = process.tlbEntryRef[index];
			
			if (tlbEntry == VMProcess.invalidTLBEntry) {
				/* empty slot */
				break;
			}
			else if (tlbEntry == PC_pn) {
				/* PC page must reside in memory */
				continue;
			}
			else if (ref == 1) {
				/* less referenced one */
				break;
			}
			else if (ref == 2) {
				/* more referenced one, make it a candidate */ 
				process.tlbEntryRef[index] = 1;
				continue;
			}
			else {
				/* ref == 0, freshly loaded tlb, don't replace it */
				/* what if all entries are freshly loaded? */
				/* demand paging should be able to avoid this situation */
				continue;
			}
		}
		
		process.tlbLastReplacement = index;
		process.tlbEntries[index] = vpn;
		process.tlbEntryRef[index] = 0;
		
		writeTLBEntry(index, vpn, entry);
	}

	/**
	 * This routine is called whenever a context switch occurs or a TLB miss is handled.
	 * <p>
	 * This routine does not acquire the lock and is guaranteed not to be interrupted.
	 * 
	 * @param process
	 */
	public void reloadTLBEntries(VMProcess process) {
		for (int i = 0; i < VMKernel.tlbSize; ++i) {
			int vpn = process.tlbEntries[i];
			
			VMPageEntry vmPage = null;;
			if (vpn != VMProcess.invalidTLBEntry) {
				vmPage = process.pageTable.getPageTableEntry(vpn);
			}
			
			writeTLBEntry(i, vpn, vmPage);
		}
	}

	/**
	 * Fetch the TLB ref bits.
	 * <p>
	 * This routine does not acquire the lock and is guaranteed not to be intterupted.
	 * 
	 * @param process
	 */
	public void fetchTLBRefBits(VMProcess process) {
		
		for (int i = 0; i < VMKernel.tlbSize; ++i) {
			TranslationEntry entry = VMKernel.processor.readTLBEntry(i);
			if (entry.valid) {
				Lib.assertTrue(process.tlbEntries[i] == entry.vpn, "how come my tlb entry is modified?");
				if (entry.used) {
					if (process.tlbEntryRef[i] < 2)
						process.tlbEntryRef[i]++;
				}
				
				int ppn = entry.ppn;
				FrameEntry frame = frames[ppn];
				if (entry.dirty) {
					switch (frame.status) {
					case FRAME_STATUS_CLEAN:
						cleanFrames.removeFromList(frame, false);
						dirtyFrames.addToTail(frame);
						frame.used = true;
						break;
						
					case FRAME_STATUS_FRESH:
						freshFrames.removeFromList(frame, false);
						dirtyFrames.addToTail(frame);
						frame.used = true;
						break;
					
					case FRAME_STATUS_DIRTY:
						frame.used = true;
						break;
					}
					
					if (frame.vmPage != null && frame.vmPage.type == VMPageEntry.TYPE_OBJECT_FILE) {
						Lib.assertTrue(!frame.vmPage.readOnly, "not a writable section?");
						frame.vmPage.storeInfo = null;
						frame.vmPage.type = VMPageEntry.TYPE_ANONYMOUS;
					}
				}
				else if (entry.used) {
					switch (frame.status) {
					case FRAME_STATUS_FRESH:
						freshFrames.removeFromList(frame, false);
						cleanFrames.addToTail(frame);
						frame.used = true;
						break; 
						
					case FRAME_STATUS_CLEAN:
						frame.used = true;
						break;
						
					case FRAME_STATUS_DIRTY:
						frame.used = true;
						break;
					}
				}
			}
			else {
				/* do nothing */
			}
		}
	}

	/**
	 * Writes the tlb entry.
	 * 
	 * @param tlb_no
	 * @param vpn
	 * @param vmPage
	 */
	private void writeTLBEntry(int tlb_no, int vpn, VMPageEntry vmPage) {
		TranslationEntry t_entry;
		if (vmPage != null && vmPage.valid) {
			t_entry = new TranslationEntry(vpn, vmPage.ppn, true, vmPage.readOnly, false, false);
		}
		else {
			t_entry = new TranslationEntry(0, 0, false, false, false, false);
		}
		VMKernel.processor.writeTLBEntry(tlb_no, t_entry);
	}

	/**
	 * Allocates a frame to the vm page if possible.
	 * <p>
	 * This method returns whether a frame is allocated to the vm page.
	 * <p>
	 * If the allocated frame already contains the content of the vm page, valid
	 * bit the page is set to true.
	 * <p>
	 * The caller may need to write back the dirty page in the frame.
	 * 
	 * @param vmPage
	 * @return		<tt>true</tt> on success, <tt>false</tt> o.w.
	 */
	private boolean allocateFrame(VMPageEntry vmPage) {
		
		/* 1. if the frame was never allocated to other vm pages,
		 * just reclaim it. This could happen when one process finds itself
		 * thrashes and gives up the frame voluntarily. Another process or the process
		 * itself may wake up finally and try to reallocate a frame to the page. */
		if (vmPage.ppn >= 0 && vmPage.ppn < numFrames) {
			if (frames[vmPage.ppn].status == FRAME_STATUS_FREE &&
				frames[vmPage.ppn].vmPage == vmPage) {
			
				FrameEntry frame = frames[vmPage.ppn];
				
				vmPage.valid = true;
				//vmPage.ppn
				
				freeFrames.removeFromList(frame, false);
				freshFrames.addToTail(frame);
				frame.used = false;
				
				return true;
			}
		}
		
		/* 2. allocate free frames if possible
		 * */
		if (freeFrames.cnt > 0) {
			/* FIFO */
			FrameEntry frame = (FrameEntry) freeFrames.head;
		
			vmPage.valid = false;	// need load
			vmPage.ppn = frame.ppn;
		
			freeFrames.removeFromList(frame, true);
			frame.used = false;
			frame.vmPage = vmPage;
			/* don't add to fresh until loading is completed */
			
			return true;
		}
		
	
		/*
		 * 3. try not-used clean frames 
		 * */
		if (cleanFrames.cnt > 0) {
			/* clock algorithm */
			FrameEntry frame = (FrameEntry) cleanFrames.head;
		
			if (frame.used) {
				while (frame.used) {
					frame.used = false;
					frame = (FrameEntry) frame.list_next;
				}
				
				/* data page */
				if (!vmPage.executable && frame == cleanFrames.head) {
					/* in case we accidentally replace a text page with
					 * a data page */
					frame = null;
				}
			}
			
			if (frame != null) {
				vmPage.valid = false;
				vmPage.ppn = frame.ppn;
			
				cleanFrames.removeFromList(frame, true);
				frame.vmPage.valid = false;
				frame.vmPage = vmPage;
				frame.used = false;
				/* need load */
				return true;
			}
		}
		
		/*
		 * 4. try not-used dirty frames
		 * */
		if (dirtyFrames.cnt > 0) {
			/* FIFO */
			FrameEntry frame = (FrameEntry) dirtyFrames.head;
		
			vmPage.valid = false;
			vmPage.ppn = frame.ppn;
	
			dirtyFrames.removeFromList(frame, true);
			frame.vmPage.valid = false;
			if (!writeBackDirtyPage(frame)) {
				/* cannot re-validate the frame.
				 * This is a fatal error since other processes are involved. */
				frame.vmPage = null;
				freeFrames.addToTail(frame);
				VMProcess.signaledHalt = true;
				return false;
			}
			frame.vmPage = vmPage;
			frame.used = false;
			/* need load */
			return true;
		}
		
		/*
		 * 5. try clean frames again if it is an data page. No don't.
		 * */
		if (cleanFrames.cnt > 0) {
			/* clock */
			FrameEntry frame = (FrameEntry) cleanFrames.head;
			
			vmPage.valid = false;
			vmPage.ppn = frame.ppn;
			
			cleanFrames.removeFromList(frame, true);
			frame.vmPage.valid = false;
			frame.vmPage = vmPage;
			frame.used = false;
			/* need load */
			return true;
		}
		
		/* maybe the process is thrashing, leave the problem to the page fault handler */
		
		return false;
	}
	
	public static final int HANDLE_PAGE_FAULT_NORMAL = 0;
	public static final int HANDLE_PAGE_FAULT_NO_AVAILABLE_FRAME = 1;
	public static final int HANDLE_PAGE_FAULT_LOAD_ERROR = 2;
	public static final int HANDLE_PAGE_FAULT_UNKNOWN = 3;
	
	public static String[] HANDLE_PAGE_FAULT_ERROR_MSG = {
		"normal",
		"no available frame",
		"load error",
		"unknown"
	};

	/**
	 * Handled a page fault. Should always be called with lock acquired.
	 * <p>
	 * This call may block when swapping the page. Before a blocking operation, this routine
	 * releases the global lock and acquires a per-page lock
	 * in order to allow multiple independent page fault to be handled
	 * concurrently.
	 * 
	 * @param process
	 * @param vmPage
	 * @return
	 */
	private int handlePageFault(VMProcess process, VMPageEntry vmPage) {
		/* lock should be already acquired */

		/* could be a shared page */
		if (vmPage.valid) return HANDLE_PAGE_FAULT_NORMAL;
		
		switch (vmPage.type) {
		case VMPageEntry.TYPE_ANONYMOUS:
		{
			if (vmPage.storeInfo != null) {
				SwapFilePage storeInfo = (SwapFilePage) vmPage.storeInfo;
				if (storeInfo.accessing) {
					/* wait for the vmPage to be ready for loading */
					storeInfo.waitForLoadingPage.sleep();
				}
			}
			
			if (allocateFrame(vmPage)) {
				if (vmPage.valid) {
					/* reclaimed a free frame containing the page */
					return HANDLE_PAGE_FAULT_NORMAL;
				}
				
				/* need to load the page */
				if (vmPage.storeInfo == null) {
					/* just zero the space */
					Arrays.fill(VMKernel.processor.getMemory(), vmPage.ppn * pageSize, (vmPage.ppn + 1) * pageSize, (byte) 0);
				}
				else {
					SwapFilePage storeInfo = (SwapFilePage) vmPage.storeInfo;
					
					Lib.assertTrue(!storeInfo.accessing, "who's accessing the exclusive page");
					
					/* be aware, context switch */
					
					storeInfo.accessing = true;
					releaseLock();
					boolean load_result = 
					swapFile.read(storeInfo.spn, VMKernel.processor.getMemory(), vmPage.ppn);
					acquireLock();
					storeInfo.accessing = false;
					/* since this page is exclusive, don't need to wake anybody up */
					
					if (!load_result) {
						frames[vmPage.ppn].vmPage = null;
						freeFrames.addToTail(frames[vmPage.ppn]);
						vmPage.valid = false;
						return HANDLE_PAGE_FAULT_LOAD_ERROR;
					}
				}
				
				vmPage.valid = true;
				freshFrames.addToTail(frames[vmPage.ppn]);
				return HANDLE_PAGE_FAULT_NORMAL;
			}
			
			else {
				/* the process may be thrashing, we shall temporarily suspend
				 * the process */
				return HANDLE_PAGE_FAULT_NO_AVAILABLE_FRAME;
			}
			
		}
		
		case VMPageEntry.TYPE_OBJECT_FILE:
		{
			ObjectFilePage storeInfo = (ObjectFilePage) vmPage.storeInfo;
			
			if (vmPage.readOnly) {
				while (storeInfo.accessing) {
					storeInfo.waitForLoadingPage.sleep();
				}
				
				if (vmPage.valid) {
					return HANDLE_PAGE_FAULT_NORMAL;
				}
				
				storeInfo.accessing = true;
			}
			
			/* in case other process try to load this page and allocate duplicate frames */
			if (allocateFrame(vmPage)) {
				if (vmPage.valid) {
					/* just reclaimed a free frame containing the page */
					storeInfo.accessing = false;
					if (vmPage.ref_count > 1) {
						storeInfo.waitForLoadingPage.wakeAll();
					}
					return HANDLE_PAGE_FAULT_NORMAL; }
				
				/* load the page (be aware of the context switch) */
				releaseLock();
					/* If loadPage fails, this thread will be killed immediately. MAYBE a DEADLOCK situation1! */
				storeInfo.section.loadPage(storeInfo.spn, vmPage.ppn);
				acquireLock();
				storeInfo.accessing = false;
				
				if (vmPage.ref_count > 1) 
					storeInfo.waitForLoadingPage.wakeAll();	// may be someone's waiting
			
				vmPage.valid = true;
				freshFrames.addToTail(frames[vmPage.ppn]);
				
				return HANDLE_PAGE_FAULT_NORMAL;
			}
			
			else {
				/* thrashing ? suspend!! */
				if (vmPage.readOnly) {
					storeInfo.accessing = false;
				}
				
				return HANDLE_PAGE_FAULT_NO_AVAILABLE_FRAME;
			}
			
		}
		case VMPageEntry.TYPE_INVALID:
		default:
			Lib.assertNotReached("invalid vm page entry");
		}
		
		return HANDLE_PAGE_FAULT_UNKNOWN;
	}
	

	/**
	 * Evict all my frames. This should only be called when 
	 * there's no page that can be allocated to a process.
	 * 
	 * @param process
	 * @return
	 */
	private void evictMyFrame(VMProcess process) {
	
		PageTableSection p_section = process.pageTable.first();
		while (p_section != null) {
			
			for (int pn = p_section.start_pn; pn < p_section.end_pn; ++pn) {
				
				if (p_section.vmPage[pn - p_section.start_pn].valid) {
					/* find one valid page; evict it */
					/* even if the page is shared by multiple processes */
					/* since next process may have the chance to reclaim the page */
					
					int ppn = p_section.vmPage[pn - p_section.start_pn].ppn;
					if (frames[ppn].status == FRAME_STATUS_FRESH) {
						p_section.vmPage[pn - p_section.start_pn].valid = false;
						freshFrames.removeFromList(frames[ppn], false);
						freeFrames.addToTail(frames[ppn]);
					}
					else if (frames[ppn].status == FRAME_STATUS_CLEAN) {
						p_section.vmPage[pn - p_section.start_pn].valid = false;
						cleanFrames.removeFromList(frames[ppn], false);
						freeFrames.addToTail(frames[ppn]);
					}
					else {
						Lib.assertNotReached("dirty page can always be allocated");
					}
				}
			}
			
			if (p_section.list_next instanceof PageTableSection) {
				p_section = (PageTableSection) p_section.list_next;
			}
			else {
				p_section = null;
			}
		}
	}

	/**
	 * Writes back the dirty page.
	 * 
	 * @param frame
	 * @return
	 */
	private boolean writeBackDirtyPage(FrameEntry frame) {
		/* the page has to be swap file page */
		VMPageEntry vmPage = frame.vmPage;

		switch (vmPage.type) {
		case VMPageEntry.TYPE_ANONYMOUS:
		{
			SwapFilePage storeInfo = null;
			if (vmPage.storeInfo == null) {
				/* allocate a new swap file page */
				int spn = swapFile.getNewSwapPage();
				storeInfo = new SwapFilePage(spn);
				vmPage.storeInfo = storeInfo;
			}
			else {
				storeInfo = (SwapFilePage) vmPage.storeInfo;
			}
			
			Lib.assertTrue(!storeInfo.accessing, "who's accessing a dirty exclusive page?");
		
			/* long operation with context switch */
			storeInfo.accessing = true;
			releaseLock();
			boolean write_result = 
			swapFile.write(storeInfo.spn, VMKernel.processor.getMemory(), frame.ppn);
			acquireLock();
			storeInfo.accessing = false;
			storeInfo.waitForLoadingPage.wakeAll();	// maybe another process is waiting
			
			if (!write_result) {
				return false;
			}
			
			return true;
		}
		
		default:
			Lib.assertNotReached("cannot write back to object file");
		}
		
		return false;
	}

	/**
	 * Number of frames available for allocation. (free + clean + dirty)
	 * 
	 * @return
	 */
	public int numAllocatableFrames() {
		return freeFrames.cnt + cleanFrames.cnt + dirtyFrames.cnt;
	}

	/**
	 * Number of free frames.
	 * 
	 * @return
	 */
	public int numFreeFrames() {
		return freeFrames.cnt;
	}

	/**
	 * Clear resources of the VM subsystem. This routine should only be
	 * called when the kernel is being terminated.
	 * 
	 */
	public void finish() {
		for (ExecutableFile exec_file : executableFiles.values()) {
			exec_file.coff.close();
		}
		
		swapFile.close();
	}
	
/* physical memory info */
	
	/** The number of physical memory frames. */
	private int numFrames;

	/** The array of frames. */
	private FrameEntry[] frames;
	
	static class FrameList {
		FrameList(int status) {
			cnt = 0;
			head = null;
			this.status = status;
		}
		
		void addToTail(FrameEntry frame) {
			if (head == null) {
				head = frame;
			}
			else {
				frame.insert_before(head);
			}
			++cnt;
			frame.status = this.status;
		}
		
		void removeFromList(FrameEntry frame, boolean move_head) {
			if (--cnt > 0) {
				if (move_head || frame == head) {
					head = (FrameEntry) frame.list_next;
				}
				frame.remove_from_list();
			}
			else {
				head = null;
			}
			frame.status = FRAME_STATUS_NOT_IN_LIST;
		}
		
		int cnt;
		FrameEntry head;
		private int status;
	}
	
	private FrameList freeFrames;
	
	private FrameList cleanFrames;
	
	private FrameList dirtyFrames;
	
	private FrameList freshFrames;

	public static final int FRAME_STATUS_NOT_IN_LIST = -1;
	public static final int FRAME_STATUS_FREE = 0;
	public static final int FRAME_STATUS_CLEAN = 1;
	public static final int FRAME_STATUS_DIRTY = 2;
	public static final int FRAME_STATUS_FRESH = 3;
	
/* the swap file */
	
	/** The swap file. */
	private SwapFile swapFile;
	
/* virtual memory page info */
	
	public static class ExecutableFile {
		ExecutableFile(OpenFile file, Coff coff) {
			this.file = file;
			this.coff = coff;
			this.ref_count = 0;
			this.vmSections = null;
		}
	
		OpenFile file;
		Coff coff;
		int ref_count;
		
		VMPageEntry[][] vmSections;
	}

	/** The book-keeping info of executable files. */
	private HashMap<String, ExecutableFile> executableFiles;

	private class FilePage {
		FilePage() {
			waitForLoadingPage = new Condition2(lock);
			accessing = false;
		}
		
		Condition2 waitForLoadingPage;
		boolean accessing;
	}
	
	private class ObjectFilePage extends FilePage {
		ObjectFilePage(CoffSection section, int spn) {
			this.section = section;
			this.spn = spn;
		}
		CoffSection section;
		int spn;
	}
	
	private class SwapFilePage extends FilePage {
		SwapFilePage(int spn) {
			this.spn = spn;
		}
		
		int spn;
	}
	
	private void acquireLock() {
		lock.acquire();
	}
	
	private void releaseLock() {
		lock.release();
	}

/* resource lock*/
	
	/** Resource lock, which protects all the VM information */
	private Lock lock;
	
/* Miscellaneous */

	public static final char dbgVM = 'v';
	
	/** The number of pages in virtual address space. */
	public static final int max_num_pages = Processor.pageFromAddress(0xFFFFFFFF) + 1;
	
	/** The page size. */
	public static final int pageSize = Processor.pageSize;
}
