package nachos.vm; 

import java.util.Arrays;

import nachos.machine.Machine;
import nachos.machine.Processor;
import nachos.machine.Lib;
import nachos.machine.OpenFile;
import nachos.userprog.UserProcess;
import nachos.userprog.UThread;

/**
 * A <tt>UserProcess</tt> that supports demand-paging.
 */
public class VMProcess extends UserProcess {
	/**
	 * Allocate a new process.
	 */
	public VMProcess() {
		super();
		
		pageTable = null;
		tlbEntries = new int[VMKernel.tlbSize];
		tlbEntryRef = new int[VMKernel.tlbSize];
		executableFile = null;
	}

	@Override
	public UThread execute(String name, String[] args) {
		
		return super.execute(name, args);
	}
	
	@Override
	protected boolean load(String name, String[] args) {
		Lib.debug(dbgVM, "VMProcess.load(\"" + name + "\")");
	
		/* Now the stack is moved to 0x80000000 and grows downward. */
		lowestSP = 0x80000000;
		
		/* creates a new page table. */
		pageTable = new PageTable();
	
		/* the process cached tlb entries. */
		Arrays.fill(tlbEntries, invalidTLBEntry);
		
		/* approximation to the tlb entry reference counts */
		Arrays.fill(tlbEntryRef, 0);
		
		/* last replacement cursor of the clock algorithm */
		tlbLastReplacement = VMKernel.tlbSize - 1;

		/* the total length of argument strings (including '\0') */
		int arg_string_length = 0;
		byte[][] args_byte = new byte[args.length][];
		for (int i = 0; i < args.length; ++i) {
			args_byte[i] = args[i].getBytes();
			arg_string_length += args_byte[i].length + 1;	// arg[i] + '\0'
		}
		
		/* aligns with the 8-byte boundary */
		arg_string_length = ((arg_string_length + 7) & ~7);
		
		/* int argc, char** argv, padding, padding, ptrs, strings */
		int total_arg_length = arg_string_length + 16 + 4 * args.length;
		
		/* append the additional argument pages to the stack space. 
		 * This is not a good idea since the stack size varies among processes,
		 * but the grader may set a tight limit on the stack size so that
		 * we have to guarantee the declared number of stack pages available
		 * to the process. */
		int additional_arg_pages = (total_arg_length + pageSize - 1) / pageSize;
		
		/* The total number of stack pages and argument pages. */
		int total_stack_pages = stackPages + additional_arg_pages;
	
		/* allocate pages for coff sections first */
		if (!VMKernel.vm.allocateVMPageForCoff(this, name)) {
			/* fail, cleanup is done by execute */
			return false;
		}
	
		/* FIXME no heap now */
		
		/* allocate pages for the stack	 */
		if (!VMKernel.vm.allocateStackPages(this, total_stack_pages)) {
			/* fail, cleanup is done by execute */
			clearProcess();
			return false;
		}
	
		/* initial PC is set by vm.allocateVMPageForCoff */
		// initialPC
		
		/* initial sp, may reside in the first argument page */
		initialSP = 0x80000000 - total_arg_length;
		
		/* argc and argv */
		argc = args.length;
		argv = initialSP + 16;

		/* Don't write the arguments until the process starts up since we cannot write the TLBs right now. */
		/* The first 16 bytes are reserved for a0 to a3 due to the MIPS call convention. */
		arg_buffer = new byte[total_arg_length - 16];
		int pargv_base = initialSP + 16;
		int str_offset = 4 * args.length;
		for (int i = 0; i < args.length; ++i) {
			Lib.bytesFromInt(arg_buffer, i * 4, pargv_base + str_offset);	/* argv[i] */
			System.arraycopy(args_byte[i], 0, arg_buffer, str_offset, args_byte[i].length); /* str pointed by argv[i]*/
			arg_buffer[str_offset + args_byte[i].length] = 0;	/* \0 */
			str_offset += args_byte[i].length + 1;
		}
		
		return true;
		
	}

	@Override
	protected boolean loadSections() {
		/* nothing to do due to demand paging */
		
		return true;
	}

	@Override
	protected void clearProcess() {
		/* cannot invoke super.clearProcess() since it will attempt to clear the memMap, which is null in this phase. */
		closeAllFD();
	
		/* release VM pages */
		VMKernel.vm.releasePages(this);
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	@Override
	public void saveState() {
		
		/* update tlb ref bits in both this processes tlb cache and physical frame 
		 * book-keeping records */
		VMKernel.vm.fetchTLBRefBits(this);
		
	}
	
	@Override
	public void initRegisters() {
		/* write arguments here */
		inSysCall = true;
		if (writeVirtualMemory(initialSP + 16, 1, arg_buffer, 0, arg_buffer.length) != arg_buffer.length) {
			Lib.debug(dbgVM, "failed to write arguments, exiting");
			UThread.exit(-1, false);
		}
		
		/* kfree(arg_buffer) */
		arg_buffer = null;	// leave the memory to be reclaimed by java gc
		inSysCall = false;
		
		exceptionHandled = true;
	
		/* initialises registers. After this, jump to user prog. */
		super.initRegisters();
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	@Override
	public void restoreState() {
	
		/* looks up the cached tlb entries in the page table, and write 
		 * valid ones of them back to the tlb */
		VMKernel.vm.reloadTLBEntries(this);
	}


	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 * <p>
	 * Note: we don't use this function. Use clearProcess() instead.
	 */
	@Override
	protected void unloadSections() {
		super.unloadSections();
	}
	
	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause
	 *            the user exception that occurred.
	 */
	public void handleException(int cause, int badVAddr) {
		/* don't use super.handleException, for exceptionHandled may be set to incorrect value */
		exceptionHandled = false;

		/* just for debug */
		currentPC = VMKernel.processor.readRegister(Processor.regPC);

		String msg = null;
		switch (cause) {
		case Processor.exceptionSyscall:
			inSysCall = true;
			int result = handleSyscall(VMKernel.processor.readRegister(Processor.regV0),
					VMKernel.processor.readRegister(Processor.regA0), VMKernel.processor
							.readRegister(Processor.regA1), VMKernel.processor
							.readRegister(Processor.regA2), VMKernel.processor
							.readRegister(Processor.regA3));
			VMKernel.processor.writeRegister(Processor.regV0, result);
			VMKernel.processor.advancePC();
			exceptionHandled = true;
			inSysCall = false;
			
			/* don't need to flush tlb since it is done by tlb miss handler */
			break;
		
		
		case Processor.exceptionIllegalInstruction:
			/*
			 * Illegal instruction
			 * 
			 * */
		case Processor.exceptionOverflow:
			/*
			 * Arithmetic overflow
			 * 
			 * */
			
			msg = "Unexpected exception: " + Processor.exceptionNames[cause];
			break;
			
		case Processor.exceptionPageFault:
			/*
			 * page fault
			 * Cannot happen when usingTLB
			 * */
		case Processor.exceptionReadOnly:
			/*
			 * access violation
			 * we are not implementing copy-on-write, so this must be
			 * a real access violation
			 * TODO implement copy-on-write (maybe after +oo?)
			 * */
		case Processor.exceptionAddressError:
			/* incorrect alignment 
			 * a broken compiler ? */
		case Processor.exceptionBusError:
			/*
			 * Invalid ppn
			 * this cannot happen
			 * */
			
			msg = "Unexpected exception: " + Processor.exceptionNames[cause] + "@0x" + Lib.toHexString(badVAddr, 8);
			break;
			
		case Processor.exceptionTLBMiss:
			/*
			 * TLB miss
			 * we shall handle it.
			 * */
			
			{
//				Lib.debug(dbgVM, "TLB miss handler @ 0x" +  Lib.toHexString(badVAddr, 8));
				int status = VMKernel.vm.handleTLBMiss(this, badVAddr);
				if (status != VM.HANDLE_TLB_MISS_NORMAL) {
					msg = "fatal error: tlb miss unhandled for " + VM.HANDLE_TLB_MISS_ERROR_MSG[status] + 
							"@0x" + Lib.toHexString(badVAddr, 8);
				}
				else {
					exceptionHandled = true;
				}
				
				/* have to flush the tlb entries here */
				VMKernel.vm.reloadTLBEntries(this);
			}
			break;
		
		default:
			Lib.debug(dbgVM, "Unknown exception " + cause + "@0x" + Lib.toHexString(badVAddr, 8));
			return ;
		}
		
		if (!exceptionHandled) {
			Lib.debug(dbgProcess, msg);
			
			if (!inSysCall) {
				/* unhandled exception in user program. Terminate the process.*/
				UThread currentThread = (UThread) UThread.currentThread();
				/* msg_buf is NULL-terminated */
				@SuppressWarnings("unused")
				byte[] msg_buf = nachos.userprog.SynchConsole.bytesFromString(msg + "\n");
				OpenFile stdout = currentThread.process.openFiles[STDOUT_FILENO];
				if (stdout != null) {
//					stdout.write(msg_buf, 0, msg_buf.length - 1);
				}

				UThread.exit(currentThread, cause, false);
			}
			
			/* a process in syscall does not terminate abruptly. Leave it to the syscall to decide. */
		}
		
		else if (signaledHalt) {
			/* someone signalled to halt the machine */
			UThread.exit(0, false);
		}
	}

	private static final int pageSize = Processor.pageSize;
	private static final char dbgProcess = 'a';
	private static final char dbgVM = 'v';
	
	/** Software managed page table. */
	public PageTable pageTable;

	/** The tlb entries of this process. 
	 * <p>
	 * Ensure that
	 * <ul>
	 * <li> one of the entries is the page that PC is pointing to
	 * <li> one of the entries is the page where the most recent data tlb miss occurs
	 * </ul> */
	public int[] tlbEntries;

	/** The ref bits of tlbEntries. */
	public int[] tlbEntryRef;

	/** The index of lastly replaced tlb entry. */
	public int tlbLastReplacement;
	
	public static final int invalidTLBEntry = -1;

	/** The bottom of the stack. */
	public int lowestSP;

	/** argv buffer, only non-null when the process is starting */
	public byte[] arg_buffer;

	/** The executable file that this process is executing. */
	public VM.ExecutableFile executableFile;

	/** The current PC when the exception handler is invoked. */
	public int currentPC;
}
