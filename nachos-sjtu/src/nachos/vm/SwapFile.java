package nachos.vm;


/**
 * The swap file interface. Swap file implementation only need to provide the read/write/maxNumberPages/close methods.
 * 
 */
public abstract class SwapFile {
	
	public SwapFile() {
		max_pn = 0;
		free_list = new java.util.LinkedList<Integer>();
	}

	/**
	 * Reads the page from swap file.
	 * 
	 * @param spn		the swap file page number
	 * @param buffer	the buffer to store the content
	 * @param pn 		the page number of the page in the buffer
	 * @return			<tt>true</tt> if the operations is successful
	 */
	public abstract boolean read(int spn, byte[] buffer, int pn);
	
	/**
	 * Writes the page from swap file.
	 * 
	 * @param spn		the swap file page number
	 * @param buffer	the buffer to store the content
	 * @param pn		the page number of the page in the buffer
	 * @return			<tt>true</tt> if the operations is successful
	 */
	public abstract boolean write(int spn, byte[] buffer, int pn);
	
	/**
	 * Close the swap file.
	 */
	public abstract void close();

	/**
	 * Returns the maximum number of swap pages supported by the implementation.
	 * 
	 * @return	the maximum number of swap pages supported by the implementation
	 */
	public abstract int maxNumberPages();

	/**
	 * Gets a new swap page.
	 * 
	 * @return	the swap page number is returned on success, -1 on failure
	 */
	public int getNewSwapPage() {
		if (free_list.isEmpty()) {
			if (max_pn == maxNumberPages()) {
				return -1;
			}
			return max_pn++;
		}
		return free_list.removeFirst();
	}

	/**
	 * Releases a swap page.
	 * 
	 * @param pn	the swap page number of the page to be released
	 */
	public void releaseSwapPage(int pn) {
		free_list.add(pn);
	}

	private int max_pn;

	private java.util.LinkedList<Integer> free_list;
	
	public static final int pageSize = nachos.machine.Processor.pageSize;
}
