package nachos.vm;

import nachos.threads.KThread;
import nachos.threads.ThreadQueue;
import nachos.threads.ThreadedKernel;
import nachos.userprog.Stats;
import nachos.userprog.UserKernel;
import nachos.machine.Lib;
import nachos.machine.Machine;
import nachos.machine.Processor;

/**
 * A kernel that can support multiple demand-paging user processes.
 */
public class VMKernel extends UserKernel {
	/**
	 * Allocate a new VM kernel.
	 */
	public VMKernel() {
		super();
		
	}

	/**
	 * Initialise this kernel.
	 */
	public void initialize(String[] args) {
		UserKernel.no_fs_init = true;
		UserKernel.no_memmap_init = true;
		
		super.initialize(args);
		
		/* sets up second ready queue */
		secondReadyQueue = ThreadedKernel.scheduler.newThreadQueue(false);
		numThrashing = 0;
		
		readyThrashingProcessFrameThreshold = Math.max(2, numPhysPages / 4);
	
		/* Sets up fs wrapper */
		if (!no_vmfs_init) {
			VMKernel.vmfs = new VMFileSystemWrapper(ThreadedKernel.fileSystem);
			UserKernel.fileSystem = VMKernel.vmfs;
		}
		/* else the wrapper is set up by the subclass, or not used */
		
		/* sets up the swap file (must before setting up vm subsystem) */
		if (!no_swap_file_init) {
			swapFile = new StubFileSystemSwapFile(VMKernel.swapFileName);
		}
		/* else this must be set by the subclass */
		
		/* sets up vm subsystem */
		vm = new VM();
	}

	/**
	 * Test this kernel.
	 */
	public void selfTest() {
//		super.selfTest();
	}

	/**
	 * Start running user programs.
	 */
	public void run() {
		super.run();
	}

	@Override
	protected void finish() {
		super.finish();
		
		vm.finish();
	}

	/**
	 * Suspend the current thread until more free memory is available.
	 * Only used by page fault exception handler.
	 * 
	 */
	public void suspendThread() {
		boolean intStatus = Machine.interrupt().disable();
		
		KThread currentThread = KThread.currentThread();
		Lib.assertTrue(currentThread.status == KThread.statusRunning);
		
		Lib.debug(dbgThread, "VMKernel: suspending thread " + currentThread.getName());
	
		currentThread.status = KThread.statusReady;
		
		++numThrashing;
		secondReadyQueue.waitForAccess(currentThread);
	
		KThread.runNextThread();
		
		Machine.interrupt().restore(intStatus);
	}
	
	@Override
	public KThread pickNextThread() {
		KThread nextThread = KThread.idleThread;
		if (KThread.numReady == 0) {
			if (numThrashing > 0) {
				--numThrashing;
				nextThread = secondReadyQueue.nextThread();
			}
		}
		
		else {
			if (numThrashing > 0 && vm.numFreeFrames() >= readyThrashingProcessFrameThreshold) {
				--numThrashing;
				nextThread = secondReadyQueue.nextThread();
			}
			else {
				--KThread.numReady;
				nextThread = KThread.readyQueue.nextThread();
			}
		}
		
//		Lib.debug(dbgThread, "VMKernel: pickNextThread " + nextThread.getName());
		
		return nextThread;
	}
	
	public static void coreDump() {
		byte[] memory = processor.getMemory();

		System.err.println();
		System.err.println("Core dump:");
		for (int i = 0; i < memory.length; i += 16) {
			if (i % Processor.pageSize == 0) {
				System.err.println();
			}
			
			StringBuilder builder = new StringBuilder();
			
			builder.append("0x").append(Lib.toHexString(i, 8));
			for (int j = 0; j < 16; ++j) {
				if (j % 4 == 0) builder.append(' ');
				builder.append(toHexString(memory[i + j]));
			}
			
			builder.append(' ');
			for (int j = 0; j < 16; ++j) {
				char ch = (char) memory[i + j];
				if ( ch < 32 || ch >= 127) ch = '.';
				builder.append(ch);
			}
			
			System.err.println(builder.toString());
		}
	}
	
	public static String toHexString(byte b) {
		String str = Lib.toHexString((int) b, 8);
		return str.substring(6);
	}

	public static final char dbgVM = 'v';
	public static final char dbgThread = 't';
	
	public static final String swapFileName = "SWAP";

	/** The global access to the vmfs. */
	public static VMFileSystemWrapper vmfs;
	public static boolean no_vmfs_init = false;

	/** The global access to the vm. */
	public static VM vm;

	/** A second ready queue which contain thrashing processes. */
	public static ThreadQueue secondReadyQueue; 
	
	/** The number of processes that are considered as the cause of thrashing. */
	public static int numThrashing;

	/** The threshold of free frames to be reached when processes should be chosen from  the second ready queue. */
	public static int readyThrashingProcessFrameThreshold;

	/** The global access to the swap file. */
	public static SwapFile swapFile;
	public static boolean no_swap_file_init = false;
}

