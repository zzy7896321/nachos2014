package nachos.vm;


/**
 * A page table section contains a continuous space in the virtual memory. 
 * 
 * @author ZZy
 *
 */
public class PageTableSection extends EmbeddedLinkedList {
	/**
	 * Constructs a page table section from the given starting page number and spans the given size.
	 * 
	 * @param start_pn	the starting page number
	 * @param size		the number of pages that this section spans
	 */
	public PageTableSection(int start_pn, int size) {
		this.start_pn = start_pn;
		this.end_pn = start_pn + size;
		this.vmPage = new VMPageEntry[size];
	}
	
	/** The start page number. The valid range is [start_pn, end_pn) */
	public int start_pn;

	/** The end page number. */
	public int end_pn;

	/** The vm page entries. */
	public VMPageEntry[] vmPage;

	/**
	 * Returns a page table section implementation-specific query object. When this object is compared
	 * to a page table section [start, end), if its page number pn is
	 * <ul>
	 * <li> pn < start, then the query object is smaller than the page table section
	 * <li> start <= pn < end, then they are equal
	 * <li> pn >= end, then the query object is larger than the page table section
	 * </ul>
	 * <p>
	 * This query object can be used to query in which page section a page resides in a TreeSet.
	 * 
	 * @param pn
	 * @return
	 */
	public static Object getQuery(int pn) {
		return new __QueryObject(pn);
	}

	/**
	 * This comparator implements the desired behaviour between query object and page table section.
	 * <p>
	 * If two page sections are compared, only their starting page numbers are compared.
	 * It is up to the caller to enforce that no two page table sections overlap. Undefined behaviour
	 * may occur otherwise.
	 */
	public static final java.util.Comparator<Object>
	less_than = (Object lhs, Object rhs) -> {
		if (lhs instanceof PageTableSection) {
			PageTableSection sec_l = (PageTableSection) lhs;
			
			if (rhs instanceof PageTableSection) {
				PageTableSection sec_r = (PageTableSection) rhs;
				return sec_l.start_pn - sec_r.start_pn;
			}
			
			else if (rhs instanceof __QueryObject) {
				__QueryObject query_r = (__QueryObject) rhs;
				if (sec_l.end_pn <= query_r.pn) {
					return -1;
				}
				if (sec_l.start_pn > query_r.pn) {
					return 1;
				}
				return 0;
			}
		}
		else if (lhs instanceof __QueryObject){
			__QueryObject query_l = (__QueryObject) lhs;
			
			if (rhs instanceof PageTableSection) {
				PageTableSection sec_r = (PageTableSection) rhs;
				if (query_l.pn < sec_r.start_pn) {
					return -1;
				}
				if (query_l.pn >= sec_r.end_pn) {
					return 1;
				}
				return 0;
			}
			
			else if (rhs instanceof __QueryObject) {
				__QueryObject query_r = (__QueryObject) rhs;
				return query_l.pn - query_r.pn;
			}
		}
		return 0;
	};
	
	@Override
	public boolean equals(Object rhs) {
		if (rhs == null) return false;
		if (rhs instanceof PageTableSection) {
			PageTableSection section = (PageTableSection) rhs;
			return start_pn == section.start_pn;
		}
		
		else if (rhs instanceof __QueryObject) {
			__QueryObject query = (__QueryObject) rhs;
			return query.pn >= start_pn && query.pn < end_pn;
		}
		return false;
	}
	
	private static class __QueryObject {
		private __QueryObject(int pn) {
			this.pn = pn;
		}
		
		@Override
		public boolean equals(Object rhs) {
			if (rhs == null) return false;
			if (rhs instanceof PageTableSection) {
				PageTableSection section = (PageTableSection) rhs;
				return pn >= section.start_pn && pn < section.end_pn;
			}
			
			else if (rhs instanceof __QueryObject) {
				return pn == ((__QueryObject) rhs).pn;
			}
			
			return false;
		}
		
		@Override
		public String toString() {
			return "PageTableSection.__QueryObject(" + pn + ")";
		}
		
		private int pn;
	}
	
//	private static int unsigned_compare(int x, int y) {
//		long xx = (((long) x) << 32L) >>> 32L;
//		long yy = (((long) y) << 32L) >>> 32L;
//		
//		if (xx < yy) return -1;
//		else if (xx == yy) return 0;
//		else return 1;
//	}
}


