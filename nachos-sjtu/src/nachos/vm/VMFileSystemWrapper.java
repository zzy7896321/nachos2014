package nachos.vm;

import nachos.machine.FileSystem;
import nachos.machine.Lib;
import nachos.machine.OpenFile;
import nachos.userprog.FileSystemWrapper;
import nachos.userprog.StubFileSystemWrapper;

/**
 * The VM file system wrapper. Provides last modification time and file lock.
 * 
 */
public class VMFileSystemWrapper extends StubFileSystemWrapper {

	public VMFileSystemWrapper(FileSystem fs) {
		super(fs);
	}

	/**
	 * Gets the last modification time of the file. 
	 * <p>
	 * This operations is synchronised. If the caller has
	 * already got exclusive access on the
	 * FileInfo, calling this method will lead to deadlock.
	 * 
	 * @param name	the file name
	 * @return		the last modification time in ms since Jan 1, 1970 UTC
	 */
	public long getLastModificationTime(String name) {
		FileInfo info = (FileInfo) acquireFileInfo(name);
		info.sem.P();
		long ret = info.lastModificationTime;
		info.sem.V();
		releaseFileInfo(info);
		return ret;
	}

	/**
	 * Gets the exclusive state of the file.
	 * <p>
	 * This operations is synchronised. If the caller has
	 * already got exclusive access on the
	 * FileInfo, calling this method will lead to deadlock.
	 * 
	 * @param name	the file name
	 * @return		the exclusive state
	 */
	public boolean isExclusive(String name) {
		FileInfo info = (FileInfo) acquireFileInfo(name);
		info.sem.P();
		boolean ret = info.isExclusive();
		info.sem.V();
		releaseFileInfo(info);
		return ret;
	}
	
	/**
	 * Returns the FileInfo object associated with the OpenFile wrapper without increment the holdCount.
	 * 
	 * @param openFile
	 * @return
	 */
	public FileInfo getFileInfo(OpenFile openFile) {
		return (FileInfo)(((OpenFileWrapper) openFile).info);
	}
	
	@Override
	protected nachos.userprog.OpenFileWrapper open_impl(nachos.userprog.FileInfo info_base, boolean truncate) {
		FileInfo info = (FileInfo) info_base;
		
		if (info.exclusive) {
			return null;
		}
		
		return super.open_impl(info, truncate);
	}

	@Override
	protected boolean remove_impl(nachos.userprog.FileInfo info_base) {
		FileInfo info = (FileInfo) info_base;
		
		if (info.exclusive) {
			return false;
		}
		
		return super.remove_impl(info);
	}
	
	@Override
	protected nachos.userprog.FileInfo newFileInfo(String name) {
		return new FileInfo(name);
	}

	@Override
	protected nachos.userprog.OpenFileWrapper newOpenFileWrapper(
			OpenFile openFile, 
			nachos.userprog.FileInfo info, 
			FileSystemWrapper fs) {
		return new OpenFileWrapper(openFile, info, fs);
	}
	
	public static class FileInfo extends StubFileSystemWrapper.FileInfo {

		protected FileInfo(String name) {
			super(name);
			
			lastModificationTime = System.currentTimeMillis();
			exclusive = false;
		}
		
		protected boolean isExclusive() {
			return exclusive;
		}
		
		protected boolean setExclusive(boolean exclusive) {
			boolean ret = this.exclusive;
			this.exclusive = exclusive;
			return ret;
		}
		
		/** The time in milliseconds since Jan 1, 1970 UTC when the file was last modified.
		 *  It is set to the creation time of this object rather than the real last modify time
		 *  for we do not have access to such information. */
		private long lastModificationTime;
		
		/** Whether the file is in exclusive state. Any further open/remove request on
		 * this file will be rejected if exclusive is set to true. */
		private boolean exclusive;
	}
	
	public static class OpenFileWrapper extends StubFileSystemWrapper.OpenFileWrapper {

		protected OpenFileWrapper(
				OpenFile openFile,
				nachos.userprog.FileInfo info, 
				FileSystemWrapper fs) {
			
			super(openFile, info, fs);
			Lib.assertTrue(info instanceof FileInfo);
			Lib.assertTrue(fs instanceof VMFileSystemWrapper);
		}
	
		@Override
		public int write(int pos, byte[] buf, int offset, int length) {
			FileInfo info = (FileInfo) this.info;
			
			info.sem.P();
			
			int ret = super.write(pos, buf, offset, length);
			info.lastModificationTime = System.currentTimeMillis();
			
			info.sem.V();
			
			return ret;
		}
		
		@Override
		public int write(byte[] buf, int offset, int length) {
			FileInfo info = (FileInfo) this.info;
			
			info.sem.P();
			
			int ret = super.write(buf, offset, length);
			info.lastModificationTime = System.currentTimeMillis();
			
			info.sem.V();
			
			return ret;
		}
		
	}
	
}
