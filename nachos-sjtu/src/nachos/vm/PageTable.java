package nachos.vm;

import java.util.TreeSet;

/**
 * A page table implementation that supports the 32-bit virtual address space.
 * <p>
 * PageTable is implemented with TreeSet. 
 * Allocating, releasing, accessing a single page table entry is of time O(lgn).
 *
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class PageTable {
	
	/**
	 * Constructs a new empty page table.
	 */
	public PageTable() {
		sections = new TreeSet(PageTableSection.less_than);
		list_head = new EmbeddedLinkedList();
	}

	/**
	 * Returns the VM page entry of the page number.
	 * 
	 * @param pn	the page number
	 * @return		the VM page entry, <tt>null</tt> if it doesn't exists
	 */
	public VMPageEntry getPageTableEntry(int pn) {
		VMPageEntry entry = null;
		
		PageTableSection section = getPageTableSection(pn);
		if (section != null) {
			entry = section.vmPage[pn - section.start_pn];
		}
		
		return entry;
	}

	/**
	 * Sets the VM page entry of the page number.
	 * 
	 * @param pn			the page number 
	 * @param vmPage		the vm page
	 * @return				<tt>true</tt> on success, <tt>false</tt> o.w.
	 */
	public boolean setPageTableEntry(int pn, VMPageEntry vmPage) {
		PageTableSection section = getPageTableSection(pn);
		if (section != null) {
			section.vmPage[pn - section.start_pn] = vmPage;
			return true;
		}
		return false;
	}

	/**
	 * Returns the page table section containing the page number.
	 * 
	 * @param pn		the page number
	 * @return			the page table containing the page number, <tt>null</tt> o.w.
	 */
	public PageTableSection getPageTableSection(int pn) {
		Object query_result = sections.floor(PageTableSection.getQuery(pn));
		if (query_result == null) return null;
		
		PageTableSection section = (PageTableSection) query_result;
		if (section.start_pn <= pn && pn < section.end_pn) 
			return section;
		else 
			return null;
	}

	/**
	 * Adds a new page table section to the page table of page number [start_pn, start_pn + length).
	 * If this section overlaps with some other section, the operation will fail without modify
	 * the page table.
	 * 
	 * @param start_pn		the start page number
	 * @param length		the number of pages
	 * @return				<tt>true</tt> on success, <tt>false</tt> o.w.
	 */
	public boolean addPageTableSection(int start_pn, int length) {
		PageTableSection section = null;
		if (sections.isEmpty()) {
			section = new PageTableSection(start_pn, length);
			section.insert_after(list_head);
		}
		else {
			int end_pn = start_pn + length;
			Object query = sections.floor(PageTableSection.getQuery(start_pn));
			if (query != null) {
				PageTableSection prev = (PageTableSection) query;
				if (prev.start_pn == start_pn) return false;
				if (prev.end_pn > start_pn) return false;
				
				if (prev.list_next instanceof PageTableSection) {
					PageTableSection next = (PageTableSection) prev.list_next;
					if (next.start_pn < end_pn) return false;
				}
				
				section = new PageTableSection(start_pn, length);
				section.insert_after(prev);
			}
			else {
				if (list_head.list_next != list_head) {
					PageTableSection next = (PageTableSection) list_head.list_next;
					if (next.start_pn < end_pn) return false;
				}
				
				section = new PageTableSection(start_pn, length);
				section.insert_after(list_head);
			}
		}
		
		sections.add(section);
		return true;
	}

	/**
	 * Remove all sections.
	 * 
	 */
	public void removeAll() {
		sections.clear();
		list_head.remove_from_list();
	}

	/**
	 * Remove the given page table section.
	 * 
	 * @param section
	 * @return	<tt>true</tt> if this page table does contain the section. <tt>false</tt> o.w.
	 */
	public boolean removePageTableSection(PageTableSection section) {
		if (sections.remove(section)) {
			section.remove_from_list();
			return true;
		}
		return false;
	}

	/**
	 * The first page table section.
	 * 
	 * @return	the first page table section, <tt>null</tt> if it is empty.
	 */
	public PageTableSection first() {
		if (list_head.list_next == list_head) return null;
		return (PageTableSection) list_head.list_next;
	}

	/**
	 * The last page table section.
	 * 
	 * @return	the last page table section, <tt>null</tt> if it is empty.
	 */
	public PageTableSection last() {
		if (list_head.list_prev == list_head) return null;
		return (PageTableSection) list_head.list_prev;
	}
	
	private TreeSet sections;
	
	private EmbeddedLinkedList list_head;
}
