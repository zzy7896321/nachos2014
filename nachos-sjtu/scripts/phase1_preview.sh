#!/bin/sh

NACHOS_FLAGS="-s 23049701"

echo BoatGrader
nachos ${NACHOS_FLAGS} -- nachos.ag.BoatGrader "-#" adults=4,children=4 -[] ../conf/proj1.conf
echo

echo DonationGrader
nachos ${NACHOS_FLAGS} -- nachos.ag.DonationGrader -[] ../conf/proj1-priority-scheduler.conf
echo

echo JoinGrader
nachos ${NACHOS_FLAGS} -- nachos.ag.JoinGrader "-#" waitTicks=1000,times=10 -[] ../conf/proj1.conf
echo

echo LockGrader11
nachos ${NACHOS_FLAGS} -- nachos.ag.LockGrader11 -[] ../conf/proj1.conf
echo

echo PriorityGrader
nachos ${NACHOS_FLAGS} -- nachos.ag.PriorityGrader "-#" threads=5,times=10,length=1000 -[] ../conf/proj1-priority-scheduler.conf
echo

echo ThreadGrader1
nachos ${NACHOS_FLAGS} -- nachos.ag.ThreadGrader1 -[] ../conf/proj1.conf
echo

echo ThreadGrader2
nachos ${NACHOS_FLAGS} -- nachos.ag.ThreadGrader2 -[] ../conf/proj1.conf
echo
